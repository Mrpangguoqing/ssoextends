package ssoExtends;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jasig.cas.client.util.CommonUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

/**
 * 
 * @author guoqing
 *
 */
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, InitializingBean {

	private ServiceProperties serviceProperties;

	private String loginUrl;

	private boolean encodeServiceUrlWithSessionId = true;

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.hasLength(this.loginUrl, "loginUrl must be specified");
		Assert.notNull(this.serviceProperties, "serviceProperties must be specified");
		Assert.notNull(this.serviceProperties.getService(), "serviceProperties.getService() cannot be null");
	}

	@Override
	public final void commence(final HttpServletRequest servletRequest, final HttpServletResponse response,
			final AuthenticationException arg2) throws IOException, ServletException {
		if (isAjaxRequest(servletRequest)) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
			final String urlEncodeService = createServiceUrl(servletRequest,response);
			final String redirectUrl = createRedirectUrl(urlEncodeService);
			preCommence(servletRequest, response);
			response.sendRedirect(redirectUrl);
		}
	}

	protected boolean isAjaxRequest(final HttpServletRequest servletRequest) {
		String xRequestedWith = servletRequest.getHeader("X-Requested-With");
		return xRequestedWith != null && xRequestedWith.trim().length() > 0;
	}

	@SuppressWarnings("deprecation")
	protected String createServiceUrl(final HttpServletRequest request, final HttpServletResponse response) {
		return CommonUtils.constructServiceUrl(null, response, this.serviceProperties.getService(), null,
				this.serviceProperties.getArtifactParameter(), this.encodeServiceUrlWithSessionId);
	}

	protected String createRedirectUrl(final String serviceUrl) {
		return CommonUtils.constructRedirectUrl(this.loginUrl, this.serviceProperties.getServiceParameter(), serviceUrl,
				this.serviceProperties.isSendRenew(), false);
	}

	protected void preCommence(final HttpServletRequest request, final HttpServletResponse response) {
	}

	public final String getLoginUrl() {
		return this.loginUrl;
	}

	public final ServiceProperties getServiceProperties() {
		return this.serviceProperties;
	}

	public final void setLoginUrl(final String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public final void setServiceProperties(final ServiceProperties serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	public final void setEncodeServiceUrlWithSessionId(final boolean encodeServiceUrlWithSessionId) {
		this.encodeServiceUrlWithSessionId = encodeServiceUrlWithSessionId;
	}

	protected boolean getEncodeServiceUrlWithSessionId() {
		return this.encodeServiceUrlWithSessionId;
	}

}