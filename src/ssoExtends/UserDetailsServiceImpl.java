package ssoExtends;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.neusoft.saca.dataviz.common.security.DefaultUser;

/**
 * @Desc 单点登录
 * @author guoqing CAS
 *
 */
public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if(null!=username && !"".equals(username)) {
			if(username.equals("admin")) {
				return new DefaultUser(username,"",new SimpleGrantedAuthority("ROLE_ADMIN"));
			}
			return new DefaultUser(username,"");
		}
		return null;
	}
}