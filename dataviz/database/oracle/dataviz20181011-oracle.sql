/*==============================================================*/
/* Table: dv_area_base                                        */
/*==============================================================*/

CREATE TABLE dv_area_base (
  area_code INTEGER       NOT NULL,
  area_name varchar2(255) DEFAULT NULL,
  common_name varchar2(255) DEFAULT NULL,
  common_spell varchar2(255) DEFAULT NULL,
  short_name varchar2(255) DEFAULT NULL,
  area_level SMALLINT      DEFAULT NULL,
  parent_code INTEGER      DEFAULT NULL,
  full_name varchar2(255) DEFAULT NULL,
  constraint PK_DV_AREA_BASE primary key (area_code)
) 
/

/*==============================================================*/
/* Table: dv_area_custom                                      */
/*==============================================================*/
create table dv_area_custom  (
   creator            VARCHAR2(64)                   default '' not null,
   custom_mapping     BLOB,
   constraint PK_DV_AREA_CUSTOM primary key (creator)
)
/

/*==============================================================*/
/* Table: dv_book_chart_use                                   */
/*==============================================================*/
create table dv_book_chart_use  (
   book_id            VARCHAR2(64)                    not null,
   chart_id           VARCHAR2(64)                    not null
   )
/
create index INDEX_BOOKID on DV_BOOK_CHART_USE (BOOK_ID)
  compress

/
alter table dv_book_chart_use add primary key(book_id,chart_id);


/*==============================================================*/
/* Table: dv_chart_instance                                   */
/*==============================================================*/
create table dv_chart_instance  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(128)                   default NULL,
   category           VARCHAR2(16)                   default NULL,
   owner              VARCHAR2(64)                   default NULL,
   private_in_public  SMALLINT                       default 0,
   description        VARCHAR2(600)                  default NULL,
   data               BLOB,
   create_time        DATE                           default NULL,
   last_update        DATE                           default NULL,
   creator            VARCHAR2(64)                   default NULL,
   project_id         VARCHAR2(64)                   default NULL,
   flag_domain        SMALLINT                       default 0,
   dataset_id         VARCHAR2(64)                   default NULL,
   constraint PK_DV_CHART_INSTANCE primary key (id)
)
/
create index index_chart_project_id on dv_chart_instance (project_id)
compress
/

/*==============================================================*/
/* Table: dv_chart_tags                                       */
/*==============================================================*/
create table dv_chart_tags  (
   chart_instance_id  VARCHAR2(64)                    not null,
   tag                VARCHAR2(400)                  default '' not null,
   constraint PK_DV_CHART_TAGS primary key (chart_instance_id, tag)
)
/


/*==============================================================*/
/* Table: dv_chartbook_info                                   */
/*==============================================================*/
create table dv_chartbook_info  (
   id                 VARCHAR2(64)                    not null,
   origin_id          VARCHAR2(64)                   default NULL,
   owner_id           VARCHAR2(64)                   default NULL,
   name               VARCHAR2(128)                  default NULL,
   author             VARCHAR2(128)                  default NULL,
   version            VARCHAR2(64)                   default NULL,
   category_id        VARCHAR2(64)                   default NULL,
   created_on         DATE                           default NULL,
   modified_on        DATE                           default NULL,
   description        VARCHAR2(600)                  default NULL,
   is_temp            SMALLINT                       default NULL,
   icon_store         VARCHAR2(255)                  default NULL,
   theme_id           VARCHAR2(64)                   default NULL,
   model_data         BLOB,
   model_compressed   SMALLINT                       default 0,
   project_id         VARCHAR2(64)                   default NULL,
   flag_domain        INTEGER                        default 0,
   private_in_public  INTEGER                       default 0,
   constraint PK_DV_CHARTBOOK_INFO primary key (id)
)
/
create index index_chartbook_project_id on dv_chartbook_info (project_id)
compress
/

/*==============================================================*/
/* Table: dv_chartbook_tags                                   */
/*==============================================================*/
create table dv_chartbook_tags  (
   chart_book_id      VARCHAR2(64)                    not null,
   tag                VARCHAR2(400)                  default '' not null,
   constraint PK_DV_CHARTBOOK_TAGS primary key (chart_book_id, tag)
)
/

/*==============================================================*/
/* Table: dv_chartbookcategory                                */
/*==============================================================*/
create table dv_chartbookcategory  (
   id                 INTEGER                         not null,
   name               VARCHAR2(128)                  default NULL,
   constraint PK_DV_CHARTBOOKCATEGORY primary key (id)
)
/

/*==============================================================*/
/* Table: dv_dataset_info                                     */
/*==============================================================*/
create table dv_dataset_info  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(128)                   default NULL,
   category           VARCHAR2(16)                   default NULL,
   owner              VARCHAR2(64)                   default NULL,
   remarks            VARCHAR2(400)                  default NULL,
   project_id         VARCHAR2(64)                   default NULL,
   model_context      BLOB,
   diagram            BLOB,
   filter_condition   BLOB,
   create_time        DATE                           default NULL,
   creator            VARCHAR2(64)                   default NULL,
   last_update        DATE                           default NULL,
   description        VARCHAR2(600)                  default NULL,
   detail_info_field  VARCHAR2(500)                  default NULL,
   bind_code_table    BLOB                           default NULL,
   constraint PK_DV_DATASET_INFO primary key (id)
)
/
create index index_dataset_project_id on dv_dataset_info (project_id)
compress
/
/*==============================================================*/
/* Table: dv_datasource_file                                  */
/*==============================================================*/
create table dv_datasource_file  (
   file_name          VARCHAR2(64)                    not null,
   datasource_id      VARCHAR2(64)                   default NULL,
   full_path          VARCHAR2(512)                  default NULL,
   table_name         VARCHAR2(128)                   default NULL,
   delete_file        SMALLINT                       default NULL,
   constraint PK_DV_DATASOURCE_FILE primary key (file_name)
)
/
create index index_datasourceID on dv_datasource_file (datasource_id)
compress
/
/*==============================================================*/
/* Table: dv_datasource_info                                  */
/*==============================================================*/
create table dv_datasource_info  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(128)                   default NULL,
   kind               VARCHAR2(16)                   default NULL,
   owner              VARCHAR2(64)                   default NULL,
   data_access        BLOB,
   meta_data          BLOB,
   cross_schema       SMALLINT                    default 0,
   use_schemas        BLOB,
   data_filter        BLOB,
   create_time        DATE                           default NULL,
   last_update        DATE                           default NULL,
   creator            VARCHAR2(64)                   default NULL,
   sample             SMALLINT                       default 0,
   data_analysis      SMALLINT                       default 0,
   system_shared      SMALLINT                       default 0,
   code_table         BLOB                           default NULL,
   constraint PK_DV_DATASOURCE_INFO primary key (id)
)
/


/*==============================================================*/
/* Table: dv_failedlocallogin                                         */
/*==============================================================*/
CREATE TABLE dv_failedlocallogin (
  login_name varchar2(255) NOT NULL,
  failure_time date NOT NULL
)
/
/*==============================================================*/
/* Table: dv_favorite                                         */
/*==============================================================*/
create table dv_favorite  (
   user_id            VARCHAR2(64)                    not null,
   resource_id        VARCHAR2(64)                    not null,
   resource_type      SMALLINT                       default NULL,
   resource_name      VARCHAR2(255)                  default NULL,
   create_time        DATE                           default NULL,
   constraint PK_DV_FAVORITE primary key (user_id, resource_id)
)
/

comment on column dv_favorite.resource_type is
'资源类型，0：图册，1：图表'
/

/*==============================================================*/
/* Table: dv_graphiccategory                                  */
/*==============================================================*/
create table dv_graphiccategory  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(128)                  default NULL,
   constraint PK_DV_GRAPHICCATEGORY primary key (id)
)
/

/*==============================================================*/
/* Table: dv_graphiccomponent                                 */
/*==============================================================*/
create table dv_graphiccomponent  (
   id                 VARCHAR2(64)                   default '1' not null,
   comp_version       VARCHAR2(64)                   default NULL,
   name               VARCHAR2(128)                  default NULL,
   category_id        VARCHAR2(64)                   default NULL,
   theme_type         SMALLINT                       default 0 not null,
   api_version        VARCHAR2(64)                   default NULL,
   author_id          VARCHAR2(64)                   default NULL,
   created_on         DATE                           default NULL,
   modified_on        DATE                           default NULL,
   meta_model_store   VARCHAR2(128)                  default NULL,
   simple_model_store VARCHAR2(128)                  default NULL,
   data_require       VARCHAR2(128)                  default NULL,
   icon_store         VARCHAR2(128)                  default NULL,
   description        VARCHAR2(500)                  default NULL,
   comp_id            VARCHAR2(64)                   default NULL,
   access1             SMALLINT                       default 0,
   aclId              NUMBER(6)                       not null,
   access_type        SMALLINT                       default 0,
   order_index        INT                       default NULL,
   constraint aclId unique (aclId),
   constraint id unique (id)
)
/

comment on column dv_graphiccomponent.theme_type is
'0：分类色，1：序列色（渐变色），2：离散色（正负色）'
/

comment on column dv_graphiccomponent.order_index is
'父节点id'
/

/*==============================================================*/
/* Table: dv_picture                                          */
/*==============================================================*/
create table dv_picture  (
   file_name          VARCHAR2(255)                  default '' not null,
   pic_name           VARCHAR2(128)                  default NULL,
   project_id         VARCHAR2(64)                   default NULL,
   create_time        DATE                           default NULL,
   delete_file        SMALLINT                       default NULL,
   constraint PK_DV_PICTURE primary key (file_name)
)
/
create index index_picture_project_id on dv_picture (project_id)
compress
/
/*==============================================================*/
/* Table: dv_project_info                                     */
/*==============================================================*/
create table dv_project_info  (
   id                 VARCHAR2(64)                   default '' not null,
   name               VARCHAR2(128)                   default NULL,
   owner              VARCHAR2(64)                   default NULL,
   description        VARCHAR2(128)                  default NULL,
   flag_cooperation   SMALLINT                       default NULL,
   create_time        DATE                           default NULL,
   last_update        DATE                           default NULL,
   creator            VARCHAR2(64)                   default NULL,
   flag_domain        SMALLINT                       default NULL,
   openness           SMALLINT                       default 0,
   constraint PK_DV_PROJECT_INFO primary key (id)
)
/
create index index_creatorID on dv_project_info (creator)
compress
/
create index index_openness on dv_project_info (openness)
compress
/
/*==============================================================*/
/* Table: dv_queryrecord                                */
/*==============================================================*/
create table dv_queryrecord (
  id varchar(64) NOT NULL,
  sql_detail blob,
  chart_id varchar2(64) DEFAULT NULL,
  dataset_id varchar2(64) DEFAULT NULL,
  query_status SMALLINT DEFAULT 0,
  query_time date DEFAULT NULL,
  cost_time number(10,0) DEFAULT 0,
  current_user_id varchar2(64) DEFAULT NULL,
  PRIMARY KEY (id)
) 
/
comment on column dv_queryrecord.id is
'Id';
comment on column dv_queryrecord.sql_detail is
'sql细节';
comment on column dv_queryrecord.chart_id is
'图表Id';
comment on column dv_queryrecord.dataset_id is
'数据集Id';
comment on column dv_queryrecord.query_status is
'查询状态';
comment on column dv_queryrecord.query_time is
'查询时间点';
comment on column dv_queryrecord.cost_time is
'查询耗时单位ms';
comment on column dv_queryrecord.current_user_id is
'当前查询用户Id';

/*==============================================================*/
/* Table: dv_public_permission                                */
/*==============================================================*/
create table dv_public_permission  (
   subject_id         VARCHAR2(64)                    not null,
   subject_type       SMALLINT                       default NULL,
   project_id         VARCHAR2(64)                   default NULL,
   resource_type      SMALLINT                        not null,
   permission         SMALLINT                        not null
)
/

/*==============================================================*/
/* Table: dv_resourcesharing                                  */
/*==============================================================*/
create table dv_resourcesharing  (
   share_code         VARCHAR2(16)                   default '' not null,
   owner_id           VARCHAR2(64)                   default NULL,
   resource_type      SMALLINT                       default NULL,
   resource_id        VARCHAR2(64)                   default NULL,
   share_type         SMALLINT                       default NULL,
   password           VARCHAR2(8)                    default NULL,
   create_time        DATE                           default NULL,
   special_code       VARCHAR2(32)                   default NULL,
   constraint PK_DV_RESOURCESHARING primary key (share_code)
)
/

CREATE TABLE dv_role_info (
  id varchar2(64) NOT NULL,
  name varchar2(255) DEFAULT NULL,
  state SMALLINT DEFAULT NULL,
  description varchar2(600) DEFAULT NULL,
  creator varchar2(64) DEFAULT NULL,
  create_time DATE DEFAULT NULL,
  PRIMARY KEY (id)
)
/
CREATE TABLE dv_role_user (
  role_id varchar2(64) NOT NULL,
  user_id varchar2(64) NOT NULL
)
/

/*==============================================================*/
/* Table: dv_sample_chart                                     */
/*==============================================================*/
create table dv_sample_chart  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(32)                   default NULL,
   description        VARCHAR2(128)                  default NULL,
   data               BLOB,
   dataset_id         VARCHAR2(64)                   default NULL,
   constraint PK_DV_SAMPLE_CHART primary key (id)
)
/

/*==============================================================*/
/* Table: dv_sample_chartbook                                 */
/*==============================================================*/
create table dv_sample_chartbook  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(127)                  default NULL,
   description        VARCHAR2(200)                  default NULL,
   theme_id           VARCHAR2(64)                    not null,
   model_data         BLOB,
   model_compressed   SMALLINT                       default 0,
   constraint PK_DV_SAMPLE_CHARTBOOK primary key (id)
)
/

/*==============================================================*/
/* Table: dv_show_menu                                 */
/*==============================================================*/
CREATE TABLE dv_show_menu (
  id varchar2(64) NOT NULL,
  dimension_id varchar2(64) DEFAULT NULL,
  dimension_type SMALLINT DEFAULT NULL,
  show_menu_type SMALLINT DEFAULT NULL,
  PRIMARY KEY (id)
) 
/

/*==============================================================*/
/* Table: dv_source_set_use                                   */
/*==============================================================*/
create table dv_source_set_use  (
   dataset_id         VARCHAR2(64)                    not null,
   datasource_id      VARCHAR2(64)                    not null
)
/
create index index_datasource_id on dv_source_set_use (datasource_id)
compress
/

/*==============================================================*/
/* Table: dv_special_access_code                              */
/*==============================================================*/
create table dv_special_access_code  (
   code               VARCHAR2(64)                    not null,
   dimension_id       VARCHAR2(64)                   default NULL,
   dimension_type     SMALLINT                       default NULL,
   res_id             VARCHAR2(64)                    not null,
   res_type           SMALLINT                        not null,
   constraint PK_DV_SPECIAL_ACCESS_CODE primary key (code)
)
/
/*==============================================================*/
/* Table: dv_tenant_pc                              */
/*==============================================================*/
create table dv_tenant_pc (
  pc_tenant varchar2(64) DEFAULT NULL,
  domain_id smallint NOT NULL
)
/


/*==============================================================*/
/* Table: dv_theme                                            */
/*==============================================================*/
create table dv_theme  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(128)                  default NULL,
   content            CLOB,
   icon_store         VARCHAR2(255)                  default NULL,
   owner_id           VARCHAR2(64)                   default NULL,
   book_id            VARCHAR2(64)                   default NULL,
   update_time        DATE                           default NULL,
   constraint PK_DV_THEME primary key (id)
)
/

/*==============================================================*/
/* Table: dv_thirdparty_app                                   */
/*==============================================================*/
create table dv_thirdparty_app  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(64)                   default NULL,
   flag_type          SMALLINT                       default NULL,
   service_url        VARCHAR2(255)                  default NULL,
   account            VARCHAR2(64)                   default NULL,
   password           VARCHAR2(255)                  default NULL,
   owner              VARCHAR2(64)                   default NULL,
   create_time        DATE                           default NULL,
   constraint PK_DV_THIRDPARTY_APP primary key (id)
);

comment on column dv_thirdparty_app.id is
'主键';

comment on column dv_thirdparty_app.name is
'应用名称';

comment on column dv_thirdparty_app.type is
'关联应用类型';

comment on column dv_thirdparty_app.service_url is
'服务地址';

comment on column dv_thirdparty_app.account is
'登录用户名';

comment on column dv_thirdparty_app.password is
'登录密码';

comment on column dv_thirdparty_app.owner is
'拥有者';

/*==============================================================*/
/* Table: dv_thirdparty_app_type                              */
/*==============================================================*/
create table dv_thirdparty_app_type  (
   id                 INTEGER                         not null,
   name               VARCHAR2(64)                   default NULL,
   img                VARCHAR2(128)                  default NULL,
   constraint PK_DV_THIRDPARTY_APP_TYPE primary key (id)
);

comment on column dv_thirdparty_app_type.id is
'主键id';

comment on column dv_thirdparty_app_type.name is
'应用类型名称';

/*==============================================================*/
/* Table: dv_thirdparty_res                                   */
/*==============================================================*/
create table dv_thirdparty_res  (
   id                 VARCHAR2(64)                    not null,
   name               VARCHAR2(64)                   default NULL,
   app_id             VARCHAR2(64)                   default NULL,
   app_type           SMALLINT                       default NULL,
   create_time        DATE                           default NULL,
   url                VARCHAR2(255)                  default NULL,
   owner              VARCHAR2(64)                   default NULL,
   project_id         VARCHAR2(64)                   default NULL,
   constraint PK_DV_THIRDPARTY_RES primary key (id)
);

comment on column dv_thirdparty_res.id is
'主键id';

comment on column dv_thirdparty_res.name is
'资源名称';

comment on column dv_thirdparty_res.app_id is
'应用实例';

comment on column dv_thirdparty_res.create_time is
'资源添加时间';

comment on column dv_thirdparty_res.url is
'资源url';

comment on column dv_thirdparty_res.owner is
'拥有者';

comment on column dv_thirdparty_res.project_id is
'项目id';

/*==============================================================*/
/* Table: dv_useraccount                                      */
/*==============================================================*/
create table dv_useraccount  (
   user_id            VARCHAR2(64)                    not null,
   user_name          VARCHAR2(128)                  default NULL,
   account_state      SMALLINT                       default 1 not null,
   account_role       SMALLINT                       default 0 not null,
   create_date        DATE                            not null,
   created_by         VARCHAR2(64)                   default NULL,
   last_login         DATE                           default NULL,
   user_data_analysis SMALLINT                      default 0 not null,
   constraint PK_DV_USERACCOUNT primary key (user_id)
)
/

/*==============================================================*/
/* Table: dv_userlocallogin                                   */
/*==============================================================*/
create table dv_userlocallogin  (
   login_name         VARCHAR2(255)                   not null,
   login_pass         VARCHAR2(255)                  default NULL,
   user_id            VARCHAR2(64)                    not null,
   ban_til            DATE                            default NULL,
   constraint PK_DV_USERLOCALLOGIN primary key (login_name)
)
/

CREATE TABLE dv_user_publicresource (
  user_id varchar2(255) NOT NULL,
  public_resource_id varchar2(255) NOT NULL,
  user_data_analysis SMALLINT DEFAULT 0,
  PRIMARY KEY (user_id)
)
/

/*==============================================================*/
/* Table: dv_mobile_carousel_manage                           */
/*==============================================================*/
CREATE TABLE dv_mobile_carousel_manage (
  id varchar2(64) NOT NULL,
  carousel_name varchar2(128) NOT NULL,
  carousel_img varchar2(255) DEFAULT NULL,
  carousel_link varchar2(255) DEFAULT NULL,
  create_time DATE DEFAULT NULL,
  edit_time DATE DEFAULT NULL,
  sort_order NUMBER(20)    DEFAULT NULL,
  constraint PK_DV_MOBILE_CAROUSEL_MANAGE primary key (id)
) 
/

comment on column dv_mobile_carousel_manage.id is
'主键id';
comment on column dv_mobile_carousel_manage.carousel_name is
'资源名称';
comment on column dv_mobile_carousel_manage.carousel_img is
'目录封面';
comment on column dv_mobile_carousel_manage.carousel_link is
'资源说明';
comment on column dv_mobile_carousel_manage.create_time is
'创建时间';
comment on column dv_mobile_carousel_manage.edit_time is
'编辑时间';
/*==============================================================*/
/* Table: dv_mobile_resource_manage                           */
/*==============================================================*/
create table dv_mobile_resource_manage  (
   id                 VARCHAR2(64)                    not null,
   dir_id             VARCHAR2(64)                    not null,
   dir_name           VARCHAR2(128)                    not null,
   source_type        SMALLINT                       default NULL,
   source_name        VARCHAR2(128)                  default NULL,
   source_value       VARCHAR2(128)                  default NULL,
   dir_img            VARCHAR2(255)                  default NULL,
   owner              VARCHAR2(64)                    not null,
   create_time        DATE                            not null,
   edit_time          DATE                            not null,
   source_access_type SMALLINT                       default NULL,
   source_orig_id     VARCHAR2(64)                   default NULL,
   source_orig_name   VARCHAR2(128)                   default NULL,
   source_des         VARCHAR2(600)                  default NULL,
   see_pc             SMALLINT                       default 1,
   see_mobile         SMALLINT                       default 1,
   constraint PK_DV_MOBILE_RESOURCE_MANAGE primary key (id)
)
/
comment on column dv_mobile_resource_manage.id is
'主键id';

comment on column dv_mobile_resource_manage.dir_id is
'父节点目录id';

comment on column dv_mobile_resource_manage.dir_name is
'目录名称';

comment on column dv_mobile_resource_manage.source_type is
'目录、报表、仪表舱、外部链接、图册、图表等';

comment on column dv_mobile_resource_manage.source_name is
'资源名称';

comment on column dv_mobile_resource_manage.source_value is
'资源内容';

comment on column dv_mobile_resource_manage.dir_img is
'目录封面';

comment on column dv_mobile_resource_manage.owner is
'拥有者';

comment on column dv_mobile_resource_manage.create_time is
'创建时间';

comment on column dv_mobile_resource_manage.edit_time is
'编辑时间';

comment on column dv_mobile_resource_manage.source_access_type is
'资源授权类型';

comment on column dv_mobile_resource_manage.source_orig_id is
'引用资源ID';

comment on column dv_mobile_resource_manage.source_orig_name is
'引用资源名称';

comment on column dv_mobile_resource_manage.source_des is
'资源说明';


CREATE TABLE dv_org_info (
  id varchar2(64) NOT NULL,
  name varchar2(255) DEFAULT NULL,
  parent_id varchar2(64) DEFAULT NULL,
  state SMALLINT DEFAULT NULL,
  description varchar2(600) DEFAULT NULL,
  creator varchar2(64) DEFAULT NULL,
  create_time DATE DEFAULT NULL,
  PRIMARY KEY (id)
)
/

CREATE TABLE dv_org_user (
  org_id varchar2(64) NOT NULL,
  user_id varchar2(64) NOT NULL,
  user_role  integer  DEFAULT 0
)
/

create table dv_dataset_sqoop2 (
  id             varchar2(128)        NOT NULL,
  dataset_id     varchar2(64)         DEFAULT NULL,
  project_id     varchar2(64)         DEFAULT NULL,
  datasource_id  varchar2(64)         DEFAULT NULL,
  catalog_model     varchar2(64)         DEFAULT NULL,
  origin_context    BLOB              DEFAULT NULL,
  job_config        BLOB              DEFAULT NULL,
  ismodel        SMALLINT             DEFAULT 0,
  crontab_info   varchar2(64)         DEFAULT NULL,
  increment_col  varchar2(64)         DEFAULT NULL,
  last_value     varchar2(64)         DEFAULT NULL,
  time_cost      NUMBER(20)           DEFAULT 0,
  job_count      NUMBER(20)           DEFAULT 0,
  partition_col  varchar2(64)         DEFAULT NULL,
  job_status     varchar2(16)         DEFAULT NULL,
  create_time    date                 DEFAULT NULL,
  last_time      date                 DEFAULT NULL,
  constraint PK_dv_dataset_sqoop2 primary key (id)
) 
/

create table dv_dataset_tags (
  dataset_id VARCHAR2(64) not null,
  tag VARCHAR2(400) not null,
  constraint PK_DV_DATASETTAGS PRIMARY KEY (dataset_id,tag)
) 
/

CREATE TABLE dv_columnscount (
  id varchar2(128)  DEFAULT '',
  column_name varchar2(64) NOT NULL,
  table_name varchar2(64) NOT NULL,
  column_count number(10,0) NOT NULL,
  constraint PK_dv_columnscount primary key (id)
) 
/

CREATE TABLE dv_custom_values (
  dataset_id varchar2(255) NOT NULL,
  field_name varchar2(255) NOT NULL,
  data_values CLOB
)
/

CREATE TABLE dv_playstatistics (
  id varchar2(64) NOT NULL,
  source_id varchar2(64) DEFAULT NULL,
  name varchar2(128) DEFAULT NULL,
  project_name varchar2(128) DEFAULT NULL,
  owner_id varchar2(64) DEFAULT NULL,
  owner_name varchar2(64) DEFAULT NULL,
  play_type number(10,0) DEFAULT NULL,
  play_count number(10,0) DEFAULT NULL,
  play_time date DEFAULT NULL,
  PRIMARY KEY (id)
) 
/

create table unieap_report_category
(
  PKID           VARCHAR2(64) not null,
  PARENTID       VARCHAR2(64) not null,
  TYPE           SMALLINT not null,
  CAPTION        VARCHAR2(128) not null,
  CREATER        VARCHAR2(64) not null,
  CRTIME         NUMBER(24) not null,
  EDITOR         VARCHAR2(64) DEFAULT NULL,
  EDTIME         NUMBER(24)  DEFAULT NULL,
  APPINFO        VARCHAR2(255) DEFAULT NULL,
  FLAG           VARCHAR2(64) DEFAULT NULL,
  DIGEST         VARCHAR2(64) DEFAULT NULL,
   constraint PK_UNIEAP_REPORT_CATEGORY primary key (PKID)
)
/

comment on column unieap_report_category.PKID is
'主键id'
/

comment on column unieap_report_category.PARENTID is
'页面显示顺序'
/ 


CREATE TABLE unieap_report_define
(
    PKID   VARCHAR2(64) NOT NULL,
    DEFINE BLOB             NULL
)
/
comment on column unieap_report_define.PKID is
'主键id'
/ 

--
-- CREATE Primary Keys
--
ALTER TABLE UNIEAP_REPORT_DEFINE
    ADD CONSTRAINT PK_UR_DEFINE
    PRIMARY KEY (PKID)
    ENABLE
    VALIDATE
/


create table unieap_report_picture
(
  PKID           VARCHAR2(64) not null,
  PICVALUES      BLOB,
  EDTIME         TIMESTAMP,
  SESSIONID      VARCHAR2(128),
  EDSTATE        INTEGER,
   constraint PK_UNIEAP_REPORT_PICTURE primary key (PKID)
)
/

create table unieap_report_tempmemo
(
  TEMPID   VARCHAR2(64) not null,
  REPORTID VARCHAR2(64),
  USERID   VARCHAR2(64),
  TEMPNAME VARCHAR2(128) not null,
  OPDATE   INTEGER,
  BLOCK    BLOB
)
/

alter table unieap_report_tempmemo
  add constraint PK_UR_TEMPMEMO primary key (TEMPID)
/

create table unieap_report_dv_ds_use (PKID varchar2(64), DSID varchar2(64))
/

CREATE TABLE qrtz_job_details
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL,
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtz_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(20) NULL,
    PREV_FIRE_TIME NUMBER(20) NULL,
    PRIORITY INTEGER NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(20) NOT NULL,
    END_TIME NUMBER(20) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR SMALLINT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
        REFERENCES qrtz_job_details(SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(20) NOT NULL,
    REPEAT_INTERVAL NUMBER(20) NOT NULL,
    TIMES_TRIGGERED NUMBER(20) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(200) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_simprop_triggers
  (          
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    STR_PROP_1 VARCHAR2(512) NULL,
    STR_PROP_2 VARCHAR2(512) NULL,
    STR_PROP_3 VARCHAR2(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 NUMBER(20) NULL,
    LONG_PROP_2 NUMBER(20) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR2(1) NULL,
    BOOL_PROP_2 VARCHAR2(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_calendars
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL,
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);

CREATE TABLE qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_fired_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(20) NOT NULL,
    SCHED_TIME NUMBER(20) NOT NULL,
    PRIORITY INTEGER NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);

CREATE TABLE qrtz_scheduler_state
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(20) NOT NULL,
    CHECKIN_INTERVAL NUMBER(20) NOT NULL,
    PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);

CREATE TABLE qrtz_locks
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

CREATE TABLE qrtztask_job_details (
  SCHED_NAME varchar2(120) NOT NULL,
  JOB_NAME varchar2(200) NOT NULL,
  JOB_GROUP varchar2(200) NOT NULL,
  DESCRIPTION varchar2(250) DEFAULT NULL,
  JOB_CLASS_NAME varchar2(250) NOT NULL,
  IS_DURABLE varchar2(1) NOT NULL,
  IS_NONCONCURRENT varchar2(1) NOT NULL,
  IS_UPDATE_DATA varchar2(1) NOT NULL,
  REQUESTS_RECOVERY varchar2(1) NOT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtztask_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  JOB_NAME varchar2(200) NOT NULL,
  JOB_GROUP varchar2(200) NOT NULL,
  DESCRIPTION varchar2(250) DEFAULT NULL,
  NEXT_FIRE_TIME NUMBER(13) DEFAULT NULL,
  PREV_FIRE_TIME NUMBER(13) DEFAULT NULL,
  PRIORITY INTEGER DEFAULT NULL,
  TRIGGER_STATE varchar2(16) NOT NULL,
  TRIGGER_TYPE varchar2(8) NOT NULL,
  START_TIME NUMBER(13) NOT NULL,
  END_TIME NUMBER(13) DEFAULT NULL,
  CALENDAR_NAME varchar2(200) DEFAULT NULL,
  MISFIRE_INSTR SMALLINT DEFAULT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
  REFERENCES qrtztask_job_details (SCHED_NAME, JOB_NAME, JOB_GROUP)
);

CREATE TABLE qrtztask_blob_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  BLOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_calendars (
  SCHED_NAME varchar2(120) NOT NULL,
  CALENDAR_NAME varchar2(200) NOT NULL,
  CALENDAR blob NOT NULL,
  PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);

CREATE TABLE qrtztask_cron_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  CRON_EXPRESSION varchar2(200) NOT NULL,
  TIME_ZONE_ID varchar2(80) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_fired_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  ENTRY_ID varchar2(95) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  INSTANCE_NAME varchar2(200) NOT NULL,
  FIRED_TIME NUMBER(13) NOT NULL,
  SCHED_TIME NUMBER(13) NOT NULL,
  PRIORITY INTEGER NOT NULL,
  STATE varchar2(16) NOT NULL,
  JOB_NAME varchar2(200) DEFAULT NULL,
  JOB_GROUP varchar2(200) DEFAULT NULL,
  IS_NONCONCURRENT varchar2(1) DEFAULT NULL,
  REQUESTS_RECOVERY varchar2(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);



CREATE TABLE qrtztask_locks (
  SCHED_NAME varchar2(120) NOT NULL,
  LOCK_NAME varchar2(40) NOT NULL,
  PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

CREATE TABLE qrtztask_paused_trigger_grps (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtztask_scheduler_state (
  SCHED_NAME varchar2(120) NOT NULL,
  INSTANCE_NAME varchar2(200) NOT NULL,
  LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
  CHECKIN_INTERVAL NUMBER(13) NOT NULL,
  PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);

CREATE TABLE qrtztask_simple_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  REPEAT_COUNT NUMBER(7) NOT NULL,
  REPEAT_INTERVAL NUMBER(12) NOT NULL,
  TIMES_TRIGGERED NUMBER(10) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_simprop_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  STR_PROP_1 varchar2(512) DEFAULT NULL,
  STR_PROP_2 varchar2(512) DEFAULT NULL,
  STR_PROP_3 varchar2(512) DEFAULT NULL,
  INT_PROP_1 int DEFAULT NULL,
  INT_PROP_2 int DEFAULT NULL,
  LONG_PROP_1 NUMBER(20) DEFAULT NULL,
  LONG_PROP_2 NUMBER(20) DEFAULT NULL,
  DEC_PROP_1 NUMERIC(13,4) DEFAULT NULL,
  DEC_PROP_2 NUMERIC(13,4) DEFAULT NULL,
  BOOL_PROP_1 varchar2(1) DEFAULT NULL,
  BOOL_PROP_2 varchar2(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
 FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE dv_datasync_log (
  id varchar2(64) NOT NULL,
  source_name varchar2(64) DEFAULT NULL,
  source_owner varchar2(64) DEFAULT NULL,
  source_id varchar2(64) DEFAULT NULL,
  source_type smallint DEFAULT NULL,
  store_type smallint DEFAULT NULL,
  local_data_id varchar2(64) NOT NULL,
  creator varchar2(64) DEFAULT NULL,
  start_time date DEFAULT NULL,
  end_time date DEFAULT NULL,
  sync_method smallint DEFAULT NULL,
  sync_type smallint DEFAULT NULL,
  rows_inserted int DEFAULT NULL,
  rows_deleted int DEFAULT NULL,
  rows_updated int DEFAULT NULL,
  store_server varchar2(64) DEFAULT NULL,
  PRIMARY KEY (id)
);
create index source_data_log_idx on dv_datasync_log (source_type,source_id)
  compress;

CREATE TABLE dv_cktable_info (
  id varchar2(64) NOT NULL,
  server_id varchar2(64) DEFAULT NULL,
  db_name varchar2(64) DEFAULT NULL,
  table_name varchar2(64) DEFAULT NULL,
  table_engine varchar2(64) DEFAULT NULL,
  cluster_name varchar2(64) DEFAULT NULL,
  shard_table_id varchar2(64) DEFAULT NULL,
  created_by varchar2(64) DEFAULT NULL,
  created_on date DEFAULT NULL,
  write_lock varchar2(64) DEFAULT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE dv_local_entity_store (
  localdata_id varchar2(64) NOT NULL,
  entity_name varchar2(64) NOT NULL,
  table_id varchar2(64) DEFAULT NULL,
  fields blob,
  PRIMARY KEY (localdata_id,entity_name)
);

CREATE TABLE dv_localdata (
  id varchar2(64) NOT NULL,
  store_type smallint NOT NULL,
  source_id varchar2(64) NOT NULL,
  source_type smallint NOT NULL,
  created_by varchar2(64) DEFAULT NULL,
  created_on date DEFAULT NULL,
  sync_settings blob,
  other_settings blob,
  PRIMARY KEY (id)
);

create index source_data_idx on dv_localdata (source_id,source_type)
  compress;

INSERT INTO dv_area_base VALUES ('110000', '北京市', '北京', 'beijing', '京', '1', '0', '北京市');
INSERT INTO dv_area_base VALUES ('110101', '东城区', '东城', '', '', '2', '110000', '北京市 东城区');
INSERT INTO dv_area_base VALUES ('110102', '西城区', '西城', '', '', '2', '110000', '北京市 西城区');
INSERT INTO dv_area_base VALUES ('110105', '朝阳区', '朝阳', '', '', '2', '110000', '北京市 朝阳区');
INSERT INTO dv_area_base VALUES ('110106', '丰台区', '丰台', '', '', '2', '110000', '北京市 丰台区');
INSERT INTO dv_area_base VALUES ('110107', '石景山区', '石景山', '', '', '2', '110000', '北京市 石景山区');
INSERT INTO dv_area_base VALUES ('110108', '海淀区', '海淀', '', '', '2', '110000', '北京市 海淀区');
INSERT INTO dv_area_base VALUES ('110109', '门头沟区', '门头沟', '', '', '2', '110000', '北京市 门头沟区');
INSERT INTO dv_area_base VALUES ('110111', '房山区', '房山', '', '', '2', '110000', '北京市 房山区');
INSERT INTO dv_area_base VALUES ('110112', '通州区', '通州', '', '', '2', '110000', '北京市 通州区');
INSERT INTO dv_area_base VALUES ('110113', '顺义区', '顺义', '', '', '2', '110000', '北京市 顺义区');
INSERT INTO dv_area_base VALUES ('110114', '昌平区', '昌平', '', '', '2', '110000', '北京市 昌平区');
INSERT INTO dv_area_base VALUES ('110115', '大兴区', '大兴', '', '', '2', '110000', '北京市 大兴区');
INSERT INTO dv_area_base VALUES ('110116', '怀柔区', '怀柔', '', '', '2', '110000', '北京市 怀柔区');
INSERT INTO dv_area_base VALUES ('110117', '平谷区', '平谷', '', '', '2', '110000', '北京市 平谷区');
INSERT INTO dv_area_base VALUES ('110228', '密云县', '密云', '', '', '2', '110000', '北京市 密云县');
INSERT INTO dv_area_base VALUES ('110229', '延庆县', '延庆', '', '', '2', '110000', '北京市 延庆县');
INSERT INTO dv_area_base VALUES ('120000', '天津市', '天津', 'tianjin', '津', '1', '0', '天津市');
INSERT INTO dv_area_base VALUES ('120101', '和平区', '和平', '', '', '2', '120000', '天津市 和平区');
INSERT INTO dv_area_base VALUES ('120102', '河东区', '河东', '', '', '2', '120000', '天津市 河东区');
INSERT INTO dv_area_base VALUES ('120103', '河西区', '河西', '', '', '2', '120000', '天津市 河西区');
INSERT INTO dv_area_base VALUES ('120104', '南开区', '南开', '', '', '2', '120000', '天津市 南开区');
INSERT INTO dv_area_base VALUES ('120105', '河北区', '河北', '', '', '2', '120000', '天津市 河北区');
INSERT INTO dv_area_base VALUES ('120106', '红桥区', '红桥', '', '', '2', '120000', '天津市 红桥区');
INSERT INTO dv_area_base VALUES ('120110', '东丽区', '东丽', '', '', '2', '120000', '天津市 东丽区');
INSERT INTO dv_area_base VALUES ('120111', '西青区', '西青', '', '', '2', '120000', '天津市 西青区');
INSERT INTO dv_area_base VALUES ('120112', '津南区', '津南', '', '', '2', '120000', '天津市 津南区');
INSERT INTO dv_area_base VALUES ('120113', '北辰区', '北辰', '', '', '2', '120000', '天津市 北辰区');
INSERT INTO dv_area_base VALUES ('120114', '武清区', '武清', '', '', '2', '120000', '天津市 武清区');
INSERT INTO dv_area_base VALUES ('120115', '宝坻区', '宝坻', '', '', '2', '120000', '天津市 宝坻区');
INSERT INTO dv_area_base VALUES ('120116', '滨海新区', '滨海', '', '', '2', '120000', '天津市 滨海新区');
INSERT INTO dv_area_base VALUES ('120117', '宁河区', '宁河', '', '', '2', '120000', '天津市 宁河区');
INSERT INTO dv_area_base VALUES ('120118', '静海区', '静海', '', '', '2', '120000', '天津市 静海区');
INSERT INTO dv_area_base VALUES ('120225', '蓟县', '', '', '', '2', '120000', '天津市 蓟县');
INSERT INTO dv_area_base VALUES ('130000', '河北省', '河北', 'hebei', '冀', '1', '0', '河北省');
INSERT INTO dv_area_base VALUES ('130100', '石家庄市', '石家庄', '', '', '2', '130000', '河北省 石家庄市');
INSERT INTO dv_area_base VALUES ('130102', '长安区', '', '', '', '3', '130100', '河北省 石家庄市 长安区');
INSERT INTO dv_area_base VALUES ('130103', '桥东区', '', '', '', '3', '130100', '河北省 石家庄市 桥东区');
INSERT INTO dv_area_base VALUES ('130104', '桥西区', '', '', '', '3', '130100', '河北省 石家庄市 桥西区');
INSERT INTO dv_area_base VALUES ('130105', '新华区', '', '', '', '3', '130100', '河北省 石家庄市 新华区');
INSERT INTO dv_area_base VALUES ('130107', '井陉矿区', '', '', '', '3', '130100', '河北省 石家庄市 井陉矿区');
INSERT INTO dv_area_base VALUES ('130108', '裕华区', '', '', '', '3', '130100', '河北省 石家庄市 裕华区');
INSERT INTO dv_area_base VALUES ('130121', '井陉县', '', '', '', '3', '130100', '河北省 石家庄市 井陉县');
INSERT INTO dv_area_base VALUES ('130123', '正定县', '', '', '', '3', '130100', '河北省 石家庄市 正定县');
INSERT INTO dv_area_base VALUES ('130124', '栾城县', '', '', '', '3', '130100', '河北省 石家庄市 栾城县');
INSERT INTO dv_area_base VALUES ('130125', '行唐县', '', '', '', '3', '130100', '河北省 石家庄市 行唐县');
INSERT INTO dv_area_base VALUES ('130126', '灵寿县', '', '', '', '3', '130100', '河北省 石家庄市 灵寿县');
INSERT INTO dv_area_base VALUES ('130127', '高邑县', '', '', '', '3', '130100', '河北省 石家庄市 高邑县');
INSERT INTO dv_area_base VALUES ('130128', '深泽县', '', '', '', '3', '130100', '河北省 石家庄市 深泽县');
INSERT INTO dv_area_base VALUES ('130129', '赞皇县', '', '', '', '3', '130100', '河北省 石家庄市 赞皇县');
INSERT INTO dv_area_base VALUES ('130130', '无极县', '', '', '', '3', '130100', '河北省 石家庄市 无极县');
INSERT INTO dv_area_base VALUES ('130131', '平山县', '', '', '', '3', '130100', '河北省 石家庄市 平山县');
INSERT INTO dv_area_base VALUES ('130132', '元氏县', '', '', '', '3', '130100', '河北省 石家庄市 元氏县');
INSERT INTO dv_area_base VALUES ('130133', '赵县', '', '', '', '3', '130100', '河北省 石家庄市 赵县');
INSERT INTO dv_area_base VALUES ('130181', '辛集市', '', '', '', '3', '130100', '河北省 石家庄市 辛集市');
INSERT INTO dv_area_base VALUES ('130182', '藁城市', '', '', '', '3', '130100', '河北省 石家庄市 藁城市');
INSERT INTO dv_area_base VALUES ('130183', '晋州市', '', '', '', '3', '130100', '河北省 石家庄市 晋州市');
INSERT INTO dv_area_base VALUES ('130184', '新乐市', '', '', '', '3', '130100', '河北省 石家庄市 新乐市');
INSERT INTO dv_area_base VALUES ('130185', '鹿泉市', '', '', '', '3', '130100', '河北省 石家庄市 鹿泉市');
INSERT INTO dv_area_base VALUES ('130200', '唐山市', '唐山', '', '', '2', '130000', '河北省 唐山市');
INSERT INTO dv_area_base VALUES ('130202', '路南区', '', '', '', '3', '130200', '河北省 唐山市 路南区');
INSERT INTO dv_area_base VALUES ('130203', '路北区', '', '', '', '3', '130200', '河北省 唐山市 路北区');
INSERT INTO dv_area_base VALUES ('130204', '古冶区', '', '', '', '3', '130200', '河北省 唐山市 古冶区');
INSERT INTO dv_area_base VALUES ('130205', '开平区', '', '', '', '3', '130200', '河北省 唐山市 开平区');
INSERT INTO dv_area_base VALUES ('130207', '丰南区', '', '', '', '3', '130200', '河北省 唐山市 丰南区');
INSERT INTO dv_area_base VALUES ('130208', '丰润区', '', '', '', '3', '130200', '河北省 唐山市 丰润区');
INSERT INTO dv_area_base VALUES ('130223', '滦县', '', '', '', '3', '130200', '河北省 唐山市 滦县');
INSERT INTO dv_area_base VALUES ('130224', '滦南县', '', '', '', '3', '130200', '河北省 唐山市 滦南县');
INSERT INTO dv_area_base VALUES ('130225', '乐亭县', '', '', '', '3', '130200', '河北省 唐山市 乐亭县');
INSERT INTO dv_area_base VALUES ('130227', '迁西县', '', '', '', '3', '130200', '河北省 唐山市 迁西县');
INSERT INTO dv_area_base VALUES ('130229', '玉田县', '', '', '', '3', '130200', '河北省 唐山市 玉田县');
INSERT INTO dv_area_base VALUES ('130230', '曹妃甸区', '', '', '', '3', '130200', '河北省 唐山市 曹妃甸区');
INSERT INTO dv_area_base VALUES ('130281', '遵化市', '', '', '', '3', '130200', '河北省 唐山市 遵化市');
INSERT INTO dv_area_base VALUES ('130283', '迁安市', '', '', '', '3', '130200', '河北省 唐山市 迁安市');
INSERT INTO dv_area_base VALUES ('130300', '秦皇岛市', '秦皇岛', '', '', '2', '130000', '河北省 秦皇岛市');
INSERT INTO dv_area_base VALUES ('130302', '海港区', '', '', '', '3', '130300', '河北省 秦皇岛市 海港区');
INSERT INTO dv_area_base VALUES ('130303', '山海关区', '', '', '', '3', '130300', '河北省 秦皇岛市 山海关区');
INSERT INTO dv_area_base VALUES ('130304', '北戴河区', '', '', '', '3', '130300', '河北省 秦皇岛市 北戴河区');
INSERT INTO dv_area_base VALUES ('130321', '青龙满族自治县', '', '', '', '3', '130300', '河北省 秦皇岛市 青龙满族自治县');
INSERT INTO dv_area_base VALUES ('130322', '昌黎县', '', '', '', '3', '130300', '河北省 秦皇岛市 昌黎县');
INSERT INTO dv_area_base VALUES ('130323', '抚宁县', '', '', '', '3', '130300', '河北省 秦皇岛市 抚宁县');
INSERT INTO dv_area_base VALUES ('130324', '卢龙县', '', '', '', '3', '130300', '河北省 秦皇岛市 卢龙县');
INSERT INTO dv_area_base VALUES ('130400', '邯郸市', '邯郸', '', '', '2', '130000', '河北省 邯郸市');
INSERT INTO dv_area_base VALUES ('130402', '邯山区', '', '', '', '3', '130400', '河北省 邯郸市 邯山区');
INSERT INTO dv_area_base VALUES ('130403', '丛台区', '', '', '', '3', '130400', '河北省 邯郸市 丛台区');
INSERT INTO dv_area_base VALUES ('130404', '复兴区', '', '', '', '3', '130400', '河北省 邯郸市 复兴区');
INSERT INTO dv_area_base VALUES ('130406', '峰峰矿区', '', '', '', '3', '130400', '河北省 邯郸市 峰峰矿区');
INSERT INTO dv_area_base VALUES ('130421', '邯郸县', '', '', '', '3', '130400', '河北省 邯郸市 邯郸县');
INSERT INTO dv_area_base VALUES ('130423', '临漳县', '', '', '', '3', '130400', '河北省 邯郸市 临漳县');
INSERT INTO dv_area_base VALUES ('130424', '成安县', '', '', '', '3', '130400', '河北省 邯郸市 成安县');
INSERT INTO dv_area_base VALUES ('130425', '大名县', '', '', '', '3', '130400', '河北省 邯郸市 大名县');
INSERT INTO dv_area_base VALUES ('130426', '涉县', '', '', '', '3', '130400', '河北省 邯郸市 涉县');
INSERT INTO dv_area_base VALUES ('130427', '磁县', '', '', '', '3', '130400', '河北省 邯郸市 磁县');
INSERT INTO dv_area_base VALUES ('130428', '肥乡县', '', '', '', '3', '130400', '河北省 邯郸市 肥乡县');
INSERT INTO dv_area_base VALUES ('130429', '永年县', '', '', '', '3', '130400', '河北省 邯郸市 永年县');
INSERT INTO dv_area_base VALUES ('130430', '邱县', '', '', '', '3', '130400', '河北省 邯郸市 邱县');
INSERT INTO dv_area_base VALUES ('130431', '鸡泽县', '', '', '', '3', '130400', '河北省 邯郸市 鸡泽县');
INSERT INTO dv_area_base VALUES ('130432', '广平县', '', '', '', '3', '130400', '河北省 邯郸市 广平县');
INSERT INTO dv_area_base VALUES ('130433', '馆陶县', '', '', '', '3', '130400', '河北省 邯郸市 馆陶县');
INSERT INTO dv_area_base VALUES ('130434', '魏县', '', '', '', '3', '130400', '河北省 邯郸市 魏县');
INSERT INTO dv_area_base VALUES ('130435', '曲周县', '', '', '', '3', '130400', '河北省 邯郸市 曲周县');
INSERT INTO dv_area_base VALUES ('130481', '武安市', '', '', '', '3', '130400', '河北省 邯郸市 武安市');
INSERT INTO dv_area_base VALUES ('130500', '邢台市', '邢台', '', '', '2', '130000', '河北省 邢台市');
INSERT INTO dv_area_base VALUES ('130502', '桥东区', '', '', '', '3', '130500', '河北省 邢台市 桥东区');
INSERT INTO dv_area_base VALUES ('130503', '桥西区', '', '', '', '3', '130500', '河北省 邢台市 桥西区');
INSERT INTO dv_area_base VALUES ('130521', '邢台县', '', '', '', '3', '130500', '河北省 邢台市 邢台县');
INSERT INTO dv_area_base VALUES ('130522', '临城县', '', '', '', '3', '130500', '河北省 邢台市 临城县');
INSERT INTO dv_area_base VALUES ('130523', '内丘县', '', '', '', '3', '130500', '河北省 邢台市 内丘县');
INSERT INTO dv_area_base VALUES ('130524', '柏乡县', '', '', '', '3', '130500', '河北省 邢台市 柏乡县');
INSERT INTO dv_area_base VALUES ('130525', '隆尧县', '', '', '', '3', '130500', '河北省 邢台市 隆尧县');
INSERT INTO dv_area_base VALUES ('130526', '任县', '', '', '', '3', '130500', '河北省 邢台市 任县');
INSERT INTO dv_area_base VALUES ('130527', '南和县', '', '', '', '3', '130500', '河北省 邢台市 南和县');
INSERT INTO dv_area_base VALUES ('130528', '宁晋县', '', '', '', '3', '130500', '河北省 邢台市 宁晋县');
INSERT INTO dv_area_base VALUES ('130529', '巨鹿县', '', '', '', '3', '130500', '河北省 邢台市 巨鹿县');
INSERT INTO dv_area_base VALUES ('130530', '新河县', '', '', '', '3', '130500', '河北省 邢台市 新河县');
INSERT INTO dv_area_base VALUES ('130531', '广宗县', '', '', '', '3', '130500', '河北省 邢台市 广宗县');
INSERT INTO dv_area_base VALUES ('130532', '平乡县', '', '', '', '3', '130500', '河北省 邢台市 平乡县');
INSERT INTO dv_area_base VALUES ('130533', '威县', '', '', '', '3', '130500', '河北省 邢台市 威县');
INSERT INTO dv_area_base VALUES ('130534', '清河县', '', '', '', '3', '130500', '河北省 邢台市 清河县');
INSERT INTO dv_area_base VALUES ('130535', '临西县', '', '', '', '3', '130500', '河北省 邢台市 临西县');
INSERT INTO dv_area_base VALUES ('130581', '南宫市', '', '', '', '3', '130500', '河北省 邢台市 南宫市');
INSERT INTO dv_area_base VALUES ('130582', '沙河市', '', '', '', '3', '130500', '河北省 邢台市 沙河市');
INSERT INTO dv_area_base VALUES ('130600', '保定市', '保定', '', '', '2', '130000', '河北省 保定市');
INSERT INTO dv_area_base VALUES ('130602', '新市区', '', '', '', '3', '130600', '河北省 保定市 新市区');
INSERT INTO dv_area_base VALUES ('130603', '北市区', '', '', '', '3', '130600', '河北省 保定市 北市区');
INSERT INTO dv_area_base VALUES ('130604', '南市区', '', '', '', '3', '130600', '河北省 保定市 南市区');
INSERT INTO dv_area_base VALUES ('130621', '满城县', '', '', '', '3', '130600', '河北省 保定市 满城县');
INSERT INTO dv_area_base VALUES ('130622', '清苑县', '', '', '', '3', '130600', '河北省 保定市 清苑县');
INSERT INTO dv_area_base VALUES ('130623', '涞水县', '', '', '', '3', '130600', '河北省 保定市 涞水县');
INSERT INTO dv_area_base VALUES ('130624', '阜平县', '', '', '', '3', '130600', '河北省 保定市 阜平县');
INSERT INTO dv_area_base VALUES ('130625', '徐水县', '', '', '', '3', '130600', '河北省 保定市 徐水县');
INSERT INTO dv_area_base VALUES ('130626', '定兴县', '', '', '', '3', '130600', '河北省 保定市 定兴县');
INSERT INTO dv_area_base VALUES ('130627', '唐县', '', '', '', '3', '130600', '河北省 保定市 唐县');
INSERT INTO dv_area_base VALUES ('130628', '高阳县', '', '', '', '3', '130600', '河北省 保定市 高阳县');
INSERT INTO dv_area_base VALUES ('130629', '容城县', '', '', '', '3', '130600', '河北省 保定市 容城县');
INSERT INTO dv_area_base VALUES ('130630', '涞源县', '', '', '', '3', '130600', '河北省 保定市 涞源县');
INSERT INTO dv_area_base VALUES ('130631', '望都县', '', '', '', '3', '130600', '河北省 保定市 望都县');
INSERT INTO dv_area_base VALUES ('130632', '安新县', '', '', '', '3', '130600', '河北省 保定市 安新县');
INSERT INTO dv_area_base VALUES ('130633', '易县', '', '', '', '3', '130600', '河北省 保定市 易县');
INSERT INTO dv_area_base VALUES ('130634', '曲阳县', '', '', '', '3', '130600', '河北省 保定市 曲阳县');
INSERT INTO dv_area_base VALUES ('130635', '蠡县', '', '', '', '3', '130600', '河北省 保定市 蠡县');
INSERT INTO dv_area_base VALUES ('130636', '顺平县', '', '', '', '3', '130600', '河北省 保定市 顺平县');
INSERT INTO dv_area_base VALUES ('130637', '博野县', '', '', '', '3', '130600', '河北省 保定市 博野县');
INSERT INTO dv_area_base VALUES ('130638', '雄县', '', '', '', '3', '130600', '河北省 保定市 雄县');
INSERT INTO dv_area_base VALUES ('130681', '涿州市', '', '', '', '3', '130600', '河北省 保定市 涿州市');
INSERT INTO dv_area_base VALUES ('130682', '定州市', '', '', '', '3', '130600', '河北省 保定市 定州市');
INSERT INTO dv_area_base VALUES ('130683', '安国市', '', '', '', '3', '130600', '河北省 保定市 安国市');
INSERT INTO dv_area_base VALUES ('130684', '高碑店市', '', '', '', '3', '130600', '河北省 保定市 高碑店市');
INSERT INTO dv_area_base VALUES ('130700', '张家口市', '张家口', '', '', '2', '130000', '河北省 张家口市');
INSERT INTO dv_area_base VALUES ('130702', '桥东区', '', '', '', '3', '130700', '河北省 张家口市 桥东区');
INSERT INTO dv_area_base VALUES ('130703', '桥西区', '', '', '', '3', '130700', '河北省 张家口市 桥西区');
INSERT INTO dv_area_base VALUES ('130705', '宣化区', '', '', '', '3', '130700', '河北省 张家口市 宣化区');
INSERT INTO dv_area_base VALUES ('130706', '下花园区', '', '', '', '3', '130700', '河北省 张家口市 下花园区');
INSERT INTO dv_area_base VALUES ('130721', '宣化县', '', '', '', '3', '130700', '河北省 张家口市 宣化县');
INSERT INTO dv_area_base VALUES ('130722', '张北县', '', '', '', '3', '130700', '河北省 张家口市 张北县');
INSERT INTO dv_area_base VALUES ('130723', '康保县', '', '', '', '3', '130700', '河北省 张家口市 康保县');
INSERT INTO dv_area_base VALUES ('130724', '沽源县', '', '', '', '3', '130700', '河北省 张家口市 沽源县');
INSERT INTO dv_area_base VALUES ('130725', '尚义县', '', '', '', '3', '130700', '河北省 张家口市 尚义县');
INSERT INTO dv_area_base VALUES ('130726', '蔚县', '', '', '', '3', '130700', '河北省 张家口市 蔚县');
INSERT INTO dv_area_base VALUES ('130727', '阳原县', '', '', '', '3', '130700', '河北省 张家口市 阳原县');
INSERT INTO dv_area_base VALUES ('130728', '怀安县', '', '', '', '3', '130700', '河北省 张家口市 怀安县');
INSERT INTO dv_area_base VALUES ('130729', '万全县', '', '', '', '3', '130700', '河北省 张家口市 万全县');
INSERT INTO dv_area_base VALUES ('130730', '怀来县', '', '', '', '3', '130700', '河北省 张家口市 怀来县');
INSERT INTO dv_area_base VALUES ('130731', '涿鹿县', '', '', '', '3', '130700', '河北省 张家口市 涿鹿县');
INSERT INTO dv_area_base VALUES ('130732', '赤城县', '', '', '', '3', '130700', '河北省 张家口市 赤城县');
INSERT INTO dv_area_base VALUES ('130733', '崇礼县', '', '', '', '3', '130700', '河北省 张家口市 崇礼县');
INSERT INTO dv_area_base VALUES ('130800', '承德市', '承德', '', '', '2', '130000', '河北省 承德市');
INSERT INTO dv_area_base VALUES ('130802', '双桥区', '', '', '', '3', '130800', '河北省 承德市 双桥区');
INSERT INTO dv_area_base VALUES ('130803', '双滦区', '', '', '', '3', '130800', '河北省 承德市 双滦区');
INSERT INTO dv_area_base VALUES ('130804', '鹰手营子矿区', '', '', '', '3', '130800', '河北省 承德市 鹰手营子矿区');
INSERT INTO dv_area_base VALUES ('130821', '承德县', '', '', '', '3', '130800', '河北省 承德市 承德县');
INSERT INTO dv_area_base VALUES ('130822', '兴隆县', '', '', '', '3', '130800', '河北省 承德市 兴隆县');
INSERT INTO dv_area_base VALUES ('130823', '平泉县', '', '', '', '3', '130800', '河北省 承德市 平泉县');
INSERT INTO dv_area_base VALUES ('130824', '滦平县', '', '', '', '3', '130800', '河北省 承德市 滦平县');
INSERT INTO dv_area_base VALUES ('130825', '隆化县', '', '', '', '3', '130800', '河北省 承德市 隆化县');
INSERT INTO dv_area_base VALUES ('130826', '丰宁满族自治县', '', '', '', '3', '130800', '河北省 承德市 丰宁满族自治县');
INSERT INTO dv_area_base VALUES ('130827', '宽城满族自治县', '', '', '', '3', '130800', '河北省 承德市 宽城满族自治县');
INSERT INTO dv_area_base VALUES ('130828', '围场满族蒙古族自治县', '', '', '', '3', '130800', '河北省 承德市 围场满族蒙古族自治县');
INSERT INTO dv_area_base VALUES ('130900', '沧州市', '沧州', '', '', '2', '130000', '河北省 沧州市');
INSERT INTO dv_area_base VALUES ('130902', '新华区', '', '', '', '3', '130900', '河北省 沧州市 新华区');
INSERT INTO dv_area_base VALUES ('130903', '运河区', '', '', '', '3', '130900', '河北省 沧州市 运河区');
INSERT INTO dv_area_base VALUES ('130921', '沧县', '', '', '', '3', '130900', '河北省 沧州市 沧县');
INSERT INTO dv_area_base VALUES ('130922', '青县', '', '', '', '3', '130900', '河北省 沧州市 青县');
INSERT INTO dv_area_base VALUES ('130923', '东光县', '', '', '', '3', '130900', '河北省 沧州市 东光县');
INSERT INTO dv_area_base VALUES ('130924', '海兴县', '', '', '', '3', '130900', '河北省 沧州市 海兴县');
INSERT INTO dv_area_base VALUES ('130925', '盐山县', '', '', '', '3', '130900', '河北省 沧州市 盐山县');
INSERT INTO dv_area_base VALUES ('130926', '肃宁县', '', '', '', '3', '130900', '河北省 沧州市 肃宁县');
INSERT INTO dv_area_base VALUES ('130927', '南皮县', '', '', '', '3', '130900', '河北省 沧州市 南皮县');
INSERT INTO dv_area_base VALUES ('130928', '吴桥县', '', '', '', '3', '130900', '河北省 沧州市 吴桥县');
INSERT INTO dv_area_base VALUES ('130929', '献县', '', '', '', '3', '130900', '河北省 沧州市 献县');
INSERT INTO dv_area_base VALUES ('130930', '孟村回族自治县', '', '', '', '3', '130900', '河北省 沧州市 孟村回族自治县');
INSERT INTO dv_area_base VALUES ('130981', '泊头市', '', '', '', '3', '130900', '河北省 沧州市 泊头市');
INSERT INTO dv_area_base VALUES ('130982', '任丘市', '', '', '', '3', '130900', '河北省 沧州市 任丘市');
INSERT INTO dv_area_base VALUES ('130983', '黄骅市', '', '', '', '3', '130900', '河北省 沧州市 黄骅市');
INSERT INTO dv_area_base VALUES ('130984', '河间市', '', '', '', '3', '130900', '河北省 沧州市 河间市');
INSERT INTO dv_area_base VALUES ('131000', '廊坊市', '廊坊', '', '', '2', '130000', '河北省 廊坊市');
INSERT INTO dv_area_base VALUES ('131002', '安次区', '', '', '', '3', '131000', '河北省 廊坊市 安次区');
INSERT INTO dv_area_base VALUES ('131003', '广阳区', '', '', '', '3', '131000', '河北省 廊坊市 广阳区');
INSERT INTO dv_area_base VALUES ('131022', '固安县', '', '', '', '3', '131000', '河北省 廊坊市 固安县');
INSERT INTO dv_area_base VALUES ('131023', '永清县', '', '', '', '3', '131000', '河北省 廊坊市 永清县');
INSERT INTO dv_area_base VALUES ('131024', '香河县', '', '', '', '3', '131000', '河北省 廊坊市 香河县');
INSERT INTO dv_area_base VALUES ('131025', '大城县', '', '', '', '3', '131000', '河北省 廊坊市 大城县');
INSERT INTO dv_area_base VALUES ('131026', '文安县', '', '', '', '3', '131000', '河北省 廊坊市 文安县');
INSERT INTO dv_area_base VALUES ('131028', '大厂回族自治县', '', '', '', '3', '131000', '河北省 廊坊市 大厂回族自治县');
INSERT INTO dv_area_base VALUES ('131081', '霸州市', '', '', '', '3', '131000', '河北省 廊坊市 霸州市');
INSERT INTO dv_area_base VALUES ('131082', '三河市', '', '', '', '3', '131000', '河北省 廊坊市 三河市');
INSERT INTO dv_area_base VALUES ('131100', '衡水市', '衡水', '', '', '2', '130000', '河北省 衡水市');
INSERT INTO dv_area_base VALUES ('131102', '桃城区', '', '', '', '3', '131100', '河北省 衡水市 桃城区');
INSERT INTO dv_area_base VALUES ('131121', '枣强县', '', '', '', '3', '131100', '河北省 衡水市 枣强县');
INSERT INTO dv_area_base VALUES ('131122', '武邑县', '', '', '', '3', '131100', '河北省 衡水市 武邑县');
INSERT INTO dv_area_base VALUES ('131123', '武强县', '', '', '', '3', '131100', '河北省 衡水市 武强县');
INSERT INTO dv_area_base VALUES ('131124', '饶阳县', '', '', '', '3', '131100', '河北省 衡水市 饶阳县');
INSERT INTO dv_area_base VALUES ('131125', '安平县', '', '', '', '3', '131100', '河北省 衡水市 安平县');
INSERT INTO dv_area_base VALUES ('131126', '故城县', '', '', '', '3', '131100', '河北省 衡水市 故城县');
INSERT INTO dv_area_base VALUES ('131127', '景县', '', '', '', '3', '131100', '河北省 衡水市 景县');
INSERT INTO dv_area_base VALUES ('131128', '阜城县', '', '', '', '3', '131100', '河北省 衡水市 阜城县');
INSERT INTO dv_area_base VALUES ('131181', '冀州市', '', '', '', '3', '131100', '河北省 衡水市 冀州市');
INSERT INTO dv_area_base VALUES ('131182', '深州市', '', '', '', '3', '131100', '河北省 衡水市 深州市');
INSERT INTO dv_area_base VALUES ('140000', '山西省', '山西', 'shanxi', '晋', '1', '0', '山西省');
INSERT INTO dv_area_base VALUES ('140100', '太原市', '太原', '', '', '2', '140000', '山西省 太原市');
INSERT INTO dv_area_base VALUES ('140105', '小店区', '', '', '', '3', '140100', '山西省 太原市 小店区');
INSERT INTO dv_area_base VALUES ('140106', '迎泽区', '', '', '', '3', '140100', '山西省 太原市 迎泽区');
INSERT INTO dv_area_base VALUES ('140107', '杏花岭区', '', '', '', '3', '140100', '山西省 太原市 杏花岭区');
INSERT INTO dv_area_base VALUES ('140108', '尖草坪区', '', '', '', '3', '140100', '山西省 太原市 尖草坪区');
INSERT INTO dv_area_base VALUES ('140109', '万柏林区', '', '', '', '3', '140100', '山西省 太原市 万柏林区');
INSERT INTO dv_area_base VALUES ('140110', '晋源区', '', '', '', '3', '140100', '山西省 太原市 晋源区');
INSERT INTO dv_area_base VALUES ('140121', '清徐县', '', '', '', '3', '140100', '山西省 太原市 清徐县');
INSERT INTO dv_area_base VALUES ('140122', '阳曲县', '', '', '', '3', '140100', '山西省 太原市 阳曲县');
INSERT INTO dv_area_base VALUES ('140123', '娄烦县', '', '', '', '3', '140100', '山西省 太原市 娄烦县');
INSERT INTO dv_area_base VALUES ('140181', '古交市', '', '', '', '3', '140100', '山西省 太原市 古交市');
INSERT INTO dv_area_base VALUES ('140200', '大同市', '大同', '', '', '2', '140000', '山西省 大同市');
INSERT INTO dv_area_base VALUES ('140202', '城区', '', '', '', '3', '140200', '山西省 大同市 城区');
INSERT INTO dv_area_base VALUES ('140211', '南郊区', '', '', '', '3', '140200', '山西省 大同市 南郊区');
INSERT INTO dv_area_base VALUES ('140212', '新荣区', '', '', '', '3', '140200', '山西省 大同市 新荣区');
INSERT INTO dv_area_base VALUES ('140221', '阳高县', '', '', '', '3', '140200', '山西省 大同市 阳高县');
INSERT INTO dv_area_base VALUES ('140222', '天镇县', '', '', '', '3', '140200', '山西省 大同市 天镇县');
INSERT INTO dv_area_base VALUES ('140223', '广灵县', '', '', '', '3', '140200', '山西省 大同市 广灵县');
INSERT INTO dv_area_base VALUES ('140224', '灵丘县', '', '', '', '3', '140200', '山西省 大同市 灵丘县');
INSERT INTO dv_area_base VALUES ('140225', '浑源县', '', '', '', '3', '140200', '山西省 大同市 浑源县');
INSERT INTO dv_area_base VALUES ('140226', '左云县', '', '', '', '3', '140200', '山西省 大同市 左云县');
INSERT INTO dv_area_base VALUES ('140227', '大同县', '', '', '', '3', '140200', '山西省 大同市 大同县');
INSERT INTO dv_area_base VALUES ('140300', '阳泉市', '阳泉', '', '', '2', '140000', '山西省 阳泉市');
INSERT INTO dv_area_base VALUES ('140302', '城区', '', '', '', '3', '140300', '山西省 阳泉市 城区');
INSERT INTO dv_area_base VALUES ('140303', '矿区', '', '', '', '3', '140300', '山西省 阳泉市 矿区');
INSERT INTO dv_area_base VALUES ('140311', '郊区', '', '', '', '3', '140300', '山西省 阳泉市 郊区');
INSERT INTO dv_area_base VALUES ('140321', '平定县', '', '', '', '3', '140300', '山西省 阳泉市 平定县');
INSERT INTO dv_area_base VALUES ('140322', '盂县', '', '', '', '3', '140300', '山西省 阳泉市 盂县');
INSERT INTO dv_area_base VALUES ('140400', '长治市', '长治', '', '', '2', '140000', '山西省 长治市');
INSERT INTO dv_area_base VALUES ('140402', '城区', '', '', '', '3', '140400', '山西省 长治市 城区');
INSERT INTO dv_area_base VALUES ('140411', '郊区', '', '', '', '3', '140400', '山西省 长治市 郊区');
INSERT INTO dv_area_base VALUES ('140421', '长治县', '', '', '', '3', '140400', '山西省 长治市 长治县');
INSERT INTO dv_area_base VALUES ('140423', '襄垣县', '', '', '', '3', '140400', '山西省 长治市 襄垣县');
INSERT INTO dv_area_base VALUES ('140424', '屯留县', '', '', '', '3', '140400', '山西省 长治市 屯留县');
INSERT INTO dv_area_base VALUES ('140425', '平顺县', '', '', '', '3', '140400', '山西省 长治市 平顺县');
INSERT INTO dv_area_base VALUES ('140426', '黎城县', '', '', '', '3', '140400', '山西省 长治市 黎城县');
INSERT INTO dv_area_base VALUES ('140427', '壶关县', '', '', '', '3', '140400', '山西省 长治市 壶关县');
INSERT INTO dv_area_base VALUES ('140428', '长子县', '', '', '', '3', '140400', '山西省 长治市 长子县');
INSERT INTO dv_area_base VALUES ('140429', '武乡县', '', '', '', '3', '140400', '山西省 长治市 武乡县');
INSERT INTO dv_area_base VALUES ('140430', '沁县', '', '', '', '3', '140400', '山西省 长治市 沁县');
INSERT INTO dv_area_base VALUES ('140431', '沁源县', '', '', '', '3', '140400', '山西省 长治市 沁源县');
INSERT INTO dv_area_base VALUES ('140481', '潞城市', '', '', '', '3', '140400', '山西省 长治市 潞城市');
INSERT INTO dv_area_base VALUES ('140500', '晋城市', '晋城', '', '', '2', '140000', '山西省 晋城市');
INSERT INTO dv_area_base VALUES ('140502', '城区', '', '', '', '3', '140500', '山西省 晋城市 城区');
INSERT INTO dv_area_base VALUES ('140521', '沁水县', '', '', '', '3', '140500', '山西省 晋城市 沁水县');
INSERT INTO dv_area_base VALUES ('140522', '阳城县', '', '', '', '3', '140500', '山西省 晋城市 阳城县');
INSERT INTO dv_area_base VALUES ('140524', '陵川县', '', '', '', '3', '140500', '山西省 晋城市 陵川县');
INSERT INTO dv_area_base VALUES ('140525', '泽州县', '', '', '', '3', '140500', '山西省 晋城市 泽州县');
INSERT INTO dv_area_base VALUES ('140581', '高平市', '', '', '', '3', '140500', '山西省 晋城市 高平市');
INSERT INTO dv_area_base VALUES ('140600', '朔州市', '朔州', '', '', '2', '140000', '山西省 朔州市');
INSERT INTO dv_area_base VALUES ('140602', '朔城区', '', '', '', '3', '140600', '山西省 朔州市 朔城区');
INSERT INTO dv_area_base VALUES ('140603', '平鲁区', '', '', '', '3', '140600', '山西省 朔州市 平鲁区');
INSERT INTO dv_area_base VALUES ('140621', '山阴县', '', '', '', '3', '140600', '山西省 朔州市 山阴县');
INSERT INTO dv_area_base VALUES ('140622', '应县', '', '', '', '3', '140600', '山西省 朔州市 应县');
INSERT INTO dv_area_base VALUES ('140623', '右玉县', '', '', '', '3', '140600', '山西省 朔州市 右玉县');
INSERT INTO dv_area_base VALUES ('140624', '怀仁县', '', '', '', '3', '140600', '山西省 朔州市 怀仁县');
INSERT INTO dv_area_base VALUES ('140700', '晋中市', '晋中', '', '', '2', '140000', '山西省 晋中市');
INSERT INTO dv_area_base VALUES ('140702', '榆次区', '', '', '', '3', '140700', '山西省 晋中市 榆次区');
INSERT INTO dv_area_base VALUES ('140721', '榆社县', '', '', '', '3', '140700', '山西省 晋中市 榆社县');
INSERT INTO dv_area_base VALUES ('140722', '左权县', '', '', '', '3', '140700', '山西省 晋中市 左权县');
INSERT INTO dv_area_base VALUES ('140723', '和顺县', '', '', '', '3', '140700', '山西省 晋中市 和顺县');
INSERT INTO dv_area_base VALUES ('140724', '昔阳县', '', '', '', '3', '140700', '山西省 晋中市 昔阳县');
INSERT INTO dv_area_base VALUES ('140725', '寿阳县', '', '', '', '3', '140700', '山西省 晋中市 寿阳县');
INSERT INTO dv_area_base VALUES ('140726', '太谷县', '', '', '', '3', '140700', '山西省 晋中市 太谷县');
INSERT INTO dv_area_base VALUES ('140727', '祁县', '', '', '', '3', '140700', '山西省 晋中市 祁县');
INSERT INTO dv_area_base VALUES ('140728', '平遥县', '', '', '', '3', '140700', '山西省 晋中市 平遥县');
INSERT INTO dv_area_base VALUES ('140729', '灵石县', '', '', '', '3', '140700', '山西省 晋中市 灵石县');
INSERT INTO dv_area_base VALUES ('140781', '介休市', '', '', '', '3', '140700', '山西省 晋中市 介休市');
INSERT INTO dv_area_base VALUES ('140800', '运城市', '运城', '', '', '2', '140000', '山西省 运城市');
INSERT INTO dv_area_base VALUES ('140802', '盐湖区', '', '', '', '3', '140800', '山西省 运城市 盐湖区');
INSERT INTO dv_area_base VALUES ('140821', '临猗县', '', '', '', '3', '140800', '山西省 运城市 临猗县');
INSERT INTO dv_area_base VALUES ('140822', '万荣县', '', '', '', '3', '140800', '山西省 运城市 万荣县');
INSERT INTO dv_area_base VALUES ('140823', '闻喜县', '', '', '', '3', '140800', '山西省 运城市 闻喜县');
INSERT INTO dv_area_base VALUES ('140824', '稷山县', '', '', '', '3', '140800', '山西省 运城市 稷山县');
INSERT INTO dv_area_base VALUES ('140825', '新绛县', '', '', '', '3', '140800', '山西省 运城市 新绛县');
INSERT INTO dv_area_base VALUES ('140826', '绛县', '', '', '', '3', '140800', '山西省 运城市 绛县');
INSERT INTO dv_area_base VALUES ('140827', '垣曲县', '', '', '', '3', '140800', '山西省 运城市 垣曲县');
INSERT INTO dv_area_base VALUES ('140828', '夏县', '', '', '', '3', '140800', '山西省 运城市 夏县');
INSERT INTO dv_area_base VALUES ('140829', '平陆县', '', '', '', '3', '140800', '山西省 运城市 平陆县');
INSERT INTO dv_area_base VALUES ('140830', '芮城县', '', '', '', '3', '140800', '山西省 运城市 芮城县');
INSERT INTO dv_area_base VALUES ('140881', '永济市', '', '', '', '3', '140800', '山西省 运城市 永济市');
INSERT INTO dv_area_base VALUES ('140882', '河津市', '', '', '', '3', '140800', '山西省 运城市 河津市');
INSERT INTO dv_area_base VALUES ('140900', '忻州市', '忻州', '', '', '2', '140000', '山西省 忻州市');
INSERT INTO dv_area_base VALUES ('140902', '忻府区', '', '', '', '3', '140900', '山西省 忻州市 忻府区');
INSERT INTO dv_area_base VALUES ('140921', '定襄县', '', '', '', '3', '140900', '山西省 忻州市 定襄县');
INSERT INTO dv_area_base VALUES ('140922', '五台县', '', '', '', '3', '140900', '山西省 忻州市 五台县');
INSERT INTO dv_area_base VALUES ('140923', '代县', '', '', '', '3', '140900', '山西省 忻州市 代县');
INSERT INTO dv_area_base VALUES ('140924', '繁峙县', '', '', '', '3', '140900', '山西省 忻州市 繁峙县');
INSERT INTO dv_area_base VALUES ('140925', '宁武县', '', '', '', '3', '140900', '山西省 忻州市 宁武县');
INSERT INTO dv_area_base VALUES ('140926', '静乐县', '', '', '', '3', '140900', '山西省 忻州市 静乐县');
INSERT INTO dv_area_base VALUES ('140927', '神池县', '', '', '', '3', '140900', '山西省 忻州市 神池县');
INSERT INTO dv_area_base VALUES ('140928', '五寨县', '', '', '', '3', '140900', '山西省 忻州市 五寨县');
INSERT INTO dv_area_base VALUES ('140929', '岢岚县', '', '', '', '3', '140900', '山西省 忻州市 岢岚县');
INSERT INTO dv_area_base VALUES ('140930', '河曲县', '', '', '', '3', '140900', '山西省 忻州市 河曲县');
INSERT INTO dv_area_base VALUES ('140931', '保德县', '', '', '', '3', '140900', '山西省 忻州市 保德县');
INSERT INTO dv_area_base VALUES ('140932', '偏关县', '', '', '', '3', '140900', '山西省 忻州市 偏关县');
INSERT INTO dv_area_base VALUES ('140981', '原平市', '', '', '', '3', '140900', '山西省 忻州市 原平市');
INSERT INTO dv_area_base VALUES ('141000', '临汾市', '临汾', '', '', '2', '140000', '山西省 临汾市');
INSERT INTO dv_area_base VALUES ('141002', '尧都区', '', '', '', '3', '141000', '山西省 临汾市 尧都区');
INSERT INTO dv_area_base VALUES ('141021', '曲沃县', '', '', '', '3', '141000', '山西省 临汾市 曲沃县');
INSERT INTO dv_area_base VALUES ('141022', '翼城县', '', '', '', '3', '141000', '山西省 临汾市 翼城县');
INSERT INTO dv_area_base VALUES ('141023', '襄汾县', '', '', '', '3', '141000', '山西省 临汾市 襄汾县');
INSERT INTO dv_area_base VALUES ('141024', '洪洞县', '', '', '', '3', '141000', '山西省 临汾市 洪洞县');
INSERT INTO dv_area_base VALUES ('141025', '古县', '', '', '', '3', '141000', '山西省 临汾市 古县');
INSERT INTO dv_area_base VALUES ('141026', '安泽县', '', '', '', '3', '141000', '山西省 临汾市 安泽县');
INSERT INTO dv_area_base VALUES ('141027', '浮山县', '', '', '', '3', '141000', '山西省 临汾市 浮山县');
INSERT INTO dv_area_base VALUES ('141028', '吉县', '', '', '', '3', '141000', '山西省 临汾市 吉县');
INSERT INTO dv_area_base VALUES ('141029', '乡宁县', '', '', '', '3', '141000', '山西省 临汾市 乡宁县');
INSERT INTO dv_area_base VALUES ('141030', '大宁县', '', '', '', '3', '141000', '山西省 临汾市 大宁县');
INSERT INTO dv_area_base VALUES ('141031', '隰县', '', '', '', '3', '141000', '山西省 临汾市 隰县');
INSERT INTO dv_area_base VALUES ('141032', '永和县', '', '', '', '3', '141000', '山西省 临汾市 永和县');
INSERT INTO dv_area_base VALUES ('141033', '蒲县', '', '', '', '3', '141000', '山西省 临汾市 蒲县');
INSERT INTO dv_area_base VALUES ('141034', '汾西县', '', '', '', '3', '141000', '山西省 临汾市 汾西县');
INSERT INTO dv_area_base VALUES ('141081', '侯马市', '', '', '', '3', '141000', '山西省 临汾市 侯马市');
INSERT INTO dv_area_base VALUES ('141082', '霍州市', '', '', '', '3', '141000', '山西省 临汾市 霍州市');
INSERT INTO dv_area_base VALUES ('141100', '吕梁市', '吕梁', '', '', '2', '140000', '山西省 吕梁市');
INSERT INTO dv_area_base VALUES ('141102', '离石区', '', '', '', '3', '141100', '山西省 吕梁市 离石区');
INSERT INTO dv_area_base VALUES ('141121', '文水县', '', '', '', '3', '141100', '山西省 吕梁市 文水县');
INSERT INTO dv_area_base VALUES ('141122', '交城县', '', '', '', '3', '141100', '山西省 吕梁市 交城县');
INSERT INTO dv_area_base VALUES ('141123', '兴县', '', '', '', '3', '141100', '山西省 吕梁市 兴县');
INSERT INTO dv_area_base VALUES ('141124', '临县', '', '', '', '3', '141100', '山西省 吕梁市 临县');
INSERT INTO dv_area_base VALUES ('141125', '柳林县', '', '', '', '3', '141100', '山西省 吕梁市 柳林县');
INSERT INTO dv_area_base VALUES ('141126', '石楼县', '', '', '', '3', '141100', '山西省 吕梁市 石楼县');
INSERT INTO dv_area_base VALUES ('141127', '岚县', '', '', '', '3', '141100', '山西省 吕梁市 岚县');
INSERT INTO dv_area_base VALUES ('141128', '方山县', '', '', '', '3', '141100', '山西省 吕梁市 方山县');
INSERT INTO dv_area_base VALUES ('141129', '中阳县', '', '', '', '3', '141100', '山西省 吕梁市 中阳县');
INSERT INTO dv_area_base VALUES ('141130', '交口县', '', '', '', '3', '141100', '山西省 吕梁市 交口县');
INSERT INTO dv_area_base VALUES ('141181', '孝义市', '', '', '', '3', '141100', '山西省 吕梁市 孝义市');
INSERT INTO dv_area_base VALUES ('141182', '汾阳市', '', '', '', '3', '141100', '山西省 吕梁市 汾阳市');
INSERT INTO dv_area_base VALUES ('150000', '内蒙古自治区', '内蒙古', 'neimeng', '蒙', '1', '0', '内蒙古自治区');
INSERT INTO dv_area_base VALUES ('150100', '呼和浩特市', '呼和浩特', '', '', '2', '150000', '内蒙古自治区 呼和浩特市');
INSERT INTO dv_area_base VALUES ('150102', '新城区', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 新城区');
INSERT INTO dv_area_base VALUES ('150103', '回民区', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 回民区');
INSERT INTO dv_area_base VALUES ('150104', '玉泉区', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 玉泉区');
INSERT INTO dv_area_base VALUES ('150105', '赛罕区', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 赛罕区');
INSERT INTO dv_area_base VALUES ('150121', '土默特左旗', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 土默特左旗');
INSERT INTO dv_area_base VALUES ('150122', '托克托县', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 托克托县');
INSERT INTO dv_area_base VALUES ('150123', '和林格尔县', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 和林格尔县');
INSERT INTO dv_area_base VALUES ('150124', '清水河县', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 清水河县');
INSERT INTO dv_area_base VALUES ('150125', '武川县', '', '', '', '3', '150100', '内蒙古自治区 呼和浩特市 武川县');
INSERT INTO dv_area_base VALUES ('150200', '包头市', '包头', '', '', '2', '150000', '内蒙古自治区 包头市');
INSERT INTO dv_area_base VALUES ('150202', '东河区', '', '', '', '3', '150200', '内蒙古自治区 包头市 东河区');
INSERT INTO dv_area_base VALUES ('150203', '昆都仑区', '', '', '', '3', '150200', '内蒙古自治区 包头市 昆都仑区');
INSERT INTO dv_area_base VALUES ('150204', '青山区', '', '', '', '3', '150200', '内蒙古自治区 包头市 青山区');
INSERT INTO dv_area_base VALUES ('150205', '石拐区', '', '', '', '3', '150200', '内蒙古自治区 包头市 石拐区');
INSERT INTO dv_area_base VALUES ('150206', '白云鄂博矿区', '', '', '', '3', '150200', '内蒙古自治区 包头市 白云鄂博矿区');
INSERT INTO dv_area_base VALUES ('150207', '九原区', '', '', '', '3', '150200', '内蒙古自治区 包头市 九原区');
INSERT INTO dv_area_base VALUES ('150221', '土默特右旗', '', '', '', '3', '150200', '内蒙古自治区 包头市 土默特右旗');
INSERT INTO dv_area_base VALUES ('150222', '固阳县', '', '', '', '3', '150200', '内蒙古自治区 包头市 固阳县');
INSERT INTO dv_area_base VALUES ('150223', '达尔罕茂明安联合旗', '', '', '', '3', '150200', '内蒙古自治区 包头市 达尔罕茂明安联合旗');
INSERT INTO dv_area_base VALUES ('150300', '乌海市', '乌海', '', '', '2', '150000', '内蒙古自治区 乌海市');
INSERT INTO dv_area_base VALUES ('150302', '海勃湾区', '', '', '', '3', '150300', '内蒙古自治区 乌海市 海勃湾区');
INSERT INTO dv_area_base VALUES ('150303', '海南区', '', '', '', '3', '150300', '内蒙古自治区 乌海市 海南区');
INSERT INTO dv_area_base VALUES ('150304', '乌达区', '', '', '', '3', '150300', '内蒙古自治区 乌海市 乌达区');
INSERT INTO dv_area_base VALUES ('150400', '赤峰市', '赤峰', '', '', '2', '150000', '内蒙古自治区 赤峰市');
INSERT INTO dv_area_base VALUES ('150402', '红山区', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 红山区');
INSERT INTO dv_area_base VALUES ('150403', '元宝山区', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 元宝山区');
INSERT INTO dv_area_base VALUES ('150404', '松山区', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 松山区');
INSERT INTO dv_area_base VALUES ('150421', '阿鲁科尔沁旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 阿鲁科尔沁旗');
INSERT INTO dv_area_base VALUES ('150422', '巴林左旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 巴林左旗');
INSERT INTO dv_area_base VALUES ('150423', '巴林右旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 巴林右旗');
INSERT INTO dv_area_base VALUES ('150424', '林西县', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 林西县');
INSERT INTO dv_area_base VALUES ('150425', '克什克腾旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 克什克腾旗');
INSERT INTO dv_area_base VALUES ('150426', '翁牛特旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 翁牛特旗');
INSERT INTO dv_area_base VALUES ('150428', '喀喇沁旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 喀喇沁旗');
INSERT INTO dv_area_base VALUES ('150429', '宁城县', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 宁城县');
INSERT INTO dv_area_base VALUES ('150430', '敖汉旗', '', '', '', '3', '150400', '内蒙古自治区 赤峰市 敖汉旗');
INSERT INTO dv_area_base VALUES ('150500', '通辽市', '通辽', '', '', '2', '150000', '内蒙古自治区 通辽市');
INSERT INTO dv_area_base VALUES ('150502', '科尔沁区', '', '', '', '3', '150500', '内蒙古自治区 通辽市 科尔沁区');
INSERT INTO dv_area_base VALUES ('150521', '科尔沁左翼中旗', '', '', '', '3', '150500', '内蒙古自治区 通辽市 科尔沁左翼中旗');
INSERT INTO dv_area_base VALUES ('150522', '科尔沁左翼后旗', '', '', '', '3', '150500', '内蒙古自治区 通辽市 科尔沁左翼后旗');
INSERT INTO dv_area_base VALUES ('150523', '开鲁县', '', '', '', '3', '150500', '内蒙古自治区 通辽市 开鲁县');
INSERT INTO dv_area_base VALUES ('150524', '库伦旗', '', '', '', '3', '150500', '内蒙古自治区 通辽市 库伦旗');
INSERT INTO dv_area_base VALUES ('150525', '奈曼旗', '', '', '', '3', '150500', '内蒙古自治区 通辽市 奈曼旗');
INSERT INTO dv_area_base VALUES ('150526', '扎鲁特旗', '', '', '', '3', '150500', '内蒙古自治区 通辽市 扎鲁特旗');
INSERT INTO dv_area_base VALUES ('150581', '霍林郭勒市', '', '', '', '3', '150500', '内蒙古自治区 通辽市 霍林郭勒市');
INSERT INTO dv_area_base VALUES ('150600', '鄂尔多斯市', '鄂尔多斯', '', '', '2', '150000', '内蒙古自治区 鄂尔多斯市');
INSERT INTO dv_area_base VALUES ('150602', '东胜区', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 东胜区');
INSERT INTO dv_area_base VALUES ('150621', '达拉特旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 达拉特旗');
INSERT INTO dv_area_base VALUES ('150622', '准格尔旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 准格尔旗');
INSERT INTO dv_area_base VALUES ('150623', '鄂托克前旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 鄂托克前旗');
INSERT INTO dv_area_base VALUES ('150624', '鄂托克旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 鄂托克旗');
INSERT INTO dv_area_base VALUES ('150625', '杭锦旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 杭锦旗');
INSERT INTO dv_area_base VALUES ('150626', '乌审旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 乌审旗');
INSERT INTO dv_area_base VALUES ('150627', '伊金霍洛旗', '', '', '', '3', '150600', '内蒙古自治区 鄂尔多斯市 伊金霍洛旗');
INSERT INTO dv_area_base VALUES ('150700', '呼伦贝尔市', '呼伦贝尔', '', '', '2', '150000', '内蒙古自治区 呼伦贝尔市');
INSERT INTO dv_area_base VALUES ('150702', '海拉尔区', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 海拉尔区');
INSERT INTO dv_area_base VALUES ('150721', '阿荣旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 阿荣旗');
INSERT INTO dv_area_base VALUES ('150722', '莫力达瓦达斡尔族自治旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 莫力达瓦达斡尔族自治旗');
INSERT INTO dv_area_base VALUES ('150723', '鄂伦春自治旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 鄂伦春自治旗');
INSERT INTO dv_area_base VALUES ('150724', '鄂温克族自治旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 鄂温克族自治旗');
INSERT INTO dv_area_base VALUES ('150725', '陈巴尔虎旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 陈巴尔虎旗');
INSERT INTO dv_area_base VALUES ('150726', '新巴尔虎左旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 新巴尔虎左旗');
INSERT INTO dv_area_base VALUES ('150727', '新巴尔虎右旗', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 新巴尔虎右旗');
INSERT INTO dv_area_base VALUES ('150781', '满洲里市', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 满洲里市');
INSERT INTO dv_area_base VALUES ('150782', '牙克石市', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 牙克石市');
INSERT INTO dv_area_base VALUES ('150783', '扎兰屯市', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 扎兰屯市');
INSERT INTO dv_area_base VALUES ('150784', '额尔古纳市', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 额尔古纳市');
INSERT INTO dv_area_base VALUES ('150785', '根河市', '', '', '', '3', '150700', '内蒙古自治区 呼伦贝尔市 根河市');
INSERT INTO dv_area_base VALUES ('150800', '巴彦淖尔市', '巴彦淖尔', '', '', '2', '150000', '内蒙古自治区 巴彦淖尔市');
INSERT INTO dv_area_base VALUES ('150802', '临河区', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 临河区');
INSERT INTO dv_area_base VALUES ('150821', '五原县', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 五原县');
INSERT INTO dv_area_base VALUES ('150822', '磴口县', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 磴口县');
INSERT INTO dv_area_base VALUES ('150823', '乌拉特前旗', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 乌拉特前旗');
INSERT INTO dv_area_base VALUES ('150824', '乌拉特中旗', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 乌拉特中旗');
INSERT INTO dv_area_base VALUES ('150825', '乌拉特后旗', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 乌拉特后旗');
INSERT INTO dv_area_base VALUES ('150826', '杭锦后旗', '', '', '', '3', '150800', '内蒙古自治区 巴彦淖尔市 杭锦后旗');
INSERT INTO dv_area_base VALUES ('150900', '乌兰察布市', '乌兰察布', '', '', '2', '150000', '内蒙古自治区 乌兰察布市');
INSERT INTO dv_area_base VALUES ('150902', '集宁区', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 集宁区');
INSERT INTO dv_area_base VALUES ('150921', '卓资县', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 卓资县');
INSERT INTO dv_area_base VALUES ('150922', '化德县', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 化德县');
INSERT INTO dv_area_base VALUES ('150923', '商都县', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 商都县');
INSERT INTO dv_area_base VALUES ('150924', '兴和县', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 兴和县');
INSERT INTO dv_area_base VALUES ('150925', '凉城县', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 凉城县');
INSERT INTO dv_area_base VALUES ('150926', '察哈尔右翼前旗', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 察哈尔右翼前旗');
INSERT INTO dv_area_base VALUES ('150927', '察哈尔右翼中旗', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 察哈尔右翼中旗');
INSERT INTO dv_area_base VALUES ('150928', '察哈尔右翼后旗', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 察哈尔右翼后旗');
INSERT INTO dv_area_base VALUES ('150929', '四子王旗', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 四子王旗');
INSERT INTO dv_area_base VALUES ('150981', '丰镇市', '', '', '', '3', '150900', '内蒙古自治区 乌兰察布市 丰镇市');
INSERT INTO dv_area_base VALUES ('152200', '兴安盟', '兴安', '', '', '2', '150000', '内蒙古自治区 兴安盟');
INSERT INTO dv_area_base VALUES ('152201', '乌兰浩特市', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 乌兰浩特市');
INSERT INTO dv_area_base VALUES ('152202', '阿尔山市', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 阿尔山市');
INSERT INTO dv_area_base VALUES ('152221', '科尔沁右翼前旗', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 科尔沁右翼前旗');
INSERT INTO dv_area_base VALUES ('152222', '科尔沁右翼中旗', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 科尔沁右翼中旗');
INSERT INTO dv_area_base VALUES ('152223', '扎赉特旗', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 扎赉特旗');
INSERT INTO dv_area_base VALUES ('152224', '突泉县', '', '', '', '3', '152200', '内蒙古自治区 兴安盟 突泉县');
INSERT INTO dv_area_base VALUES ('152500', '锡林郭勒盟', '锡林郭勒', '', '', '2', '150000', '内蒙古自治区 锡林郭勒盟');
INSERT INTO dv_area_base VALUES ('152501', '二连浩特市', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 二连浩特市');
INSERT INTO dv_area_base VALUES ('152502', '锡林浩特市', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 锡林浩特市');
INSERT INTO dv_area_base VALUES ('152522', '阿巴嘎旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 阿巴嘎旗');
INSERT INTO dv_area_base VALUES ('152523', '苏尼特左旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 苏尼特左旗');
INSERT INTO dv_area_base VALUES ('152524', '苏尼特右旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 苏尼特右旗');
INSERT INTO dv_area_base VALUES ('152525', '东乌珠穆沁旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 东乌珠穆沁旗');
INSERT INTO dv_area_base VALUES ('152526', '西乌珠穆沁旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 西乌珠穆沁旗');
INSERT INTO dv_area_base VALUES ('152527', '太仆寺旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 太仆寺旗');
INSERT INTO dv_area_base VALUES ('152528', '镶黄旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 镶黄旗');
INSERT INTO dv_area_base VALUES ('152529', '正镶白旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 正镶白旗');
INSERT INTO dv_area_base VALUES ('152530', '正蓝旗', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 正蓝旗');
INSERT INTO dv_area_base VALUES ('152531', '多伦县', '', '', '', '3', '152500', '内蒙古自治区 锡林郭勒盟 多伦县');
INSERT INTO dv_area_base VALUES ('152900', '阿拉善盟', '阿拉善', '', '', '2', '150000', '内蒙古自治区 阿拉善盟');
INSERT INTO dv_area_base VALUES ('152921', '阿拉善左旗', '', '', '', '3', '152900', '内蒙古自治区 阿拉善盟 阿拉善左旗');
INSERT INTO dv_area_base VALUES ('152922', '阿拉善右旗', '', '', '', '3', '152900', '内蒙古自治区 阿拉善盟 阿拉善右旗');
INSERT INTO dv_area_base VALUES ('152923', '额济纳旗', '', '', '', '3', '152900', '内蒙古自治区 阿拉善盟 额济纳旗');
INSERT INTO dv_area_base VALUES ('210000', '辽宁省', '辽宁', 'liaoning', '辽', '1', '0', '辽宁省');
INSERT INTO dv_area_base VALUES ('210100', '沈阳市', '沈阳', '', '', '2', '210000', '辽宁省 沈阳市');
INSERT INTO dv_area_base VALUES ('210102', '和平区', '', '', '', '3', '210100', '辽宁省 沈阳市 和平区');
INSERT INTO dv_area_base VALUES ('210103', '沈河区', '', '', '', '3', '210100', '辽宁省 沈阳市 沈河区');
INSERT INTO dv_area_base VALUES ('210104', '大东区', '', '', '', '3', '210100', '辽宁省 沈阳市 大东区');
INSERT INTO dv_area_base VALUES ('210105', '皇姑区', '', '', '', '3', '210100', '辽宁省 沈阳市 皇姑区');
INSERT INTO dv_area_base VALUES ('210106', '铁西区', '', '', '', '3', '210100', '辽宁省 沈阳市 铁西区');
INSERT INTO dv_area_base VALUES ('210111', '苏家屯区', '', '', '', '3', '210100', '辽宁省 沈阳市 苏家屯区');
INSERT INTO dv_area_base VALUES ('210112', '东陵区', '', '', '', '3', '210100', '辽宁省 沈阳市 东陵区');
INSERT INTO dv_area_base VALUES ('210113', '沈北新区', '', '', '', '3', '210100', '辽宁省 沈阳市 沈北新区');
INSERT INTO dv_area_base VALUES ('210114', '于洪区', '', '', '', '3', '210100', '辽宁省 沈阳市 于洪区');
INSERT INTO dv_area_base VALUES ('210122', '辽中县', '', '', '', '3', '210100', '辽宁省 沈阳市 辽中县');
INSERT INTO dv_area_base VALUES ('210123', '康平县', '', '', '', '3', '210100', '辽宁省 沈阳市 康平县');
INSERT INTO dv_area_base VALUES ('210124', '法库县', '', '', '', '3', '210100', '辽宁省 沈阳市 法库县');
INSERT INTO dv_area_base VALUES ('210181', '新民市', '', '', '', '3', '210100', '辽宁省 沈阳市 新民市');
INSERT INTO dv_area_base VALUES ('210200', '大连市', '大连', '', '', '2', '210000', '辽宁省 大连市');
INSERT INTO dv_area_base VALUES ('210202', '中山区', '', '', '', '3', '210200', '辽宁省 大连市 中山区');
INSERT INTO dv_area_base VALUES ('210203', '西岗区', '', '', '', '3', '210200', '辽宁省 大连市 西岗区');
INSERT INTO dv_area_base VALUES ('210204', '沙河口区', '', '', '', '3', '210200', '辽宁省 大连市 沙河口区');
INSERT INTO dv_area_base VALUES ('210211', '甘井子区', '', '', '', '3', '210200', '辽宁省 大连市 甘井子区');
INSERT INTO dv_area_base VALUES ('210212', '旅顺口区', '', '', '', '3', '210200', '辽宁省 大连市 旅顺口区');
INSERT INTO dv_area_base VALUES ('210213', '金州区', '', '', '', '3', '210200', '辽宁省 大连市 金州区');
INSERT INTO dv_area_base VALUES ('210224', '长海县', '', '', '', '3', '210200', '辽宁省 大连市 长海县');
INSERT INTO dv_area_base VALUES ('210281', '瓦房店市', '', '', '', '3', '210200', '辽宁省 大连市 瓦房店市');
INSERT INTO dv_area_base VALUES ('210282', '普兰店市', '', '', '', '3', '210200', '辽宁省 大连市 普兰店市');
INSERT INTO dv_area_base VALUES ('210283', '庄河市', '', '', '', '3', '210200', '辽宁省 大连市 庄河市');
INSERT INTO dv_area_base VALUES ('210300', '鞍山市', '鞍山', '', '', '2', '210000', '辽宁省 鞍山市');
INSERT INTO dv_area_base VALUES ('210302', '铁东区', '', '', '', '3', '210300', '辽宁省 鞍山市 铁东区');
INSERT INTO dv_area_base VALUES ('210303', '铁西区', '', '', '', '3', '210300', '辽宁省 鞍山市 铁西区');
INSERT INTO dv_area_base VALUES ('210304', '立山区', '', '', '', '3', '210300', '辽宁省 鞍山市 立山区');
INSERT INTO dv_area_base VALUES ('210311', '千山区', '', '', '', '3', '210300', '辽宁省 鞍山市 千山区');
INSERT INTO dv_area_base VALUES ('210321', '台安县', '', '', '', '3', '210300', '辽宁省 鞍山市 台安县');
INSERT INTO dv_area_base VALUES ('210323', '岫岩满族自治县', '', '', '', '3', '210300', '辽宁省 鞍山市 岫岩满族自治县');
INSERT INTO dv_area_base VALUES ('210381', '海城市', '', '', '', '3', '210300', '辽宁省 鞍山市 海城市');
INSERT INTO dv_area_base VALUES ('210400', '抚顺市', '抚顺', '', '', '2', '210000', '辽宁省 抚顺市');
INSERT INTO dv_area_base VALUES ('210402', '新抚区', '', '', '', '3', '210400', '辽宁省 抚顺市 新抚区');
INSERT INTO dv_area_base VALUES ('210403', '东洲区', '', '', '', '3', '210400', '辽宁省 抚顺市 东洲区');
INSERT INTO dv_area_base VALUES ('210404', '望花区', '', '', '', '3', '210400', '辽宁省 抚顺市 望花区');
INSERT INTO dv_area_base VALUES ('210411', '顺城区', '', '', '', '3', '210400', '辽宁省 抚顺市 顺城区');
INSERT INTO dv_area_base VALUES ('210421', '抚顺县', '', '', '', '3', '210400', '辽宁省 抚顺市 抚顺县');
INSERT INTO dv_area_base VALUES ('210422', '新宾满族自治县', '', '', '', '3', '210400', '辽宁省 抚顺市 新宾满族自治县');
INSERT INTO dv_area_base VALUES ('210423', '清原满族自治县', '', '', '', '3', '210400', '辽宁省 抚顺市 清原满族自治县');
INSERT INTO dv_area_base VALUES ('210500', '本溪市', '本溪', '', '', '2', '210000', '辽宁省 本溪市');
INSERT INTO dv_area_base VALUES ('210502', '平山区', '', '', '', '3', '210500', '辽宁省 本溪市 平山区');
INSERT INTO dv_area_base VALUES ('210503', '溪湖区', '', '', '', '3', '210500', '辽宁省 本溪市 溪湖区');
INSERT INTO dv_area_base VALUES ('210504', '明山区', '', '', '', '3', '210500', '辽宁省 本溪市 明山区');
INSERT INTO dv_area_base VALUES ('210505', '南芬区', '', '', '', '3', '210500', '辽宁省 本溪市 南芬区');
INSERT INTO dv_area_base VALUES ('210521', '本溪满族自治县', '', '', '', '3', '210500', '辽宁省 本溪市 本溪满族自治县');
INSERT INTO dv_area_base VALUES ('210522', '桓仁满族自治县', '', '', '', '3', '210500', '辽宁省 本溪市 桓仁满族自治县');
INSERT INTO dv_area_base VALUES ('210600', '丹东市', '丹东', '', '', '2', '210000', '辽宁省 丹东市');
INSERT INTO dv_area_base VALUES ('210602', '元宝区', '', '', '', '3', '210600', '辽宁省 丹东市 元宝区');
INSERT INTO dv_area_base VALUES ('210603', '振兴区', '', '', '', '3', '210600', '辽宁省 丹东市 振兴区');
INSERT INTO dv_area_base VALUES ('210604', '振安区', '', '', '', '3', '210600', '辽宁省 丹东市 振安区');
INSERT INTO dv_area_base VALUES ('210624', '宽甸满族自治县', '', '', '', '3', '210600', '辽宁省 丹东市 宽甸满族自治县');
INSERT INTO dv_area_base VALUES ('210681', '东港市', '', '', '', '3', '210600', '辽宁省 丹东市 东港市');
INSERT INTO dv_area_base VALUES ('210682', '凤城市', '', '', '', '3', '210600', '辽宁省 丹东市 凤城市');
INSERT INTO dv_area_base VALUES ('210700', '锦州市', '锦州', '', '', '2', '210000', '辽宁省 锦州市');
INSERT INTO dv_area_base VALUES ('210702', '古塔区', '', '', '', '3', '210700', '辽宁省 锦州市 古塔区');
INSERT INTO dv_area_base VALUES ('210703', '凌河区', '', '', '', '3', '210700', '辽宁省 锦州市 凌河区');
INSERT INTO dv_area_base VALUES ('210711', '太和区', '', '', '', '3', '210700', '辽宁省 锦州市 太和区');
INSERT INTO dv_area_base VALUES ('210726', '黑山县', '', '', '', '3', '210700', '辽宁省 锦州市 黑山县');
INSERT INTO dv_area_base VALUES ('210727', '义县', '', '', '', '3', '210700', '辽宁省 锦州市 义县');
INSERT INTO dv_area_base VALUES ('210781', '凌海市', '', '', '', '3', '210700', '辽宁省 锦州市 凌海市');
INSERT INTO dv_area_base VALUES ('210782', '北镇市', '', '', '', '3', '210700', '辽宁省 锦州市 北镇市');
INSERT INTO dv_area_base VALUES ('210800', '营口市', '营口', '', '', '2', '210000', '辽宁省 营口市');
INSERT INTO dv_area_base VALUES ('210802', '站前区', '', '', '', '3', '210800', '辽宁省 营口市 站前区');
INSERT INTO dv_area_base VALUES ('210803', '西市区', '', '', '', '3', '210800', '辽宁省 营口市 西市区');
INSERT INTO dv_area_base VALUES ('210804', '鲅鱼圈区', '', '', '', '3', '210800', '辽宁省 营口市 鲅鱼圈区');
INSERT INTO dv_area_base VALUES ('210811', '老边区', '', '', '', '3', '210800', '辽宁省 营口市 老边区');
INSERT INTO dv_area_base VALUES ('210881', '盖州市', '', '', '', '3', '210800', '辽宁省 营口市 盖州市');
INSERT INTO dv_area_base VALUES ('210882', '大石桥市', '', '', '', '3', '210800', '辽宁省 营口市 大石桥市');
INSERT INTO dv_area_base VALUES ('210900', '阜新市', '阜新', '', '', '2', '210000', '辽宁省 阜新市');
INSERT INTO dv_area_base VALUES ('210902', '海州区', '', '', '', '3', '210900', '辽宁省 阜新市 海州区');
INSERT INTO dv_area_base VALUES ('210903', '新邱区', '', '', '', '3', '210900', '辽宁省 阜新市 新邱区');
INSERT INTO dv_area_base VALUES ('210904', '太平区', '', '', '', '3', '210900', '辽宁省 阜新市 太平区');
INSERT INTO dv_area_base VALUES ('210905', '清河门区', '', '', '', '3', '210900', '辽宁省 阜新市 清河门区');
INSERT INTO dv_area_base VALUES ('210911', '细河区', '', '', '', '3', '210900', '辽宁省 阜新市 细河区');
INSERT INTO dv_area_base VALUES ('210921', '阜新蒙古族自治县', '', '', '', '3', '210900', '辽宁省 阜新市 阜新蒙古族自治县');
INSERT INTO dv_area_base VALUES ('210922', '彰武县', '', '', '', '3', '210900', '辽宁省 阜新市 彰武县');
INSERT INTO dv_area_base VALUES ('211000', '辽阳市', '辽阳', '', '', '2', '210000', '辽宁省 辽阳市');
INSERT INTO dv_area_base VALUES ('211002', '白塔区', '', '', '', '3', '211000', '辽宁省 辽阳市 白塔区');
INSERT INTO dv_area_base VALUES ('211003', '文圣区', '', '', '', '3', '211000', '辽宁省 辽阳市 文圣区');
INSERT INTO dv_area_base VALUES ('211004', '宏伟区', '', '', '', '3', '211000', '辽宁省 辽阳市 宏伟区');
INSERT INTO dv_area_base VALUES ('211005', '弓长岭区', '', '', '', '3', '211000', '辽宁省 辽阳市 弓长岭区');
INSERT INTO dv_area_base VALUES ('211011', '太子河区', '', '', '', '3', '211000', '辽宁省 辽阳市 太子河区');
INSERT INTO dv_area_base VALUES ('211021', '辽阳县', '', '', '', '3', '211000', '辽宁省 辽阳市 辽阳县');
INSERT INTO dv_area_base VALUES ('211081', '灯塔市', '', '', '', '3', '211000', '辽宁省 辽阳市 灯塔市');
INSERT INTO dv_area_base VALUES ('211100', '盘锦市', '盘锦', '', '', '2', '210000', '辽宁省 盘锦市');
INSERT INTO dv_area_base VALUES ('211102', '双台子区', '', '', '', '3', '211100', '辽宁省 盘锦市 双台子区');
INSERT INTO dv_area_base VALUES ('211103', '兴隆台区', '', '', '', '3', '211100', '辽宁省 盘锦市 兴隆台区');
INSERT INTO dv_area_base VALUES ('211121', '大洼县', '', '', '', '3', '211100', '辽宁省 盘锦市 大洼县');
INSERT INTO dv_area_base VALUES ('211122', '盘山县', '', '', '', '3', '211100', '辽宁省 盘锦市 盘山县');
INSERT INTO dv_area_base VALUES ('211200', '铁岭市', '铁岭', '', '', '2', '210000', '辽宁省 铁岭市');
INSERT INTO dv_area_base VALUES ('211202', '银州区', '', '', '', '3', '211200', '辽宁省 铁岭市 银州区');
INSERT INTO dv_area_base VALUES ('211204', '清河区', '', '', '', '3', '211200', '辽宁省 铁岭市 清河区');
INSERT INTO dv_area_base VALUES ('211221', '铁岭县', '', '', '', '3', '211200', '辽宁省 铁岭市 铁岭县');
INSERT INTO dv_area_base VALUES ('211223', '西丰县', '', '', '', '3', '211200', '辽宁省 铁岭市 西丰县');
INSERT INTO dv_area_base VALUES ('211224', '昌图县', '', '', '', '3', '211200', '辽宁省 铁岭市 昌图县');
INSERT INTO dv_area_base VALUES ('211281', '调兵山市', '', '', '', '3', '211200', '辽宁省 铁岭市 调兵山市');
INSERT INTO dv_area_base VALUES ('211282', '开原市', '', '', '', '3', '211200', '辽宁省 铁岭市 开原市');
INSERT INTO dv_area_base VALUES ('211300', '朝阳市', '朝阳', '', '', '2', '210000', '辽宁省 朝阳市');
INSERT INTO dv_area_base VALUES ('211302', '双塔区', '', '', '', '3', '211300', '辽宁省 朝阳市 双塔区');
INSERT INTO dv_area_base VALUES ('211303', '龙城区', '', '', '', '3', '211300', '辽宁省 朝阳市 龙城区');
INSERT INTO dv_area_base VALUES ('211321', '朝阳县', '', '', '', '3', '211300', '辽宁省 朝阳市 朝阳县');
INSERT INTO dv_area_base VALUES ('211322', '建平县', '', '', '', '3', '211300', '辽宁省 朝阳市 建平县');
INSERT INTO dv_area_base VALUES ('211324', '喀喇沁左翼蒙古族自治县', '', '', '', '3', '211300', '辽宁省 朝阳市 喀喇沁左翼蒙古族自治县');
INSERT INTO dv_area_base VALUES ('211381', '北票市', '', '', '', '3', '211300', '辽宁省 朝阳市 北票市');
INSERT INTO dv_area_base VALUES ('211382', '凌源市', '', '', '', '3', '211300', '辽宁省 朝阳市 凌源市');
INSERT INTO dv_area_base VALUES ('211400', '葫芦岛市', '葫芦岛', '', '', '2', '210000', '辽宁省 葫芦岛市');
INSERT INTO dv_area_base VALUES ('211402', '连山区', '', '', '', '3', '211400', '辽宁省 葫芦岛市 连山区');
INSERT INTO dv_area_base VALUES ('211403', '龙港区', '', '', '', '3', '211400', '辽宁省 葫芦岛市 龙港区');
INSERT INTO dv_area_base VALUES ('211404', '南票区', '', '', '', '3', '211400', '辽宁省 葫芦岛市 南票区');
INSERT INTO dv_area_base VALUES ('211421', '绥中县', '', '', '', '3', '211400', '辽宁省 葫芦岛市 绥中县');
INSERT INTO dv_area_base VALUES ('211422', '建昌县', '', '', '', '3', '211400', '辽宁省 葫芦岛市 建昌县');
INSERT INTO dv_area_base VALUES ('211481', '兴城市', '', '', '', '3', '211400', '辽宁省 葫芦岛市 兴城市');
INSERT INTO dv_area_base VALUES ('220000', '吉林省', '吉林', 'jilin', '吉', '1', '0', '吉林省');
INSERT INTO dv_area_base VALUES ('220100', '长春市', '长春', '', '', '2', '220000', '吉林省 长春市');
INSERT INTO dv_area_base VALUES ('220102', '南关区', '', '', '', '3', '220100', '吉林省 长春市 南关区');
INSERT INTO dv_area_base VALUES ('220103', '宽城区', '', '', '', '3', '220100', '吉林省 长春市 宽城区');
INSERT INTO dv_area_base VALUES ('220104', '朝阳区', '', '', '', '3', '220100', '吉林省 长春市 朝阳区');
INSERT INTO dv_area_base VALUES ('220105', '二道区', '', '', '', '3', '220100', '吉林省 长春市 二道区');
INSERT INTO dv_area_base VALUES ('220106', '绿园区', '', '', '', '3', '220100', '吉林省 长春市 绿园区');
INSERT INTO dv_area_base VALUES ('220112', '双阳区', '', '', '', '3', '220100', '吉林省 长春市 双阳区');
INSERT INTO dv_area_base VALUES ('220122', '农安县', '', '', '', '3', '220100', '吉林省 长春市 农安县');
INSERT INTO dv_area_base VALUES ('220181', '九台市', '', '', '', '3', '220100', '吉林省 长春市 九台市');
INSERT INTO dv_area_base VALUES ('220182', '榆树市', '', '', '', '3', '220100', '吉林省 长春市 榆树市');
INSERT INTO dv_area_base VALUES ('220183', '德惠市', '', '', '', '3', '220100', '吉林省 长春市 德惠市');
INSERT INTO dv_area_base VALUES ('220200', '吉林市', '吉林', '', '', '2', '220000', '吉林省 吉林市');
INSERT INTO dv_area_base VALUES ('220202', '昌邑区', '', '', '', '3', '220200', '吉林省 吉林市 昌邑区');
INSERT INTO dv_area_base VALUES ('220203', '龙潭区', '', '', '', '3', '220200', '吉林省 吉林市 龙潭区');
INSERT INTO dv_area_base VALUES ('220204', '船营区', '', '', '', '3', '220200', '吉林省 吉林市 船营区');
INSERT INTO dv_area_base VALUES ('220211', '丰满区', '', '', '', '3', '220200', '吉林省 吉林市 丰满区');
INSERT INTO dv_area_base VALUES ('220221', '永吉县', '', '', '', '3', '220200', '吉林省 吉林市 永吉县');
INSERT INTO dv_area_base VALUES ('220281', '蛟河市', '', '', '', '3', '220200', '吉林省 吉林市 蛟河市');
INSERT INTO dv_area_base VALUES ('220282', '桦甸市', '', '', '', '3', '220200', '吉林省 吉林市 桦甸市');
INSERT INTO dv_area_base VALUES ('220283', '舒兰市', '', '', '', '3', '220200', '吉林省 吉林市 舒兰市');
INSERT INTO dv_area_base VALUES ('220284', '磐石市', '', '', '', '3', '220200', '吉林省 吉林市 磐石市');
INSERT INTO dv_area_base VALUES ('220300', '四平市', '四平', '', '', '2', '220000', '吉林省 四平市');
INSERT INTO dv_area_base VALUES ('220302', '铁西区', '', '', '', '3', '220300', '吉林省 四平市 铁西区');
INSERT INTO dv_area_base VALUES ('220303', '铁东区', '', '', '', '3', '220300', '吉林省 四平市 铁东区');
INSERT INTO dv_area_base VALUES ('220322', '梨树县', '', '', '', '3', '220300', '吉林省 四平市 梨树县');
INSERT INTO dv_area_base VALUES ('220323', '伊通满族自治县', '', '', '', '3', '220300', '吉林省 四平市 伊通满族自治县');
INSERT INTO dv_area_base VALUES ('220381', '公主岭市', '', '', '', '3', '220300', '吉林省 四平市 公主岭市');
INSERT INTO dv_area_base VALUES ('220382', '双辽市', '', '', '', '3', '220300', '吉林省 四平市 双辽市');
INSERT INTO dv_area_base VALUES ('220400', '辽源市', '辽源', '', '', '2', '220000', '吉林省 辽源市');
INSERT INTO dv_area_base VALUES ('220402', '龙山区', '', '', '', '3', '220400', '吉林省 辽源市 龙山区');
INSERT INTO dv_area_base VALUES ('220403', '西安区', '', '', '', '3', '220400', '吉林省 辽源市 西安区');
INSERT INTO dv_area_base VALUES ('220421', '东丰县', '', '', '', '3', '220400', '吉林省 辽源市 东丰县');
INSERT INTO dv_area_base VALUES ('220422', '东辽县', '', '', '', '3', '220400', '吉林省 辽源市 东辽县');
INSERT INTO dv_area_base VALUES ('220500', '通化市', '通化', '', '', '2', '220000', '吉林省 通化市');
INSERT INTO dv_area_base VALUES ('220502', '东昌区', '', '', '', '3', '220500', '吉林省 通化市 东昌区');
INSERT INTO dv_area_base VALUES ('220503', '二道江区', '', '', '', '3', '220500', '吉林省 通化市 二道江区');
INSERT INTO dv_area_base VALUES ('220521', '通化县', '', '', '', '3', '220500', '吉林省 通化市 通化县');
INSERT INTO dv_area_base VALUES ('220523', '辉南县', '', '', '', '3', '220500', '吉林省 通化市 辉南县');
INSERT INTO dv_area_base VALUES ('220524', '柳河县', '', '', '', '3', '220500', '吉林省 通化市 柳河县');
INSERT INTO dv_area_base VALUES ('220581', '梅河口市', '', '', '', '3', '220500', '吉林省 通化市 梅河口市');
INSERT INTO dv_area_base VALUES ('220582', '集安市', '', '', '', '3', '220500', '吉林省 通化市 集安市');
INSERT INTO dv_area_base VALUES ('220600', '白山市', '白山', '', '', '2', '220000', '吉林省 白山市');
INSERT INTO dv_area_base VALUES ('220602', '浑江区', '', '', '', '3', '220600', '吉林省 白山市 浑江区');
INSERT INTO dv_area_base VALUES ('220605', '江源区', '', '', '', '3', '220600', '吉林省 白山市 江源区');
INSERT INTO dv_area_base VALUES ('220621', '抚松县', '', '', '', '3', '220600', '吉林省 白山市 抚松县');
INSERT INTO dv_area_base VALUES ('220622', '靖宇县', '', '', '', '3', '220600', '吉林省 白山市 靖宇县');
INSERT INTO dv_area_base VALUES ('220623', '长白朝鲜族自治县', '', '', '', '3', '220600', '吉林省 白山市 长白朝鲜族自治县');
INSERT INTO dv_area_base VALUES ('220681', '临江市', '', '', '', '3', '220600', '吉林省 白山市 临江市');
INSERT INTO dv_area_base VALUES ('220700', '松原市', '松原', '', '', '2', '220000', '吉林省 松原市');
INSERT INTO dv_area_base VALUES ('220702', '宁江区', '', '', '', '3', '220700', '吉林省 松原市 宁江区');
INSERT INTO dv_area_base VALUES ('220721', '前郭尔罗斯蒙古族自治县', '', '', '', '3', '220700', '吉林省 松原市 前郭尔罗斯蒙古族自治县');
INSERT INTO dv_area_base VALUES ('220722', '长岭县', '', '', '', '3', '220700', '吉林省 松原市 长岭县');
INSERT INTO dv_area_base VALUES ('220723', '乾安县', '', '', '', '3', '220700', '吉林省 松原市 乾安县');
INSERT INTO dv_area_base VALUES ('220724', '扶余县', '', '', '', '3', '220700', '吉林省 松原市 扶余县');
INSERT INTO dv_area_base VALUES ('220800', '白城市', '白城', '', '', '2', '220000', '吉林省 白城市');
INSERT INTO dv_area_base VALUES ('220802', '洮北区', '', '', '', '3', '220800', '吉林省 白城市 洮北区');
INSERT INTO dv_area_base VALUES ('220821', '镇赉县', '', '', '', '3', '220800', '吉林省 白城市 镇赉县');
INSERT INTO dv_area_base VALUES ('220822', '通榆县', '', '', '', '3', '220800', '吉林省 白城市 通榆县');
INSERT INTO dv_area_base VALUES ('220881', '洮南市', '', '', '', '3', '220800', '吉林省 白城市 洮南市');
INSERT INTO dv_area_base VALUES ('220882', '大安市', '', '', '', '3', '220800', '吉林省 白城市 大安市');
INSERT INTO dv_area_base VALUES ('222400', '延边朝鲜族自治州', '延边', '', '', '2', '220000', '吉林省 延边朝鲜族自治州');
INSERT INTO dv_area_base VALUES ('222401', '延吉市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 延吉市');
INSERT INTO dv_area_base VALUES ('222402', '图们市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 图们市');
INSERT INTO dv_area_base VALUES ('222403', '敦化市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 敦化市');
INSERT INTO dv_area_base VALUES ('222404', '珲春市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 珲春市');
INSERT INTO dv_area_base VALUES ('222405', '龙井市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 龙井市');
INSERT INTO dv_area_base VALUES ('222406', '和龙市', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 和龙市');
INSERT INTO dv_area_base VALUES ('222424', '汪清县', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 汪清县');
INSERT INTO dv_area_base VALUES ('222426', '安图县', '', '', '', '3', '222400', '吉林省 延边朝鲜族自治州 安图县');
INSERT INTO dv_area_base VALUES ('230000', '黑龙江省', '黑龙江', 'heilongjiang', '黑', '1', '0', '黑龙江省');
INSERT INTO dv_area_base VALUES ('230100', '哈尔滨市', '哈尔滨', '', '', '2', '230000', '黑龙江省 哈尔滨市');
INSERT INTO dv_area_base VALUES ('230102', '道里区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 道里区');
INSERT INTO dv_area_base VALUES ('230103', '南岗区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 南岗区');
INSERT INTO dv_area_base VALUES ('230104', '道外区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 道外区');
INSERT INTO dv_area_base VALUES ('230108', '平房区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 平房区');
INSERT INTO dv_area_base VALUES ('230109', '松北区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 松北区');
INSERT INTO dv_area_base VALUES ('230110', '香坊区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 香坊区');
INSERT INTO dv_area_base VALUES ('230111', '呼兰区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 呼兰区');
INSERT INTO dv_area_base VALUES ('230112', '阿城区', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 阿城区');
INSERT INTO dv_area_base VALUES ('230123', '依兰县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 依兰县');
INSERT INTO dv_area_base VALUES ('230124', '方正县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 方正县');
INSERT INTO dv_area_base VALUES ('230125', '宾县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 宾县');
INSERT INTO dv_area_base VALUES ('230126', '巴彦县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 巴彦县');
INSERT INTO dv_area_base VALUES ('230127', '木兰县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 木兰县');
INSERT INTO dv_area_base VALUES ('230128', '通河县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 通河县');
INSERT INTO dv_area_base VALUES ('230129', '延寿县', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 延寿县');
INSERT INTO dv_area_base VALUES ('230182', '双城市', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 双城市');
INSERT INTO dv_area_base VALUES ('230183', '尚志市', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 尚志市');
INSERT INTO dv_area_base VALUES ('230184', '五常市', '', '', '', '3', '230100', '黑龙江省 哈尔滨市 五常市');
INSERT INTO dv_area_base VALUES ('230200', '齐齐哈尔市', '齐齐哈尔', '', '', '2', '230000', '黑龙江省 齐齐哈尔市');
INSERT INTO dv_area_base VALUES ('230202', '龙沙区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 龙沙区');
INSERT INTO dv_area_base VALUES ('230203', '建华区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 建华区');
INSERT INTO dv_area_base VALUES ('230204', '铁锋区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 铁锋区');
INSERT INTO dv_area_base VALUES ('230205', '昂昂溪区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 昂昂溪区');
INSERT INTO dv_area_base VALUES ('230206', '富拉尔基区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 富拉尔基区');
INSERT INTO dv_area_base VALUES ('230207', '碾子山区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 碾子山区');
INSERT INTO dv_area_base VALUES ('230208', '梅里斯达斡尔族区', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 梅里斯达斡尔族区');
INSERT INTO dv_area_base VALUES ('230221', '龙江县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 龙江县');
INSERT INTO dv_area_base VALUES ('230223', '依安县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 依安县');
INSERT INTO dv_area_base VALUES ('230224', '泰来县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 泰来县');
INSERT INTO dv_area_base VALUES ('230225', '甘南县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 甘南县');
INSERT INTO dv_area_base VALUES ('230227', '富裕县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 富裕县');
INSERT INTO dv_area_base VALUES ('230229', '克山县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 克山县');
INSERT INTO dv_area_base VALUES ('230230', '克东县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 克东县');
INSERT INTO dv_area_base VALUES ('230231', '拜泉县', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 拜泉县');
INSERT INTO dv_area_base VALUES ('230281', '讷河市', '', '', '', '3', '230200', '黑龙江省 齐齐哈尔市 讷河市');
INSERT INTO dv_area_base VALUES ('230300', '鸡西市', '鸡西', '', '', '2', '230000', '黑龙江省 鸡西市');
INSERT INTO dv_area_base VALUES ('230302', '鸡冠区', '', '', '', '3', '230300', '黑龙江省 鸡西市 鸡冠区');
INSERT INTO dv_area_base VALUES ('230303', '恒山区', '', '', '', '3', '230300', '黑龙江省 鸡西市 恒山区');
INSERT INTO dv_area_base VALUES ('230304', '滴道区', '', '', '', '3', '230300', '黑龙江省 鸡西市 滴道区');
INSERT INTO dv_area_base VALUES ('230305', '梨树区', '', '', '', '3', '230300', '黑龙江省 鸡西市 梨树区');
INSERT INTO dv_area_base VALUES ('230306', '城子河区', '', '', '', '3', '230300', '黑龙江省 鸡西市 城子河区');
INSERT INTO dv_area_base VALUES ('230307', '麻山区', '', '', '', '3', '230300', '黑龙江省 鸡西市 麻山区');
INSERT INTO dv_area_base VALUES ('230321', '鸡东县', '', '', '', '3', '230300', '黑龙江省 鸡西市 鸡东县');
INSERT INTO dv_area_base VALUES ('230381', '虎林市', '', '', '', '3', '230300', '黑龙江省 鸡西市 虎林市');
INSERT INTO dv_area_base VALUES ('230382', '密山市', '', '', '', '3', '230300', '黑龙江省 鸡西市 密山市');
INSERT INTO dv_area_base VALUES ('230400', '鹤岗市', '鹤岗', '', '', '2', '230000', '黑龙江省 鹤岗市');
INSERT INTO dv_area_base VALUES ('230402', '向阳区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 向阳区');
INSERT INTO dv_area_base VALUES ('230403', '工农区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 工农区');
INSERT INTO dv_area_base VALUES ('230404', '南山区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 南山区');
INSERT INTO dv_area_base VALUES ('230405', '兴安区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 兴安区');
INSERT INTO dv_area_base VALUES ('230406', '东山区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 东山区');
INSERT INTO dv_area_base VALUES ('230407', '兴山区', '', '', '', '3', '230400', '黑龙江省 鹤岗市 兴山区');
INSERT INTO dv_area_base VALUES ('230421', '萝北县', '', '', '', '3', '230400', '黑龙江省 鹤岗市 萝北县');
INSERT INTO dv_area_base VALUES ('230422', '绥滨县', '', '', '', '3', '230400', '黑龙江省 鹤岗市 绥滨县');
INSERT INTO dv_area_base VALUES ('230500', '双鸭山市', '双鸭山', '', '', '2', '230000', '黑龙江省 双鸭山市');
INSERT INTO dv_area_base VALUES ('230502', '尖山区', '', '', '', '3', '230500', '黑龙江省 双鸭山市 尖山区');
INSERT INTO dv_area_base VALUES ('230503', '岭东区', '', '', '', '3', '230500', '黑龙江省 双鸭山市 岭东区');
INSERT INTO dv_area_base VALUES ('230505', '四方台区', '', '', '', '3', '230500', '黑龙江省 双鸭山市 四方台区');
INSERT INTO dv_area_base VALUES ('230506', '宝山区', '', '', '', '3', '230500', '黑龙江省 双鸭山市 宝山区');
INSERT INTO dv_area_base VALUES ('230521', '集贤县', '', '', '', '3', '230500', '黑龙江省 双鸭山市 集贤县');
INSERT INTO dv_area_base VALUES ('230522', '友谊县', '', '', '', '3', '230500', '黑龙江省 双鸭山市 友谊县');
INSERT INTO dv_area_base VALUES ('230523', '宝清县', '', '', '', '3', '230500', '黑龙江省 双鸭山市 宝清县');
INSERT INTO dv_area_base VALUES ('230524', '饶河县', '', '', '', '3', '230500', '黑龙江省 双鸭山市 饶河县');
INSERT INTO dv_area_base VALUES ('230600', '大庆市', '大庆', '', '', '2', '230000', '黑龙江省 大庆市');
INSERT INTO dv_area_base VALUES ('230602', '萨尔图区', '', '', '', '3', '230600', '黑龙江省 大庆市 萨尔图区');
INSERT INTO dv_area_base VALUES ('230603', '龙凤区', '', '', '', '3', '230600', '黑龙江省 大庆市 龙凤区');
INSERT INTO dv_area_base VALUES ('230604', '让胡路区', '', '', '', '3', '230600', '黑龙江省 大庆市 让胡路区');
INSERT INTO dv_area_base VALUES ('230605', '红岗区', '', '', '', '3', '230600', '黑龙江省 大庆市 红岗区');
INSERT INTO dv_area_base VALUES ('230606', '大同区', '', '', '', '3', '230600', '黑龙江省 大庆市 大同区');
INSERT INTO dv_area_base VALUES ('230621', '肇州县', '', '', '', '3', '230600', '黑龙江省 大庆市 肇州县');
INSERT INTO dv_area_base VALUES ('230622', '肇源县', '', '', '', '3', '230600', '黑龙江省 大庆市 肇源县');
INSERT INTO dv_area_base VALUES ('230623', '林甸县', '', '', '', '3', '230600', '黑龙江省 大庆市 林甸县');
INSERT INTO dv_area_base VALUES ('230624', '杜尔伯特蒙古族自治县', '', '', '', '3', '230600', '黑龙江省 大庆市 杜尔伯特蒙古族自治县');
INSERT INTO dv_area_base VALUES ('230700', '伊春市', '伊春', '', '', '2', '230000', '黑龙江省 伊春市');
INSERT INTO dv_area_base VALUES ('230702', '伊春区', '', '', '', '3', '230700', '黑龙江省 伊春市 伊春区');
INSERT INTO dv_area_base VALUES ('230703', '南岔区', '', '', '', '3', '230700', '黑龙江省 伊春市 南岔区');
INSERT INTO dv_area_base VALUES ('230704', '友好区', '', '', '', '3', '230700', '黑龙江省 伊春市 友好区');
INSERT INTO dv_area_base VALUES ('230705', '西林区', '', '', '', '3', '230700', '黑龙江省 伊春市 西林区');
INSERT INTO dv_area_base VALUES ('230706', '翠峦区', '', '', '', '3', '230700', '黑龙江省 伊春市 翠峦区');
INSERT INTO dv_area_base VALUES ('230707', '新青区', '', '', '', '3', '230700', '黑龙江省 伊春市 新青区');
INSERT INTO dv_area_base VALUES ('230708', '美溪区', '', '', '', '3', '230700', '黑龙江省 伊春市 美溪区');
INSERT INTO dv_area_base VALUES ('230709', '金山屯区', '', '', '', '3', '230700', '黑龙江省 伊春市 金山屯区');
INSERT INTO dv_area_base VALUES ('230710', '五营区', '', '', '', '3', '230700', '黑龙江省 伊春市 五营区');
INSERT INTO dv_area_base VALUES ('230711', '乌马河区', '', '', '', '3', '230700', '黑龙江省 伊春市 乌马河区');
INSERT INTO dv_area_base VALUES ('230712', '汤旺河区', '', '', '', '3', '230700', '黑龙江省 伊春市 汤旺河区');
INSERT INTO dv_area_base VALUES ('230713', '带岭区', '', '', '', '3', '230700', '黑龙江省 伊春市 带岭区');
INSERT INTO dv_area_base VALUES ('230714', '乌伊岭区', '', '', '', '3', '230700', '黑龙江省 伊春市 乌伊岭区');
INSERT INTO dv_area_base VALUES ('230715', '红星区', '', '', '', '3', '230700', '黑龙江省 伊春市 红星区');
INSERT INTO dv_area_base VALUES ('230716', '上甘岭区', '', '', '', '3', '230700', '黑龙江省 伊春市 上甘岭区');
INSERT INTO dv_area_base VALUES ('230722', '嘉荫县', '', '', '', '3', '230700', '黑龙江省 伊春市 嘉荫县');
INSERT INTO dv_area_base VALUES ('230781', '铁力市', '', '', '', '3', '230700', '黑龙江省 伊春市 铁力市');
INSERT INTO dv_area_base VALUES ('230800', '佳木斯市', '佳木斯', '', '', '2', '230000', '黑龙江省 佳木斯市');
INSERT INTO dv_area_base VALUES ('230803', '向阳区', '', '', '', '3', '230800', '黑龙江省 佳木斯市 向阳区');
INSERT INTO dv_area_base VALUES ('230804', '前进区', '', '', '', '3', '230800', '黑龙江省 佳木斯市 前进区');
INSERT INTO dv_area_base VALUES ('230805', '东风区', '', '', '', '3', '230800', '黑龙江省 佳木斯市 东风区');
INSERT INTO dv_area_base VALUES ('230811', '郊区', '', '', '', '3', '230800', '黑龙江省 佳木斯市 郊区');
INSERT INTO dv_area_base VALUES ('230822', '桦南县', '', '', '', '3', '230800', '黑龙江省 佳木斯市 桦南县');
INSERT INTO dv_area_base VALUES ('230826', '桦川县', '', '', '', '3', '230800', '黑龙江省 佳木斯市 桦川县');
INSERT INTO dv_area_base VALUES ('230828', '汤原县', '', '', '', '3', '230800', '黑龙江省 佳木斯市 汤原县');
INSERT INTO dv_area_base VALUES ('230833', '抚远县', '', '', '', '3', '230800', '黑龙江省 佳木斯市 抚远县');
INSERT INTO dv_area_base VALUES ('230881', '同江市', '', '', '', '3', '230800', '黑龙江省 佳木斯市 同江市');
INSERT INTO dv_area_base VALUES ('230882', '富锦市', '', '', '', '3', '230800', '黑龙江省 佳木斯市 富锦市');
INSERT INTO dv_area_base VALUES ('230900', '七台河市', '七台河', '', '', '2', '230000', '黑龙江省 七台河市');
INSERT INTO dv_area_base VALUES ('230902', '新兴区', '', '', '', '3', '230900', '黑龙江省 七台河市 新兴区');
INSERT INTO dv_area_base VALUES ('230903', '桃山区', '', '', '', '3', '230900', '黑龙江省 七台河市 桃山区');
INSERT INTO dv_area_base VALUES ('230904', '茄子河区', '', '', '', '3', '230900', '黑龙江省 七台河市 茄子河区');
INSERT INTO dv_area_base VALUES ('230921', '勃利县', '', '', '', '3', '230900', '黑龙江省 七台河市 勃利县');
INSERT INTO dv_area_base VALUES ('231000', '牡丹江市', '牡丹江', '', '', '2', '230000', '黑龙江省 牡丹江市');
INSERT INTO dv_area_base VALUES ('231002', '东安区', '', '', '', '3', '231000', '黑龙江省 牡丹江市 东安区');
INSERT INTO dv_area_base VALUES ('231003', '阳明区', '', '', '', '3', '231000', '黑龙江省 牡丹江市 阳明区');
INSERT INTO dv_area_base VALUES ('231004', '爱民区', '', '', '', '3', '231000', '黑龙江省 牡丹江市 爱民区');
INSERT INTO dv_area_base VALUES ('231005', '西安区', '', '', '', '3', '231000', '黑龙江省 牡丹江市 西安区');
INSERT INTO dv_area_base VALUES ('231024', '东宁县', '', '', '', '3', '231000', '黑龙江省 牡丹江市 东宁县');
INSERT INTO dv_area_base VALUES ('231025', '林口县', '', '', '', '3', '231000', '黑龙江省 牡丹江市 林口县');
INSERT INTO dv_area_base VALUES ('231081', '绥芬河市', '', '', '', '3', '231000', '黑龙江省 牡丹江市 绥芬河市');
INSERT INTO dv_area_base VALUES ('231083', '海林市', '', '', '', '3', '231000', '黑龙江省 牡丹江市 海林市');
INSERT INTO dv_area_base VALUES ('231084', '宁安市', '', '', '', '3', '231000', '黑龙江省 牡丹江市 宁安市');
INSERT INTO dv_area_base VALUES ('231085', '穆棱市', '', '', '', '3', '231000', '黑龙江省 牡丹江市 穆棱市');
INSERT INTO dv_area_base VALUES ('231100', '黑河市', '黑河', '', '', '2', '230000', '黑龙江省 黑河市');
INSERT INTO dv_area_base VALUES ('231102', '爱辉区', '', '', '', '3', '231100', '黑龙江省 黑河市 爱辉区');
INSERT INTO dv_area_base VALUES ('231121', '嫩江县', '', '', '', '3', '231100', '黑龙江省 黑河市 嫩江县');
INSERT INTO dv_area_base VALUES ('231123', '逊克县', '', '', '', '3', '231100', '黑龙江省 黑河市 逊克县');
INSERT INTO dv_area_base VALUES ('231124', '孙吴县', '', '', '', '3', '231100', '黑龙江省 黑河市 孙吴县');
INSERT INTO dv_area_base VALUES ('231181', '北安市', '', '', '', '3', '231100', '黑龙江省 黑河市 北安市');
INSERT INTO dv_area_base VALUES ('231182', '五大连池市', '', '', '', '3', '231100', '黑龙江省 黑河市 五大连池市');
INSERT INTO dv_area_base VALUES ('231200', '绥化市', '绥化', '', '', '2', '230000', '黑龙江省 绥化市');
INSERT INTO dv_area_base VALUES ('231202', '北林区', '', '', '', '3', '231200', '黑龙江省 绥化市 北林区');
INSERT INTO dv_area_base VALUES ('231221', '望奎县', '', '', '', '3', '231200', '黑龙江省 绥化市 望奎县');
INSERT INTO dv_area_base VALUES ('231222', '兰西县', '', '', '', '3', '231200', '黑龙江省 绥化市 兰西县');
INSERT INTO dv_area_base VALUES ('231223', '青冈县', '', '', '', '3', '231200', '黑龙江省 绥化市 青冈县');
INSERT INTO dv_area_base VALUES ('231224', '庆安县', '', '', '', '3', '231200', '黑龙江省 绥化市 庆安县');
INSERT INTO dv_area_base VALUES ('231225', '明水县', '', '', '', '3', '231200', '黑龙江省 绥化市 明水县');
INSERT INTO dv_area_base VALUES ('231226', '绥棱县', '', '', '', '3', '231200', '黑龙江省 绥化市 绥棱县');
INSERT INTO dv_area_base VALUES ('231281', '安达市', '', '', '', '3', '231200', '黑龙江省 绥化市 安达市');
INSERT INTO dv_area_base VALUES ('231282', '肇东市', '', '', '', '3', '231200', '黑龙江省 绥化市 肇东市');
INSERT INTO dv_area_base VALUES ('231283', '海伦市', '', '', '', '3', '231200', '黑龙江省 绥化市 海伦市');
INSERT INTO dv_area_base VALUES ('232700', '大兴安岭地区', '大兴安岭', '', '', '2', '230000', '黑龙江省 大兴安岭地区');
INSERT INTO dv_area_base VALUES ('232721', '呼玛县', '', '', '', '3', '232700', '黑龙江省 大兴安岭地区 呼玛县');
INSERT INTO dv_area_base VALUES ('232722', '塔河县', '', '', '', '3', '232700', '黑龙江省 大兴安岭地区 塔河县');
INSERT INTO dv_area_base VALUES ('232723', '漠河县', '', '', '', '3', '232700', '黑龙江省 大兴安岭地区 漠河县');
INSERT INTO dv_area_base VALUES ('310000', '上海市', '上海', 'shanghai', '沪', '1', '0', '上海市');
INSERT INTO dv_area_base VALUES ('310101', '黄浦区', '黄浦', '', '', '2', '310000', '上海市 黄浦区');
INSERT INTO dv_area_base VALUES ('310104', '徐汇区', '徐汇', '', '', '2', '310000', '上海市 徐汇区');
INSERT INTO dv_area_base VALUES ('310105', '长宁区', '长宁', '', '', '2', '310000', '上海市 长宁区');
INSERT INTO dv_area_base VALUES ('310106', '静安区', '静安', '', '', '2', '310000', '上海市 静安区');
INSERT INTO dv_area_base VALUES ('310107', '普陀区', '普陀', '', '', '2', '310000', '上海市 普陀区');
INSERT INTO dv_area_base VALUES ('310108', '闸北区', '闸北', '', '', '2', '310000', '上海市 闸北区');
INSERT INTO dv_area_base VALUES ('310109', '虹口区', '虹口', '', '', '2', '310000', '上海市 虹口区');
INSERT INTO dv_area_base VALUES ('310110', '杨浦区', '杨浦', '', '', '2', '310000', '上海市 杨浦区');
INSERT INTO dv_area_base VALUES ('310112', '闵行区', '闵行', '', '', '2', '310000', '上海市 闵行区');
INSERT INTO dv_area_base VALUES ('310113', '宝山区', '宝山', '', '', '2', '310000', '上海市 宝山区');
INSERT INTO dv_area_base VALUES ('310114', '嘉定区', '嘉定', '', '', '2', '310000', '上海市 嘉定区');
INSERT INTO dv_area_base VALUES ('310115', '浦东新区', '浦东', '', '', '2', '310000', '上海市 浦东新区');
INSERT INTO dv_area_base VALUES ('310116', '金山区', '金山', '', '', '2', '310000', '上海市 金山区');
INSERT INTO dv_area_base VALUES ('310117', '松江区', '松江', '', '', '2', '310000', '上海市 松江区');
INSERT INTO dv_area_base VALUES ('310118', '青浦区', '青浦', '', '', '2', '310000', '上海市 青浦区');
INSERT INTO dv_area_base VALUES ('310120', '奉贤区', '奉贤', '', '', '2', '310000', '上海市 奉贤区');
INSERT INTO dv_area_base VALUES ('310230', '崇明县', '崇明', '', '', '2', '310000', '上海市 崇明县');
INSERT INTO dv_area_base VALUES ('320000', '江苏省', '江苏', 'jiangsu', '苏', '1', '0', '江苏省');
INSERT INTO dv_area_base VALUES ('320100', '南京市', '南京', '', '', '2', '320000', '江苏省 南京市');
INSERT INTO dv_area_base VALUES ('320102', '玄武区', '', '', '', '3', '320100', '江苏省 南京市 玄武区');
INSERT INTO dv_area_base VALUES ('320103', '白下区', '', '', '', '3', '320100', '江苏省 南京市 白下区');
INSERT INTO dv_area_base VALUES ('320104', '秦淮区', '', '', '', '3', '320100', '江苏省 南京市 秦淮区');
INSERT INTO dv_area_base VALUES ('320105', '建邺区', '', '', '', '3', '320100', '江苏省 南京市 建邺区');
INSERT INTO dv_area_base VALUES ('320106', '鼓楼区', '', '', '', '3', '320100', '江苏省 南京市 鼓楼区');
INSERT INTO dv_area_base VALUES ('320107', '下关区', '', '', '', '3', '320100', '江苏省 南京市 下关区');
INSERT INTO dv_area_base VALUES ('320111', '浦口区', '', '', '', '3', '320100', '江苏省 南京市 浦口区');
INSERT INTO dv_area_base VALUES ('320113', '栖霞区', '', '', '', '3', '320100', '江苏省 南京市 栖霞区');
INSERT INTO dv_area_base VALUES ('320114', '雨花台区', '', '', '', '3', '320100', '江苏省 南京市 雨花台区');
INSERT INTO dv_area_base VALUES ('320115', '江宁区', '', '', '', '3', '320100', '江苏省 南京市 江宁区');
INSERT INTO dv_area_base VALUES ('320116', '六合区', '', '', '', '3', '320100', '江苏省 南京市 六合区');
INSERT INTO dv_area_base VALUES ('320124', '溧水县', '', '', '', '3', '320100', '江苏省 南京市 溧水县');
INSERT INTO dv_area_base VALUES ('320125', '高淳县', '', '', '', '3', '320100', '江苏省 南京市 高淳县');
INSERT INTO dv_area_base VALUES ('320200', '无锡市', '无锡', '', '', '2', '320000', '江苏省 无锡市');
INSERT INTO dv_area_base VALUES ('320202', '崇安区', '', '', '', '3', '320200', '江苏省 无锡市 崇安区');
INSERT INTO dv_area_base VALUES ('320203', '南长区', '', '', '', '3', '320200', '江苏省 无锡市 南长区');
INSERT INTO dv_area_base VALUES ('320204', '北塘区', '', '', '', '3', '320200', '江苏省 无锡市 北塘区');
INSERT INTO dv_area_base VALUES ('320205', '锡山区', '', '', '', '3', '320200', '江苏省 无锡市 锡山区');
INSERT INTO dv_area_base VALUES ('320206', '惠山区', '', '', '', '3', '320200', '江苏省 无锡市 惠山区');
INSERT INTO dv_area_base VALUES ('320211', '滨湖区', '', '', '', '3', '320200', '江苏省 无锡市 滨湖区');
INSERT INTO dv_area_base VALUES ('320281', '江阴市', '', '', '', '3', '320200', '江苏省 无锡市 江阴市');
INSERT INTO dv_area_base VALUES ('320282', '宜兴市', '', '', '', '3', '320200', '江苏省 无锡市 宜兴市');
INSERT INTO dv_area_base VALUES ('320300', '徐州市', '徐州', '', '', '2', '320000', '江苏省 徐州市');
INSERT INTO dv_area_base VALUES ('320302', '鼓楼区', '', '', '', '3', '320300', '江苏省 徐州市 鼓楼区');
INSERT INTO dv_area_base VALUES ('320303', '云龙区', '', '', '', '3', '320300', '江苏省 徐州市 云龙区');
INSERT INTO dv_area_base VALUES ('320305', '贾汪区', '', '', '', '3', '320300', '江苏省 徐州市 贾汪区');
INSERT INTO dv_area_base VALUES ('320311', '泉山区', '', '', '', '3', '320300', '江苏省 徐州市 泉山区');
INSERT INTO dv_area_base VALUES ('320312', '铜山区', '', '', '', '3', '320300', '江苏省 徐州市 铜山区');
INSERT INTO dv_area_base VALUES ('320321', '丰县', '', '', '', '3', '320300', '江苏省 徐州市 丰县');
INSERT INTO dv_area_base VALUES ('320322', '沛县', '', '', '', '3', '320300', '江苏省 徐州市 沛县');
INSERT INTO dv_area_base VALUES ('320324', '睢宁县', '', '', '', '3', '320300', '江苏省 徐州市 睢宁县');
INSERT INTO dv_area_base VALUES ('320381', '新沂市', '', '', '', '3', '320300', '江苏省 徐州市 新沂市');
INSERT INTO dv_area_base VALUES ('320382', '邳州市', '', '', '', '3', '320300', '江苏省 徐州市 邳州市');
INSERT INTO dv_area_base VALUES ('320400', '常州市', '常州', '', '', '2', '320000', '江苏省 常州市');
INSERT INTO dv_area_base VALUES ('320402', '天宁区', '', '', '', '3', '320400', '江苏省 常州市 天宁区');
INSERT INTO dv_area_base VALUES ('320404', '钟楼区', '', '', '', '3', '320400', '江苏省 常州市 钟楼区');
INSERT INTO dv_area_base VALUES ('320405', '戚墅堰区', '', '', '', '3', '320400', '江苏省 常州市 戚墅堰区');
INSERT INTO dv_area_base VALUES ('320411', '新北区', '', '', '', '3', '320400', '江苏省 常州市 新北区');
INSERT INTO dv_area_base VALUES ('320412', '武进区', '', '', '', '3', '320400', '江苏省 常州市 武进区');
INSERT INTO dv_area_base VALUES ('320481', '溧阳市', '', '', '', '3', '320400', '江苏省 常州市 溧阳市');
INSERT INTO dv_area_base VALUES ('320482', '金坛市', '', '', '', '3', '320400', '江苏省 常州市 金坛市');
INSERT INTO dv_area_base VALUES ('320500', '苏州市', '苏州', '', '', '2', '320000', '江苏省 苏州市');
INSERT INTO dv_area_base VALUES ('320502', '姑苏区', '', '', '', '3', '320500', '江苏省 苏州市 姑苏区');
INSERT INTO dv_area_base VALUES ('320505', '虎丘区', '', '', '', '3', '320500', '江苏省 苏州市 虎丘区');
INSERT INTO dv_area_base VALUES ('320506', '吴中区', '', '', '', '3', '320500', '江苏省 苏州市 吴中区');
INSERT INTO dv_area_base VALUES ('320507', '相城区', '', '', '', '3', '320500', '江苏省 苏州市 相城区');
INSERT INTO dv_area_base VALUES ('320581', '常熟市', '', '', '', '3', '320500', '江苏省 苏州市 常熟市');
INSERT INTO dv_area_base VALUES ('320582', '张家港市', '', '', '', '3', '320500', '江苏省 苏州市 张家港市');
INSERT INTO dv_area_base VALUES ('320583', '昆山市', '', '', '', '3', '320500', '江苏省 苏州市 昆山市');
INSERT INTO dv_area_base VALUES ('320584', '吴江区', '', '', '', '3', '320500', '江苏省 苏州市 吴江区');
INSERT INTO dv_area_base VALUES ('320585', '太仓市', '', '', '', '3', '320500', '江苏省 苏州市 太仓市');
INSERT INTO dv_area_base VALUES ('320600', '南通市', '南通', '', '', '2', '320000', '江苏省 南通市');
INSERT INTO dv_area_base VALUES ('320602', '崇川区', '', '', '', '3', '320600', '江苏省 南通市 崇川区');
INSERT INTO dv_area_base VALUES ('320611', '港闸区', '', '', '', '3', '320600', '江苏省 南通市 港闸区');
INSERT INTO dv_area_base VALUES ('320621', '海安县', '', '', '', '3', '320600', '江苏省 南通市 海安县');
INSERT INTO dv_area_base VALUES ('320623', '如东县', '', '', '', '3', '320600', '江苏省 南通市 如东县');
INSERT INTO dv_area_base VALUES ('320681', '启东市', '', '', '', '3', '320600', '江苏省 南通市 启东市');
INSERT INTO dv_area_base VALUES ('320682', '如皋市', '', '', '', '3', '320600', '江苏省 南通市 如皋市');
INSERT INTO dv_area_base VALUES ('320683', '通州区', '', '', '', '3', '320600', '江苏省 南通市 通州区');
INSERT INTO dv_area_base VALUES ('320684', '海门市', '', '', '', '3', '320600', '江苏省 南通市 海门市');
INSERT INTO dv_area_base VALUES ('320700', '连云港市', '连云港', '', '', '2', '320000', '江苏省 连云港市');
INSERT INTO dv_area_base VALUES ('320703', '连云区', '', '', '', '3', '320700', '江苏省 连云港市 连云区');
INSERT INTO dv_area_base VALUES ('320705', '新浦区', '', '', '', '3', '320700', '江苏省 连云港市 新浦区');
INSERT INTO dv_area_base VALUES ('320706', '海州区', '', '', '', '3', '320700', '江苏省 连云港市 海州区');
INSERT INTO dv_area_base VALUES ('320721', '赣榆县', '', '', '', '3', '320700', '江苏省 连云港市 赣榆县');
INSERT INTO dv_area_base VALUES ('320722', '东海县', '', '', '', '3', '320700', '江苏省 连云港市 东海县');
INSERT INTO dv_area_base VALUES ('320723', '灌云县', '', '', '', '3', '320700', '江苏省 连云港市 灌云县');
INSERT INTO dv_area_base VALUES ('320724', '灌南县', '', '', '', '3', '320700', '江苏省 连云港市 灌南县');
INSERT INTO dv_area_base VALUES ('320800', '淮安市', '淮安', '', '', '2', '320000', '江苏省 淮安市');
INSERT INTO dv_area_base VALUES ('320802', '清河区', '', '', '', '3', '320800', '江苏省 淮安市 清河区');
INSERT INTO dv_area_base VALUES ('320803', '淮安区', '', '', '', '3', '320800', '江苏省 淮安市 淮安区');
INSERT INTO dv_area_base VALUES ('320804', '淮阴区', '', '', '', '3', '320800', '江苏省 淮安市 淮阴区');
INSERT INTO dv_area_base VALUES ('320811', '清浦区', '', '', '', '3', '320800', '江苏省 淮安市 清浦区');
INSERT INTO dv_area_base VALUES ('320826', '涟水县', '', '', '', '3', '320800', '江苏省 淮安市 涟水县');
INSERT INTO dv_area_base VALUES ('320829', '洪泽县', '', '', '', '3', '320800', '江苏省 淮安市 洪泽县');
INSERT INTO dv_area_base VALUES ('320830', '盱眙县', '', '', '', '3', '320800', '江苏省 淮安市 盱眙县');
INSERT INTO dv_area_base VALUES ('320831', '金湖县', '', '', '', '3', '320800', '江苏省 淮安市 金湖县');
INSERT INTO dv_area_base VALUES ('320900', '盐城市', '盐城', '', '', '2', '320000', '江苏省 盐城市');
INSERT INTO dv_area_base VALUES ('320902', '亭湖区', '', '', '', '3', '320900', '江苏省 盐城市 亭湖区');
INSERT INTO dv_area_base VALUES ('320903', '盐都区', '', '', '', '3', '320900', '江苏省 盐城市 盐都区');
INSERT INTO dv_area_base VALUES ('320921', '响水县', '', '', '', '3', '320900', '江苏省 盐城市 响水县');
INSERT INTO dv_area_base VALUES ('320922', '滨海县', '', '', '', '3', '320900', '江苏省 盐城市 滨海县');
INSERT INTO dv_area_base VALUES ('320923', '阜宁县', '', '', '', '3', '320900', '江苏省 盐城市 阜宁县');
INSERT INTO dv_area_base VALUES ('320924', '射阳县', '', '', '', '3', '320900', '江苏省 盐城市 射阳县');
INSERT INTO dv_area_base VALUES ('320925', '建湖县', '', '', '', '3', '320900', '江苏省 盐城市 建湖县');
INSERT INTO dv_area_base VALUES ('320981', '东台市', '', '', '', '3', '320900', '江苏省 盐城市 东台市');
INSERT INTO dv_area_base VALUES ('320982', '大丰市', '', '', '', '3', '320900', '江苏省 盐城市 大丰市');
INSERT INTO dv_area_base VALUES ('321000', '扬州市', '扬州', '', '', '2', '320000', '江苏省 扬州市');
INSERT INTO dv_area_base VALUES ('321002', '广陵区', '', '', '', '3', '321000', '江苏省 扬州市 广陵区');
INSERT INTO dv_area_base VALUES ('321003', '邗江区', '', '', '', '3', '321000', '江苏省 扬州市 邗江区');
INSERT INTO dv_area_base VALUES ('321023', '宝应县', '', '', '', '3', '321000', '江苏省 扬州市 宝应县');
INSERT INTO dv_area_base VALUES ('321081', '仪征市', '', '', '', '3', '321000', '江苏省 扬州市 仪征市');
INSERT INTO dv_area_base VALUES ('321084', '高邮市', '', '', '', '3', '321000', '江苏省 扬州市 高邮市');
INSERT INTO dv_area_base VALUES ('321088', '江都区', '', '', '', '3', '321000', '江苏省 扬州市 江都区');
INSERT INTO dv_area_base VALUES ('321100', '镇江市', '镇江', '', '', '2', '320000', '江苏省 镇江市');
INSERT INTO dv_area_base VALUES ('321102', '京口区', '', '', '', '3', '321100', '江苏省 镇江市 京口区');
INSERT INTO dv_area_base VALUES ('321111', '润州区', '', '', '', '3', '321100', '江苏省 镇江市 润州区');
INSERT INTO dv_area_base VALUES ('321112', '丹徒区', '', '', '', '3', '321100', '江苏省 镇江市 丹徒区');
INSERT INTO dv_area_base VALUES ('321181', '丹阳市', '', '', '', '3', '321100', '江苏省 镇江市 丹阳市');
INSERT INTO dv_area_base VALUES ('321182', '扬中市', '', '', '', '3', '321100', '江苏省 镇江市 扬中市');
INSERT INTO dv_area_base VALUES ('321183', '句容市', '', '', '', '3', '321100', '江苏省 镇江市 句容市');
INSERT INTO dv_area_base VALUES ('321200', '泰州市', '泰州', '', '', '2', '320000', '江苏省 泰州市');
INSERT INTO dv_area_base VALUES ('321202', '海陵区', '', '', '', '3', '321200', '江苏省 泰州市 海陵区');
INSERT INTO dv_area_base VALUES ('321203', '高港区', '', '', '', '3', '321200', '江苏省 泰州市 高港区');
INSERT INTO dv_area_base VALUES ('321281', '兴化市', '', '', '', '3', '321200', '江苏省 泰州市 兴化市');
INSERT INTO dv_area_base VALUES ('321282', '靖江市', '', '', '', '3', '321200', '江苏省 泰州市 靖江市');
INSERT INTO dv_area_base VALUES ('321283', '泰兴市', '', '', '', '3', '321200', '江苏省 泰州市 泰兴市');
INSERT INTO dv_area_base VALUES ('321284', '姜堰市', '', '', '', '3', '321200', '江苏省 泰州市 姜堰市');
INSERT INTO dv_area_base VALUES ('321300', '宿迁市', '宿迁', '', '', '2', '320000', '江苏省 宿迁市');
INSERT INTO dv_area_base VALUES ('321302', '宿城区', '', '', '', '3', '321300', '江苏省 宿迁市 宿城区');
INSERT INTO dv_area_base VALUES ('321311', '宿豫区', '', '', '', '3', '321300', '江苏省 宿迁市 宿豫区');
INSERT INTO dv_area_base VALUES ('321322', '沭阳县', '', '', '', '3', '321300', '江苏省 宿迁市 沭阳县');
INSERT INTO dv_area_base VALUES ('321323', '泗阳县', '', '', '', '3', '321300', '江苏省 宿迁市 泗阳县');
INSERT INTO dv_area_base VALUES ('321324', '泗洪县', '', '', '', '3', '321300', '江苏省 宿迁市 泗洪县');
INSERT INTO dv_area_base VALUES ('330000', '浙江省', '浙江', 'zhejiang', '浙', '1', '0', '浙江省');
INSERT INTO dv_area_base VALUES ('330100', '杭州市', '杭州', '', '', '2', '330000', '浙江省 杭州市');
INSERT INTO dv_area_base VALUES ('330102', '上城区', '', '', '', '3', '330100', '浙江省 杭州市 上城区');
INSERT INTO dv_area_base VALUES ('330103', '下城区', '', '', '', '3', '330100', '浙江省 杭州市 下城区');
INSERT INTO dv_area_base VALUES ('330104', '江干区', '', '', '', '3', '330100', '浙江省 杭州市 江干区');
INSERT INTO dv_area_base VALUES ('330105', '拱墅区', '', '', '', '3', '330100', '浙江省 杭州市 拱墅区');
INSERT INTO dv_area_base VALUES ('330106', '西湖区', '', '', '', '3', '330100', '浙江省 杭州市 西湖区');
INSERT INTO dv_area_base VALUES ('330108', '滨江区', '', '', '', '3', '330100', '浙江省 杭州市 滨江区');
INSERT INTO dv_area_base VALUES ('330109', '萧山区', '', '', '', '3', '330100', '浙江省 杭州市 萧山区');
INSERT INTO dv_area_base VALUES ('330110', '余杭区', '', '', '', '3', '330100', '浙江省 杭州市 余杭区');
INSERT INTO dv_area_base VALUES ('330122', '桐庐县', '', '', '', '3', '330100', '浙江省 杭州市 桐庐县');
INSERT INTO dv_area_base VALUES ('330127', '淳安县', '', '', '', '3', '330100', '浙江省 杭州市 淳安县');
INSERT INTO dv_area_base VALUES ('330182', '建德市', '', '', '', '3', '330100', '浙江省 杭州市 建德市');
INSERT INTO dv_area_base VALUES ('330183', '富阳市', '', '', '', '3', '330100', '浙江省 杭州市 富阳市');
INSERT INTO dv_area_base VALUES ('330185', '临安市', '', '', '', '3', '330100', '浙江省 杭州市 临安市');
INSERT INTO dv_area_base VALUES ('330200', '宁波市', '宁波', '', '', '2', '330000', '浙江省 宁波市');
INSERT INTO dv_area_base VALUES ('330203', '海曙区', '', '', '', '3', '330200', '浙江省 宁波市 海曙区');
INSERT INTO dv_area_base VALUES ('330204', '江东区', '', '', '', '3', '330200', '浙江省 宁波市 江东区');
INSERT INTO dv_area_base VALUES ('330205', '江北区', '', '', '', '3', '330200', '浙江省 宁波市 江北区');
INSERT INTO dv_area_base VALUES ('330206', '北仑区', '', '', '', '3', '330200', '浙江省 宁波市 北仑区');
INSERT INTO dv_area_base VALUES ('330211', '镇海区', '', '', '', '3', '330200', '浙江省 宁波市 镇海区');
INSERT INTO dv_area_base VALUES ('330212', '鄞州区', '', '', '', '3', '330200', '浙江省 宁波市 鄞州区');
INSERT INTO dv_area_base VALUES ('330225', '象山县', '', '', '', '3', '330200', '浙江省 宁波市 象山县');
INSERT INTO dv_area_base VALUES ('330226', '宁海县', '', '', '', '3', '330200', '浙江省 宁波市 宁海县');
INSERT INTO dv_area_base VALUES ('330281', '余姚市', '', '', '', '3', '330200', '浙江省 宁波市 余姚市');
INSERT INTO dv_area_base VALUES ('330282', '慈溪市', '', '', '', '3', '330200', '浙江省 宁波市 慈溪市');
INSERT INTO dv_area_base VALUES ('330283', '奉化市', '', '', '', '3', '330200', '浙江省 宁波市 奉化市');
INSERT INTO dv_area_base VALUES ('330300', '温州市', '温州', '', '', '2', '330000', '浙江省 温州市');
INSERT INTO dv_area_base VALUES ('330302', '鹿城区', '', '', '', '3', '330300', '浙江省 温州市 鹿城区');
INSERT INTO dv_area_base VALUES ('330303', '龙湾区', '', '', '', '3', '330300', '浙江省 温州市 龙湾区');
INSERT INTO dv_area_base VALUES ('330304', '瓯海区', '', '', '', '3', '330300', '浙江省 温州市 瓯海区');
INSERT INTO dv_area_base VALUES ('330322', '洞头县', '', '', '', '3', '330300', '浙江省 温州市 洞头县');
INSERT INTO dv_area_base VALUES ('330324', '永嘉县', '', '', '', '3', '330300', '浙江省 温州市 永嘉县');
INSERT INTO dv_area_base VALUES ('330326', '平阳县', '', '', '', '3', '330300', '浙江省 温州市 平阳县');
INSERT INTO dv_area_base VALUES ('330327', '苍南县', '', '', '', '3', '330300', '浙江省 温州市 苍南县');
INSERT INTO dv_area_base VALUES ('330328', '文成县', '', '', '', '3', '330300', '浙江省 温州市 文成县');
INSERT INTO dv_area_base VALUES ('330329', '泰顺县', '', '', '', '3', '330300', '浙江省 温州市 泰顺县');
INSERT INTO dv_area_base VALUES ('330381', '瑞安市', '', '', '', '3', '330300', '浙江省 温州市 瑞安市');
INSERT INTO dv_area_base VALUES ('330382', '乐清市', '', '', '', '3', '330300', '浙江省 温州市 乐清市');
INSERT INTO dv_area_base VALUES ('330400', '嘉兴市', '嘉兴', '', '', '2', '330000', '浙江省 嘉兴市');
INSERT INTO dv_area_base VALUES ('330402', '南湖区', '', '', '', '3', '330400', '浙江省 嘉兴市 南湖区');
INSERT INTO dv_area_base VALUES ('330411', '秀洲区', '', '', '', '3', '330400', '浙江省 嘉兴市 秀洲区');
INSERT INTO dv_area_base VALUES ('330421', '嘉善县', '', '', '', '3', '330400', '浙江省 嘉兴市 嘉善县');
INSERT INTO dv_area_base VALUES ('330424', '海盐县', '', '', '', '3', '330400', '浙江省 嘉兴市 海盐县');
INSERT INTO dv_area_base VALUES ('330481', '海宁市', '', '', '', '3', '330400', '浙江省 嘉兴市 海宁市');
INSERT INTO dv_area_base VALUES ('330482', '平湖市', '', '', '', '3', '330400', '浙江省 嘉兴市 平湖市');
INSERT INTO dv_area_base VALUES ('330483', '桐乡市', '', '', '', '3', '330400', '浙江省 嘉兴市 桐乡市');
INSERT INTO dv_area_base VALUES ('330500', '湖州市', '湖州', '', '', '2', '330000', '浙江省 湖州市');
INSERT INTO dv_area_base VALUES ('330502', '吴兴区', '', '', '', '3', '330500', '浙江省 湖州市 吴兴区');
INSERT INTO dv_area_base VALUES ('330503', '南浔区', '', '', '', '3', '330500', '浙江省 湖州市 南浔区');
INSERT INTO dv_area_base VALUES ('330521', '德清县', '', '', '', '3', '330500', '浙江省 湖州市 德清县');
INSERT INTO dv_area_base VALUES ('330522', '长兴县', '', '', '', '3', '330500', '浙江省 湖州市 长兴县');
INSERT INTO dv_area_base VALUES ('330523', '安吉县', '', '', '', '3', '330500', '浙江省 湖州市 安吉县');
INSERT INTO dv_area_base VALUES ('330600', '绍兴市', '绍兴', '', '', '2', '330000', '浙江省 绍兴市');
INSERT INTO dv_area_base VALUES ('330602', '越城区', '', '', '', '3', '330600', '浙江省 绍兴市 越城区');
INSERT INTO dv_area_base VALUES ('330621', '绍兴县', '', '', '', '3', '330600', '浙江省 绍兴市 绍兴县');
INSERT INTO dv_area_base VALUES ('330624', '新昌县', '', '', '', '3', '330600', '浙江省 绍兴市 新昌县');
INSERT INTO dv_area_base VALUES ('330681', '诸暨市', '', '', '', '3', '330600', '浙江省 绍兴市 诸暨市');
INSERT INTO dv_area_base VALUES ('330682', '上虞市', '', '', '', '3', '330600', '浙江省 绍兴市 上虞市');
INSERT INTO dv_area_base VALUES ('330683', '嵊州市', '', '', '', '3', '330600', '浙江省 绍兴市 嵊州市');
INSERT INTO dv_area_base VALUES ('330700', '金华市', '金华', '', '', '2', '330000', '浙江省 金华市');
INSERT INTO dv_area_base VALUES ('330702', '婺城区', '', '', '', '3', '330700', '浙江省 金华市 婺城区');
INSERT INTO dv_area_base VALUES ('330703', '金东区', '', '', '', '3', '330700', '浙江省 金华市 金东区');
INSERT INTO dv_area_base VALUES ('330723', '武义县', '', '', '', '3', '330700', '浙江省 金华市 武义县');
INSERT INTO dv_area_base VALUES ('330726', '浦江县', '', '', '', '3', '330700', '浙江省 金华市 浦江县');
INSERT INTO dv_area_base VALUES ('330727', '磐安县', '', '', '', '3', '330700', '浙江省 金华市 磐安县');
INSERT INTO dv_area_base VALUES ('330781', '兰溪市', '', '', '', '3', '330700', '浙江省 金华市 兰溪市');
INSERT INTO dv_area_base VALUES ('330782', '义乌市', '', '', '', '3', '330700', '浙江省 金华市 义乌市');
INSERT INTO dv_area_base VALUES ('330783', '东阳市', '', '', '', '3', '330700', '浙江省 金华市 东阳市');
INSERT INTO dv_area_base VALUES ('330784', '永康市', '', '', '', '3', '330700', '浙江省 金华市 永康市');
INSERT INTO dv_area_base VALUES ('330800', '衢州市', '衢州', '', '', '2', '330000', '浙江省 衢州市');
INSERT INTO dv_area_base VALUES ('330802', '柯城区', '', '', '', '3', '330800', '浙江省 衢州市 柯城区');
INSERT INTO dv_area_base VALUES ('330803', '衢江区', '', '', '', '3', '330800', '浙江省 衢州市 衢江区');
INSERT INTO dv_area_base VALUES ('330822', '常山县', '', '', '', '3', '330800', '浙江省 衢州市 常山县');
INSERT INTO dv_area_base VALUES ('330824', '开化县', '', '', '', '3', '330800', '浙江省 衢州市 开化县');
INSERT INTO dv_area_base VALUES ('330825', '龙游县', '', '', '', '3', '330800', '浙江省 衢州市 龙游县');
INSERT INTO dv_area_base VALUES ('330881', '江山市', '', '', '', '3', '330800', '浙江省 衢州市 江山市');
INSERT INTO dv_area_base VALUES ('330900', '舟山市', '舟山', '', '', '2', '330000', '浙江省 舟山市');
INSERT INTO dv_area_base VALUES ('330902', '定海区', '', '', '', '3', '330900', '浙江省 舟山市 定海区');
INSERT INTO dv_area_base VALUES ('330903', '普陀区', '', '', '', '3', '330900', '浙江省 舟山市 普陀区');
INSERT INTO dv_area_base VALUES ('330921', '岱山县', '', '', '', '3', '330900', '浙江省 舟山市 岱山县');
INSERT INTO dv_area_base VALUES ('330922', '嵊泗县', '', '', '', '3', '330900', '浙江省 舟山市 嵊泗县');
INSERT INTO dv_area_base VALUES ('331000', '台州市', '台州', '', '', '2', '330000', '浙江省 台州市');
INSERT INTO dv_area_base VALUES ('331002', '椒江区', '', '', '', '3', '331000', '浙江省 台州市 椒江区');
INSERT INTO dv_area_base VALUES ('331003', '黄岩区', '', '', '', '3', '331000', '浙江省 台州市 黄岩区');
INSERT INTO dv_area_base VALUES ('331004', '路桥区', '', '', '', '3', '331000', '浙江省 台州市 路桥区');
INSERT INTO dv_area_base VALUES ('331021', '玉环县', '', '', '', '3', '331000', '浙江省 台州市 玉环县');
INSERT INTO dv_area_base VALUES ('331022', '三门县', '', '', '', '3', '331000', '浙江省 台州市 三门县');
INSERT INTO dv_area_base VALUES ('331023', '天台县', '', '', '', '3', '331000', '浙江省 台州市 天台县');
INSERT INTO dv_area_base VALUES ('331024', '仙居县', '', '', '', '3', '331000', '浙江省 台州市 仙居县');
INSERT INTO dv_area_base VALUES ('331081', '温岭市', '', '', '', '3', '331000', '浙江省 台州市 温岭市');
INSERT INTO dv_area_base VALUES ('331082', '临海市', '', '', '', '3', '331000', '浙江省 台州市 临海市');
INSERT INTO dv_area_base VALUES ('331100', '丽水市', '丽水', '', '', '2', '330000', '浙江省 丽水市');
INSERT INTO dv_area_base VALUES ('331102', '莲都区', '', '', '', '3', '331100', '浙江省 丽水市 莲都区');
INSERT INTO dv_area_base VALUES ('331121', '青田县', '', '', '', '3', '331100', '浙江省 丽水市 青田县');
INSERT INTO dv_area_base VALUES ('331122', '缙云县', '', '', '', '3', '331100', '浙江省 丽水市 缙云县');
INSERT INTO dv_area_base VALUES ('331123', '遂昌县', '', '', '', '3', '331100', '浙江省 丽水市 遂昌县');
INSERT INTO dv_area_base VALUES ('331124', '松阳县', '', '', '', '3', '331100', '浙江省 丽水市 松阳县');
INSERT INTO dv_area_base VALUES ('331125', '云和县', '', '', '', '3', '331100', '浙江省 丽水市 云和县');
INSERT INTO dv_area_base VALUES ('331126', '庆元县', '', '', '', '3', '331100', '浙江省 丽水市 庆元县');
INSERT INTO dv_area_base VALUES ('331127', '景宁畲族自治县', '', '', '', '3', '331100', '浙江省 丽水市 景宁畲族自治县');
INSERT INTO dv_area_base VALUES ('331181', '龙泉市', '', '', '', '3', '331100', '浙江省 丽水市 龙泉市');
INSERT INTO dv_area_base VALUES ('340000', '安徽省', '安徽', 'anhui', '皖', '1', '0', '安徽省');
INSERT INTO dv_area_base VALUES ('340100', '合肥市', '合肥', '', '', '2', '340000', '安徽省 合肥市');
INSERT INTO dv_area_base VALUES ('340102', '瑶海区', '', '', '', '3', '340100', '安徽省 合肥市 瑶海区');
INSERT INTO dv_area_base VALUES ('340103', '庐阳区', '', '', '', '3', '340100', '安徽省 合肥市 庐阳区');
INSERT INTO dv_area_base VALUES ('340104', '蜀山区', '', '', '', '3', '340100', '安徽省 合肥市 蜀山区');
INSERT INTO dv_area_base VALUES ('340111', '包河区', '', '', '', '3', '340100', '安徽省 合肥市 包河区');
INSERT INTO dv_area_base VALUES ('340121', '长丰县', '', '', '', '3', '340100', '安徽省 合肥市 长丰县');
INSERT INTO dv_area_base VALUES ('340122', '肥东县', '', '', '', '3', '340100', '安徽省 合肥市 肥东县');
INSERT INTO dv_area_base VALUES ('340123', '肥西县', '', '', '', '3', '340100', '安徽省 合肥市 肥西县');
INSERT INTO dv_area_base VALUES ('340124', '巢湖市', '', '', '', '3', '340100', '安徽省 合肥市 巢湖市');
INSERT INTO dv_area_base VALUES ('340125', '庐江县', '', '', '', '3', '340100', '安徽省 合肥市 庐江县');
INSERT INTO dv_area_base VALUES ('340200', '芜湖市', '芜湖', '', '', '2', '340000', '安徽省 芜湖市');
INSERT INTO dv_area_base VALUES ('340202', '镜湖区', '', '', '', '3', '340200', '安徽省 芜湖市 镜湖区');
INSERT INTO dv_area_base VALUES ('340203', '弋江区', '', '', '', '3', '340200', '安徽省 芜湖市 弋江区');
INSERT INTO dv_area_base VALUES ('340207', '鸠江区', '', '', '', '3', '340200', '安徽省 芜湖市 鸠江区');
INSERT INTO dv_area_base VALUES ('340208', '三山区', '', '', '', '3', '340200', '安徽省 芜湖市 三山区');
INSERT INTO dv_area_base VALUES ('340221', '芜湖县', '', '', '', '3', '340200', '安徽省 芜湖市 芜湖县');
INSERT INTO dv_area_base VALUES ('340222', '繁昌县', '', '', '', '3', '340200', '安徽省 芜湖市 繁昌县');
INSERT INTO dv_area_base VALUES ('340223', '南陵县', '', '', '', '3', '340200', '安徽省 芜湖市 南陵县');
INSERT INTO dv_area_base VALUES ('340224', '无为县', '', '', '', '3', '340200', '安徽省 芜湖市 无为县');
INSERT INTO dv_area_base VALUES ('340300', '蚌埠市', '蚌埠', '', '', '2', '340000', '安徽省 蚌埠市');
INSERT INTO dv_area_base VALUES ('340302', '龙子湖区', '', '', '', '3', '340300', '安徽省 蚌埠市 龙子湖区');
INSERT INTO dv_area_base VALUES ('340303', '蚌山区', '', '', '', '3', '340300', '安徽省 蚌埠市 蚌山区');
INSERT INTO dv_area_base VALUES ('340304', '禹会区', '', '', '', '3', '340300', '安徽省 蚌埠市 禹会区');
INSERT INTO dv_area_base VALUES ('340311', '淮上区', '', '', '', '3', '340300', '安徽省 蚌埠市 淮上区');
INSERT INTO dv_area_base VALUES ('340321', '怀远县', '', '', '', '3', '340300', '安徽省 蚌埠市 怀远县');
INSERT INTO dv_area_base VALUES ('340322', '五河县', '', '', '', '3', '340300', '安徽省 蚌埠市 五河县');
INSERT INTO dv_area_base VALUES ('340323', '固镇县', '', '', '', '3', '340300', '安徽省 蚌埠市 固镇县');
INSERT INTO dv_area_base VALUES ('340400', '淮南市', '淮南', '', '', '2', '340000', '安徽省 淮南市');
INSERT INTO dv_area_base VALUES ('340402', '大通区', '', '', '', '3', '340400', '安徽省 淮南市 大通区');
INSERT INTO dv_area_base VALUES ('340403', '田家庵区', '', '', '', '3', '340400', '安徽省 淮南市 田家庵区');
INSERT INTO dv_area_base VALUES ('340404', '谢家集区', '', '', '', '3', '340400', '安徽省 淮南市 谢家集区');
INSERT INTO dv_area_base VALUES ('340405', '八公山区', '', '', '', '3', '340400', '安徽省 淮南市 八公山区');
INSERT INTO dv_area_base VALUES ('340406', '潘集区', '', '', '', '3', '340400', '安徽省 淮南市 潘集区');
INSERT INTO dv_area_base VALUES ('340421', '凤台县', '', '', '', '3', '340400', '安徽省 淮南市 凤台县');
INSERT INTO dv_area_base VALUES ('340500', '马鞍山市', '马鞍山', '', '', '2', '340000', '安徽省 马鞍山市');
INSERT INTO dv_area_base VALUES ('340503', '花山区', '', '', '', '3', '340500', '安徽省 马鞍山市 花山区');
INSERT INTO dv_area_base VALUES ('340504', '雨山区', '', '', '', '3', '340500', '安徽省 马鞍山市 雨山区');
INSERT INTO dv_area_base VALUES ('340521', '当涂县', '', '', '', '3', '340500', '安徽省 马鞍山市 当涂县');
INSERT INTO dv_area_base VALUES ('340522', '含山县', '', '', '', '3', '340500', '安徽省 马鞍山市 含山县');
INSERT INTO dv_area_base VALUES ('340523', '和县', '', '', '', '3', '340500', '安徽省 马鞍山市 和县');
INSERT INTO dv_area_base VALUES ('340600', '淮北市', '淮北', '', '', '2', '340000', '安徽省 淮北市');
INSERT INTO dv_area_base VALUES ('340602', '杜集区', '', '', '', '3', '340600', '安徽省 淮北市 杜集区');
INSERT INTO dv_area_base VALUES ('340603', '相山区', '', '', '', '3', '340600', '安徽省 淮北市 相山区');
INSERT INTO dv_area_base VALUES ('340604', '烈山区', '', '', '', '3', '340600', '安徽省 淮北市 烈山区');
INSERT INTO dv_area_base VALUES ('340621', '濉溪县', '', '', '', '3', '340600', '安徽省 淮北市 濉溪县');
INSERT INTO dv_area_base VALUES ('340700', '铜陵市', '铜陵', '', '', '2', '340000', '安徽省 铜陵市');
INSERT INTO dv_area_base VALUES ('340702', '铜官山区', '', '', '', '3', '340700', '安徽省 铜陵市 铜官山区');
INSERT INTO dv_area_base VALUES ('340703', '狮子山区', '', '', '', '3', '340700', '安徽省 铜陵市 狮子山区');
INSERT INTO dv_area_base VALUES ('340711', '郊区', '', '', '', '3', '340700', '安徽省 铜陵市 郊区');
INSERT INTO dv_area_base VALUES ('340721', '铜陵县', '', '', '', '3', '340700', '安徽省 铜陵市 铜陵县');
INSERT INTO dv_area_base VALUES ('340800', '安庆市', '安庆', '', '', '2', '340000', '安徽省 安庆市');
INSERT INTO dv_area_base VALUES ('340802', '迎江区', '', '', '', '3', '340800', '安徽省 安庆市 迎江区');
INSERT INTO dv_area_base VALUES ('340803', '大观区', '', '', '', '3', '340800', '安徽省 安庆市 大观区');
INSERT INTO dv_area_base VALUES ('340811', '宜秀区', '', '', '', '3', '340800', '安徽省 安庆市 宜秀区');
INSERT INTO dv_area_base VALUES ('340822', '怀宁县', '', '', '', '3', '340800', '安徽省 安庆市 怀宁县');
INSERT INTO dv_area_base VALUES ('340823', '枞阳县', '', '', '', '3', '340800', '安徽省 安庆市 枞阳县');
INSERT INTO dv_area_base VALUES ('340824', '潜山县', '', '', '', '3', '340800', '安徽省 安庆市 潜山县');
INSERT INTO dv_area_base VALUES ('340825', '太湖县', '', '', '', '3', '340800', '安徽省 安庆市 太湖县');
INSERT INTO dv_area_base VALUES ('340826', '宿松县', '', '', '', '3', '340800', '安徽省 安庆市 宿松县');
INSERT INTO dv_area_base VALUES ('340827', '望江县', '', '', '', '3', '340800', '安徽省 安庆市 望江县');
INSERT INTO dv_area_base VALUES ('340828', '岳西县', '', '', '', '3', '340800', '安徽省 安庆市 岳西县');
INSERT INTO dv_area_base VALUES ('340881', '桐城市', '', '', '', '3', '340800', '安徽省 安庆市 桐城市');
INSERT INTO dv_area_base VALUES ('341000', '黄山市', '黄山', '', '', '2', '340000', '安徽省 黄山市');
INSERT INTO dv_area_base VALUES ('341002', '屯溪区', '', '', '', '3', '341000', '安徽省 黄山市 屯溪区');
INSERT INTO dv_area_base VALUES ('341003', '黄山区', '', '', '', '3', '341000', '安徽省 黄山市 黄山区');
INSERT INTO dv_area_base VALUES ('341004', '徽州区', '', '', '', '3', '341000', '安徽省 黄山市 徽州区');
INSERT INTO dv_area_base VALUES ('341021', '歙县', '', '', '', '3', '341000', '安徽省 黄山市 歙县');
INSERT INTO dv_area_base VALUES ('341022', '休宁县', '', '', '', '3', '341000', '安徽省 黄山市 休宁县');
INSERT INTO dv_area_base VALUES ('341023', '黟县', '', '', '', '3', '341000', '安徽省 黄山市 黟县');
INSERT INTO dv_area_base VALUES ('341024', '祁门县', '', '', '', '3', '341000', '安徽省 黄山市 祁门县');
INSERT INTO dv_area_base VALUES ('341100', '滁州市', '滁州', '', '', '2', '340000', '安徽省 滁州市');
INSERT INTO dv_area_base VALUES ('341102', '琅琊区', '', '', '', '3', '341100', '安徽省 滁州市 琅琊区');
INSERT INTO dv_area_base VALUES ('341103', '南谯区', '', '', '', '3', '341100', '安徽省 滁州市 南谯区');
INSERT INTO dv_area_base VALUES ('341122', '来安县', '', '', '', '3', '341100', '安徽省 滁州市 来安县');
INSERT INTO dv_area_base VALUES ('341124', '全椒县', '', '', '', '3', '341100', '安徽省 滁州市 全椒县');
INSERT INTO dv_area_base VALUES ('341125', '定远县', '', '', '', '3', '341100', '安徽省 滁州市 定远县');
INSERT INTO dv_area_base VALUES ('341126', '凤阳县', '', '', '', '3', '341100', '安徽省 滁州市 凤阳县');
INSERT INTO dv_area_base VALUES ('341181', '天长市', '', '', '', '3', '341100', '安徽省 滁州市 天长市');
INSERT INTO dv_area_base VALUES ('341182', '明光市', '', '', '', '3', '341100', '安徽省 滁州市 明光市');
INSERT INTO dv_area_base VALUES ('341200', '阜阳市', '阜阳', '', '', '2', '340000', '安徽省 阜阳市');
INSERT INTO dv_area_base VALUES ('341202', '颍州区', '', '', '', '3', '341200', '安徽省 阜阳市 颍州区');
INSERT INTO dv_area_base VALUES ('341203', '颍东区', '', '', '', '3', '341200', '安徽省 阜阳市 颍东区');
INSERT INTO dv_area_base VALUES ('341204', '颍泉区', '', '', '', '3', '341200', '安徽省 阜阳市 颍泉区');
INSERT INTO dv_area_base VALUES ('341221', '临泉县', '', '', '', '3', '341200', '安徽省 阜阳市 临泉县');
INSERT INTO dv_area_base VALUES ('341222', '太和县', '', '', '', '3', '341200', '安徽省 阜阳市 太和县');
INSERT INTO dv_area_base VALUES ('341225', '阜南县', '', '', '', '3', '341200', '安徽省 阜阳市 阜南县');
INSERT INTO dv_area_base VALUES ('341226', '颍上县', '', '', '', '3', '341200', '安徽省 阜阳市 颍上县');
INSERT INTO dv_area_base VALUES ('341282', '界首市', '', '', '', '3', '341200', '安徽省 阜阳市 界首市');
INSERT INTO dv_area_base VALUES ('341300', '宿州市', '宿州', '', '', '2', '340000', '安徽省 宿州市');
INSERT INTO dv_area_base VALUES ('341302', '埇桥区', '', '', '', '3', '341300', '安徽省 宿州市 埇桥区');
INSERT INTO dv_area_base VALUES ('341321', '砀山县', '', '', '', '3', '341300', '安徽省 宿州市 砀山县');
INSERT INTO dv_area_base VALUES ('341322', '萧县', '', '', '', '3', '341300', '安徽省 宿州市 萧县');
INSERT INTO dv_area_base VALUES ('341323', '灵璧县', '', '', '', '3', '341300', '安徽省 宿州市 灵璧县');
INSERT INTO dv_area_base VALUES ('341324', '泗县', '', '', '', '3', '341300', '安徽省 宿州市 泗县');
INSERT INTO dv_area_base VALUES ('341500', '六安市', '六安', '', '', '2', '340000', '安徽省 六安市');
INSERT INTO dv_area_base VALUES ('341502', '金安区', '', '', '', '3', '341500', '安徽省 六安市 金安区');
INSERT INTO dv_area_base VALUES ('341503', '裕安区', '', '', '', '3', '341500', '安徽省 六安市 裕安区');
INSERT INTO dv_area_base VALUES ('341521', '寿县', '', '', '', '3', '341500', '安徽省 六安市 寿县');
INSERT INTO dv_area_base VALUES ('341522', '霍邱县', '', '', '', '3', '341500', '安徽省 六安市 霍邱县');
INSERT INTO dv_area_base VALUES ('341523', '舒城县', '', '', '', '3', '341500', '安徽省 六安市 舒城县');
INSERT INTO dv_area_base VALUES ('341524', '金寨县', '', '', '', '3', '341500', '安徽省 六安市 金寨县');
INSERT INTO dv_area_base VALUES ('341525', '霍山县', '', '', '', '3', '341500', '安徽省 六安市 霍山县');
INSERT INTO dv_area_base VALUES ('341600', '亳州市', '亳州', '', '', '2', '340000', '安徽省 亳州市');
INSERT INTO dv_area_base VALUES ('341602', '谯城区', '', '', '', '3', '341600', '安徽省 亳州市 谯城区');
INSERT INTO dv_area_base VALUES ('341621', '涡阳县', '', '', '', '3', '341600', '安徽省 亳州市 涡阳县');
INSERT INTO dv_area_base VALUES ('341622', '蒙城县', '', '', '', '3', '341600', '安徽省 亳州市 蒙城县');
INSERT INTO dv_area_base VALUES ('341623', '利辛县', '', '', '', '3', '341600', '安徽省 亳州市 利辛县');
INSERT INTO dv_area_base VALUES ('341700', '池州市', '池州', '', '', '2', '340000', '安徽省 池州市');
INSERT INTO dv_area_base VALUES ('341702', '贵池区', '', '', '', '3', '341700', '安徽省 池州市 贵池区');
INSERT INTO dv_area_base VALUES ('341721', '东至县', '', '', '', '3', '341700', '安徽省 池州市 东至县');
INSERT INTO dv_area_base VALUES ('341722', '石台县', '', '', '', '3', '341700', '安徽省 池州市 石台县');
INSERT INTO dv_area_base VALUES ('341723', '青阳县', '', '', '', '3', '341700', '安徽省 池州市 青阳县');
INSERT INTO dv_area_base VALUES ('341800', '宣城市', '宣城', '', '', '2', '340000', '安徽省 宣城市');
INSERT INTO dv_area_base VALUES ('341802', '宣州区', '', '', '', '3', '341800', '安徽省 宣城市 宣州区');
INSERT INTO dv_area_base VALUES ('341821', '郎溪县', '', '', '', '3', '341800', '安徽省 宣城市 郎溪县');
INSERT INTO dv_area_base VALUES ('341822', '广德县', '', '', '', '3', '341800', '安徽省 宣城市 广德县');
INSERT INTO dv_area_base VALUES ('341823', '泾县', '', '', '', '3', '341800', '安徽省 宣城市 泾县');
INSERT INTO dv_area_base VALUES ('341824', '绩溪县', '', '', '', '3', '341800', '安徽省 宣城市 绩溪县');
INSERT INTO dv_area_base VALUES ('341825', '旌德县', '', '', '', '3', '341800', '安徽省 宣城市 旌德县');
INSERT INTO dv_area_base VALUES ('341881', '宁国市', '', '', '', '3', '341800', '安徽省 宣城市 宁国市');
INSERT INTO dv_area_base VALUES ('350000', '福建省', '福建', 'fujian', '闽', '1', '0', '福建省');
INSERT INTO dv_area_base VALUES ('350100', '福州市', '福州', '', '', '2', '350000', '福建省 福州市');
INSERT INTO dv_area_base VALUES ('350102', '鼓楼区', '', '', '', '3', '350100', '福建省 福州市 鼓楼区');
INSERT INTO dv_area_base VALUES ('350103', '台江区', '', '', '', '3', '350100', '福建省 福州市 台江区');
INSERT INTO dv_area_base VALUES ('350104', '仓山区', '', '', '', '3', '350100', '福建省 福州市 仓山区');
INSERT INTO dv_area_base VALUES ('350105', '马尾区', '', '', '', '3', '350100', '福建省 福州市 马尾区');
INSERT INTO dv_area_base VALUES ('350111', '晋安区', '', '', '', '3', '350100', '福建省 福州市 晋安区');
INSERT INTO dv_area_base VALUES ('350121', '闽侯县', '', '', '', '3', '350100', '福建省 福州市 闽侯县');
INSERT INTO dv_area_base VALUES ('350122', '连江县', '', '', '', '3', '350100', '福建省 福州市 连江县');
INSERT INTO dv_area_base VALUES ('350123', '罗源县', '', '', '', '3', '350100', '福建省 福州市 罗源县');
INSERT INTO dv_area_base VALUES ('350124', '闽清县', '', '', '', '3', '350100', '福建省 福州市 闽清县');
INSERT INTO dv_area_base VALUES ('350125', '永泰县', '', '', '', '3', '350100', '福建省 福州市 永泰县');
INSERT INTO dv_area_base VALUES ('350128', '平潭县', '', '', '', '3', '350100', '福建省 福州市 平潭县');
INSERT INTO dv_area_base VALUES ('350181', '福清市', '', '', '', '3', '350100', '福建省 福州市 福清市');
INSERT INTO dv_area_base VALUES ('350182', '长乐市', '', '', '', '3', '350100', '福建省 福州市 长乐市');
INSERT INTO dv_area_base VALUES ('350200', '厦门市', '厦门', '', '', '2', '350000', '福建省 厦门市');
INSERT INTO dv_area_base VALUES ('350203', '思明区', '', '', '', '3', '350200', '福建省 厦门市 思明区');
INSERT INTO dv_area_base VALUES ('350205', '海沧区', '', '', '', '3', '350200', '福建省 厦门市 海沧区');
INSERT INTO dv_area_base VALUES ('350206', '湖里区', '', '', '', '3', '350200', '福建省 厦门市 湖里区');
INSERT INTO dv_area_base VALUES ('350211', '集美区', '', '', '', '3', '350200', '福建省 厦门市 集美区');
INSERT INTO dv_area_base VALUES ('350212', '同安区', '', '', '', '3', '350200', '福建省 厦门市 同安区');
INSERT INTO dv_area_base VALUES ('350213', '翔安区', '', '', '', '3', '350200', '福建省 厦门市 翔安区');
INSERT INTO dv_area_base VALUES ('350300', '莆田市', '莆田', '', '', '2', '350000', '福建省 莆田市');
INSERT INTO dv_area_base VALUES ('350302', '城厢区', '', '', '', '3', '350300', '福建省 莆田市 城厢区');
INSERT INTO dv_area_base VALUES ('350303', '涵江区', '', '', '', '3', '350300', '福建省 莆田市 涵江区');
INSERT INTO dv_area_base VALUES ('350304', '荔城区', '', '', '', '3', '350300', '福建省 莆田市 荔城区');
INSERT INTO dv_area_base VALUES ('350305', '秀屿区', '', '', '', '3', '350300', '福建省 莆田市 秀屿区');
INSERT INTO dv_area_base VALUES ('350322', '仙游县', '', '', '', '3', '350300', '福建省 莆田市 仙游县');
INSERT INTO dv_area_base VALUES ('350400', '三明市', '三明', '', '', '2', '350000', '福建省 三明市');
INSERT INTO dv_area_base VALUES ('350402', '梅列区', '', '', '', '3', '350400', '福建省 三明市 梅列区');
INSERT INTO dv_area_base VALUES ('350403', '三元区', '', '', '', '3', '350400', '福建省 三明市 三元区');
INSERT INTO dv_area_base VALUES ('350421', '明溪县', '', '', '', '3', '350400', '福建省 三明市 明溪县');
INSERT INTO dv_area_base VALUES ('350423', '清流县', '', '', '', '3', '350400', '福建省 三明市 清流县');
INSERT INTO dv_area_base VALUES ('350424', '宁化县', '', '', '', '3', '350400', '福建省 三明市 宁化县');
INSERT INTO dv_area_base VALUES ('350425', '大田县', '', '', '', '3', '350400', '福建省 三明市 大田县');
INSERT INTO dv_area_base VALUES ('350426', '尤溪县', '', '', '', '3', '350400', '福建省 三明市 尤溪县');
INSERT INTO dv_area_base VALUES ('350427', '沙县', '', '', '', '3', '350400', '福建省 三明市 沙县');
INSERT INTO dv_area_base VALUES ('350428', '将乐县', '', '', '', '3', '350400', '福建省 三明市 将乐县');
INSERT INTO dv_area_base VALUES ('350429', '泰宁县', '', '', '', '3', '350400', '福建省 三明市 泰宁县');
INSERT INTO dv_area_base VALUES ('350430', '建宁县', '', '', '', '3', '350400', '福建省 三明市 建宁县');
INSERT INTO dv_area_base VALUES ('350481', '永安市', '', '', '', '3', '350400', '福建省 三明市 永安市');
INSERT INTO dv_area_base VALUES ('350500', '泉州市', '泉州', '', '', '2', '350000', '福建省 泉州市');
INSERT INTO dv_area_base VALUES ('350502', '鲤城区', '', '', '', '3', '350500', '福建省 泉州市 鲤城区');
INSERT INTO dv_area_base VALUES ('350503', '丰泽区', '', '', '', '3', '350500', '福建省 泉州市 丰泽区');
INSERT INTO dv_area_base VALUES ('350504', '洛江区', '', '', '', '3', '350500', '福建省 泉州市 洛江区');
INSERT INTO dv_area_base VALUES ('350505', '泉港区', '', '', '', '3', '350500', '福建省 泉州市 泉港区');
INSERT INTO dv_area_base VALUES ('350521', '惠安县', '', '', '', '3', '350500', '福建省 泉州市 惠安县');
INSERT INTO dv_area_base VALUES ('350524', '安溪县', '', '', '', '3', '350500', '福建省 泉州市 安溪县');
INSERT INTO dv_area_base VALUES ('350525', '永春县', '', '', '', '3', '350500', '福建省 泉州市 永春县');
INSERT INTO dv_area_base VALUES ('350526', '德化县', '', '', '', '3', '350500', '福建省 泉州市 德化县');
INSERT INTO dv_area_base VALUES ('350527', '金门县', '', '', '', '3', '350500', '福建省 泉州市 金门县');
INSERT INTO dv_area_base VALUES ('350581', '石狮市', '', '', '', '3', '350500', '福建省 泉州市 石狮市');
INSERT INTO dv_area_base VALUES ('350582', '晋江市', '', '', '', '3', '350500', '福建省 泉州市 晋江市');
INSERT INTO dv_area_base VALUES ('350583', '南安市', '', '', '', '3', '350500', '福建省 泉州市 南安市');
INSERT INTO dv_area_base VALUES ('350600', '漳州市', '漳州', '', '', '2', '350000', '福建省 漳州市');
INSERT INTO dv_area_base VALUES ('350602', '芗城区', '', '', '', '3', '350600', '福建省 漳州市 芗城区');
INSERT INTO dv_area_base VALUES ('350603', '龙文区', '', '', '', '3', '350600', '福建省 漳州市 龙文区');
INSERT INTO dv_area_base VALUES ('350622', '云霄县', '', '', '', '3', '350600', '福建省 漳州市 云霄县');
INSERT INTO dv_area_base VALUES ('350623', '漳浦县', '', '', '', '3', '350600', '福建省 漳州市 漳浦县');
INSERT INTO dv_area_base VALUES ('350624', '诏安县', '', '', '', '3', '350600', '福建省 漳州市 诏安县');
INSERT INTO dv_area_base VALUES ('350625', '长泰县', '', '', '', '3', '350600', '福建省 漳州市 长泰县');
INSERT INTO dv_area_base VALUES ('350626', '东山县', '', '', '', '3', '350600', '福建省 漳州市 东山县');
INSERT INTO dv_area_base VALUES ('350627', '南靖县', '', '', '', '3', '350600', '福建省 漳州市 南靖县');
INSERT INTO dv_area_base VALUES ('350628', '平和县', '', '', '', '3', '350600', '福建省 漳州市 平和县');
INSERT INTO dv_area_base VALUES ('350629', '华安县', '', '', '', '3', '350600', '福建省 漳州市 华安县');
INSERT INTO dv_area_base VALUES ('350681', '龙海市', '', '', '', '3', '350600', '福建省 漳州市 龙海市');
INSERT INTO dv_area_base VALUES ('350700', '南平市', '南平', '', '', '2', '350000', '福建省 南平市');
INSERT INTO dv_area_base VALUES ('350702', '延平区', '', '', '', '3', '350700', '福建省 南平市 延平区');
INSERT INTO dv_area_base VALUES ('350721', '顺昌县', '', '', '', '3', '350700', '福建省 南平市 顺昌县');
INSERT INTO dv_area_base VALUES ('350722', '浦城县', '', '', '', '3', '350700', '福建省 南平市 浦城县');
INSERT INTO dv_area_base VALUES ('350723', '光泽县', '', '', '', '3', '350700', '福建省 南平市 光泽县');
INSERT INTO dv_area_base VALUES ('350724', '松溪县', '', '', '', '3', '350700', '福建省 南平市 松溪县');
INSERT INTO dv_area_base VALUES ('350725', '政和县', '', '', '', '3', '350700', '福建省 南平市 政和县');
INSERT INTO dv_area_base VALUES ('350781', '邵武市', '', '', '', '3', '350700', '福建省 南平市 邵武市');
INSERT INTO dv_area_base VALUES ('350782', '武夷山市', '', '', '', '3', '350700', '福建省 南平市 武夷山市');
INSERT INTO dv_area_base VALUES ('350783', '建瓯市', '', '', '', '3', '350700', '福建省 南平市 建瓯市');
INSERT INTO dv_area_base VALUES ('350784', '建阳市', '', '', '', '3', '350700', '福建省 南平市 建阳市');
INSERT INTO dv_area_base VALUES ('350800', '龙岩市', '龙岩', '', '', '2', '350000', '福建省 龙岩市');
INSERT INTO dv_area_base VALUES ('350802', '新罗区', '', '', '', '3', '350800', '福建省 龙岩市 新罗区');
INSERT INTO dv_area_base VALUES ('350821', '长汀县', '', '', '', '3', '350800', '福建省 龙岩市 长汀县');
INSERT INTO dv_area_base VALUES ('350822', '永定县', '', '', '', '3', '350800', '福建省 龙岩市 永定县');
INSERT INTO dv_area_base VALUES ('350823', '上杭县', '', '', '', '3', '350800', '福建省 龙岩市 上杭县');
INSERT INTO dv_area_base VALUES ('350824', '武平县', '', '', '', '3', '350800', '福建省 龙岩市 武平县');
INSERT INTO dv_area_base VALUES ('350825', '连城县', '', '', '', '3', '350800', '福建省 龙岩市 连城县');
INSERT INTO dv_area_base VALUES ('350881', '漳平市', '', '', '', '3', '350800', '福建省 龙岩市 漳平市');
INSERT INTO dv_area_base VALUES ('350900', '宁德市', '宁德', '', '', '2', '350000', '福建省 宁德市');
INSERT INTO dv_area_base VALUES ('350902', '蕉城区', '', '', '', '3', '350900', '福建省 宁德市 蕉城区');
INSERT INTO dv_area_base VALUES ('350921', '霞浦县', '', '', '', '3', '350900', '福建省 宁德市 霞浦县');
INSERT INTO dv_area_base VALUES ('350922', '古田县', '', '', '', '3', '350900', '福建省 宁德市 古田县');
INSERT INTO dv_area_base VALUES ('350923', '屏南县', '', '', '', '3', '350900', '福建省 宁德市 屏南县');
INSERT INTO dv_area_base VALUES ('350924', '寿宁县', '', '', '', '3', '350900', '福建省 宁德市 寿宁县');
INSERT INTO dv_area_base VALUES ('350925', '周宁县', '', '', '', '3', '350900', '福建省 宁德市 周宁县');
INSERT INTO dv_area_base VALUES ('350926', '柘荣县', '', '', '', '3', '350900', '福建省 宁德市 柘荣县');
INSERT INTO dv_area_base VALUES ('350981', '福安市', '', '', '', '3', '350900', '福建省 宁德市 福安市');
INSERT INTO dv_area_base VALUES ('350982', '福鼎市', '', '', '', '3', '350900', '福建省 宁德市 福鼎市');
INSERT INTO dv_area_base VALUES ('360000', '江西省', '江西', 'jiangxi', '赣', '1', '0', '江西省');
INSERT INTO dv_area_base VALUES ('360100', '南昌市', '南昌', '', '', '2', '360000', '江西省 南昌市');
INSERT INTO dv_area_base VALUES ('360102', '东湖区', '', '', '', '3', '360100', '江西省 南昌市 东湖区');
INSERT INTO dv_area_base VALUES ('360103', '西湖区', '', '', '', '3', '360100', '江西省 南昌市 西湖区');
INSERT INTO dv_area_base VALUES ('360104', '青云谱区', '', '', '', '3', '360100', '江西省 南昌市 青云谱区');
INSERT INTO dv_area_base VALUES ('360105', '湾里区', '', '', '', '3', '360100', '江西省 南昌市 湾里区');
INSERT INTO dv_area_base VALUES ('360111', '青山湖区', '', '', '', '3', '360100', '江西省 南昌市 青山湖区');
INSERT INTO dv_area_base VALUES ('360121', '南昌县', '', '', '', '3', '360100', '江西省 南昌市 南昌县');
INSERT INTO dv_area_base VALUES ('360122', '新建县', '', '', '', '3', '360100', '江西省 南昌市 新建县');
INSERT INTO dv_area_base VALUES ('360123', '安义县', '', '', '', '3', '360100', '江西省 南昌市 安义县');
INSERT INTO dv_area_base VALUES ('360124', '进贤县', '', '', '', '3', '360100', '江西省 南昌市 进贤县');
INSERT INTO dv_area_base VALUES ('360200', '景德镇市', '景德镇', '', '', '2', '360000', '江西省 景德镇市');
INSERT INTO dv_area_base VALUES ('360202', '昌江区', '', '', '', '3', '360200', '江西省 景德镇市 昌江区');
INSERT INTO dv_area_base VALUES ('360203', '珠山区', '', '', '', '3', '360200', '江西省 景德镇市 珠山区');
INSERT INTO dv_area_base VALUES ('360222', '浮梁县', '', '', '', '3', '360200', '江西省 景德镇市 浮梁县');
INSERT INTO dv_area_base VALUES ('360281', '乐平市', '', '', '', '3', '360200', '江西省 景德镇市 乐平市');
INSERT INTO dv_area_base VALUES ('360300', '萍乡市', '萍乡', '', '', '2', '360000', '江西省 萍乡市');
INSERT INTO dv_area_base VALUES ('360302', '安源区', '', '', '', '3', '360300', '江西省 萍乡市 安源区');
INSERT INTO dv_area_base VALUES ('360313', '湘东区', '', '', '', '3', '360300', '江西省 萍乡市 湘东区');
INSERT INTO dv_area_base VALUES ('360321', '莲花县', '', '', '', '3', '360300', '江西省 萍乡市 莲花县');
INSERT INTO dv_area_base VALUES ('360322', '上栗县', '', '', '', '3', '360300', '江西省 萍乡市 上栗县');
INSERT INTO dv_area_base VALUES ('360323', '芦溪县', '', '', '', '3', '360300', '江西省 萍乡市 芦溪县');
INSERT INTO dv_area_base VALUES ('360400', '九江市', '九江', '', '', '2', '360000', '江西省 九江市');
INSERT INTO dv_area_base VALUES ('360402', '庐山区', '', '', '', '3', '360400', '江西省 九江市 庐山区');
INSERT INTO dv_area_base VALUES ('360403', '浔阳区', '', '', '', '3', '360400', '江西省 九江市 浔阳区');
INSERT INTO dv_area_base VALUES ('360421', '九江县', '', '', '', '3', '360400', '江西省 九江市 九江县');
INSERT INTO dv_area_base VALUES ('360423', '武宁县', '', '', '', '3', '360400', '江西省 九江市 武宁县');
INSERT INTO dv_area_base VALUES ('360424', '修水县', '', '', '', '3', '360400', '江西省 九江市 修水县');
INSERT INTO dv_area_base VALUES ('360425', '永修县', '', '', '', '3', '360400', '江西省 九江市 永修县');
INSERT INTO dv_area_base VALUES ('360426', '德安县', '', '', '', '3', '360400', '江西省 九江市 德安县');
INSERT INTO dv_area_base VALUES ('360427', '星子县', '', '', '', '3', '360400', '江西省 九江市 星子县');
INSERT INTO dv_area_base VALUES ('360428', '都昌县', '', '', '', '3', '360400', '江西省 九江市 都昌县');
INSERT INTO dv_area_base VALUES ('360429', '湖口县', '', '', '', '3', '360400', '江西省 九江市 湖口县');
INSERT INTO dv_area_base VALUES ('360430', '彭泽县', '', '', '', '3', '360400', '江西省 九江市 彭泽县');
INSERT INTO dv_area_base VALUES ('360481', '瑞昌市', '', '', '', '3', '360400', '江西省 九江市 瑞昌市');
INSERT INTO dv_area_base VALUES ('360500', '新余市', '新余', '', '', '2', '360000', '江西省 新余市');
INSERT INTO dv_area_base VALUES ('360502', '渝水区', '', '', '', '3', '360500', '江西省 新余市 渝水区');
INSERT INTO dv_area_base VALUES ('360521', '分宜县', '', '', '', '3', '360500', '江西省 新余市 分宜县');
INSERT INTO dv_area_base VALUES ('360600', '鹰潭市', '鹰潭', '', '', '2', '360000', '江西省 鹰潭市');
INSERT INTO dv_area_base VALUES ('360602', '月湖区', '', '', '', '3', '360600', '江西省 鹰潭市 月湖区');
INSERT INTO dv_area_base VALUES ('360622', '余江县', '', '', '', '3', '360600', '江西省 鹰潭市 余江县');
INSERT INTO dv_area_base VALUES ('360681', '贵溪市', '', '', '', '3', '360600', '江西省 鹰潭市 贵溪市');
INSERT INTO dv_area_base VALUES ('360700', '赣州市', '赣州', '', '', '2', '360000', '江西省 赣州市');
INSERT INTO dv_area_base VALUES ('360702', '章贡区', '', '', '', '3', '360700', '江西省 赣州市 章贡区');
INSERT INTO dv_area_base VALUES ('360721', '赣县', '', '', '', '3', '360700', '江西省 赣州市 赣县');
INSERT INTO dv_area_base VALUES ('360722', '信丰县', '', '', '', '3', '360700', '江西省 赣州市 信丰县');
INSERT INTO dv_area_base VALUES ('360723', '大余县', '', '', '', '3', '360700', '江西省 赣州市 大余县');
INSERT INTO dv_area_base VALUES ('360724', '上犹县', '', '', '', '3', '360700', '江西省 赣州市 上犹县');
INSERT INTO dv_area_base VALUES ('360725', '崇义县', '', '', '', '3', '360700', '江西省 赣州市 崇义县');
INSERT INTO dv_area_base VALUES ('360726', '安远县', '', '', '', '3', '360700', '江西省 赣州市 安远县');
INSERT INTO dv_area_base VALUES ('360727', '龙南县', '', '', '', '3', '360700', '江西省 赣州市 龙南县');
INSERT INTO dv_area_base VALUES ('360728', '定南县', '', '', '', '3', '360700', '江西省 赣州市 定南县');
INSERT INTO dv_area_base VALUES ('360729', '全南县', '', '', '', '3', '360700', '江西省 赣州市 全南县');
INSERT INTO dv_area_base VALUES ('360730', '宁都县', '', '', '', '3', '360700', '江西省 赣州市 宁都县');
INSERT INTO dv_area_base VALUES ('360731', '于都县', '', '', '', '3', '360700', '江西省 赣州市 于都县');
INSERT INTO dv_area_base VALUES ('360732', '兴国县', '', '', '', '3', '360700', '江西省 赣州市 兴国县');
INSERT INTO dv_area_base VALUES ('360733', '会昌县', '', '', '', '3', '360700', '江西省 赣州市 会昌县');
INSERT INTO dv_area_base VALUES ('360734', '寻乌县', '', '', '', '3', '360700', '江西省 赣州市 寻乌县');
INSERT INTO dv_area_base VALUES ('360735', '石城县', '', '', '', '3', '360700', '江西省 赣州市 石城县');
INSERT INTO dv_area_base VALUES ('360781', '瑞金市', '', '', '', '3', '360700', '江西省 赣州市 瑞金市');
INSERT INTO dv_area_base VALUES ('360782', '南康市', '', '', '', '3', '360700', '江西省 赣州市 南康市');
INSERT INTO dv_area_base VALUES ('360800', '吉安市', '吉安', '', '', '2', '360000', '江西省 吉安市');
INSERT INTO dv_area_base VALUES ('360802', '吉州区', '', '', '', '3', '360800', '江西省 吉安市 吉州区');
INSERT INTO dv_area_base VALUES ('360803', '青原区', '', '', '', '3', '360800', '江西省 吉安市 青原区');
INSERT INTO dv_area_base VALUES ('360821', '吉安县', '', '', '', '3', '360800', '江西省 吉安市 吉安县');
INSERT INTO dv_area_base VALUES ('360822', '吉水县', '', '', '', '3', '360800', '江西省 吉安市 吉水县');
INSERT INTO dv_area_base VALUES ('360823', '峡江县', '', '', '', '3', '360800', '江西省 吉安市 峡江县');
INSERT INTO dv_area_base VALUES ('360824', '新干县', '', '', '', '3', '360800', '江西省 吉安市 新干县');
INSERT INTO dv_area_base VALUES ('360825', '永丰县', '', '', '', '3', '360800', '江西省 吉安市 永丰县');
INSERT INTO dv_area_base VALUES ('360826', '泰和县', '', '', '', '3', '360800', '江西省 吉安市 泰和县');
INSERT INTO dv_area_base VALUES ('360827', '遂川县', '', '', '', '3', '360800', '江西省 吉安市 遂川县');
INSERT INTO dv_area_base VALUES ('360828', '万安县', '', '', '', '3', '360800', '江西省 吉安市 万安县');
INSERT INTO dv_area_base VALUES ('360829', '安福县', '', '', '', '3', '360800', '江西省 吉安市 安福县');
INSERT INTO dv_area_base VALUES ('360830', '永新县', '', '', '', '3', '360800', '江西省 吉安市 永新县');
INSERT INTO dv_area_base VALUES ('360881', '井冈山市', '', '', '', '3', '360800', '江西省 吉安市 井冈山市');
INSERT INTO dv_area_base VALUES ('360900', '宜春市', '宜春', '', '', '2', '360000', '江西省 宜春市');
INSERT INTO dv_area_base VALUES ('360902', '袁州区', '', '', '', '3', '360900', '江西省 宜春市 袁州区');
INSERT INTO dv_area_base VALUES ('360921', '奉新县', '', '', '', '3', '360900', '江西省 宜春市 奉新县');
INSERT INTO dv_area_base VALUES ('360922', '万载县', '', '', '', '3', '360900', '江西省 宜春市 万载县');
INSERT INTO dv_area_base VALUES ('360923', '上高县', '', '', '', '3', '360900', '江西省 宜春市 上高县');
INSERT INTO dv_area_base VALUES ('360924', '宜丰县', '', '', '', '3', '360900', '江西省 宜春市 宜丰县');
INSERT INTO dv_area_base VALUES ('360925', '靖安县', '', '', '', '3', '360900', '江西省 宜春市 靖安县');
INSERT INTO dv_area_base VALUES ('360926', '铜鼓县', '', '', '', '3', '360900', '江西省 宜春市 铜鼓县');
INSERT INTO dv_area_base VALUES ('360981', '丰城市', '', '', '', '3', '360900', '江西省 宜春市 丰城市');
INSERT INTO dv_area_base VALUES ('360982', '樟树市', '', '', '', '3', '360900', '江西省 宜春市 樟树市');
INSERT INTO dv_area_base VALUES ('360983', '高安市', '', '', '', '3', '360900', '江西省 宜春市 高安市');
INSERT INTO dv_area_base VALUES ('361000', '抚州市', '抚州', '', '', '2', '360000', '江西省 抚州市');
INSERT INTO dv_area_base VALUES ('361002', '临川区', '', '', '', '3', '361000', '江西省 抚州市 临川区');
INSERT INTO dv_area_base VALUES ('361021', '南城县', '', '', '', '3', '361000', '江西省 抚州市 南城县');
INSERT INTO dv_area_base VALUES ('361022', '黎川县', '', '', '', '3', '361000', '江西省 抚州市 黎川县');
INSERT INTO dv_area_base VALUES ('361023', '南丰县', '', '', '', '3', '361000', '江西省 抚州市 南丰县');
INSERT INTO dv_area_base VALUES ('361024', '崇仁县', '', '', '', '3', '361000', '江西省 抚州市 崇仁县');
INSERT INTO dv_area_base VALUES ('361025', '乐安县', '', '', '', '3', '361000', '江西省 抚州市 乐安县');
INSERT INTO dv_area_base VALUES ('361026', '宜黄县', '', '', '', '3', '361000', '江西省 抚州市 宜黄县');
INSERT INTO dv_area_base VALUES ('361027', '金溪县', '', '', '', '3', '361000', '江西省 抚州市 金溪县');
INSERT INTO dv_area_base VALUES ('361028', '资溪县', '', '', '', '3', '361000', '江西省 抚州市 资溪县');
INSERT INTO dv_area_base VALUES ('361029', '东乡县', '', '', '', '3', '361000', '江西省 抚州市 东乡县');
INSERT INTO dv_area_base VALUES ('361030', '广昌县', '', '', '', '3', '361000', '江西省 抚州市 广昌县');
INSERT INTO dv_area_base VALUES ('361100', '上饶市', '上饶', '', '', '2', '360000', '江西省 上饶市');
INSERT INTO dv_area_base VALUES ('361102', '信州区', '', '', '', '3', '361100', '江西省 上饶市 信州区');
INSERT INTO dv_area_base VALUES ('361121', '上饶县', '', '', '', '3', '361100', '江西省 上饶市 上饶县');
INSERT INTO dv_area_base VALUES ('361122', '广丰县', '', '', '', '3', '361100', '江西省 上饶市 广丰县');
INSERT INTO dv_area_base VALUES ('361123', '玉山县', '', '', '', '3', '361100', '江西省 上饶市 玉山县');
INSERT INTO dv_area_base VALUES ('361124', '铅山县', '', '', '', '3', '361100', '江西省 上饶市 铅山县');
INSERT INTO dv_area_base VALUES ('361125', '横峰县', '', '', '', '3', '361100', '江西省 上饶市 横峰县');
INSERT INTO dv_area_base VALUES ('361126', '弋阳县', '', '', '', '3', '361100', '江西省 上饶市 弋阳县');
INSERT INTO dv_area_base VALUES ('361127', '余干县', '', '', '', '3', '361100', '江西省 上饶市 余干县');
INSERT INTO dv_area_base VALUES ('361128', '鄱阳县', '', '', '', '3', '361100', '江西省 上饶市 鄱阳县');
INSERT INTO dv_area_base VALUES ('361129', '万年县', '', '', '', '3', '361100', '江西省 上饶市 万年县');
INSERT INTO dv_area_base VALUES ('361130', '婺源县', '', '', '', '3', '361100', '江西省 上饶市 婺源县');
INSERT INTO dv_area_base VALUES ('361181', '德兴市', '', '', '', '3', '361100', '江西省 上饶市 德兴市');
INSERT INTO dv_area_base VALUES ('370000', '山东省', '山东', 'shandong', '鲁', '1', '0', '山东省');
INSERT INTO dv_area_base VALUES ('370100', '济南市', '济南', '', '', '2', '370000', '山东省 济南市');
INSERT INTO dv_area_base VALUES ('370102', '历下区', '', '', '', '3', '370100', '山东省 济南市 历下区');
INSERT INTO dv_area_base VALUES ('370103', '市中区', '', '', '', '3', '370100', '山东省 济南市 市中区');
INSERT INTO dv_area_base VALUES ('370104', '槐荫区', '', '', '', '3', '370100', '山东省 济南市 槐荫区');
INSERT INTO dv_area_base VALUES ('370105', '天桥区', '', '', '', '3', '370100', '山东省 济南市 天桥区');
INSERT INTO dv_area_base VALUES ('370112', '历城区', '', '', '', '3', '370100', '山东省 济南市 历城区');
INSERT INTO dv_area_base VALUES ('370113', '长清区', '', '', '', '3', '370100', '山东省 济南市 长清区');
INSERT INTO dv_area_base VALUES ('370124', '平阴县', '', '', '', '3', '370100', '山东省 济南市 平阴县');
INSERT INTO dv_area_base VALUES ('370125', '济阳县', '', '', '', '3', '370100', '山东省 济南市 济阳县');
INSERT INTO dv_area_base VALUES ('370126', '商河县', '', '', '', '3', '370100', '山东省 济南市 商河县');
INSERT INTO dv_area_base VALUES ('370181', '章丘市', '', '', '', '3', '370100', '山东省 济南市 章丘市');
INSERT INTO dv_area_base VALUES ('370200', '青岛市', '青岛', '', '', '2', '370000', '山东省 青岛市');
INSERT INTO dv_area_base VALUES ('370202', '市南区', '', '', '', '3', '370200', '山东省 青岛市 市南区');
INSERT INTO dv_area_base VALUES ('370203', '市北区', '', '', '', '3', '370200', '山东省 青岛市 市北区');
INSERT INTO dv_area_base VALUES ('370205', '四方区', '', '', '', '3', '370200', '山东省 青岛市 四方区');
INSERT INTO dv_area_base VALUES ('370211', '黄岛区', '', '', '', '3', '370200', '山东省 青岛市 黄岛区');
INSERT INTO dv_area_base VALUES ('370212', '崂山区', '', '', '', '3', '370200', '山东省 青岛市 崂山区');
INSERT INTO dv_area_base VALUES ('370213', '李沧区', '', '', '', '3', '370200', '山东省 青岛市 李沧区');
INSERT INTO dv_area_base VALUES ('370214', '城阳区', '', '', '', '3', '370200', '山东省 青岛市 城阳区');
INSERT INTO dv_area_base VALUES ('370281', '胶州市', '', '', '', '3', '370200', '山东省 青岛市 胶州市');
INSERT INTO dv_area_base VALUES ('370282', '即墨市', '', '', '', '3', '370200', '山东省 青岛市 即墨市');
INSERT INTO dv_area_base VALUES ('370283', '平度市', '', '', '', '3', '370200', '山东省 青岛市 平度市');
INSERT INTO dv_area_base VALUES ('370284', '胶南市', '', '', '', '3', '370200', '山东省 青岛市 胶南市');
INSERT INTO dv_area_base VALUES ('370285', '莱西市', '', '', '', '3', '370200', '山东省 青岛市 莱西市');
INSERT INTO dv_area_base VALUES ('370300', '淄博市', '淄博', '', '', '2', '370000', '山东省 淄博市');
INSERT INTO dv_area_base VALUES ('370302', '淄川区', '', '', '', '3', '370300', '山东省 淄博市 淄川区');
INSERT INTO dv_area_base VALUES ('370303', '张店区', '', '', '', '3', '370300', '山东省 淄博市 张店区');
INSERT INTO dv_area_base VALUES ('370304', '博山区', '', '', '', '3', '370300', '山东省 淄博市 博山区');
INSERT INTO dv_area_base VALUES ('370305', '临淄区', '', '', '', '3', '370300', '山东省 淄博市 临淄区');
INSERT INTO dv_area_base VALUES ('370306', '周村区', '', '', '', '3', '370300', '山东省 淄博市 周村区');
INSERT INTO dv_area_base VALUES ('370321', '桓台县', '', '', '', '3', '370300', '山东省 淄博市 桓台县');
INSERT INTO dv_area_base VALUES ('370322', '高青县', '', '', '', '3', '370300', '山东省 淄博市 高青县');
INSERT INTO dv_area_base VALUES ('370323', '沂源县', '', '', '', '3', '370300', '山东省 淄博市 沂源县');
INSERT INTO dv_area_base VALUES ('370400', '枣庄市', '枣庄', '', '', '2', '370000', '山东省 枣庄市');
INSERT INTO dv_area_base VALUES ('370402', '市中区', '', '', '', '3', '370400', '山东省 枣庄市 市中区');
INSERT INTO dv_area_base VALUES ('370403', '薛城区', '', '', '', '3', '370400', '山东省 枣庄市 薛城区');
INSERT INTO dv_area_base VALUES ('370404', '峄城区', '', '', '', '3', '370400', '山东省 枣庄市 峄城区');
INSERT INTO dv_area_base VALUES ('370405', '台儿庄区', '', '', '', '3', '370400', '山东省 枣庄市 台儿庄区');
INSERT INTO dv_area_base VALUES ('370406', '山亭区', '', '', '', '3', '370400', '山东省 枣庄市 山亭区');
INSERT INTO dv_area_base VALUES ('370481', '滕州市', '', '', '', '3', '370400', '山东省 枣庄市 滕州市');
INSERT INTO dv_area_base VALUES ('370500', '东营市', '东营', '', '', '2', '370000', '山东省 东营市');
INSERT INTO dv_area_base VALUES ('370502', '东营区', '', '', '', '3', '370500', '山东省 东营市 东营区');
INSERT INTO dv_area_base VALUES ('370503', '河口区', '', '', '', '3', '370500', '山东省 东营市 河口区');
INSERT INTO dv_area_base VALUES ('370521', '垦利县', '', '', '', '3', '370500', '山东省 东营市 垦利县');
INSERT INTO dv_area_base VALUES ('370522', '利津县', '', '', '', '3', '370500', '山东省 东营市 利津县');
INSERT INTO dv_area_base VALUES ('370523', '广饶县', '', '', '', '3', '370500', '山东省 东营市 广饶县');
INSERT INTO dv_area_base VALUES ('370600', '烟台市', '烟台', '', '', '2', '370000', '山东省 烟台市');
INSERT INTO dv_area_base VALUES ('370602', '芝罘区', '', '', '', '3', '370600', '山东省 烟台市 芝罘区');
INSERT INTO dv_area_base VALUES ('370611', '福山区', '', '', '', '3', '370600', '山东省 烟台市 福山区');
INSERT INTO dv_area_base VALUES ('370612', '牟平区', '', '', '', '3', '370600', '山东省 烟台市 牟平区');
INSERT INTO dv_area_base VALUES ('370613', '莱山区', '', '', '', '3', '370600', '山东省 烟台市 莱山区');
INSERT INTO dv_area_base VALUES ('370634', '长岛县', '', '', '', '3', '370600', '山东省 烟台市 长岛县');
INSERT INTO dv_area_base VALUES ('370681', '龙口市', '', '', '', '3', '370600', '山东省 烟台市 龙口市');
INSERT INTO dv_area_base VALUES ('370682', '莱阳市', '', '', '', '3', '370600', '山东省 烟台市 莱阳市');
INSERT INTO dv_area_base VALUES ('370683', '莱州市', '', '', '', '3', '370600', '山东省 烟台市 莱州市');
INSERT INTO dv_area_base VALUES ('370684', '蓬莱市', '', '', '', '3', '370600', '山东省 烟台市 蓬莱市');
INSERT INTO dv_area_base VALUES ('370685', '招远市', '', '', '', '3', '370600', '山东省 烟台市 招远市');
INSERT INTO dv_area_base VALUES ('370686', '栖霞市', '', '', '', '3', '370600', '山东省 烟台市 栖霞市');
INSERT INTO dv_area_base VALUES ('370687', '海阳市', '', '', '', '3', '370600', '山东省 烟台市 海阳市');
INSERT INTO dv_area_base VALUES ('370700', '潍坊市', '潍坊', '', '', '2', '370000', '山东省 潍坊市');
INSERT INTO dv_area_base VALUES ('370702', '潍城区', '', '', '', '3', '370700', '山东省 潍坊市 潍城区');
INSERT INTO dv_area_base VALUES ('370703', '寒亭区', '', '', '', '3', '370700', '山东省 潍坊市 寒亭区');
INSERT INTO dv_area_base VALUES ('370704', '坊子区', '', '', '', '3', '370700', '山东省 潍坊市 坊子区');
INSERT INTO dv_area_base VALUES ('370705', '奎文区', '', '', '', '3', '370700', '山东省 潍坊市 奎文区');
INSERT INTO dv_area_base VALUES ('370724', '临朐县', '', '', '', '3', '370700', '山东省 潍坊市 临朐县');
INSERT INTO dv_area_base VALUES ('370725', '昌乐县', '', '', '', '3', '370700', '山东省 潍坊市 昌乐县');
INSERT INTO dv_area_base VALUES ('370781', '青州市', '', '', '', '3', '370700', '山东省 潍坊市 青州市');
INSERT INTO dv_area_base VALUES ('370782', '诸城市', '', '', '', '3', '370700', '山东省 潍坊市 诸城市');
INSERT INTO dv_area_base VALUES ('370783', '寿光市', '', '', '', '3', '370700', '山东省 潍坊市 寿光市');
INSERT INTO dv_area_base VALUES ('370784', '安丘市', '', '', '', '3', '370700', '山东省 潍坊市 安丘市');
INSERT INTO dv_area_base VALUES ('370785', '高密市', '', '', '', '3', '370700', '山东省 潍坊市 高密市');
INSERT INTO dv_area_base VALUES ('370786', '昌邑市', '', '', '', '3', '370700', '山东省 潍坊市 昌邑市');
INSERT INTO dv_area_base VALUES ('370800', '济宁市', '济宁', '', '', '2', '370000', '山东省 济宁市');
INSERT INTO dv_area_base VALUES ('370802', '市中区', '', '', '', '3', '370800', '山东省 济宁市 市中区');
INSERT INTO dv_area_base VALUES ('370811', '任城区', '', '', '', '3', '370800', '山东省 济宁市 任城区');
INSERT INTO dv_area_base VALUES ('370826', '微山县', '', '', '', '3', '370800', '山东省 济宁市 微山县');
INSERT INTO dv_area_base VALUES ('370827', '鱼台县', '', '', '', '3', '370800', '山东省 济宁市 鱼台县');
INSERT INTO dv_area_base VALUES ('370828', '金乡县', '', '', '', '3', '370800', '山东省 济宁市 金乡县');
INSERT INTO dv_area_base VALUES ('370829', '嘉祥县', '', '', '', '3', '370800', '山东省 济宁市 嘉祥县');
INSERT INTO dv_area_base VALUES ('370830', '汶上县', '', '', '', '3', '370800', '山东省 济宁市 汶上县');
INSERT INTO dv_area_base VALUES ('370831', '泗水县', '', '', '', '3', '370800', '山东省 济宁市 泗水县');
INSERT INTO dv_area_base VALUES ('370832', '梁山县', '', '', '', '3', '370800', '山东省 济宁市 梁山县');
INSERT INTO dv_area_base VALUES ('370881', '曲阜市', '', '', '', '3', '370800', '山东省 济宁市 曲阜市');
INSERT INTO dv_area_base VALUES ('370882', '兖州市', '', '', '', '3', '370800', '山东省 济宁市 兖州市');
INSERT INTO dv_area_base VALUES ('370883', '邹城市', '', '', '', '3', '370800', '山东省 济宁市 邹城市');
INSERT INTO dv_area_base VALUES ('370900', '泰安市', '泰安', '', '', '2', '370000', '山东省 泰安市');
INSERT INTO dv_area_base VALUES ('370902', '泰山区', '', '', '', '3', '370900', '山东省 泰安市 泰山区');
INSERT INTO dv_area_base VALUES ('370911', '岱岳区', '', '', '', '3', '370900', '山东省 泰安市 岱岳区');
INSERT INTO dv_area_base VALUES ('370921', '宁阳县', '', '', '', '3', '370900', '山东省 泰安市 宁阳县');
INSERT INTO dv_area_base VALUES ('370923', '东平县', '', '', '', '3', '370900', '山东省 泰安市 东平县');
INSERT INTO dv_area_base VALUES ('370982', '新泰市', '', '', '', '3', '370900', '山东省 泰安市 新泰市');
INSERT INTO dv_area_base VALUES ('370983', '肥城市', '', '', '', '3', '370900', '山东省 泰安市 肥城市');
INSERT INTO dv_area_base VALUES ('371000', '威海市', '威海', '', '', '2', '370000', '山东省 威海市');
INSERT INTO dv_area_base VALUES ('371002', '环翠区', '', '', '', '3', '371000', '山东省 威海市 环翠区');
INSERT INTO dv_area_base VALUES ('371081', '文登市', '', '', '', '3', '371000', '山东省 威海市 文登市');
INSERT INTO dv_area_base VALUES ('371082', '荣成市', '', '', '', '3', '371000', '山东省 威海市 荣成市');
INSERT INTO dv_area_base VALUES ('371083', '乳山市', '', '', '', '3', '371000', '山东省 威海市 乳山市');
INSERT INTO dv_area_base VALUES ('371100', '日照市', '日照', '', '', '2', '370000', '山东省 日照市');
INSERT INTO dv_area_base VALUES ('371102', '东港区', '', '', '', '3', '371100', '山东省 日照市 东港区');
INSERT INTO dv_area_base VALUES ('371103', '岚山区', '', '', '', '3', '371100', '山东省 日照市 岚山区');
INSERT INTO dv_area_base VALUES ('371121', '五莲县', '', '', '', '3', '371100', '山东省 日照市 五莲县');
INSERT INTO dv_area_base VALUES ('371122', '莒县', '', '', '', '3', '371100', '山东省 日照市 莒县');
INSERT INTO dv_area_base VALUES ('371200', '莱芜市', '莱芜', '', '', '2', '370000', '山东省 莱芜市');
INSERT INTO dv_area_base VALUES ('371202', '莱城区', '', '', '', '3', '371200', '山东省 莱芜市 莱城区');
INSERT INTO dv_area_base VALUES ('371203', '钢城区', '', '', '', '3', '371200', '山东省 莱芜市 钢城区');
INSERT INTO dv_area_base VALUES ('371300', '临沂市', '临沂', '', '', '2', '370000', '山东省 临沂市');
INSERT INTO dv_area_base VALUES ('371302', '兰山区', '', '', '', '3', '371300', '山东省 临沂市 兰山区');
INSERT INTO dv_area_base VALUES ('371311', '罗庄区', '', '', '', '3', '371300', '山东省 临沂市 罗庄区');
INSERT INTO dv_area_base VALUES ('371312', '河东区', '', '', '', '3', '371300', '山东省 临沂市 河东区');
INSERT INTO dv_area_base VALUES ('371321', '沂南县', '', '', '', '3', '371300', '山东省 临沂市 沂南县');
INSERT INTO dv_area_base VALUES ('371322', '郯城县', '', '', '', '3', '371300', '山东省 临沂市 郯城县');
INSERT INTO dv_area_base VALUES ('371323', '沂水县', '', '', '', '3', '371300', '山东省 临沂市 沂水县');
INSERT INTO dv_area_base VALUES ('371324', '苍山县', '', '', '', '3', '371300', '山东省 临沂市 苍山县');
INSERT INTO dv_area_base VALUES ('371325', '费县', '', '', '', '3', '371300', '山东省 临沂市 费县');
INSERT INTO dv_area_base VALUES ('371326', '平邑县', '', '', '', '3', '371300', '山东省 临沂市 平邑县');
INSERT INTO dv_area_base VALUES ('371327', '莒南县', '', '', '', '3', '371300', '山东省 临沂市 莒南县');
INSERT INTO dv_area_base VALUES ('371328', '蒙阴县', '', '', '', '3', '371300', '山东省 临沂市 蒙阴县');
INSERT INTO dv_area_base VALUES ('371329', '临沭县', '', '', '', '3', '371300', '山东省 临沂市 临沭县');
INSERT INTO dv_area_base VALUES ('371400', '德州市', '德州', '', '', '2', '370000', '山东省 德州市');
INSERT INTO dv_area_base VALUES ('371402', '德城区', '', '', '', '3', '371400', '山东省 德州市 德城区');
INSERT INTO dv_area_base VALUES ('371421', '陵县', '', '', '', '3', '371400', '山东省 德州市 陵县');
INSERT INTO dv_area_base VALUES ('371422', '宁津县', '', '', '', '3', '371400', '山东省 德州市 宁津县');
INSERT INTO dv_area_base VALUES ('371423', '庆云县', '', '', '', '3', '371400', '山东省 德州市 庆云县');
INSERT INTO dv_area_base VALUES ('371424', '临邑县', '', '', '', '3', '371400', '山东省 德州市 临邑县');
INSERT INTO dv_area_base VALUES ('371425', '齐河县', '', '', '', '3', '371400', '山东省 德州市 齐河县');
INSERT INTO dv_area_base VALUES ('371426', '平原县', '', '', '', '3', '371400', '山东省 德州市 平原县');
INSERT INTO dv_area_base VALUES ('371427', '夏津县', '', '', '', '3', '371400', '山东省 德州市 夏津县');
INSERT INTO dv_area_base VALUES ('371428', '武城县', '', '', '', '3', '371400', '山东省 德州市 武城县');
INSERT INTO dv_area_base VALUES ('371481', '乐陵市', '', '', '', '3', '371400', '山东省 德州市 乐陵市');
INSERT INTO dv_area_base VALUES ('371482', '禹城市', '', '', '', '3', '371400', '山东省 德州市 禹城市');
INSERT INTO dv_area_base VALUES ('371500', '聊城市', '聊城', '', '', '2', '370000', '山东省 聊城市');
INSERT INTO dv_area_base VALUES ('371502', '东昌府区', '', '', '', '3', '371500', '山东省 聊城市 东昌府区');
INSERT INTO dv_area_base VALUES ('371521', '阳谷县', '', '', '', '3', '371500', '山东省 聊城市 阳谷县');
INSERT INTO dv_area_base VALUES ('371522', '莘县', '', '', '', '3', '371500', '山东省 聊城市 莘县');
INSERT INTO dv_area_base VALUES ('371523', '茌平县', '', '', '', '3', '371500', '山东省 聊城市 茌平县');
INSERT INTO dv_area_base VALUES ('371524', '东阿县', '', '', '', '3', '371500', '山东省 聊城市 东阿县');
INSERT INTO dv_area_base VALUES ('371525', '冠县', '', '', '', '3', '371500', '山东省 聊城市 冠县');
INSERT INTO dv_area_base VALUES ('371526', '高唐县', '', '', '', '3', '371500', '山东省 聊城市 高唐县');
INSERT INTO dv_area_base VALUES ('371581', '临清市', '', '', '', '3', '371500', '山东省 聊城市 临清市');
INSERT INTO dv_area_base VALUES ('371600', '滨州市', '滨州', '', '', '2', '370000', '山东省 滨州市');
INSERT INTO dv_area_base VALUES ('371602', '滨城区', '', '', '', '3', '371600', '山东省 滨州市 滨城区');
INSERT INTO dv_area_base VALUES ('371621', '惠民县', '', '', '', '3', '371600', '山东省 滨州市 惠民县');
INSERT INTO dv_area_base VALUES ('371622', '阳信县', '', '', '', '3', '371600', '山东省 滨州市 阳信县');
INSERT INTO dv_area_base VALUES ('371623', '无棣县', '', '', '', '3', '371600', '山东省 滨州市 无棣县');
INSERT INTO dv_area_base VALUES ('371624', '沾化县', '', '', '', '3', '371600', '山东省 滨州市 沾化县');
INSERT INTO dv_area_base VALUES ('371625', '博兴县', '', '', '', '3', '371600', '山东省 滨州市 博兴县');
INSERT INTO dv_area_base VALUES ('371626', '邹平县', '', '', '', '3', '371600', '山东省 滨州市 邹平县');
INSERT INTO dv_area_base VALUES ('371700', '菏泽市', '菏泽', '', '', '2', '370000', '山东省 菏泽市');
INSERT INTO dv_area_base VALUES ('371702', '牡丹区', '', '', '', '3', '371700', '山东省 菏泽市 牡丹区');
INSERT INTO dv_area_base VALUES ('371721', '曹县', '', '', '', '3', '371700', '山东省 菏泽市 曹县');
INSERT INTO dv_area_base VALUES ('371722', '单县', '', '', '', '3', '371700', '山东省 菏泽市 单县');
INSERT INTO dv_area_base VALUES ('371723', '成武县', '', '', '', '3', '371700', '山东省 菏泽市 成武县');
INSERT INTO dv_area_base VALUES ('371724', '巨野县', '', '', '', '3', '371700', '山东省 菏泽市 巨野县');
INSERT INTO dv_area_base VALUES ('371725', '郓城县', '', '', '', '3', '371700', '山东省 菏泽市 郓城县');
INSERT INTO dv_area_base VALUES ('371726', '鄄城县', '', '', '', '3', '371700', '山东省 菏泽市 鄄城县');
INSERT INTO dv_area_base VALUES ('371727', '定陶县', '', '', '', '3', '371700', '山东省 菏泽市 定陶县');
INSERT INTO dv_area_base VALUES ('371728', '东明县', '', '', '', '3', '371700', '山东省 菏泽市 东明县');
INSERT INTO dv_area_base VALUES ('410000', '河南省', '河南', 'henan', '豫', '1', '0', '河南省');
INSERT INTO dv_area_base VALUES ('410100', '郑州市', '郑州', '', '', '2', '410000', '河南省 郑州市');
INSERT INTO dv_area_base VALUES ('410102', '中原区', '', '', '', '3', '410100', '河南省 郑州市 中原区');
INSERT INTO dv_area_base VALUES ('410103', '二七区', '', '', '', '3', '410100', '河南省 郑州市 二七区');
INSERT INTO dv_area_base VALUES ('410104', '管城回族区', '', '', '', '3', '410100', '河南省 郑州市 管城回族区');
INSERT INTO dv_area_base VALUES ('410105', '金水区', '', '', '', '3', '410100', '河南省 郑州市 金水区');
INSERT INTO dv_area_base VALUES ('410106', '上街区', '', '', '', '3', '410100', '河南省 郑州市 上街区');
INSERT INTO dv_area_base VALUES ('410108', '惠济区', '', '', '', '3', '410100', '河南省 郑州市 惠济区');
INSERT INTO dv_area_base VALUES ('410122', '中牟县', '', '', '', '3', '410100', '河南省 郑州市 中牟县');
INSERT INTO dv_area_base VALUES ('410181', '巩义市', '', '', '', '3', '410100', '河南省 郑州市 巩义市');
INSERT INTO dv_area_base VALUES ('410182', '荥阳市', '', '', '', '3', '410100', '河南省 郑州市 荥阳市');
INSERT INTO dv_area_base VALUES ('410183', '新密市', '', '', '', '3', '410100', '河南省 郑州市 新密市');
INSERT INTO dv_area_base VALUES ('410184', '新郑市', '', '', '', '3', '410100', '河南省 郑州市 新郑市');
INSERT INTO dv_area_base VALUES ('410185', '登封市', '', '', '', '3', '410100', '河南省 郑州市 登封市');
INSERT INTO dv_area_base VALUES ('410200', '开封市', '开封', '', '', '2', '410000', '河南省 开封市');
INSERT INTO dv_area_base VALUES ('410202', '龙亭区', '', '', '', '3', '410200', '河南省 开封市 龙亭区');
INSERT INTO dv_area_base VALUES ('410203', '顺河回族区', '', '', '', '3', '410200', '河南省 开封市 顺河回族区');
INSERT INTO dv_area_base VALUES ('410204', '鼓楼区', '', '', '', '3', '410200', '河南省 开封市 鼓楼区');
INSERT INTO dv_area_base VALUES ('410205', '禹王台区', '', '', '', '3', '410200', '河南省 开封市 禹王台区');
INSERT INTO dv_area_base VALUES ('410211', '金明区', '', '', '', '3', '410200', '河南省 开封市 金明区');
INSERT INTO dv_area_base VALUES ('410221', '杞县', '', '', '', '3', '410200', '河南省 开封市 杞县');
INSERT INTO dv_area_base VALUES ('410222', '通许县', '', '', '', '3', '410200', '河南省 开封市 通许县');
INSERT INTO dv_area_base VALUES ('410223', '尉氏县', '', '', '', '3', '410200', '河南省 开封市 尉氏县');
INSERT INTO dv_area_base VALUES ('410224', '开封县', '', '', '', '3', '410200', '河南省 开封市 开封县');
INSERT INTO dv_area_base VALUES ('410225', '兰考县', '', '', '', '3', '410200', '河南省 开封市 兰考县');
INSERT INTO dv_area_base VALUES ('410300', '洛阳市', '洛阳', '', '', '2', '410000', '河南省 洛阳市');
INSERT INTO dv_area_base VALUES ('410302', '老城区', '', '', '', '3', '410300', '河南省 洛阳市 老城区');
INSERT INTO dv_area_base VALUES ('410303', '西工区', '', '', '', '3', '410300', '河南省 洛阳市 西工区');
INSERT INTO dv_area_base VALUES ('410304', '瀍河回族区', '', '', '', '3', '410300', '河南省 洛阳市 瀍河回族区');
INSERT INTO dv_area_base VALUES ('410305', '涧西区', '', '', '', '3', '410300', '河南省 洛阳市 涧西区');
INSERT INTO dv_area_base VALUES ('410306', '吉利区', '', '', '', '3', '410300', '河南省 洛阳市 吉利区');
INSERT INTO dv_area_base VALUES ('410307', '洛龙区', '', '', '', '3', '410300', '河南省 洛阳市 洛龙区');
INSERT INTO dv_area_base VALUES ('410322', '孟津县', '', '', '', '3', '410300', '河南省 洛阳市 孟津县');
INSERT INTO dv_area_base VALUES ('410323', '新安县', '', '', '', '3', '410300', '河南省 洛阳市 新安县');
INSERT INTO dv_area_base VALUES ('410324', '栾川县', '', '', '', '3', '410300', '河南省 洛阳市 栾川县');
INSERT INTO dv_area_base VALUES ('410325', '嵩县', '', '', '', '3', '410300', '河南省 洛阳市 嵩县');
INSERT INTO dv_area_base VALUES ('410326', '汝阳县', '', '', '', '3', '410300', '河南省 洛阳市 汝阳县');
INSERT INTO dv_area_base VALUES ('410327', '宜阳县', '', '', '', '3', '410300', '河南省 洛阳市 宜阳县');
INSERT INTO dv_area_base VALUES ('410328', '洛宁县', '', '', '', '3', '410300', '河南省 洛阳市 洛宁县');
INSERT INTO dv_area_base VALUES ('410329', '伊川县', '', '', '', '3', '410300', '河南省 洛阳市 伊川县');
INSERT INTO dv_area_base VALUES ('410381', '偃师市', '', '', '', '3', '410300', '河南省 洛阳市 偃师市');
INSERT INTO dv_area_base VALUES ('410400', '平顶山市', '平顶山', '', '', '2', '410000', '河南省 平顶山市');
INSERT INTO dv_area_base VALUES ('410402', '新华区', '', '', '', '3', '410400', '河南省 平顶山市 新华区');
INSERT INTO dv_area_base VALUES ('410403', '卫东区', '', '', '', '3', '410400', '河南省 平顶山市 卫东区');
INSERT INTO dv_area_base VALUES ('410404', '石龙区', '', '', '', '3', '410400', '河南省 平顶山市 石龙区');
INSERT INTO dv_area_base VALUES ('410411', '湛河区', '', '', '', '3', '410400', '河南省 平顶山市 湛河区');
INSERT INTO dv_area_base VALUES ('410421', '宝丰县', '', '', '', '3', '410400', '河南省 平顶山市 宝丰县');
INSERT INTO dv_area_base VALUES ('410422', '叶县', '', '', '', '3', '410400', '河南省 平顶山市 叶县');
INSERT INTO dv_area_base VALUES ('410423', '鲁山县', '', '', '', '3', '410400', '河南省 平顶山市 鲁山县');
INSERT INTO dv_area_base VALUES ('410425', '郏县', '', '', '', '3', '410400', '河南省 平顶山市 郏县');
INSERT INTO dv_area_base VALUES ('410481', '舞钢市', '', '', '', '3', '410400', '河南省 平顶山市 舞钢市');
INSERT INTO dv_area_base VALUES ('410482', '汝州市', '', '', '', '3', '410400', '河南省 平顶山市 汝州市');
INSERT INTO dv_area_base VALUES ('410500', '安阳市', '安阳', '', '', '2', '410000', '河南省 安阳市');
INSERT INTO dv_area_base VALUES ('410502', '文峰区', '', '', '', '3', '410500', '河南省 安阳市 文峰区');
INSERT INTO dv_area_base VALUES ('410503', '北关区', '', '', '', '3', '410500', '河南省 安阳市 北关区');
INSERT INTO dv_area_base VALUES ('410505', '殷都区', '', '', '', '3', '410500', '河南省 安阳市 殷都区');
INSERT INTO dv_area_base VALUES ('410506', '龙安区', '', '', '', '3', '410500', '河南省 安阳市 龙安区');
INSERT INTO dv_area_base VALUES ('410522', '安阳县', '', '', '', '3', '410500', '河南省 安阳市 安阳县');
INSERT INTO dv_area_base VALUES ('410523', '汤阴县', '', '', '', '3', '410500', '河南省 安阳市 汤阴县');
INSERT INTO dv_area_base VALUES ('410526', '滑县', '', '', '', '3', '410500', '河南省 安阳市 滑县');
INSERT INTO dv_area_base VALUES ('410527', '内黄县', '', '', '', '3', '410500', '河南省 安阳市 内黄县');
INSERT INTO dv_area_base VALUES ('410581', '林州市', '', '', '', '3', '410500', '河南省 安阳市 林州市');
INSERT INTO dv_area_base VALUES ('410600', '鹤壁市', '鹤壁', '', '', '2', '410000', '河南省 鹤壁市');
INSERT INTO dv_area_base VALUES ('410602', '鹤山区', '', '', '', '3', '410600', '河南省 鹤壁市 鹤山区');
INSERT INTO dv_area_base VALUES ('410603', '山城区', '', '', '', '3', '410600', '河南省 鹤壁市 山城区');
INSERT INTO dv_area_base VALUES ('410611', '淇滨区', '', '', '', '3', '410600', '河南省 鹤壁市 淇滨区');
INSERT INTO dv_area_base VALUES ('410621', '浚县', '', '', '', '3', '410600', '河南省 鹤壁市 浚县');
INSERT INTO dv_area_base VALUES ('410622', '淇县', '', '', '', '3', '410600', '河南省 鹤壁市 淇县');
INSERT INTO dv_area_base VALUES ('410700', '新乡市', '新乡', '', '', '2', '410000', '河南省 新乡市');
INSERT INTO dv_area_base VALUES ('410702', '红旗区', '', '', '', '3', '410700', '河南省 新乡市 红旗区');
INSERT INTO dv_area_base VALUES ('410703', '卫滨区', '', '', '', '3', '410700', '河南省 新乡市 卫滨区');
INSERT INTO dv_area_base VALUES ('410704', '凤泉区', '', '', '', '3', '410700', '河南省 新乡市 凤泉区');
INSERT INTO dv_area_base VALUES ('410711', '牧野区', '', '', '', '3', '410700', '河南省 新乡市 牧野区');
INSERT INTO dv_area_base VALUES ('410721', '新乡县', '', '', '', '3', '410700', '河南省 新乡市 新乡县');
INSERT INTO dv_area_base VALUES ('410724', '获嘉县', '', '', '', '3', '410700', '河南省 新乡市 获嘉县');
INSERT INTO dv_area_base VALUES ('410725', '原阳县', '', '', '', '3', '410700', '河南省 新乡市 原阳县');
INSERT INTO dv_area_base VALUES ('410726', '延津县', '', '', '', '3', '410700', '河南省 新乡市 延津县');
INSERT INTO dv_area_base VALUES ('410727', '封丘县', '', '', '', '3', '410700', '河南省 新乡市 封丘县');
INSERT INTO dv_area_base VALUES ('410728', '长垣县', '', '', '', '3', '410700', '河南省 新乡市 长垣县');
INSERT INTO dv_area_base VALUES ('410781', '卫辉市', '', '', '', '3', '410700', '河南省 新乡市 卫辉市');
INSERT INTO dv_area_base VALUES ('410782', '辉县市', '', '', '', '3', '410700', '河南省 新乡市 辉县市');
INSERT INTO dv_area_base VALUES ('410800', '焦作市', '焦作', '', '', '2', '410000', '河南省 焦作市');
INSERT INTO dv_area_base VALUES ('410802', '解放区', '', '', '', '3', '410800', '河南省 焦作市 解放区');
INSERT INTO dv_area_base VALUES ('410803', '中站区', '', '', '', '3', '410800', '河南省 焦作市 中站区');
INSERT INTO dv_area_base VALUES ('410804', '马村区', '', '', '', '3', '410800', '河南省 焦作市 马村区');
INSERT INTO dv_area_base VALUES ('410811', '山阳区', '', '', '', '3', '410800', '河南省 焦作市 山阳区');
INSERT INTO dv_area_base VALUES ('410821', '修武县', '', '', '', '3', '410800', '河南省 焦作市 修武县');
INSERT INTO dv_area_base VALUES ('410822', '博爱县', '', '', '', '3', '410800', '河南省 焦作市 博爱县');
INSERT INTO dv_area_base VALUES ('410823', '武陟县', '', '', '', '3', '410800', '河南省 焦作市 武陟县');
INSERT INTO dv_area_base VALUES ('410825', '温县', '', '', '', '3', '410800', '河南省 焦作市 温县');
INSERT INTO dv_area_base VALUES ('410882', '沁阳市', '', '', '', '3', '410800', '河南省 焦作市 沁阳市');
INSERT INTO dv_area_base VALUES ('410883', '孟州市', '', '', '', '3', '410800', '河南省 焦作市 孟州市');
INSERT INTO dv_area_base VALUES ('410900', '濮阳市', '濮阳', '', '', '2', '410000', '河南省 濮阳市');
INSERT INTO dv_area_base VALUES ('410902', '华龙区', '', '', '', '3', '410900', '河南省 濮阳市 华龙区');
INSERT INTO dv_area_base VALUES ('410922', '清丰县', '', '', '', '3', '410900', '河南省 濮阳市 清丰县');
INSERT INTO dv_area_base VALUES ('410923', '南乐县', '', '', '', '3', '410900', '河南省 濮阳市 南乐县');
INSERT INTO dv_area_base VALUES ('410926', '范县', '', '', '', '3', '410900', '河南省 濮阳市 范县');
INSERT INTO dv_area_base VALUES ('410927', '台前县', '', '', '', '3', '410900', '河南省 濮阳市 台前县');
INSERT INTO dv_area_base VALUES ('410928', '濮阳县', '', '', '', '3', '410900', '河南省 濮阳市 濮阳县');
INSERT INTO dv_area_base VALUES ('411000', '许昌市', '许昌', '', '', '2', '410000', '河南省 许昌市');
INSERT INTO dv_area_base VALUES ('411002', '魏都区', '', '', '', '3', '411000', '河南省 许昌市 魏都区');
INSERT INTO dv_area_base VALUES ('411023', '许昌县', '', '', '', '3', '411000', '河南省 许昌市 许昌县');
INSERT INTO dv_area_base VALUES ('411024', '鄢陵县', '', '', '', '3', '411000', '河南省 许昌市 鄢陵县');
INSERT INTO dv_area_base VALUES ('411025', '襄城县', '', '', '', '3', '411000', '河南省 许昌市 襄城县');
INSERT INTO dv_area_base VALUES ('411081', '禹州市', '', '', '', '3', '411000', '河南省 许昌市 禹州市');
INSERT INTO dv_area_base VALUES ('411082', '长葛市', '', '', '', '3', '411000', '河南省 许昌市 长葛市');
INSERT INTO dv_area_base VALUES ('411100', '漯河市', '漯河', '', '', '2', '410000', '河南省 漯河市');
INSERT INTO dv_area_base VALUES ('411102', '源汇区', '', '', '', '3', '411100', '河南省 漯河市 源汇区');
INSERT INTO dv_area_base VALUES ('411103', '郾城区', '', '', '', '3', '411100', '河南省 漯河市 郾城区');
INSERT INTO dv_area_base VALUES ('411104', '召陵区', '', '', '', '3', '411100', '河南省 漯河市 召陵区');
INSERT INTO dv_area_base VALUES ('411121', '舞阳县', '', '', '', '3', '411100', '河南省 漯河市 舞阳县');
INSERT INTO dv_area_base VALUES ('411122', '临颍县', '', '', '', '3', '411100', '河南省 漯河市 临颍县');
INSERT INTO dv_area_base VALUES ('411200', '三门峡市', '三门峡', '', '', '2', '410000', '河南省 三门峡市');
INSERT INTO dv_area_base VALUES ('411202', '湖滨区', '', '', '', '3', '411200', '河南省 三门峡市 湖滨区');
INSERT INTO dv_area_base VALUES ('411221', '渑池县', '', '', '', '3', '411200', '河南省 三门峡市 渑池县');
INSERT INTO dv_area_base VALUES ('411222', '陕县', '', '', '', '3', '411200', '河南省 三门峡市 陕县');
INSERT INTO dv_area_base VALUES ('411224', '卢氏县', '', '', '', '3', '411200', '河南省 三门峡市 卢氏县');
INSERT INTO dv_area_base VALUES ('411281', '义马市', '', '', '', '3', '411200', '河南省 三门峡市 义马市');
INSERT INTO dv_area_base VALUES ('411282', '灵宝市', '', '', '', '3', '411200', '河南省 三门峡市 灵宝市');
INSERT INTO dv_area_base VALUES ('411300', '南阳市', '南阳', '', '', '2', '410000', '河南省 南阳市');
INSERT INTO dv_area_base VALUES ('411302', '宛城区', '', '', '', '3', '411300', '河南省 南阳市 宛城区');
INSERT INTO dv_area_base VALUES ('411303', '卧龙区', '', '', '', '3', '411300', '河南省 南阳市 卧龙区');
INSERT INTO dv_area_base VALUES ('411321', '南召县', '', '', '', '3', '411300', '河南省 南阳市 南召县');
INSERT INTO dv_area_base VALUES ('411322', '方城县', '', '', '', '3', '411300', '河南省 南阳市 方城县');
INSERT INTO dv_area_base VALUES ('411323', '西峡县', '', '', '', '3', '411300', '河南省 南阳市 西峡县');
INSERT INTO dv_area_base VALUES ('411324', '镇平县', '', '', '', '3', '411300', '河南省 南阳市 镇平县');
INSERT INTO dv_area_base VALUES ('411325', '内乡县', '', '', '', '3', '411300', '河南省 南阳市 内乡县');
INSERT INTO dv_area_base VALUES ('411326', '淅川县', '', '', '', '3', '411300', '河南省 南阳市 淅川县');
INSERT INTO dv_area_base VALUES ('411327', '社旗县', '', '', '', '3', '411300', '河南省 南阳市 社旗县');
INSERT INTO dv_area_base VALUES ('411328', '唐河县', '', '', '', '3', '411300', '河南省 南阳市 唐河县');
INSERT INTO dv_area_base VALUES ('411329', '新野县', '', '', '', '3', '411300', '河南省 南阳市 新野县');
INSERT INTO dv_area_base VALUES ('411330', '桐柏县', '', '', '', '3', '411300', '河南省 南阳市 桐柏县');
INSERT INTO dv_area_base VALUES ('411381', '邓州市', '', '', '', '3', '411300', '河南省 南阳市 邓州市');
INSERT INTO dv_area_base VALUES ('411400', '商丘市', '商丘', '', '', '2', '410000', '河南省 商丘市');
INSERT INTO dv_area_base VALUES ('411402', '梁园区', '', '', '', '3', '411400', '河南省 商丘市 梁园区');
INSERT INTO dv_area_base VALUES ('411403', '睢阳区', '', '', '', '3', '411400', '河南省 商丘市 睢阳区');
INSERT INTO dv_area_base VALUES ('411421', '民权县', '', '', '', '3', '411400', '河南省 商丘市 民权县');
INSERT INTO dv_area_base VALUES ('411422', '睢县', '', '', '', '3', '411400', '河南省 商丘市 睢县');
INSERT INTO dv_area_base VALUES ('411423', '宁陵县', '', '', '', '3', '411400', '河南省 商丘市 宁陵县');
INSERT INTO dv_area_base VALUES ('411424', '柘城县', '', '', '', '3', '411400', '河南省 商丘市 柘城县');
INSERT INTO dv_area_base VALUES ('411425', '虞城县', '', '', '', '3', '411400', '河南省 商丘市 虞城县');
INSERT INTO dv_area_base VALUES ('411426', '夏邑县', '', '', '', '3', '411400', '河南省 商丘市 夏邑县');
INSERT INTO dv_area_base VALUES ('411481', '永城市', '', '', '', '3', '411400', '河南省 商丘市 永城市');
INSERT INTO dv_area_base VALUES ('411500', '信阳市', '信阳', '', '', '2', '410000', '河南省 信阳市');
INSERT INTO dv_area_base VALUES ('411502', '浉河区', '', '', '', '3', '411500', '河南省 信阳市 浉河区');
INSERT INTO dv_area_base VALUES ('411503', '平桥区', '', '', '', '3', '411500', '河南省 信阳市 平桥区');
INSERT INTO dv_area_base VALUES ('411521', '罗山县', '', '', '', '3', '411500', '河南省 信阳市 罗山县');
INSERT INTO dv_area_base VALUES ('411522', '光山县', '', '', '', '3', '411500', '河南省 信阳市 光山县');
INSERT INTO dv_area_base VALUES ('411523', '新县', '', '', '', '3', '411500', '河南省 信阳市 新县');
INSERT INTO dv_area_base VALUES ('411524', '商城县', '', '', '', '3', '411500', '河南省 信阳市 商城县');
INSERT INTO dv_area_base VALUES ('411525', '固始县', '', '', '', '3', '411500', '河南省 信阳市 固始县');
INSERT INTO dv_area_base VALUES ('411526', '潢川县', '', '', '', '3', '411500', '河南省 信阳市 潢川县');
INSERT INTO dv_area_base VALUES ('411527', '淮滨县', '', '', '', '3', '411500', '河南省 信阳市 淮滨县');
INSERT INTO dv_area_base VALUES ('411528', '息县', '', '', '', '3', '411500', '河南省 信阳市 息县');
INSERT INTO dv_area_base VALUES ('411600', '周口市', '周口', '', '', '2', '410000', '河南省 周口市');
INSERT INTO dv_area_base VALUES ('411602', '川汇区', '', '', '', '3', '411600', '河南省 周口市 川汇区');
INSERT INTO dv_area_base VALUES ('411621', '扶沟县', '', '', '', '3', '411600', '河南省 周口市 扶沟县');
INSERT INTO dv_area_base VALUES ('411622', '西华县', '', '', '', '3', '411600', '河南省 周口市 西华县');
INSERT INTO dv_area_base VALUES ('411623', '商水县', '', '', '', '3', '411600', '河南省 周口市 商水县');
INSERT INTO dv_area_base VALUES ('411624', '沈丘县', '', '', '', '3', '411600', '河南省 周口市 沈丘县');
INSERT INTO dv_area_base VALUES ('411625', '郸城县', '', '', '', '3', '411600', '河南省 周口市 郸城县');
INSERT INTO dv_area_base VALUES ('411626', '淮阳县', '', '', '', '3', '411600', '河南省 周口市 淮阳县');
INSERT INTO dv_area_base VALUES ('411627', '太康县', '', '', '', '3', '411600', '河南省 周口市 太康县');
INSERT INTO dv_area_base VALUES ('411628', '鹿邑县', '', '', '', '3', '411600', '河南省 周口市 鹿邑县');
INSERT INTO dv_area_base VALUES ('411681', '项城市', '', '', '', '3', '411600', '河南省 周口市 项城市');
INSERT INTO dv_area_base VALUES ('411700', '驻马店市', '驻马店', '', '', '2', '410000', '河南省 驻马店市');
INSERT INTO dv_area_base VALUES ('411702', '驿城区', '', '', '', '3', '411700', '河南省 驻马店市 驿城区');
INSERT INTO dv_area_base VALUES ('411721', '西平县', '', '', '', '3', '411700', '河南省 驻马店市 西平县');
INSERT INTO dv_area_base VALUES ('411722', '上蔡县', '', '', '', '3', '411700', '河南省 驻马店市 上蔡县');
INSERT INTO dv_area_base VALUES ('411723', '平舆县', '', '', '', '3', '411700', '河南省 驻马店市 平舆县');
INSERT INTO dv_area_base VALUES ('411724', '正阳县', '', '', '', '3', '411700', '河南省 驻马店市 正阳县');
INSERT INTO dv_area_base VALUES ('411725', '确山县', '', '', '', '3', '411700', '河南省 驻马店市 确山县');
INSERT INTO dv_area_base VALUES ('411726', '泌阳县', '', '', '', '3', '411700', '河南省 驻马店市 泌阳县');
INSERT INTO dv_area_base VALUES ('411727', '汝南县', '', '', '', '3', '411700', '河南省 驻马店市 汝南县');
INSERT INTO dv_area_base VALUES ('411728', '遂平县', '', '', '', '3', '411700', '河南省 驻马店市 遂平县');
INSERT INTO dv_area_base VALUES ('411729', '新蔡县', '', '', '', '3', '411700', '河南省 驻马店市 新蔡县');
INSERT INTO dv_area_base VALUES ('419001', '济源市', '济源', '', '', '2', '410000', '河南省 济源市');
INSERT INTO dv_area_base VALUES ('420000', '湖北省', '湖北', 'hubei', '鄂', '1', '0', '湖北省');
INSERT INTO dv_area_base VALUES ('420100', '武汉市', '武汉', '', '', '2', '420000', '湖北省 武汉市');
INSERT INTO dv_area_base VALUES ('420102', '江岸区', '', '', '', '3', '420100', '湖北省 武汉市 江岸区');
INSERT INTO dv_area_base VALUES ('420103', '江汉区', '', '', '', '3', '420100', '湖北省 武汉市 江汉区');
INSERT INTO dv_area_base VALUES ('420104', '硚口区', '', '', '', '3', '420100', '湖北省 武汉市 硚口区');
INSERT INTO dv_area_base VALUES ('420105', '汉阳区', '', '', '', '3', '420100', '湖北省 武汉市 汉阳区');
INSERT INTO dv_area_base VALUES ('420106', '武昌区', '', '', '', '3', '420100', '湖北省 武汉市 武昌区');
INSERT INTO dv_area_base VALUES ('420107', '青山区', '', '', '', '3', '420100', '湖北省 武汉市 青山区');
INSERT INTO dv_area_base VALUES ('420111', '洪山区', '', '', '', '3', '420100', '湖北省 武汉市 洪山区');
INSERT INTO dv_area_base VALUES ('420112', '东西湖区', '', '', '', '3', '420100', '湖北省 武汉市 东西湖区');
INSERT INTO dv_area_base VALUES ('420113', '汉南区', '', '', '', '3', '420100', '湖北省 武汉市 汉南区');
INSERT INTO dv_area_base VALUES ('420114', '蔡甸区', '', '', '', '3', '420100', '湖北省 武汉市 蔡甸区');
INSERT INTO dv_area_base VALUES ('420115', '江夏区', '', '', '', '3', '420100', '湖北省 武汉市 江夏区');
INSERT INTO dv_area_base VALUES ('420116', '黄陂区', '', '', '', '3', '420100', '湖北省 武汉市 黄陂区');
INSERT INTO dv_area_base VALUES ('420117', '新洲区', '', '', '', '3', '420100', '湖北省 武汉市 新洲区');
INSERT INTO dv_area_base VALUES ('420200', '黄石市', '黄石', '', '', '2', '420000', '湖北省 黄石市');
INSERT INTO dv_area_base VALUES ('420202', '黄石港区', '', '', '', '3', '420200', '湖北省 黄石市 黄石港区');
INSERT INTO dv_area_base VALUES ('420203', '西塞山区', '', '', '', '3', '420200', '湖北省 黄石市 西塞山区');
INSERT INTO dv_area_base VALUES ('420204', '下陆区', '', '', '', '3', '420200', '湖北省 黄石市 下陆区');
INSERT INTO dv_area_base VALUES ('420205', '铁山区', '', '', '', '3', '420200', '湖北省 黄石市 铁山区');
INSERT INTO dv_area_base VALUES ('420222', '阳新县', '', '', '', '3', '420200', '湖北省 黄石市 阳新县');
INSERT INTO dv_area_base VALUES ('420281', '大冶市', '', '', '', '3', '420200', '湖北省 黄石市 大冶市');
INSERT INTO dv_area_base VALUES ('420300', '十堰市', '十堰', '', '', '2', '420000', '湖北省 十堰市');
INSERT INTO dv_area_base VALUES ('420302', '茅箭区', '', '', '', '3', '420300', '湖北省 十堰市 茅箭区');
INSERT INTO dv_area_base VALUES ('420303', '张湾区', '', '', '', '3', '420300', '湖北省 十堰市 张湾区');
INSERT INTO dv_area_base VALUES ('420321', '郧县', '', '', '', '3', '420300', '湖北省 十堰市 郧县');
INSERT INTO dv_area_base VALUES ('420322', '郧西县', '', '', '', '3', '420300', '湖北省 十堰市 郧西县');
INSERT INTO dv_area_base VALUES ('420323', '竹山县', '', '', '', '3', '420300', '湖北省 十堰市 竹山县');
INSERT INTO dv_area_base VALUES ('420324', '竹溪县', '', '', '', '3', '420300', '湖北省 十堰市 竹溪县');
INSERT INTO dv_area_base VALUES ('420325', '房县', '', '', '', '3', '420300', '湖北省 十堰市 房县');
INSERT INTO dv_area_base VALUES ('420381', '丹江口市', '', '', '', '3', '420300', '湖北省 十堰市 丹江口市');
INSERT INTO dv_area_base VALUES ('420500', '宜昌市', '宜昌', '', '', '2', '420000', '湖北省 宜昌市');
INSERT INTO dv_area_base VALUES ('420502', '西陵区', '', '', '', '3', '420500', '湖北省 宜昌市 西陵区');
INSERT INTO dv_area_base VALUES ('420503', '伍家岗区', '', '', '', '3', '420500', '湖北省 宜昌市 伍家岗区');
INSERT INTO dv_area_base VALUES ('420504', '点军区', '', '', '', '3', '420500', '湖北省 宜昌市 点军区');
INSERT INTO dv_area_base VALUES ('420505', '猇亭区', '', '', '', '3', '420500', '湖北省 宜昌市 猇亭区');
INSERT INTO dv_area_base VALUES ('420506', '夷陵区', '', '', '', '3', '420500', '湖北省 宜昌市 夷陵区');
INSERT INTO dv_area_base VALUES ('420525', '远安县', '', '', '', '3', '420500', '湖北省 宜昌市 远安县');
INSERT INTO dv_area_base VALUES ('420526', '兴山县', '', '', '', '3', '420500', '湖北省 宜昌市 兴山县');
INSERT INTO dv_area_base VALUES ('420527', '秭归县', '', '', '', '3', '420500', '湖北省 宜昌市 秭归县');
INSERT INTO dv_area_base VALUES ('420528', '长阳土家族自治县', '', '', '', '3', '420500', '湖北省 宜昌市 长阳土家族自治县');
INSERT INTO dv_area_base VALUES ('420529', '五峰土家族自治县', '', '', '', '3', '420500', '湖北省 宜昌市 五峰土家族自治县');
INSERT INTO dv_area_base VALUES ('420581', '宜都市', '', '', '', '3', '420500', '湖北省 宜昌市 宜都市');
INSERT INTO dv_area_base VALUES ('420582', '当阳市', '', '', '', '3', '420500', '湖北省 宜昌市 当阳市');
INSERT INTO dv_area_base VALUES ('420583', '枝江市', '', '', '', '3', '420500', '湖北省 宜昌市 枝江市');
INSERT INTO dv_area_base VALUES ('420600', '襄阳市', '襄阳', '', '', '2', '420000', '湖北省 襄阳市');
INSERT INTO dv_area_base VALUES ('420602', '襄城区', '', '', '', '3', '420600', '湖北省 襄阳市 襄城区');
INSERT INTO dv_area_base VALUES ('420606', '樊城区', '', '', '', '3', '420600', '湖北省 襄阳市 樊城区');
INSERT INTO dv_area_base VALUES ('420607', '襄州区', '', '', '', '3', '420600', '湖北省 襄阳市 襄州区');
INSERT INTO dv_area_base VALUES ('420624', '南漳县', '', '', '', '3', '420600', '湖北省 襄阳市 南漳县');
INSERT INTO dv_area_base VALUES ('420625', '谷城县', '', '', '', '3', '420600', '湖北省 襄阳市 谷城县');
INSERT INTO dv_area_base VALUES ('420626', '保康县', '', '', '', '3', '420600', '湖北省 襄阳市 保康县');
INSERT INTO dv_area_base VALUES ('420682', '老河口市', '', '', '', '3', '420600', '湖北省 襄阳市 老河口市');
INSERT INTO dv_area_base VALUES ('420683', '枣阳市', '', '', '', '3', '420600', '湖北省 襄阳市 枣阳市');
INSERT INTO dv_area_base VALUES ('420684', '宜城市', '', '', '', '3', '420600', '湖北省 襄阳市 宜城市');
INSERT INTO dv_area_base VALUES ('420700', '鄂州市', '鄂州', '', '', '2', '420000', '湖北省 鄂州市');
INSERT INTO dv_area_base VALUES ('420702', '梁子湖区', '', '', '', '3', '420700', '湖北省 鄂州市 梁子湖区');
INSERT INTO dv_area_base VALUES ('420703', '华容区', '', '', '', '3', '420700', '湖北省 鄂州市 华容区');
INSERT INTO dv_area_base VALUES ('420704', '鄂城区', '', '', '', '3', '420700', '湖北省 鄂州市 鄂城区');
INSERT INTO dv_area_base VALUES ('420800', '荆门市', '荆门', '', '', '2', '420000', '湖北省 荆门市');
INSERT INTO dv_area_base VALUES ('420802', '东宝区', '', '', '', '3', '420800', '湖北省 荆门市 东宝区');
INSERT INTO dv_area_base VALUES ('420804', '掇刀区', '', '', '', '3', '420800', '湖北省 荆门市 掇刀区');
INSERT INTO dv_area_base VALUES ('420821', '京山县', '', '', '', '3', '420800', '湖北省 荆门市 京山县');
INSERT INTO dv_area_base VALUES ('420822', '沙洋县', '', '', '', '3', '420800', '湖北省 荆门市 沙洋县');
INSERT INTO dv_area_base VALUES ('420881', '钟祥市', '', '', '', '3', '420800', '湖北省 荆门市 钟祥市');
INSERT INTO dv_area_base VALUES ('420900', '孝感市', '孝感', '', '', '2', '420000', '湖北省 孝感市');
INSERT INTO dv_area_base VALUES ('420902', '孝南区', '', '', '', '3', '420900', '湖北省 孝感市 孝南区');
INSERT INTO dv_area_base VALUES ('420921', '孝昌县', '', '', '', '3', '420900', '湖北省 孝感市 孝昌县');
INSERT INTO dv_area_base VALUES ('420922', '大悟县', '', '', '', '3', '420900', '湖北省 孝感市 大悟县');
INSERT INTO dv_area_base VALUES ('420923', '云梦县', '', '', '', '3', '420900', '湖北省 孝感市 云梦县');
INSERT INTO dv_area_base VALUES ('420981', '应城市', '', '', '', '3', '420900', '湖北省 孝感市 应城市');
INSERT INTO dv_area_base VALUES ('420982', '安陆市', '', '', '', '3', '420900', '湖北省 孝感市 安陆市');
INSERT INTO dv_area_base VALUES ('420984', '汉川市', '', '', '', '3', '420900', '湖北省 孝感市 汉川市');
INSERT INTO dv_area_base VALUES ('421000', '荆州市', '荆州', '', '', '2', '420000', '湖北省 荆州市');
INSERT INTO dv_area_base VALUES ('421002', '沙市区', '', '', '', '3', '421000', '湖北省 荆州市 沙市区');
INSERT INTO dv_area_base VALUES ('421003', '荆州区', '', '', '', '3', '421000', '湖北省 荆州市 荆州区');
INSERT INTO dv_area_base VALUES ('421022', '公安县', '', '', '', '3', '421000', '湖北省 荆州市 公安县');
INSERT INTO dv_area_base VALUES ('421023', '监利县', '', '', '', '3', '421000', '湖北省 荆州市 监利县');
INSERT INTO dv_area_base VALUES ('421024', '江陵县', '', '', '', '3', '421000', '湖北省 荆州市 江陵县');
INSERT INTO dv_area_base VALUES ('421081', '石首市', '', '', '', '3', '421000', '湖北省 荆州市 石首市');
INSERT INTO dv_area_base VALUES ('421083', '洪湖市', '', '', '', '3', '421000', '湖北省 荆州市 洪湖市');
INSERT INTO dv_area_base VALUES ('421087', '松滋市', '', '', '', '3', '421000', '湖北省 荆州市 松滋市');
INSERT INTO dv_area_base VALUES ('421100', '黄冈市', '黄冈', '', '', '2', '420000', '湖北省 黄冈市');
INSERT INTO dv_area_base VALUES ('421102', '黄州区', '', '', '', '3', '421100', '湖北省 黄冈市 黄州区');
INSERT INTO dv_area_base VALUES ('421121', '团风县', '', '', '', '3', '421100', '湖北省 黄冈市 团风县');
INSERT INTO dv_area_base VALUES ('421122', '红安县', '', '', '', '3', '421100', '湖北省 黄冈市 红安县');
INSERT INTO dv_area_base VALUES ('421123', '罗田县', '', '', '', '3', '421100', '湖北省 黄冈市 罗田县');
INSERT INTO dv_area_base VALUES ('421124', '英山县', '', '', '', '3', '421100', '湖北省 黄冈市 英山县');
INSERT INTO dv_area_base VALUES ('421125', '浠水县', '', '', '', '3', '421100', '湖北省 黄冈市 浠水县');
INSERT INTO dv_area_base VALUES ('421126', '蕲春县', '', '', '', '3', '421100', '湖北省 黄冈市 蕲春县');
INSERT INTO dv_area_base VALUES ('421127', '黄梅县', '', '', '', '3', '421100', '湖北省 黄冈市 黄梅县');
INSERT INTO dv_area_base VALUES ('421181', '麻城市', '', '', '', '3', '421100', '湖北省 黄冈市 麻城市');
INSERT INTO dv_area_base VALUES ('421182', '武穴市', '', '', '', '3', '421100', '湖北省 黄冈市 武穴市');
INSERT INTO dv_area_base VALUES ('421200', '咸宁市', '咸宁', '', '', '2', '420000', '湖北省 咸宁市');
INSERT INTO dv_area_base VALUES ('421202', '咸安区', '', '', '', '3', '421200', '湖北省 咸宁市 咸安区');
INSERT INTO dv_area_base VALUES ('421221', '嘉鱼县', '', '', '', '3', '421200', '湖北省 咸宁市 嘉鱼县');
INSERT INTO dv_area_base VALUES ('421222', '通城县', '', '', '', '3', '421200', '湖北省 咸宁市 通城县');
INSERT INTO dv_area_base VALUES ('421223', '崇阳县', '', '', '', '3', '421200', '湖北省 咸宁市 崇阳县');
INSERT INTO dv_area_base VALUES ('421224', '通山县', '', '', '', '3', '421200', '湖北省 咸宁市 通山县');
INSERT INTO dv_area_base VALUES ('421281', '赤壁市', '', '', '', '3', '421200', '湖北省 咸宁市 赤壁市');
INSERT INTO dv_area_base VALUES ('421300', '随州市', '随州', '', '', '2', '420000', '湖北省 随州市');
INSERT INTO dv_area_base VALUES ('421302', '曾都区', '', '', '', '3', '421300', '湖北省 随州市 曾都区');
INSERT INTO dv_area_base VALUES ('421381', '广水市', '', '', '', '3', '421300', '湖北省 随州市 广水市');
INSERT INTO dv_area_base VALUES ('422800', '恩施土家族苗族自治州', '恩施', '', '', '2', '420000', '湖北省 恩施土家族苗族自治州');
INSERT INTO dv_area_base VALUES ('422801', '恩施市', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 恩施市');
INSERT INTO dv_area_base VALUES ('422802', '利川市', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 利川市');
INSERT INTO dv_area_base VALUES ('422822', '建始县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 建始县');
INSERT INTO dv_area_base VALUES ('422823', '巴东县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 巴东县');
INSERT INTO dv_area_base VALUES ('422825', '宣恩县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 宣恩县');
INSERT INTO dv_area_base VALUES ('422826', '咸丰县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 咸丰县');
INSERT INTO dv_area_base VALUES ('422827', '来凤县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 来凤县');
INSERT INTO dv_area_base VALUES ('422828', '鹤峰县', '', '', '', '3', '422800', '湖北省 恩施土家族苗族自治州 鹤峰县');
INSERT INTO dv_area_base VALUES ('429004', '仙桃市', '仙桃', '', '', '2', '420000', '湖北省 仙桃市');
INSERT INTO dv_area_base VALUES ('429005', '潜江市', '潜江', '', '', '2', '420000', '湖北省 潜江市');
INSERT INTO dv_area_base VALUES ('429006', '天门市', '天门', '', '', '2', '420000', '湖北省 天门市');
INSERT INTO dv_area_base VALUES ('429021', '神农架林区', '神农架', '', '', '2', '420000', '湖北省 神农架林区');
INSERT INTO dv_area_base VALUES ('430000', '湖南省', '湖南', 'hunan', '湘', '1', '0', '湖南省');
INSERT INTO dv_area_base VALUES ('430100', '长沙市', '长沙', '', '', '2', '430000', '湖南省 长沙市');
INSERT INTO dv_area_base VALUES ('430102', '芙蓉区', '', '', '', '3', '430100', '湖南省 长沙市 芙蓉区');
INSERT INTO dv_area_base VALUES ('430103', '天心区', '', '', '', '3', '430100', '湖南省 长沙市 天心区');
INSERT INTO dv_area_base VALUES ('430104', '岳麓区', '', '', '', '3', '430100', '湖南省 长沙市 岳麓区');
INSERT INTO dv_area_base VALUES ('430105', '开福区', '', '', '', '3', '430100', '湖南省 长沙市 开福区');
INSERT INTO dv_area_base VALUES ('430111', '雨花区', '', '', '', '3', '430100', '湖南省 长沙市 雨花区');
INSERT INTO dv_area_base VALUES ('430121', '长沙县', '', '', '', '3', '430100', '湖南省 长沙市 长沙县');
INSERT INTO dv_area_base VALUES ('430122', '望城区', '', '', '', '3', '430100', '湖南省 长沙市 望城区');
INSERT INTO dv_area_base VALUES ('430124', '宁乡县', '', '', '', '3', '430100', '湖南省 长沙市 宁乡县');
INSERT INTO dv_area_base VALUES ('430181', '浏阳市', '', '', '', '3', '430100', '湖南省 长沙市 浏阳市');
INSERT INTO dv_area_base VALUES ('430200', '株洲市', '株洲', '', '', '2', '430000', '湖南省 株洲市');
INSERT INTO dv_area_base VALUES ('430202', '荷塘区', '', '', '', '3', '430200', '湖南省 株洲市 荷塘区');
INSERT INTO dv_area_base VALUES ('430203', '芦淞区', '', '', '', '3', '430200', '湖南省 株洲市 芦淞区');
INSERT INTO dv_area_base VALUES ('430204', '石峰区', '', '', '', '3', '430200', '湖南省 株洲市 石峰区');
INSERT INTO dv_area_base VALUES ('430211', '天元区', '', '', '', '3', '430200', '湖南省 株洲市 天元区');
INSERT INTO dv_area_base VALUES ('430221', '株洲县', '', '', '', '3', '430200', '湖南省 株洲市 株洲县');
INSERT INTO dv_area_base VALUES ('430223', '攸县', '', '', '', '3', '430200', '湖南省 株洲市 攸县');
INSERT INTO dv_area_base VALUES ('430224', '茶陵县', '', '', '', '3', '430200', '湖南省 株洲市 茶陵县');
INSERT INTO dv_area_base VALUES ('430225', '炎陵县', '', '', '', '3', '430200', '湖南省 株洲市 炎陵县');
INSERT INTO dv_area_base VALUES ('430281', '醴陵市', '', '', '', '3', '430200', '湖南省 株洲市 醴陵市');
INSERT INTO dv_area_base VALUES ('430300', '湘潭市', '湘潭', '', '', '2', '430000', '湖南省 湘潭市');
INSERT INTO dv_area_base VALUES ('430302', '雨湖区', '', '', '', '3', '430300', '湖南省 湘潭市 雨湖区');
INSERT INTO dv_area_base VALUES ('430304', '岳塘区', '', '', '', '3', '430300', '湖南省 湘潭市 岳塘区');
INSERT INTO dv_area_base VALUES ('430321', '湘潭县', '', '', '', '3', '430300', '湖南省 湘潭市 湘潭县');
INSERT INTO dv_area_base VALUES ('430381', '湘乡市', '', '', '', '3', '430300', '湖南省 湘潭市 湘乡市');
INSERT INTO dv_area_base VALUES ('430382', '韶山市', '', '', '', '3', '430300', '湖南省 湘潭市 韶山市');
INSERT INTO dv_area_base VALUES ('430400', '衡阳市', '衡阳', '', '', '2', '430000', '湖南省 衡阳市');
INSERT INTO dv_area_base VALUES ('430405', '珠晖区', '', '', '', '3', '430400', '湖南省 衡阳市 珠晖区');
INSERT INTO dv_area_base VALUES ('430406', '雁峰区', '', '', '', '3', '430400', '湖南省 衡阳市 雁峰区');
INSERT INTO dv_area_base VALUES ('430407', '石鼓区', '', '', '', '3', '430400', '湖南省 衡阳市 石鼓区');
INSERT INTO dv_area_base VALUES ('430408', '蒸湘区', '', '', '', '3', '430400', '湖南省 衡阳市 蒸湘区');
INSERT INTO dv_area_base VALUES ('430412', '南岳区', '', '', '', '3', '430400', '湖南省 衡阳市 南岳区');
INSERT INTO dv_area_base VALUES ('430421', '衡阳县', '', '', '', '3', '430400', '湖南省 衡阳市 衡阳县');
INSERT INTO dv_area_base VALUES ('430422', '衡南县', '', '', '', '3', '430400', '湖南省 衡阳市 衡南县');
INSERT INTO dv_area_base VALUES ('430423', '衡山县', '', '', '', '3', '430400', '湖南省 衡阳市 衡山县');
INSERT INTO dv_area_base VALUES ('430424', '衡东县', '', '', '', '3', '430400', '湖南省 衡阳市 衡东县');
INSERT INTO dv_area_base VALUES ('430426', '祁东县', '', '', '', '3', '430400', '湖南省 衡阳市 祁东县');
INSERT INTO dv_area_base VALUES ('430481', '耒阳市', '', '', '', '3', '430400', '湖南省 衡阳市 耒阳市');
INSERT INTO dv_area_base VALUES ('430482', '常宁市', '', '', '', '3', '430400', '湖南省 衡阳市 常宁市');
INSERT INTO dv_area_base VALUES ('430500', '邵阳市', '邵阳', '', '', '2', '430000', '湖南省 邵阳市');
INSERT INTO dv_area_base VALUES ('430502', '双清区', '', '', '', '3', '430500', '湖南省 邵阳市 双清区');
INSERT INTO dv_area_base VALUES ('430503', '大祥区', '', '', '', '3', '430500', '湖南省 邵阳市 大祥区');
INSERT INTO dv_area_base VALUES ('430511', '北塔区', '', '', '', '3', '430500', '湖南省 邵阳市 北塔区');
INSERT INTO dv_area_base VALUES ('430521', '邵东县', '', '', '', '3', '430500', '湖南省 邵阳市 邵东县');
INSERT INTO dv_area_base VALUES ('430522', '新邵县', '', '', '', '3', '430500', '湖南省 邵阳市 新邵县');
INSERT INTO dv_area_base VALUES ('430523', '邵阳县', '', '', '', '3', '430500', '湖南省 邵阳市 邵阳县');
INSERT INTO dv_area_base VALUES ('430524', '隆回县', '', '', '', '3', '430500', '湖南省 邵阳市 隆回县');
INSERT INTO dv_area_base VALUES ('430525', '洞口县', '', '', '', '3', '430500', '湖南省 邵阳市 洞口县');
INSERT INTO dv_area_base VALUES ('430527', '绥宁县', '', '', '', '3', '430500', '湖南省 邵阳市 绥宁县');
INSERT INTO dv_area_base VALUES ('430528', '新宁县', '', '', '', '3', '430500', '湖南省 邵阳市 新宁县');
INSERT INTO dv_area_base VALUES ('430529', '城步苗族自治县', '', '', '', '3', '430500', '湖南省 邵阳市 城步苗族自治县');
INSERT INTO dv_area_base VALUES ('430581', '武冈市', '', '', '', '3', '430500', '湖南省 邵阳市 武冈市');
INSERT INTO dv_area_base VALUES ('430600', '岳阳市', '岳阳', '', '', '2', '430000', '湖南省 岳阳市');
INSERT INTO dv_area_base VALUES ('430602', '岳阳楼区', '', '', '', '3', '430600', '湖南省 岳阳市 岳阳楼区');
INSERT INTO dv_area_base VALUES ('430603', '云溪区', '', '', '', '3', '430600', '湖南省 岳阳市 云溪区');
INSERT INTO dv_area_base VALUES ('430611', '君山区', '', '', '', '3', '430600', '湖南省 岳阳市 君山区');
INSERT INTO dv_area_base VALUES ('430621', '岳阳县', '', '', '', '3', '430600', '湖南省 岳阳市 岳阳县');
INSERT INTO dv_area_base VALUES ('430623', '华容县', '', '', '', '3', '430600', '湖南省 岳阳市 华容县');
INSERT INTO dv_area_base VALUES ('430624', '湘阴县', '', '', '', '3', '430600', '湖南省 岳阳市 湘阴县');
INSERT INTO dv_area_base VALUES ('430626', '平江县', '', '', '', '3', '430600', '湖南省 岳阳市 平江县');
INSERT INTO dv_area_base VALUES ('430681', '汨罗市', '', '', '', '3', '430600', '湖南省 岳阳市 汨罗市');
INSERT INTO dv_area_base VALUES ('430682', '临湘市', '', '', '', '3', '430600', '湖南省 岳阳市 临湘市');
INSERT INTO dv_area_base VALUES ('430700', '常德市', '常德', '', '', '2', '430000', '湖南省 常德市');
INSERT INTO dv_area_base VALUES ('430702', '武陵区', '', '', '', '3', '430700', '湖南省 常德市 武陵区');
INSERT INTO dv_area_base VALUES ('430703', '鼎城区', '', '', '', '3', '430700', '湖南省 常德市 鼎城区');
INSERT INTO dv_area_base VALUES ('430721', '安乡县', '', '', '', '3', '430700', '湖南省 常德市 安乡县');
INSERT INTO dv_area_base VALUES ('430722', '汉寿县', '', '', '', '3', '430700', '湖南省 常德市 汉寿县');
INSERT INTO dv_area_base VALUES ('430723', '澧县', '', '', '', '3', '430700', '湖南省 常德市 澧县');
INSERT INTO dv_area_base VALUES ('430724', '临澧县', '', '', '', '3', '430700', '湖南省 常德市 临澧县');
INSERT INTO dv_area_base VALUES ('430725', '桃源县', '', '', '', '3', '430700', '湖南省 常德市 桃源县');
INSERT INTO dv_area_base VALUES ('430726', '石门县', '', '', '', '3', '430700', '湖南省 常德市 石门县');
INSERT INTO dv_area_base VALUES ('430781', '津市市', '', '', '', '3', '430700', '湖南省 常德市 津市市');
INSERT INTO dv_area_base VALUES ('430800', '张家界市', '张家界', '', '', '2', '430000', '湖南省 张家界市');
INSERT INTO dv_area_base VALUES ('430802', '永定区', '', '', '', '3', '430800', '湖南省 张家界市 永定区');
INSERT INTO dv_area_base VALUES ('430811', '武陵源区', '', '', '', '3', '430800', '湖南省 张家界市 武陵源区');
INSERT INTO dv_area_base VALUES ('430821', '慈利县', '', '', '', '3', '430800', '湖南省 张家界市 慈利县');
INSERT INTO dv_area_base VALUES ('430822', '桑植县', '', '', '', '3', '430800', '湖南省 张家界市 桑植县');
INSERT INTO dv_area_base VALUES ('430900', '益阳市', '益阳', '', '', '2', '430000', '湖南省 益阳市');
INSERT INTO dv_area_base VALUES ('430902', '资阳区', '', '', '', '3', '430900', '湖南省 益阳市 资阳区');
INSERT INTO dv_area_base VALUES ('430903', '赫山区', '', '', '', '3', '430900', '湖南省 益阳市 赫山区');
INSERT INTO dv_area_base VALUES ('430921', '南县', '', '', '', '3', '430900', '湖南省 益阳市 南县');
INSERT INTO dv_area_base VALUES ('430922', '桃江县', '', '', '', '3', '430900', '湖南省 益阳市 桃江县');
INSERT INTO dv_area_base VALUES ('430923', '安化县', '', '', '', '3', '430900', '湖南省 益阳市 安化县');
INSERT INTO dv_area_base VALUES ('430981', '沅江市', '', '', '', '3', '430900', '湖南省 益阳市 沅江市');
INSERT INTO dv_area_base VALUES ('431000', '郴州市', '郴州', '', '', '2', '430000', '湖南省 郴州市');
INSERT INTO dv_area_base VALUES ('431002', '北湖区', '', '', '', '3', '431000', '湖南省 郴州市 北湖区');
INSERT INTO dv_area_base VALUES ('431003', '苏仙区', '', '', '', '3', '431000', '湖南省 郴州市 苏仙区');
INSERT INTO dv_area_base VALUES ('431021', '桂阳县', '', '', '', '3', '431000', '湖南省 郴州市 桂阳县');
INSERT INTO dv_area_base VALUES ('431022', '宜章县', '', '', '', '3', '431000', '湖南省 郴州市 宜章县');
INSERT INTO dv_area_base VALUES ('431023', '永兴县', '', '', '', '3', '431000', '湖南省 郴州市 永兴县');
INSERT INTO dv_area_base VALUES ('431024', '嘉禾县', '', '', '', '3', '431000', '湖南省 郴州市 嘉禾县');
INSERT INTO dv_area_base VALUES ('431025', '临武县', '', '', '', '3', '431000', '湖南省 郴州市 临武县');
INSERT INTO dv_area_base VALUES ('431026', '汝城县', '', '', '', '3', '431000', '湖南省 郴州市 汝城县');
INSERT INTO dv_area_base VALUES ('431027', '桂东县', '', '', '', '3', '431000', '湖南省 郴州市 桂东县');
INSERT INTO dv_area_base VALUES ('431028', '安仁县', '', '', '', '3', '431000', '湖南省 郴州市 安仁县');
INSERT INTO dv_area_base VALUES ('431081', '资兴市', '', '', '', '3', '431000', '湖南省 郴州市 资兴市');
INSERT INTO dv_area_base VALUES ('431100', '永州市', '永州', '', '', '2', '430000', '湖南省 永州市');
INSERT INTO dv_area_base VALUES ('431102', '零陵区', '', '', '', '3', '431100', '湖南省 永州市 零陵区');
INSERT INTO dv_area_base VALUES ('431103', '冷水滩区', '', '', '', '3', '431100', '湖南省 永州市 冷水滩区');
INSERT INTO dv_area_base VALUES ('431121', '祁阳县', '', '', '', '3', '431100', '湖南省 永州市 祁阳县');
INSERT INTO dv_area_base VALUES ('431122', '东安县', '', '', '', '3', '431100', '湖南省 永州市 东安县');
INSERT INTO dv_area_base VALUES ('431123', '双牌县', '', '', '', '3', '431100', '湖南省 永州市 双牌县');
INSERT INTO dv_area_base VALUES ('431124', '道县', '', '', '', '3', '431100', '湖南省 永州市 道县');
INSERT INTO dv_area_base VALUES ('431125', '江永县', '', '', '', '3', '431100', '湖南省 永州市 江永县');
INSERT INTO dv_area_base VALUES ('431126', '宁远县', '', '', '', '3', '431100', '湖南省 永州市 宁远县');
INSERT INTO dv_area_base VALUES ('431127', '蓝山县', '', '', '', '3', '431100', '湖南省 永州市 蓝山县');
INSERT INTO dv_area_base VALUES ('431128', '新田县', '', '', '', '3', '431100', '湖南省 永州市 新田县');
INSERT INTO dv_area_base VALUES ('431129', '江华瑶族自治县', '', '', '', '3', '431100', '湖南省 永州市 江华瑶族自治县');
INSERT INTO dv_area_base VALUES ('431200', '怀化市', '怀化', '', '', '2', '430000', '湖南省 怀化市');
INSERT INTO dv_area_base VALUES ('431202', '鹤城区', '', '', '', '3', '431200', '湖南省 怀化市 鹤城区');
INSERT INTO dv_area_base VALUES ('431221', '中方县', '', '', '', '3', '431200', '湖南省 怀化市 中方县');
INSERT INTO dv_area_base VALUES ('431222', '沅陵县', '', '', '', '3', '431200', '湖南省 怀化市 沅陵县');
INSERT INTO dv_area_base VALUES ('431223', '辰溪县', '', '', '', '3', '431200', '湖南省 怀化市 辰溪县');
INSERT INTO dv_area_base VALUES ('431224', '溆浦县', '', '', '', '3', '431200', '湖南省 怀化市 溆浦县');
INSERT INTO dv_area_base VALUES ('431225', '会同县', '', '', '', '3', '431200', '湖南省 怀化市 会同县');
INSERT INTO dv_area_base VALUES ('431226', '麻阳苗族自治县', '', '', '', '3', '431200', '湖南省 怀化市 麻阳苗族自治县');
INSERT INTO dv_area_base VALUES ('431227', '新晃侗族自治县', '', '', '', '3', '431200', '湖南省 怀化市 新晃侗族自治县');
INSERT INTO dv_area_base VALUES ('431228', '芷江侗族自治县', '', '', '', '3', '431200', '湖南省 怀化市 芷江侗族自治县');
INSERT INTO dv_area_base VALUES ('431229', '靖州苗族侗族自治县', '', '', '', '3', '431200', '湖南省 怀化市 靖州苗族侗族自治县');
INSERT INTO dv_area_base VALUES ('431230', '通道侗族自治县', '', '', '', '3', '431200', '湖南省 怀化市 通道侗族自治县');
INSERT INTO dv_area_base VALUES ('431281', '洪江市', '', '', '', '3', '431200', '湖南省 怀化市 洪江市');
INSERT INTO dv_area_base VALUES ('431300', '娄底市', '娄底', '', '', '2', '430000', '湖南省 娄底市');
INSERT INTO dv_area_base VALUES ('431302', '娄星区', '', '', '', '3', '431300', '湖南省 娄底市 娄星区');
INSERT INTO dv_area_base VALUES ('431321', '双峰县', '', '', '', '3', '431300', '湖南省 娄底市 双峰县');
INSERT INTO dv_area_base VALUES ('431322', '新化县', '', '', '', '3', '431300', '湖南省 娄底市 新化县');
INSERT INTO dv_area_base VALUES ('431381', '冷水江市', '', '', '', '3', '431300', '湖南省 娄底市 冷水江市');
INSERT INTO dv_area_base VALUES ('431382', '涟源市', '', '', '', '3', '431300', '湖南省 娄底市 涟源市');
INSERT INTO dv_area_base VALUES ('433100', '湘西土家族苗族自治州', '湘西', '', '', '2', '430000', '湖南省 湘西土家族苗族自治州');
INSERT INTO dv_area_base VALUES ('433101', '吉首市', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 吉首市');
INSERT INTO dv_area_base VALUES ('433122', '泸溪县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 泸溪县');
INSERT INTO dv_area_base VALUES ('433123', '凤凰县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 凤凰县');
INSERT INTO dv_area_base VALUES ('433124', '花垣县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 花垣县');
INSERT INTO dv_area_base VALUES ('433125', '保靖县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 保靖县');
INSERT INTO dv_area_base VALUES ('433126', '古丈县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 古丈县');
INSERT INTO dv_area_base VALUES ('433127', '永顺县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 永顺县');
INSERT INTO dv_area_base VALUES ('433130', '龙山县', '', '', '', '3', '433100', '湖南省 湘西土家族苗族自治州 龙山县');
INSERT INTO dv_area_base VALUES ('440000', '广东省', '广东', 'guangdong', '粤', '1', '0', '广东省');
INSERT INTO dv_area_base VALUES ('440100', '广州市', '广州', '', '', '2', '440000', '广东省 广州市');
INSERT INTO dv_area_base VALUES ('440103', '荔湾区', '', '', '', '3', '440100', '广东省 广州市 荔湾区');
INSERT INTO dv_area_base VALUES ('440104', '越秀区', '', '', '', '3', '440100', '广东省 广州市 越秀区');
INSERT INTO dv_area_base VALUES ('440105', '海珠区', '', '', '', '3', '440100', '广东省 广州市 海珠区');
INSERT INTO dv_area_base VALUES ('440106', '天河区', '', '', '', '3', '440100', '广东省 广州市 天河区');
INSERT INTO dv_area_base VALUES ('440111', '白云区', '', '', '', '3', '440100', '广东省 广州市 白云区');
INSERT INTO dv_area_base VALUES ('440112', '黄埔区', '', '', '', '3', '440100', '广东省 广州市 黄埔区');
INSERT INTO dv_area_base VALUES ('440113', '番禺区', '', '', '', '3', '440100', '广东省 广州市 番禺区');
INSERT INTO dv_area_base VALUES ('440114', '花都区', '', '', '', '3', '440100', '广东省 广州市 花都区');
INSERT INTO dv_area_base VALUES ('440115', '南沙区', '', '', '', '3', '440100', '广东省 广州市 南沙区');
INSERT INTO dv_area_base VALUES ('440116', '萝岗区', '', '', '', '3', '440100', '广东省 广州市 萝岗区');
INSERT INTO dv_area_base VALUES ('440183', '增城市', '', '', '', '3', '440100', '广东省 广州市 增城市');
INSERT INTO dv_area_base VALUES ('440184', '从化市', '', '', '', '3', '440100', '广东省 广州市 从化市');
INSERT INTO dv_area_base VALUES ('440200', '韶关市', '韶关', '', '', '2', '440000', '广东省 韶关市');
INSERT INTO dv_area_base VALUES ('440203', '武江区', '', '', '', '3', '440200', '广东省 韶关市 武江区');
INSERT INTO dv_area_base VALUES ('440204', '浈江区', '', '', '', '3', '440200', '广东省 韶关市 浈江区');
INSERT INTO dv_area_base VALUES ('440205', '曲江区', '', '', '', '3', '440200', '广东省 韶关市 曲江区');
INSERT INTO dv_area_base VALUES ('440222', '始兴县', '', '', '', '3', '440200', '广东省 韶关市 始兴县');
INSERT INTO dv_area_base VALUES ('440224', '仁化县', '', '', '', '3', '440200', '广东省 韶关市 仁化县');
INSERT INTO dv_area_base VALUES ('440229', '翁源县', '', '', '', '3', '440200', '广东省 韶关市 翁源县');
INSERT INTO dv_area_base VALUES ('440232', '乳源瑶族自治县', '', '', '', '3', '440200', '广东省 韶关市 乳源瑶族自治县');
INSERT INTO dv_area_base VALUES ('440233', '新丰县', '', '', '', '3', '440200', '广东省 韶关市 新丰县');
INSERT INTO dv_area_base VALUES ('440281', '乐昌市', '', '', '', '3', '440200', '广东省 韶关市 乐昌市');
INSERT INTO dv_area_base VALUES ('440282', '南雄市', '', '', '', '3', '440200', '广东省 韶关市 南雄市');
INSERT INTO dv_area_base VALUES ('440300', '深圳市', '深圳', '', '', '2', '440000', '广东省 深圳市');
INSERT INTO dv_area_base VALUES ('440303', '罗湖区', '', '', '', '3', '440300', '广东省 深圳市 罗湖区');
INSERT INTO dv_area_base VALUES ('440304', '福田区', '', '', '', '3', '440300', '广东省 深圳市 福田区');
INSERT INTO dv_area_base VALUES ('440305', '南山区', '', '', '', '3', '440300', '广东省 深圳市 南山区');
INSERT INTO dv_area_base VALUES ('440306', '宝安区', '', '', '', '3', '440300', '广东省 深圳市 宝安区');
INSERT INTO dv_area_base VALUES ('440307', '龙岗区', '', '', '', '3', '440300', '广东省 深圳市 龙岗区');
INSERT INTO dv_area_base VALUES ('440308', '盐田区', '', '', '', '3', '440300', '广东省 深圳市 盐田区');
INSERT INTO dv_area_base VALUES ('440400', '珠海市', '珠海', '', '', '2', '440000', '广东省 珠海市');
INSERT INTO dv_area_base VALUES ('440402', '香洲区', '', '', '', '3', '440400', '广东省 珠海市 香洲区');
INSERT INTO dv_area_base VALUES ('440403', '斗门区', '', '', '', '3', '440400', '广东省 珠海市 斗门区');
INSERT INTO dv_area_base VALUES ('440404', '金湾区', '', '', '', '3', '440400', '广东省 珠海市 金湾区');
INSERT INTO dv_area_base VALUES ('440500', '汕头市', '汕头', '', '', '2', '440000', '广东省 汕头市');
INSERT INTO dv_area_base VALUES ('440507', '龙湖区', '', '', '', '3', '440500', '广东省 汕头市 龙湖区');
INSERT INTO dv_area_base VALUES ('440511', '金平区', '', '', '', '3', '440500', '广东省 汕头市 金平区');
INSERT INTO dv_area_base VALUES ('440512', '濠江区', '', '', '', '3', '440500', '广东省 汕头市 濠江区');
INSERT INTO dv_area_base VALUES ('440513', '潮阳区', '', '', '', '3', '440500', '广东省 汕头市 潮阳区');
INSERT INTO dv_area_base VALUES ('440514', '潮南区', '', '', '', '3', '440500', '广东省 汕头市 潮南区');
INSERT INTO dv_area_base VALUES ('440515', '澄海区', '', '', '', '3', '440500', '广东省 汕头市 澄海区');
INSERT INTO dv_area_base VALUES ('440523', '南澳县', '', '', '', '3', '440500', '广东省 汕头市 南澳县');
INSERT INTO dv_area_base VALUES ('440600', '佛山市', '佛山', '', '', '2', '440000', '广东省 佛山市');
INSERT INTO dv_area_base VALUES ('440604', '禅城区', '', '', '', '3', '440600', '广东省 佛山市 禅城区');
INSERT INTO dv_area_base VALUES ('440605', '南海区', '', '', '', '3', '440600', '广东省 佛山市 南海区');
INSERT INTO dv_area_base VALUES ('440606', '顺德区', '', '', '', '3', '440600', '广东省 佛山市 顺德区');
INSERT INTO dv_area_base VALUES ('440607', '三水区', '', '', '', '3', '440600', '广东省 佛山市 三水区');
INSERT INTO dv_area_base VALUES ('440608', '高明区', '', '', '', '3', '440600', '广东省 佛山市 高明区');
INSERT INTO dv_area_base VALUES ('440700', '江门市', '江门', '', '', '2', '440000', '广东省 江门市');
INSERT INTO dv_area_base VALUES ('440703', '蓬江区', '', '', '', '3', '440700', '广东省 江门市 蓬江区');
INSERT INTO dv_area_base VALUES ('440704', '江海区', '', '', '', '3', '440700', '广东省 江门市 江海区');
INSERT INTO dv_area_base VALUES ('440705', '新会区', '', '', '', '3', '440700', '广东省 江门市 新会区');
INSERT INTO dv_area_base VALUES ('440781', '台山市', '', '', '', '3', '440700', '广东省 江门市 台山市');
INSERT INTO dv_area_base VALUES ('440783', '开平市', '', '', '', '3', '440700', '广东省 江门市 开平市');
INSERT INTO dv_area_base VALUES ('440784', '鹤山市', '', '', '', '3', '440700', '广东省 江门市 鹤山市');
INSERT INTO dv_area_base VALUES ('440785', '恩平市', '', '', '', '3', '440700', '广东省 江门市 恩平市');
INSERT INTO dv_area_base VALUES ('440800', '湛江市', '湛江', '', '', '2', '440000', '广东省 湛江市');
INSERT INTO dv_area_base VALUES ('440802', '赤坎区', '', '', '', '3', '440800', '广东省 湛江市 赤坎区');
INSERT INTO dv_area_base VALUES ('440803', '霞山区', '', '', '', '3', '440800', '广东省 湛江市 霞山区');
INSERT INTO dv_area_base VALUES ('440804', '坡头区', '', '', '', '3', '440800', '广东省 湛江市 坡头区');
INSERT INTO dv_area_base VALUES ('440811', '麻章区', '', '', '', '3', '440800', '广东省 湛江市 麻章区');
INSERT INTO dv_area_base VALUES ('440823', '遂溪县', '', '', '', '3', '440800', '广东省 湛江市 遂溪县');
INSERT INTO dv_area_base VALUES ('440825', '徐闻县', '', '', '', '3', '440800', '广东省 湛江市 徐闻县');
INSERT INTO dv_area_base VALUES ('440881', '廉江市', '', '', '', '3', '440800', '广东省 湛江市 廉江市');
INSERT INTO dv_area_base VALUES ('440882', '雷州市', '', '', '', '3', '440800', '广东省 湛江市 雷州市');
INSERT INTO dv_area_base VALUES ('440883', '吴川市', '', '', '', '3', '440800', '广东省 湛江市 吴川市');
INSERT INTO dv_area_base VALUES ('440900', '茂名市', '茂名', '', '', '2', '440000', '广东省 茂名市');
INSERT INTO dv_area_base VALUES ('440902', '茂南区', '', '', '', '3', '440900', '广东省 茂名市 茂南区');
INSERT INTO dv_area_base VALUES ('440903', '茂港区', '', '', '', '3', '440900', '广东省 茂名市 茂港区');
INSERT INTO dv_area_base VALUES ('440923', '电白县', '', '', '', '3', '440900', '广东省 茂名市 电白县');
INSERT INTO dv_area_base VALUES ('440981', '高州市', '', '', '', '3', '440900', '广东省 茂名市 高州市');
INSERT INTO dv_area_base VALUES ('440982', '化州市', '', '', '', '3', '440900', '广东省 茂名市 化州市');
INSERT INTO dv_area_base VALUES ('440983', '信宜市', '', '', '', '3', '440900', '广东省 茂名市 信宜市');
INSERT INTO dv_area_base VALUES ('441200', '肇庆市', '肇庆', '', '', '2', '440000', '广东省 肇庆市');
INSERT INTO dv_area_base VALUES ('441202', '端州区', '', '', '', '3', '441200', '广东省 肇庆市 端州区');
INSERT INTO dv_area_base VALUES ('441203', '鼎湖区', '', '', '', '3', '441200', '广东省 肇庆市 鼎湖区');
INSERT INTO dv_area_base VALUES ('441223', '广宁县', '', '', '', '3', '441200', '广东省 肇庆市 广宁县');
INSERT INTO dv_area_base VALUES ('441224', '怀集县', '', '', '', '3', '441200', '广东省 肇庆市 怀集县');
INSERT INTO dv_area_base VALUES ('441225', '封开县', '', '', '', '3', '441200', '广东省 肇庆市 封开县');
INSERT INTO dv_area_base VALUES ('441226', '德庆县', '', '', '', '3', '441200', '广东省 肇庆市 德庆县');
INSERT INTO dv_area_base VALUES ('441283', '高要市', '', '', '', '3', '441200', '广东省 肇庆市 高要市');
INSERT INTO dv_area_base VALUES ('441284', '四会市', '', '', '', '3', '441200', '广东省 肇庆市 四会市');
INSERT INTO dv_area_base VALUES ('441300', '惠州市', '惠州', '', '', '2', '440000', '广东省 惠州市');
INSERT INTO dv_area_base VALUES ('441302', '惠城区', '', '', '', '3', '441300', '广东省 惠州市 惠城区');
INSERT INTO dv_area_base VALUES ('441303', '惠阳区', '', '', '', '3', '441300', '广东省 惠州市 惠阳区');
INSERT INTO dv_area_base VALUES ('441322', '博罗县', '', '', '', '3', '441300', '广东省 惠州市 博罗县');
INSERT INTO dv_area_base VALUES ('441323', '惠东县', '', '', '', '3', '441300', '广东省 惠州市 惠东县');
INSERT INTO dv_area_base VALUES ('441324', '龙门县', '', '', '', '3', '441300', '广东省 惠州市 龙门县');
INSERT INTO dv_area_base VALUES ('441400', '梅州市', '梅州', '', '', '2', '440000', '广东省 梅州市');
INSERT INTO dv_area_base VALUES ('441402', '梅江区', '', '', '', '3', '441400', '广东省 梅州市 梅江区');
INSERT INTO dv_area_base VALUES ('441421', '梅县', '', '', '', '3', '441400', '广东省 梅州市 梅县');
INSERT INTO dv_area_base VALUES ('441422', '大埔县', '', '', '', '3', '441400', '广东省 梅州市 大埔县');
INSERT INTO dv_area_base VALUES ('441423', '丰顺县', '', '', '', '3', '441400', '广东省 梅州市 丰顺县');
INSERT INTO dv_area_base VALUES ('441424', '五华县', '', '', '', '3', '441400', '广东省 梅州市 五华县');
INSERT INTO dv_area_base VALUES ('441426', '平远县', '', '', '', '3', '441400', '广东省 梅州市 平远县');
INSERT INTO dv_area_base VALUES ('441427', '蕉岭县', '', '', '', '3', '441400', '广东省 梅州市 蕉岭县');
INSERT INTO dv_area_base VALUES ('441481', '兴宁市', '', '', '', '3', '441400', '广东省 梅州市 兴宁市');
INSERT INTO dv_area_base VALUES ('441500', '汕尾市', '汕尾', '', '', '2', '440000', '广东省 汕尾市');
INSERT INTO dv_area_base VALUES ('441502', '城区', '', '', '', '3', '441500', '广东省 汕尾市 城区');
INSERT INTO dv_area_base VALUES ('441521', '海丰县', '', '', '', '3', '441500', '广东省 汕尾市 海丰县');
INSERT INTO dv_area_base VALUES ('441523', '陆河县', '', '', '', '3', '441500', '广东省 汕尾市 陆河县');
INSERT INTO dv_area_base VALUES ('441581', '陆丰市', '', '', '', '3', '441500', '广东省 汕尾市 陆丰市');
INSERT INTO dv_area_base VALUES ('441600', '河源市', '河源', '', '', '2', '440000', '广东省 河源市');
INSERT INTO dv_area_base VALUES ('441602', '源城区', '', '', '', '3', '441600', '广东省 河源市 源城区');
INSERT INTO dv_area_base VALUES ('441621', '紫金县', '', '', '', '3', '441600', '广东省 河源市 紫金县');
INSERT INTO dv_area_base VALUES ('441622', '龙川县', '', '', '', '3', '441600', '广东省 河源市 龙川县');
INSERT INTO dv_area_base VALUES ('441623', '连平县', '', '', '', '3', '441600', '广东省 河源市 连平县');
INSERT INTO dv_area_base VALUES ('441624', '和平县', '', '', '', '3', '441600', '广东省 河源市 和平县');
INSERT INTO dv_area_base VALUES ('441625', '东源县', '', '', '', '3', '441600', '广东省 河源市 东源县');
INSERT INTO dv_area_base VALUES ('441700', '阳江市', '阳江', '', '', '2', '440000', '广东省 阳江市');
INSERT INTO dv_area_base VALUES ('441702', '江城区', '', '', '', '3', '441700', '广东省 阳江市 江城区');
INSERT INTO dv_area_base VALUES ('441721', '阳西县', '', '', '', '3', '441700', '广东省 阳江市 阳西县');
INSERT INTO dv_area_base VALUES ('441723', '阳东县', '', '', '', '3', '441700', '广东省 阳江市 阳东县');
INSERT INTO dv_area_base VALUES ('441781', '阳春市', '', '', '', '3', '441700', '广东省 阳江市 阳春市');
INSERT INTO dv_area_base VALUES ('441800', '清远市', '清远', '', '', '2', '440000', '广东省 清远市');
INSERT INTO dv_area_base VALUES ('441802', '清城区', '', '', '', '3', '441800', '广东省 清远市 清城区');
INSERT INTO dv_area_base VALUES ('441821', '佛冈县', '', '', '', '3', '441800', '广东省 清远市 佛冈县');
INSERT INTO dv_area_base VALUES ('441823', '阳山县', '', '', '', '3', '441800', '广东省 清远市 阳山县');
INSERT INTO dv_area_base VALUES ('441825', '连山壮族瑶族自治县', '', '', '', '3', '441800', '广东省 清远市 连山壮族瑶族自治县');
INSERT INTO dv_area_base VALUES ('441826', '连南瑶族自治县', '', '', '', '3', '441800', '广东省 清远市 连南瑶族自治县');
INSERT INTO dv_area_base VALUES ('441827', '清新县', '', '', '', '3', '441800', '广东省 清远市 清新县');
INSERT INTO dv_area_base VALUES ('441881', '英德市', '', '', '', '3', '441800', '广东省 清远市 英德市');
INSERT INTO dv_area_base VALUES ('441882', '连州市', '', '', '', '3', '441800', '广东省 清远市 连州市');
INSERT INTO dv_area_base VALUES ('441900', '东莞市', '东莞', '', '', '2', '440000', '广东省 东莞市');
INSERT INTO dv_area_base VALUES ('442000', '中山市', '中山', '', '', '2', '440000', '广东省 中山市');
INSERT INTO dv_area_base VALUES ('445100', '潮州市', '潮州', '', '', '2', '440000', '广东省 潮州市');
INSERT INTO dv_area_base VALUES ('445102', '湘桥区', '', '', '', '3', '445100', '广东省 潮州市 湘桥区');
INSERT INTO dv_area_base VALUES ('445121', '潮安县', '', '', '', '3', '445100', '广东省 潮州市 潮安县');
INSERT INTO dv_area_base VALUES ('445122', '饶平县', '', '', '', '3', '445100', '广东省 潮州市 饶平县');
INSERT INTO dv_area_base VALUES ('445200', '揭阳市', '揭阳', '', '', '2', '440000', '广东省 揭阳市');
INSERT INTO dv_area_base VALUES ('445202', '榕城区', '', '', '', '3', '445200', '广东省 揭阳市 榕城区');
INSERT INTO dv_area_base VALUES ('445221', '揭东县', '', '', '', '3', '445200', '广东省 揭阳市 揭东县');
INSERT INTO dv_area_base VALUES ('445222', '揭西县', '', '', '', '3', '445200', '广东省 揭阳市 揭西县');
INSERT INTO dv_area_base VALUES ('445224', '惠来县', '', '', '', '3', '445200', '广东省 揭阳市 惠来县');
INSERT INTO dv_area_base VALUES ('445281', '普宁市', '', '', '', '3', '445200', '广东省 揭阳市 普宁市');
INSERT INTO dv_area_base VALUES ('445300', '云浮市', '云浮', '', '', '2', '440000', '广东省 云浮市');
INSERT INTO dv_area_base VALUES ('445302', '云城区', '', '', '', '3', '445300', '广东省 云浮市 云城区');
INSERT INTO dv_area_base VALUES ('445321', '新兴县', '', '', '', '3', '445300', '广东省 云浮市 新兴县');
INSERT INTO dv_area_base VALUES ('445322', '郁南县', '', '', '', '3', '445300', '广东省 云浮市 郁南县');
INSERT INTO dv_area_base VALUES ('445323', '云安县', '', '', '', '3', '445300', '广东省 云浮市 云安县');
INSERT INTO dv_area_base VALUES ('445381', '罗定市', '', '', '', '3', '445300', '广东省 云浮市 罗定市');
INSERT INTO dv_area_base VALUES ('450000', '广西壮族自治区', '广西', 'guangxi', '桂', '1', '0', '广西壮族自治区');
INSERT INTO dv_area_base VALUES ('450100', '南宁市', '南宁', '', '', '2', '450000', '广西壮族自治区 南宁市');
INSERT INTO dv_area_base VALUES ('450102', '兴宁区', '', '', '', '3', '450100', '广西省 南宁市 兴宁区');
INSERT INTO dv_area_base VALUES ('450103', '青秀区', '', '', '', '3', '450100', '广西省 南宁市 青秀区');
INSERT INTO dv_area_base VALUES ('450105', '江南区', '', '', '', '3', '450100', '广西省 南宁市 江南区');
INSERT INTO dv_area_base VALUES ('450107', '西乡塘区', '', '', '', '3', '450100', '广西省 南宁市 西乡塘区');
INSERT INTO dv_area_base VALUES ('450108', '良庆区', '', '', '', '3', '450100', '广西省 南宁市 良庆区');
INSERT INTO dv_area_base VALUES ('450109', '邕宁区', '', '', '', '3', '450100', '广西省 南宁市 邕宁区');
INSERT INTO dv_area_base VALUES ('450122', '武鸣县', '', '', '', '3', '450100', '广西省 南宁市 武鸣县');
INSERT INTO dv_area_base VALUES ('450123', '隆安县', '', '', '', '3', '450100', '广西省 南宁市 隆安县');
INSERT INTO dv_area_base VALUES ('450124', '马山县', '', '', '', '3', '450100', '广西省 南宁市 马山县');
INSERT INTO dv_area_base VALUES ('450125', '上林县', '', '', '', '3', '450100', '广西省 南宁市 上林县');
INSERT INTO dv_area_base VALUES ('450126', '宾阳县', '', '', '', '3', '450100', '广西省 南宁市 宾阳县');
INSERT INTO dv_area_base VALUES ('450127', '横县', '', '', '', '3', '450100', '广西省 南宁市 横县');
INSERT INTO dv_area_base VALUES ('450200', '柳州市', '柳州', '', '', '2', '450000', '广西壮族自治区 南宁市');
INSERT INTO dv_area_base VALUES ('450202', '城中区', '', '', '', '3', '450200', '广西省 柳州市 城中区');
INSERT INTO dv_area_base VALUES ('450203', '鱼峰区', '', '', '', '3', '450200', '广西省 柳州市 鱼峰区');
INSERT INTO dv_area_base VALUES ('450204', '柳南区', '', '', '', '3', '450200', '广西省 柳州市 柳南区');
INSERT INTO dv_area_base VALUES ('450205', '柳北区', '', '', '', '3', '450200', '广西省 柳州市 柳北区');
INSERT INTO dv_area_base VALUES ('450221', '柳江县', '', '', '', '3', '450200', '广西省 柳州市 柳江县');
INSERT INTO dv_area_base VALUES ('450222', '柳城县', '', '', '', '3', '450200', '广西省 柳州市 柳城县');
INSERT INTO dv_area_base VALUES ('450223', '鹿寨县', '', '', '', '3', '450200', '广西省 柳州市 鹿寨县');
INSERT INTO dv_area_base VALUES ('450224', '融安县', '', '', '', '3', '450200', '广西省 柳州市 融安县');
INSERT INTO dv_area_base VALUES ('450225', '融水苗族自治县', '', '', '', '3', '450200', '广西省 柳州市 融水苗族自治县');
INSERT INTO dv_area_base VALUES ('450226', '三江侗族自治县', '', '', '', '3', '450200', '广西省 柳州市 三江侗族自治县');
INSERT INTO dv_area_base VALUES ('450300', '桂林市', '桂林', '', '', '2', '450000', '广西壮族自治区 柳州市');
INSERT INTO dv_area_base VALUES ('450302', '秀峰区', '', '', '', '3', '450300', '广西省 桂林市 秀峰区');
INSERT INTO dv_area_base VALUES ('450303', '叠彩区', '', '', '', '3', '450300', '广西省 桂林市 叠彩区');
INSERT INTO dv_area_base VALUES ('450304', '象山区', '', '', '', '3', '450300', '广西省 桂林市 象山区');
INSERT INTO dv_area_base VALUES ('450305', '七星区', '', '', '', '3', '450300', '广西省 桂林市 七星区');
INSERT INTO dv_area_base VALUES ('450311', '雁山区', '', '', '', '3', '450300', '广西省 桂林市 雁山区');
INSERT INTO dv_area_base VALUES ('450321', '阳朔县', '', '', '', '3', '450300', '广西省 桂林市 阳朔县');
INSERT INTO dv_area_base VALUES ('450322', '临桂县', '', '', '', '3', '450300', '广西省 桂林市 临桂县');
INSERT INTO dv_area_base VALUES ('450323', '灵川县', '', '', '', '3', '450300', '广西省 桂林市 灵川县');
INSERT INTO dv_area_base VALUES ('450324', '全州县', '', '', '', '3', '450300', '广西省 桂林市 全州县');
INSERT INTO dv_area_base VALUES ('450325', '兴安县', '', '', '', '3', '450300', '广西省 桂林市 兴安县');
INSERT INTO dv_area_base VALUES ('450326', '永福县', '', '', '', '3', '450300', '广西省 桂林市 永福县');
INSERT INTO dv_area_base VALUES ('450327', '灌阳县', '', '', '', '3', '450300', '广西省 桂林市 灌阳县');
INSERT INTO dv_area_base VALUES ('450328', '龙胜各族自治县', '', '', '', '3', '450300', '广西省 桂林市 龙胜各族自治县');
INSERT INTO dv_area_base VALUES ('450329', '资源县', '', '', '', '3', '450300', '广西省 桂林市 资源县');
INSERT INTO dv_area_base VALUES ('450330', '平乐县', '', '', '', '3', '450300', '广西省 桂林市 平乐县');
INSERT INTO dv_area_base VALUES ('450331', '荔蒲县', '', '', '', '3', '450300', '广西省 桂林市 荔蒲县');
INSERT INTO dv_area_base VALUES ('450332', '恭城瑶族自治县', '', '', '', '3', '450300', '广西省 桂林市 恭城瑶族自治县');
INSERT INTO dv_area_base VALUES ('450400', '梧州市', '梧州', '', '', '2', '450000', '广西壮族自治区 梧州市');
INSERT INTO dv_area_base VALUES ('450403', '万秀区', '', '', '', '3', '450400', '广西省 梧州市 万秀区');
INSERT INTO dv_area_base VALUES ('450404', '蝶山区', '', '', '', '3', '450400', '广西省 梧州市 蝶山区');
INSERT INTO dv_area_base VALUES ('450405', '长洲区', '', '', '', '3', '450400', '广西省 梧州市 长洲区');
INSERT INTO dv_area_base VALUES ('450421', '苍梧县', '', '', '', '3', '450400', '广西省 梧州市 苍梧县');
INSERT INTO dv_area_base VALUES ('450422', '藤县', '', '', '', '3', '450400', '广西省 梧州市 藤县');
INSERT INTO dv_area_base VALUES ('450423', '蒙山县', '', '', '', '3', '450400', '广西省 梧州市 蒙山县');
INSERT INTO dv_area_base VALUES ('450481', '岑溪市', '', '', '', '3', '450400', '广西省 梧州市 岑溪市');
INSERT INTO dv_area_base VALUES ('450500', '北海市', '北海', '', '', '2', '450000', '广西壮族自治区 北海市');
INSERT INTO dv_area_base VALUES ('450502', '海城区', '', '', '', '3', '450500', '广西省 北海市 海城区');
INSERT INTO dv_area_base VALUES ('450503', '银海区', '', '', '', '3', '450500', '广西省 北海市 银海区');
INSERT INTO dv_area_base VALUES ('450512', '铁山港区', '', '', '', '3', '450500', '广西省 北海市 铁山港区');
INSERT INTO dv_area_base VALUES ('450521', '合浦县', '', '', '', '3', '450500', '广西省 北海市 合浦县');
INSERT INTO dv_area_base VALUES ('450600', '防城港市', '防城港', '', '', '2', '450000', '广西壮族自治区 防城港市');
INSERT INTO dv_area_base VALUES ('450602', '港口区', '', '', '', '3', '450600', '广西省 防城港市 港口区');
INSERT INTO dv_area_base VALUES ('450603', '防城区', '', '', '', '3', '450600', '广西省 防城港市 防城区');
INSERT INTO dv_area_base VALUES ('450621', '上思县', '', '', '', '3', '450600', '广西省 防城港市 上思县');
INSERT INTO dv_area_base VALUES ('450681', '东兴市', '', '', '', '3', '450600', '广西省 防城港市 东兴市');
INSERT INTO dv_area_base VALUES ('450700', '钦州市', '钦州', '', '', '2', '450000', '广西壮族自治区 钦州市');
INSERT INTO dv_area_base VALUES ('450702', '钦南区', '', '', '', '3', '450700', '广西省 钦州市 钦南区');
INSERT INTO dv_area_base VALUES ('450703', '钦北区', '', '', '', '3', '450700', '广西省 钦州市 钦北区');
INSERT INTO dv_area_base VALUES ('450721', '灵山县', '', '', '', '3', '450700', '广西省 钦州市 灵山县');
INSERT INTO dv_area_base VALUES ('450722', '浦北县', '', '', '', '3', '450700', '广西省 钦州市 浦北县');
INSERT INTO dv_area_base VALUES ('450800', '贵港市', '贵港', '', '', '2', '450000', '广西壮族自治区 贵港市');
INSERT INTO dv_area_base VALUES ('450802', '港北区', '', '', '', '3', '450800', '广西省 贵港市 港北区');
INSERT INTO dv_area_base VALUES ('450803', '港南区', '', '', '', '3', '450800', '广西省 贵港市 港南区');
INSERT INTO dv_area_base VALUES ('450804', '覃塘区', '', '', '', '3', '450800', '广西省 贵港市 覃塘区');
INSERT INTO dv_area_base VALUES ('450821', '平南县', '', '', '', '3', '450800', '广西省 贵港市 平南县');
INSERT INTO dv_area_base VALUES ('450881', '桂平市', '', '', '', '3', '450800', '广西省 贵港市 桂平市');
INSERT INTO dv_area_base VALUES ('450900', '玉林市', '玉林', '', '', '2', '450000', '广西壮族自治区 玉林市');
INSERT INTO dv_area_base VALUES ('450902', '玉州区', '', '', '', '3', '450900', '广西省 玉林市 玉州区');
INSERT INTO dv_area_base VALUES ('450921', '容县', '', '', '', '3', '450900', '广西省 玉林市 容县');
INSERT INTO dv_area_base VALUES ('450922', '陆川县', '', '', '', '3', '450900', '广西省 玉林市 陆川县');
INSERT INTO dv_area_base VALUES ('450923', '博白县', '', '', '', '3', '450900', '广西省 玉林市 博白县');
INSERT INTO dv_area_base VALUES ('450924', '兴业县', '', '', '', '3', '450900', '广西省 玉林市 兴业县');
INSERT INTO dv_area_base VALUES ('450981', '北流市', '', '', '', '3', '450900', '广西省 玉林市 北流市');
INSERT INTO dv_area_base VALUES ('451000', '百色市', '百色', '', '', '2', '450000', '广西壮族自治区 百色市');
INSERT INTO dv_area_base VALUES ('451002', '右江区', '', '', '', '3', '451000', '广西省 百色市 右江区');
INSERT INTO dv_area_base VALUES ('451021', '田阳县', '', '', '', '3', '451000', '广西省 百色市 田阳县');
INSERT INTO dv_area_base VALUES ('451022', '田东县', '', '', '', '3', '451000', '广西省 百色市 田东县');
INSERT INTO dv_area_base VALUES ('451023', '平果县', '', '', '', '3', '451000', '广西省 百色市 平果县');
INSERT INTO dv_area_base VALUES ('451024', '德保县', '', '', '', '3', '451000', '广西省 百色市 德保县');
INSERT INTO dv_area_base VALUES ('451025', '靖西县', '', '', '', '3', '451000', '广西省 百色市 靖西县');
INSERT INTO dv_area_base VALUES ('451026', '那坡县', '', '', '', '3', '451000', '广西省 百色市 那坡县');
INSERT INTO dv_area_base VALUES ('451027', '凌云县', '', '', '', '3', '451000', '广西省 百色市 凌云县');
INSERT INTO dv_area_base VALUES ('451028', '乐业县', '', '', '', '3', '451000', '广西省 百色市 乐业县');
INSERT INTO dv_area_base VALUES ('451029', '田林县', '', '', '', '3', '451000', '广西省 百色市 田林县');
INSERT INTO dv_area_base VALUES ('451030', '西林县', '', '', '', '3', '451000', '广西省 百色市 西林县');
INSERT INTO dv_area_base VALUES ('451031', '隆林各族自治县', '', '', '', '3', '451000', '广西省 百色市 隆林各族自治县');
INSERT INTO dv_area_base VALUES ('451100', '贺州市', '贺州', '', '', '2', '450000', '广西壮族自治区 贺州市');
INSERT INTO dv_area_base VALUES ('451102', '八步区', '', '', '', '3', '451100', '广西省 贺州市 八步区');
INSERT INTO dv_area_base VALUES ('451121', '昭平县', '', '', '', '3', '451100', '广西省 贺州市 昭平县');
INSERT INTO dv_area_base VALUES ('451122', '钟山县', '', '', '', '3', '451100', '广西省 贺州市 钟山县');
INSERT INTO dv_area_base VALUES ('451123', '富川瑶族自治县', '', '', '', '3', '451100', '广西省 贺州市 富川瑶族自治县');
INSERT INTO dv_area_base VALUES ('451200', '河池市', '河池', '', '', '2', '450000', '广西壮族自治区 河池市');
INSERT INTO dv_area_base VALUES ('451202', '金城江区', '', '', '', '3', '451200', '广西省 河池市 金城江区');
INSERT INTO dv_area_base VALUES ('451221', '南丹县', '', '', '', '3', '451200', '广西省 河池市 南丹县');
INSERT INTO dv_area_base VALUES ('451222', '天峨县', '', '', '', '3', '451200', '广西省 河池市 天峨县');
INSERT INTO dv_area_base VALUES ('451223', '凤山县', '', '', '', '3', '451200', '广西省 河池市 凤山县');
INSERT INTO dv_area_base VALUES ('451224', '东兰县', '', '', '', '3', '451200', '广西省 河池市 东兰县');
INSERT INTO dv_area_base VALUES ('451225', '罗城仫佬族自治县', '', '', '', '3', '451200', '广西省 河池市 罗城仫佬族自治县');
INSERT INTO dv_area_base VALUES ('451226', '环江毛南族自治县', '', '', '', '3', '451200', '广西省 河池市 环江毛南族自治县');
INSERT INTO dv_area_base VALUES ('451227', '巴马瑶族自治县', '', '', '', '3', '451200', '广西省 河池市 巴马瑶族自治县');
INSERT INTO dv_area_base VALUES ('451228', '都安瑶族自治县', '', '', '', '3', '451200', '广西省 河池市 都安瑶族自治县');
INSERT INTO dv_area_base VALUES ('451229', '大化瑶族自治县', '', '', '', '3', '451200', '广西省 河池市 大化瑶族自治县');
INSERT INTO dv_area_base VALUES ('451281', '宜州市', '', '', '', '3', '451200', '广西省 河池市 宜州市');
INSERT INTO dv_area_base VALUES ('451300', '来宾市', '来宾', '', '', '2', '450000', '广西壮族自治区 来宾市');
INSERT INTO dv_area_base VALUES ('451302', '兴宾区', '', '', '', '3', '451300', '广西省 来宾市 兴宾区');
INSERT INTO dv_area_base VALUES ('451321', '忻城县', '', '', '', '3', '451300', '广西省 来宾市 忻城县');
INSERT INTO dv_area_base VALUES ('451322', '象州县', '', '', '', '3', '451300', '广西省 来宾市 象州县');
INSERT INTO dv_area_base VALUES ('451323', '武宣县', '', '', '', '3', '451300', '广西省 来宾市 武宣县');
INSERT INTO dv_area_base VALUES ('451324', '金秀瑶族自治县', '', '', '', '3', '451300', '广西省 来宾市 金秀瑶族自治县');
INSERT INTO dv_area_base VALUES ('451381', '合山市', '', '', '', '3', '451300', '广西省 来宾市 合山市');
INSERT INTO dv_area_base VALUES ('451400', '崇左市', '崇左', '', '', '2', '450000', '广西壮族自治区 崇左市');
INSERT INTO dv_area_base VALUES ('451402', '江州区', '', '', '', '3', '451400', '广西省 崇左市 江州区');
INSERT INTO dv_area_base VALUES ('451421', '扶绥县', '', '', '', '3', '451400', '广西省 崇左市 扶绥县');
INSERT INTO dv_area_base VALUES ('451422', '宁明县', '', '', '', '3', '451400', '广西省 崇左市 宁明县');
INSERT INTO dv_area_base VALUES ('451423', '龙州县', '', '', '', '3', '451400', '广西省 崇左市 龙州县');
INSERT INTO dv_area_base VALUES ('451424', '大新县', '', '', '', '3', '451400', '广西省 崇左市 大新县');
INSERT INTO dv_area_base VALUES ('451425', '天等县', '', '', '', '3', '451400', '广西省 崇左市 天等县');
INSERT INTO dv_area_base VALUES ('451481', '凭祥市', '', '', '', '3', '451400', '广西省 崇左市 凭祥市');
INSERT INTO dv_area_base VALUES ('460000', '海南省', '海南', 'hainan', '琼', '1', '0', '海南省');
INSERT INTO dv_area_base VALUES ('460100', '海口市', '海口', '', '', '2', '460000', '海南省 海口市');
INSERT INTO dv_area_base VALUES ('460105', '秀英区', '', '', '', '3', '460100', '海南省 海口市 秀英区');
INSERT INTO dv_area_base VALUES ('460106', '龙华区', '', '', '', '3', '460100', '海南省 海口市 龙华区');
INSERT INTO dv_area_base VALUES ('460107', '琼山区', '', '', '', '3', '460100', '海南省 海口市 琼山区');
INSERT INTO dv_area_base VALUES ('460108', '美兰区', '', '', '', '3', '460100', '海南省 海口市 美兰区');
INSERT INTO dv_area_base VALUES ('460200', '三亚市', '三亚', '', '', '2', '460000', '海南省 三亚市');
INSERT INTO dv_area_base VALUES ('460300', '三沙市', '三沙', '', '', '2', '460000', '海南省 三沙市');
INSERT INTO dv_area_base VALUES ('460301', '西沙群岛', '', '', '', '3', '460300', '海南省 三沙市 西沙群岛');
INSERT INTO dv_area_base VALUES ('460302', '南沙群岛', '', '', '', '3', '460300', '海南省 三沙市 南沙群岛');
INSERT INTO dv_area_base VALUES ('460303', '中沙群岛的岛礁及其海域', '', '', '', '3', '460300', '海南省 三沙市 中沙群岛的岛礁及其海域');
INSERT INTO dv_area_base VALUES ('469001', '五指山市', '五指山', '', '', '2', '460000', '海南省 五指山市');
INSERT INTO dv_area_base VALUES ('469002', '琼海市', '琼海', '', '', '2', '460000', '海南省 琼海市');
INSERT INTO dv_area_base VALUES ('469003', '儋州市', '儋州', '', '', '2', '460000', '海南省 儋州市');
INSERT INTO dv_area_base VALUES ('469005', '文昌市', '文昌', '', '', '2', '460000', '海南省 文昌市');
INSERT INTO dv_area_base VALUES ('469006', '万宁市', '万宁', '', '', '2', '460000', '海南省 万宁市');
INSERT INTO dv_area_base VALUES ('469007', '东方市', '东方', '', '', '2', '460000', '海南省 东方市');
INSERT INTO dv_area_base VALUES ('469021', '定安县', '定安', '', '', '2', '460000', '海南省 定安县');
INSERT INTO dv_area_base VALUES ('469022', '屯昌县', '屯昌', '', '', '2', '460000', '海南省 屯昌县');
INSERT INTO dv_area_base VALUES ('469023', '澄迈县', '澄迈', '', '', '2', '460000', '海南省 澄迈县');
INSERT INTO dv_area_base VALUES ('469024', '临高县', '临高', '', '', '2', '460000', '海南省 临高县');
INSERT INTO dv_area_base VALUES ('469025', '白沙黎族自治县', '白沙', '', '', '2', '460000', '海南省 白沙黎族自治县');
INSERT INTO dv_area_base VALUES ('469026', '昌江黎族自治县', '昌江', '', '', '2', '460000', '海南省 昌江黎族自治县');
INSERT INTO dv_area_base VALUES ('469027', '乐东黎族自治县', '乐东', '', '', '2', '460000', '海南省 乐东黎族自治县');
INSERT INTO dv_area_base VALUES ('469028', '陵水黎族自治县', '陵水', '', '', '2', '460000', '海南省 陵水黎族自治县');
INSERT INTO dv_area_base VALUES ('469029', '保亭黎族苗族自治县', '保亭', '', '', '2', '460000', '海南省 保亭黎族苗族自治县');
INSERT INTO dv_area_base VALUES ('469030', '琼中黎族苗族自治县', '琼中', '', '', '2', '460000', '海南省 琼中黎族苗族自治县');
INSERT INTO dv_area_base VALUES ('500000', '重庆市', '重庆', 'chongqing', '川', '1', '0', '重庆市');
INSERT INTO dv_area_base VALUES ('500101', '万州区', '万州', '', '', '2', '500000', '重庆市 万州区');
INSERT INTO dv_area_base VALUES ('500102', '涪陵区', '涪陵', '', '', '2', '500000', '重庆市 涪陵区');
INSERT INTO dv_area_base VALUES ('500103', '渝中区', '渝中', '', '', '2', '500000', '重庆市 渝中区');
INSERT INTO dv_area_base VALUES ('500104', '大渡口区', '大渡口', '', '', '2', '500000', '重庆市 大渡口区');
INSERT INTO dv_area_base VALUES ('500105', '江北区', '江北', '', '', '2', '500000', '重庆市 江北区');
INSERT INTO dv_area_base VALUES ('500106', '沙坪坝区', '沙坪坝', '', '', '2', '500000', '重庆市 沙坪坝区');
INSERT INTO dv_area_base VALUES ('500107', '九龙坡区', '九龙坡', '', '', '2', '500000', '重庆市 九龙坡区');
INSERT INTO dv_area_base VALUES ('500108', '南岸区', '南岸', '', '', '2', '500000', '重庆市 南岸区');
INSERT INTO dv_area_base VALUES ('500109', '北碚区', '北碚', '', '', '2', '500000', '重庆市 北碚区');
INSERT INTO dv_area_base VALUES ('500110', '綦江区', '綦江', '', '', '2', '500000', '重庆市 綦江区');
INSERT INTO dv_area_base VALUES ('500111', '大足区', '大足', '', '', '2', '500000', '重庆市 大足区');
INSERT INTO dv_area_base VALUES ('500112', '渝北区', '渝北', '', '', '2', '500000', '重庆市 渝北区');
INSERT INTO dv_area_base VALUES ('500113', '巴南区', '巴南', '', '', '2', '500000', '重庆市 巴南区');
INSERT INTO dv_area_base VALUES ('500114', '黔江区', '黔江', '', '', '2', '500000', '重庆市 黔江区');
INSERT INTO dv_area_base VALUES ('500115', '长寿区', '长寿', '', '', '2', '500000', '重庆市 长寿区');
INSERT INTO dv_area_base VALUES ('500116', '江津区', '江津', '', '', '2', '500000', '重庆市 江津区');
INSERT INTO dv_area_base VALUES ('500117', '合川区', '合川', '', '', '2', '500000', '重庆市 合川区');
INSERT INTO dv_area_base VALUES ('500118', '永川区', '永川', '', '', '2', '500000', '重庆市 永川区');
INSERT INTO dv_area_base VALUES ('500119', '南川区', '南川', '', '', '2', '500000', '重庆市 南川区');
INSERT INTO dv_area_base VALUES ('500120', '璧山区', '璧山', '', '', '2', '500000', '重庆市 璧山区');
INSERT INTO dv_area_base VALUES ('500151', '铜梁区', '铜梁', '', '', '2', '500000', '重庆市 铜梁区');
INSERT INTO dv_area_base VALUES ('500152', '潼南区', '潼南', '', '', '2', '500000', '重庆市 潼南区');
INSERT INTO dv_area_base VALUES ('500153', '荣昌区', '荣昌', '', '', '2', '500000', '重庆市 荣昌区');
INSERT INTO dv_area_base VALUES ('500228', '梁平县', '梁平', '', '', '2', '500000', '重庆市 梁平县');
INSERT INTO dv_area_base VALUES ('500229', '城口县', '城口', '', '', '2', '500000', '重庆市 城口县');
INSERT INTO dv_area_base VALUES ('500230', '丰都县', '丰都', '', '', '2', '500000', '重庆市 丰都县');
INSERT INTO dv_area_base VALUES ('500231', '垫江县', '垫江', '', '', '2', '500000', '重庆市 垫江县');
INSERT INTO dv_area_base VALUES ('500232', '武隆县', '武隆', '', '', '2', '500000', '重庆市 武隆县');
INSERT INTO dv_area_base VALUES ('500233', '忠县', '', '', '', '2', '500000', '重庆市 忠县');
INSERT INTO dv_area_base VALUES ('500234', '开县', '', '', '', '2', '500000', '重庆市 开县');
INSERT INTO dv_area_base VALUES ('500235', '云阳县', '云阳', '', '', '2', '500000', '重庆市 云阳县');
INSERT INTO dv_area_base VALUES ('500236', '奉节县', '奉节', '', '', '2', '500000', '重庆市 奉节县');
INSERT INTO dv_area_base VALUES ('500237', '巫山县', '巫山', '', '', '2', '500000', '重庆市 巫山县');
INSERT INTO dv_area_base VALUES ('500238', '巫溪县', '巫溪', '', '', '2', '500000', '重庆市 巫溪县');
INSERT INTO dv_area_base VALUES ('500240', '石柱土家族自治县', '石柱县', '', '', '2', '500000', '重庆市 石柱土家族自治县');
INSERT INTO dv_area_base VALUES ('500241', '秀山土家族苗族自治县', '秀山县', '', '', '2', '500000', '重庆市 秀山土家族苗族自治县');
INSERT INTO dv_area_base VALUES ('500242', '酉阳土家族苗族自治县', '酉阳县', '', '', '2', '500000', '重庆市 酉阳土家族苗族自治县');
INSERT INTO dv_area_base VALUES ('500243', '彭水苗族土家族自治县', '彭水县', '', '', '2', '500000', '重庆市 彭水苗族土家族自治县');
INSERT INTO dv_area_base VALUES ('510000', '四川省', '四川', 'sichuan', '贵', '1', '0', '四川省');
INSERT INTO dv_area_base VALUES ('510100', '成都市', '成都', '', '', '2', '510000', '四川省 成都市');
INSERT INTO dv_area_base VALUES ('510104', '锦江区', '', '', '', '3', '510100', '四川省 成都市 锦江区');
INSERT INTO dv_area_base VALUES ('510105', '青羊区', '', '', '', '3', '510100', '四川省 成都市 青羊区');
INSERT INTO dv_area_base VALUES ('510106', '金牛区', '', '', '', '3', '510100', '四川省 成都市 金牛区');
INSERT INTO dv_area_base VALUES ('510107', '武侯区', '', '', '', '3', '510100', '四川省 成都市 武侯区');
INSERT INTO dv_area_base VALUES ('510108', '成华区', '', '', '', '3', '510100', '四川省 成都市 成华区');
INSERT INTO dv_area_base VALUES ('510112', '龙泉驿区', '', '', '', '3', '510100', '四川省 成都市 龙泉驿区');
INSERT INTO dv_area_base VALUES ('510113', '青白江区', '', '', '', '3', '510100', '四川省 成都市 青白江区');
INSERT INTO dv_area_base VALUES ('510114', '新都区', '', '', '', '3', '510100', '四川省 成都市 新都区');
INSERT INTO dv_area_base VALUES ('510115', '温江区', '', '', '', '3', '510100', '四川省 成都市 温江区');
INSERT INTO dv_area_base VALUES ('510121', '金堂县', '', '', '', '3', '510100', '四川省 成都市 金堂县');
INSERT INTO dv_area_base VALUES ('510122', '双流县', '', '', '', '3', '510100', '四川省 成都市 双流县');
INSERT INTO dv_area_base VALUES ('510124', '郫县', '', '', '', '3', '510100', '四川省 成都市 郫县');
INSERT INTO dv_area_base VALUES ('510129', '大邑县', '', '', '', '3', '510100', '四川省 成都市 大邑县');
INSERT INTO dv_area_base VALUES ('510131', '蒲江县', '', '', '', '3', '510100', '四川省 成都市 蒲江县');
INSERT INTO dv_area_base VALUES ('510132', '新津县', '', '', '', '3', '510100', '四川省 成都市 新津县');
INSERT INTO dv_area_base VALUES ('510181', '都江堰市', '', '', '', '3', '510100', '四川省 成都市 都江堰市');
INSERT INTO dv_area_base VALUES ('510182', '彭州市', '', '', '', '3', '510100', '四川省 成都市 彭州市');
INSERT INTO dv_area_base VALUES ('510183', '邛崃市', '', '', '', '3', '510100', '四川省 成都市 邛崃市');
INSERT INTO dv_area_base VALUES ('510184', '崇州市', '', '', '', '3', '510100', '四川省 成都市 崇州市');
INSERT INTO dv_area_base VALUES ('510300', '自贡市', '自贡', '', '', '2', '510000', '四川省 自贡市');
INSERT INTO dv_area_base VALUES ('510302', '自流井区', '', '', '', '3', '510300', '四川省 自贡市 自流井区');
INSERT INTO dv_area_base VALUES ('510303', '贡井区', '', '', '', '3', '510300', '四川省 自贡市 贡井区');
INSERT INTO dv_area_base VALUES ('510304', '大安区', '', '', '', '3', '510300', '四川省 自贡市 大安区');
INSERT INTO dv_area_base VALUES ('510311', '沿滩区', '', '', '', '3', '510300', '四川省 自贡市 沿滩区');
INSERT INTO dv_area_base VALUES ('510321', '荣县', '', '', '', '3', '510300', '四川省 自贡市 荣县');
INSERT INTO dv_area_base VALUES ('510322', '富顺县', '', '', '', '3', '510300', '四川省 自贡市 富顺县');
INSERT INTO dv_area_base VALUES ('510400', '攀枝花市', '攀枝花', '', '', '2', '510000', '四川省 攀枝花市');
INSERT INTO dv_area_base VALUES ('510402', '东区', '', '', '', '3', '510400', '四川省 攀枝花市 东区');
INSERT INTO dv_area_base VALUES ('510403', '西区', '', '', '', '3', '510400', '四川省 攀枝花市 西区');
INSERT INTO dv_area_base VALUES ('510411', '仁和区', '', '', '', '3', '510400', '四川省 攀枝花市 仁和区');
INSERT INTO dv_area_base VALUES ('510421', '米易县', '', '', '', '3', '510400', '四川省 攀枝花市 米易县');
INSERT INTO dv_area_base VALUES ('510422', '盐边县', '', '', '', '3', '510400', '四川省 攀枝花市 盐边县');
INSERT INTO dv_area_base VALUES ('510500', '泸州市', '泸州', '', '', '2', '510000', '四川省 泸州市');
INSERT INTO dv_area_base VALUES ('510502', '江阳区', '', '', '', '3', '510500', '四川省 泸州市 江阳区');
INSERT INTO dv_area_base VALUES ('510503', '纳溪区', '', '', '', '3', '510500', '四川省 泸州市 纳溪区');
INSERT INTO dv_area_base VALUES ('510504', '龙马潭区', '', '', '', '3', '510500', '四川省 泸州市 龙马潭区');
INSERT INTO dv_area_base VALUES ('510521', '泸县', '', '', '', '3', '510500', '四川省 泸州市 泸县');
INSERT INTO dv_area_base VALUES ('510522', '合江县', '', '', '', '3', '510500', '四川省 泸州市 合江县');
INSERT INTO dv_area_base VALUES ('510524', '叙永县', '', '', '', '3', '510500', '四川省 泸州市 叙永县');
INSERT INTO dv_area_base VALUES ('510525', '古蔺县', '', '', '', '3', '510500', '四川省 泸州市 古蔺县');
INSERT INTO dv_area_base VALUES ('510600', '德阳市', '德阳', '', '', '2', '510000', '四川省 德阳市');
INSERT INTO dv_area_base VALUES ('510603', '旌阳区', '', '', '', '3', '510600', '四川省 德阳市 旌阳区');
INSERT INTO dv_area_base VALUES ('510623', '中江县', '', '', '', '3', '510600', '四川省 德阳市 中江县');
INSERT INTO dv_area_base VALUES ('510626', '罗江县', '', '', '', '3', '510600', '四川省 德阳市 罗江县');
INSERT INTO dv_area_base VALUES ('510681', '广汉市', '', '', '', '3', '510600', '四川省 德阳市 广汉市');
INSERT INTO dv_area_base VALUES ('510682', '什邡市', '', '', '', '3', '510600', '四川省 德阳市 什邡市');
INSERT INTO dv_area_base VALUES ('510683', '绵竹市', '', '', '', '3', '510600', '四川省 德阳市 绵竹市');
INSERT INTO dv_area_base VALUES ('510700', '绵阳市', '绵阳', '', '', '2', '510000', '四川省 绵阳市');
INSERT INTO dv_area_base VALUES ('510703', '涪城区', '', '', '', '3', '510700', '四川省 绵阳市 涪城区');
INSERT INTO dv_area_base VALUES ('510704', '游仙区', '', '', '', '3', '510700', '四川省 绵阳市 游仙区');
INSERT INTO dv_area_base VALUES ('510722', '三台县', '', '', '', '3', '510700', '四川省 绵阳市 三台县');
INSERT INTO dv_area_base VALUES ('510723', '盐亭县', '', '', '', '3', '510700', '四川省 绵阳市 盐亭县');
INSERT INTO dv_area_base VALUES ('510724', '安县', '', '', '', '3', '510700', '四川省 绵阳市 安县');
INSERT INTO dv_area_base VALUES ('510725', '梓潼县', '', '', '', '3', '510700', '四川省 绵阳市 梓潼县');
INSERT INTO dv_area_base VALUES ('510726', '北川羌族自治县', '', '', '', '3', '510700', '四川省 绵阳市 北川羌族自治县');
INSERT INTO dv_area_base VALUES ('510727', '平武县', '', '', '', '3', '510700', '四川省 绵阳市 平武县');
INSERT INTO dv_area_base VALUES ('510781', '江油市', '', '', '', '3', '510700', '四川省 绵阳市 江油市');
INSERT INTO dv_area_base VALUES ('510800', '广元市', '广元', '', '', '2', '510000', '四川省 广元市');
INSERT INTO dv_area_base VALUES ('510802', '利州区', '', '', '', '3', '510800', '四川省 广元市 利州区');
INSERT INTO dv_area_base VALUES ('510811', '元坝区', '', '', '', '3', '510800', '四川省 广元市 元坝区');
INSERT INTO dv_area_base VALUES ('510812', '朝天区', '', '', '', '3', '510800', '四川省 广元市 朝天区');
INSERT INTO dv_area_base VALUES ('510821', '旺苍县', '', '', '', '3', '510800', '四川省 广元市 旺苍县');
INSERT INTO dv_area_base VALUES ('510822', '青川县', '', '', '', '3', '510800', '四川省 广元市 青川县');
INSERT INTO dv_area_base VALUES ('510823', '剑阁县', '', '', '', '3', '510800', '四川省 广元市 剑阁县');
INSERT INTO dv_area_base VALUES ('510824', '苍溪县', '', '', '', '3', '510800', '四川省 广元市 苍溪县');
INSERT INTO dv_area_base VALUES ('510900', '遂宁市', '遂宁', '', '', '2', '510000', '四川省 遂宁市');
INSERT INTO dv_area_base VALUES ('510903', '船山区', '', '', '', '3', '510900', '四川省 遂宁市 船山区');
INSERT INTO dv_area_base VALUES ('510904', '安居区', '', '', '', '3', '510900', '四川省 遂宁市 安居区');
INSERT INTO dv_area_base VALUES ('510921', '蓬溪县', '', '', '', '3', '510900', '四川省 遂宁市 蓬溪县');
INSERT INTO dv_area_base VALUES ('510922', '射洪县', '', '', '', '3', '510900', '四川省 遂宁市 射洪县');
INSERT INTO dv_area_base VALUES ('510923', '大英县', '', '', '', '3', '510900', '四川省 遂宁市 大英县');
INSERT INTO dv_area_base VALUES ('511000', '内江市', '内江', '', '', '2', '510000', '四川省 内江市');
INSERT INTO dv_area_base VALUES ('511002', '市中区', '', '', '', '3', '511000', '四川省 内江市 市中区');
INSERT INTO dv_area_base VALUES ('511011', '东兴区', '', '', '', '3', '511000', '四川省 内江市 东兴区');
INSERT INTO dv_area_base VALUES ('511024', '威远县', '', '', '', '3', '511000', '四川省 内江市 威远县');
INSERT INTO dv_area_base VALUES ('511025', '资中县', '', '', '', '3', '511000', '四川省 内江市 资中县');
INSERT INTO dv_area_base VALUES ('511028', '隆昌县', '', '', '', '3', '511000', '四川省 内江市 隆昌县');
INSERT INTO dv_area_base VALUES ('511100', '乐山市', '乐山', '', '', '2', '510000', '四川省 乐山市');
INSERT INTO dv_area_base VALUES ('511102', '市中区', '', '', '', '3', '511100', '四川省 乐山市 市中区');
INSERT INTO dv_area_base VALUES ('511111', '沙湾区', '', '', '', '3', '511100', '四川省 乐山市 沙湾区');
INSERT INTO dv_area_base VALUES ('511112', '五通桥区', '', '', '', '3', '511100', '四川省 乐山市 五通桥区');
INSERT INTO dv_area_base VALUES ('511113', '金口河区', '', '', '', '3', '511100', '四川省 乐山市 金口河区');
INSERT INTO dv_area_base VALUES ('511123', '犍为县', '', '', '', '3', '511100', '四川省 乐山市 犍为县');
INSERT INTO dv_area_base VALUES ('511124', '井研县', '', '', '', '3', '511100', '四川省 乐山市 井研县');
INSERT INTO dv_area_base VALUES ('511126', '夹江县', '', '', '', '3', '511100', '四川省 乐山市 夹江县');
INSERT INTO dv_area_base VALUES ('511129', '沐川县', '', '', '', '3', '511100', '四川省 乐山市 沐川县');
INSERT INTO dv_area_base VALUES ('511132', '峨边彝族自治县', '', '', '', '3', '511100', '四川省 乐山市 峨边彝族自治县');
INSERT INTO dv_area_base VALUES ('511133', '马边彝族自治县', '', '', '', '3', '511100', '四川省 乐山市 马边彝族自治县');
INSERT INTO dv_area_base VALUES ('511181', '峨眉山市', '', '', '', '3', '511100', '四川省 乐山市 峨眉山市');
INSERT INTO dv_area_base VALUES ('511300', '南充市', '南充', '', '', '2', '510000', '四川省 南充市');
INSERT INTO dv_area_base VALUES ('511302', '顺庆区', '', '', '', '3', '511300', '四川省 南充市 顺庆区');
INSERT INTO dv_area_base VALUES ('511303', '高坪区', '', '', '', '3', '511300', '四川省 南充市 高坪区');
INSERT INTO dv_area_base VALUES ('511304', '嘉陵区', '', '', '', '3', '511300', '四川省 南充市 嘉陵区');
INSERT INTO dv_area_base VALUES ('511321', '南部县', '', '', '', '3', '511300', '四川省 南充市 南部县');
INSERT INTO dv_area_base VALUES ('511322', '营山县', '', '', '', '3', '511300', '四川省 南充市 营山县');
INSERT INTO dv_area_base VALUES ('511323', '蓬安县', '', '', '', '3', '511300', '四川省 南充市 蓬安县');
INSERT INTO dv_area_base VALUES ('511324', '仪陇县', '', '', '', '3', '511300', '四川省 南充市 仪陇县');
INSERT INTO dv_area_base VALUES ('511325', '西充县', '', '', '', '3', '511300', '四川省 南充市 西充县');
INSERT INTO dv_area_base VALUES ('511381', '阆中市', '', '', '', '3', '511300', '四川省 南充市 阆中市');
INSERT INTO dv_area_base VALUES ('511400', '眉山市', '眉山', '', '', '2', '510000', '四川省 眉山市');
INSERT INTO dv_area_base VALUES ('511402', '东坡区', '', '', '', '3', '511400', '四川省 眉山市 东坡区');
INSERT INTO dv_area_base VALUES ('511421', '仁寿县', '', '', '', '3', '511400', '四川省 眉山市 仁寿县');
INSERT INTO dv_area_base VALUES ('511422', '彭山县', '', '', '', '3', '511400', '四川省 眉山市 彭山县');
INSERT INTO dv_area_base VALUES ('511423', '洪雅县', '', '', '', '3', '511400', '四川省 眉山市 洪雅县');
INSERT INTO dv_area_base VALUES ('511424', '丹棱县', '', '', '', '3', '511400', '四川省 眉山市 丹棱县');
INSERT INTO dv_area_base VALUES ('511425', '青神县', '', '', '', '3', '511400', '四川省 眉山市 青神县');
INSERT INTO dv_area_base VALUES ('511500', '宜宾市', '宜宾', '', '', '2', '510000', '四川省 宜宾市');
INSERT INTO dv_area_base VALUES ('511502', '翠屏区', '', '', '', '3', '511500', '四川省 宜宾市 翠屏区');
INSERT INTO dv_area_base VALUES ('511521', '宜宾县', '', '', '', '3', '511500', '四川省 宜宾市 宜宾县');
INSERT INTO dv_area_base VALUES ('511522', '南溪区', '', '', '', '3', '511500', '四川省 宜宾市 南溪区');
INSERT INTO dv_area_base VALUES ('511523', '江安县', '', '', '', '3', '511500', '四川省 宜宾市 江安县');
INSERT INTO dv_area_base VALUES ('511524', '长宁县', '', '', '', '3', '511500', '四川省 宜宾市 长宁县');
INSERT INTO dv_area_base VALUES ('511525', '高县', '', '', '', '3', '511500', '四川省 宜宾市 高县');
INSERT INTO dv_area_base VALUES ('511526', '珙县', '', '', '', '3', '511500', '四川省 宜宾市 珙县');
INSERT INTO dv_area_base VALUES ('511527', '筠连县', '', '', '', '3', '511500', '四川省 宜宾市 筠连县');
INSERT INTO dv_area_base VALUES ('511528', '兴文县', '', '', '', '3', '511500', '四川省 宜宾市 兴文县');
INSERT INTO dv_area_base VALUES ('511529', '屏山县', '', '', '', '3', '511500', '四川省 宜宾市 屏山县');
INSERT INTO dv_area_base VALUES ('511600', '广安市', '广安', '', '', '2', '510000', '四川省 广安市');
INSERT INTO dv_area_base VALUES ('511602', '广安区', '', '', '', '3', '511600', '四川省 广安市 广安区');
INSERT INTO dv_area_base VALUES ('511621', '岳池县', '', '', '', '3', '511600', '四川省 广安市 岳池县');
INSERT INTO dv_area_base VALUES ('511622', '武胜县', '', '', '', '3', '511600', '四川省 广安市 武胜县');
INSERT INTO dv_area_base VALUES ('511623', '邻水县', '', '', '', '3', '511600', '四川省 广安市 邻水县');
INSERT INTO dv_area_base VALUES ('511681', '华蓥市', '', '', '', '3', '511600', '四川省 广安市 华蓥市');
INSERT INTO dv_area_base VALUES ('511700', '达州市', '达州', '', '', '2', '510000', '四川省 达州市');
INSERT INTO dv_area_base VALUES ('511702', '通川区', '', '', '', '3', '511700', '四川省 达州市 通川区');
INSERT INTO dv_area_base VALUES ('511721', '达县', '', '', '', '3', '511700', '四川省 达州市 达县');
INSERT INTO dv_area_base VALUES ('511722', '宣汉县', '', '', '', '3', '511700', '四川省 达州市 宣汉县');
INSERT INTO dv_area_base VALUES ('511723', '开江县', '', '', '', '3', '511700', '四川省 达州市 开江县');
INSERT INTO dv_area_base VALUES ('511724', '大竹县', '', '', '', '3', '511700', '四川省 达州市 大竹县');
INSERT INTO dv_area_base VALUES ('511725', '渠县', '', '', '', '3', '511700', '四川省 达州市 渠县');
INSERT INTO dv_area_base VALUES ('511781', '万源市', '', '', '', '3', '511700', '四川省 达州市 万源市');
INSERT INTO dv_area_base VALUES ('511800', '雅安市', '雅安', '', '', '2', '510000', '四川省 雅安市');
INSERT INTO dv_area_base VALUES ('511802', '雨城区', '', '', '', '3', '511800', '四川省 雅安市 雨城区');
INSERT INTO dv_area_base VALUES ('511821', '名山县', '', '', '', '3', '511800', '四川省 雅安市 名山县');
INSERT INTO dv_area_base VALUES ('511822', '荥经县', '', '', '', '3', '511800', '四川省 雅安市 荥经县');
INSERT INTO dv_area_base VALUES ('511823', '汉源县', '', '', '', '3', '511800', '四川省 雅安市 汉源县');
INSERT INTO dv_area_base VALUES ('511824', '石棉县', '', '', '', '3', '511800', '四川省 雅安市 石棉县');
INSERT INTO dv_area_base VALUES ('511825', '天全县', '', '', '', '3', '511800', '四川省 雅安市 天全县');
INSERT INTO dv_area_base VALUES ('511826', '芦山县', '', '', '', '3', '511800', '四川省 雅安市 芦山县');
INSERT INTO dv_area_base VALUES ('511827', '宝兴县', '', '', '', '3', '511800', '四川省 雅安市 宝兴县');
INSERT INTO dv_area_base VALUES ('511900', '巴中市', '巴中', '', '', '2', '510000', '四川省 巴中市');
INSERT INTO dv_area_base VALUES ('511902', '巴州区', '', '', '', '3', '511900', '四川省 巴中市 巴州区');
INSERT INTO dv_area_base VALUES ('511921', '通江县', '', '', '', '3', '511900', '四川省 巴中市 通江县');
INSERT INTO dv_area_base VALUES ('511922', '南江县', '', '', '', '3', '511900', '四川省 巴中市 南江县');
INSERT INTO dv_area_base VALUES ('511923', '平昌县', '', '', '', '3', '511900', '四川省 巴中市 平昌县');
INSERT INTO dv_area_base VALUES ('512000', '资阳市', '资阳', '', '', '2', '510000', '四川省 资阳市');
INSERT INTO dv_area_base VALUES ('512002', '雁江区', '', '', '', '3', '512000', '四川省 资阳市 雁江区');
INSERT INTO dv_area_base VALUES ('512021', '安岳县', '', '', '', '3', '512000', '四川省 资阳市 安岳县');
INSERT INTO dv_area_base VALUES ('512022', '乐至县', '', '', '', '3', '512000', '四川省 资阳市 乐至县');
INSERT INTO dv_area_base VALUES ('512081', '简阳市', '', '', '', '3', '512000', '四川省 资阳市 简阳市');
INSERT INTO dv_area_base VALUES ('513200', '阿坝藏族羌族自治州', '阿坝', '', '', '2', '510000', '四川省 阿坝藏族羌族自治州');
INSERT INTO dv_area_base VALUES ('513221', '汶川县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 汶川县');
INSERT INTO dv_area_base VALUES ('513222', '理县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 理县');
INSERT INTO dv_area_base VALUES ('513223', '茂县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 茂县');
INSERT INTO dv_area_base VALUES ('513224', '松潘县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 松潘县');
INSERT INTO dv_area_base VALUES ('513225', '九寨沟县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 九寨沟县');
INSERT INTO dv_area_base VALUES ('513226', '金川县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 金川县');
INSERT INTO dv_area_base VALUES ('513227', '小金县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 小金县');
INSERT INTO dv_area_base VALUES ('513228', '黑水县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 黑水县');
INSERT INTO dv_area_base VALUES ('513229', '马尔康县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 马尔康县');
INSERT INTO dv_area_base VALUES ('513230', '壤塘县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 壤塘县');
INSERT INTO dv_area_base VALUES ('513231', '阿坝县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 阿坝县');
INSERT INTO dv_area_base VALUES ('513232', '若尔盖县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 若尔盖县');
INSERT INTO dv_area_base VALUES ('513233', '红原县', '', '', '', '3', '513200', '四川省 阿坝藏族羌族自治州 红原县');
INSERT INTO dv_area_base VALUES ('513300', '甘孜藏族自治州', '甘孜', '', '', '2', '510000', '四川省 甘孜藏族自治州');
INSERT INTO dv_area_base VALUES ('513321', '康定县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 康定县');
INSERT INTO dv_area_base VALUES ('513322', '泸定县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 泸定县');
INSERT INTO dv_area_base VALUES ('513323', '丹巴县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 丹巴县');
INSERT INTO dv_area_base VALUES ('513324', '九龙县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 九龙县');
INSERT INTO dv_area_base VALUES ('513325', '雅江县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 雅江县');
INSERT INTO dv_area_base VALUES ('513326', '道孚县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 道孚县');
INSERT INTO dv_area_base VALUES ('513327', '炉霍县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 炉霍县');
INSERT INTO dv_area_base VALUES ('513328', '甘孜县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 甘孜县');
INSERT INTO dv_area_base VALUES ('513329', '新龙县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 新龙县');
INSERT INTO dv_area_base VALUES ('513330', '德格县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 德格县');
INSERT INTO dv_area_base VALUES ('513331', '白玉县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 白玉县');
INSERT INTO dv_area_base VALUES ('513332', '石渠县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 石渠县');
INSERT INTO dv_area_base VALUES ('513333', '色达县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 色达县');
INSERT INTO dv_area_base VALUES ('513334', '理塘县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 理塘县');
INSERT INTO dv_area_base VALUES ('513335', '巴塘县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 巴塘县');
INSERT INTO dv_area_base VALUES ('513336', '乡城县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 乡城县');
INSERT INTO dv_area_base VALUES ('513337', '稻城县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 稻城县');
INSERT INTO dv_area_base VALUES ('513338', '得荣县', '', '', '', '3', '513300', '四川省 甘孜藏族自治州 得荣县');
INSERT INTO dv_area_base VALUES ('513400', '凉山彝族自治州', '凉山', '', '', '2', '510000', '四川省 凉山彝族自治州');
INSERT INTO dv_area_base VALUES ('513401', '西昌市', '', '', '', '3', '513400', '四川省 凉山彝族自治州 西昌市');
INSERT INTO dv_area_base VALUES ('513422', '木里藏族自治县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 木里藏族自治县');
INSERT INTO dv_area_base VALUES ('513423', '盐源县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 盐源县');
INSERT INTO dv_area_base VALUES ('513424', '德昌县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 德昌县');
INSERT INTO dv_area_base VALUES ('513425', '会理县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 会理县');
INSERT INTO dv_area_base VALUES ('513426', '会东县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 会东县');
INSERT INTO dv_area_base VALUES ('513427', '宁南县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 宁南县');
INSERT INTO dv_area_base VALUES ('513428', '普格县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 普格县');
INSERT INTO dv_area_base VALUES ('513429', '布拖县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 布拖县');
INSERT INTO dv_area_base VALUES ('513430', '金阳县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 金阳县');
INSERT INTO dv_area_base VALUES ('513431', '昭觉县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 昭觉县');
INSERT INTO dv_area_base VALUES ('513432', '喜德县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 喜德县');
INSERT INTO dv_area_base VALUES ('513433', '冕宁县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 冕宁县');
INSERT INTO dv_area_base VALUES ('513434', '越西县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 越西县');
INSERT INTO dv_area_base VALUES ('513435', '甘洛县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 甘洛县');
INSERT INTO dv_area_base VALUES ('513436', '美姑县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 美姑县');
INSERT INTO dv_area_base VALUES ('513437', '雷波县', '', '', '', '3', '513400', '四川省 凉山彝族自治州 雷波县');
INSERT INTO dv_area_base VALUES ('520000', '贵州省', '贵州', 'guizhou', '云', '1', '0', '贵州省');
INSERT INTO dv_area_base VALUES ('520100', '贵阳市', '贵阳', '', '', '2', '520000', '贵州省 贵阳市');
INSERT INTO dv_area_base VALUES ('520102', '南明区', '', '', '', '3', '520100', '贵州省 贵阳市 南明区');
INSERT INTO dv_area_base VALUES ('520103', '云岩区', '', '', '', '3', '520100', '贵州省 贵阳市 云岩区');
INSERT INTO dv_area_base VALUES ('520111', '花溪区', '', '', '', '3', '520100', '贵州省 贵阳市 花溪区');
INSERT INTO dv_area_base VALUES ('520112', '乌当区', '', '', '', '3', '520100', '贵州省 贵阳市 乌当区');
INSERT INTO dv_area_base VALUES ('520113', '白云区', '', '', '', '3', '520100', '贵州省 贵阳市 白云区');
INSERT INTO dv_area_base VALUES ('520114', '小河区', '', '', '', '3', '520100', '贵州省 贵阳市 小河区');
INSERT INTO dv_area_base VALUES ('520121', '开阳县', '', '', '', '3', '520100', '贵州省 贵阳市 开阳县');
INSERT INTO dv_area_base VALUES ('520122', '息烽县', '', '', '', '3', '520100', '贵州省 贵阳市 息烽县');
INSERT INTO dv_area_base VALUES ('520123', '修文县', '', '', '', '3', '520100', '贵州省 贵阳市 修文县');
INSERT INTO dv_area_base VALUES ('520181', '清镇市', '', '', '', '3', '520100', '贵州省 贵阳市 清镇市');
INSERT INTO dv_area_base VALUES ('520200', '六盘水市', '六盘水', '', '', '2', '520000', '贵州省 六盘水市');
INSERT INTO dv_area_base VALUES ('520201', '钟山区', '', '', '', '3', '520200', '贵州省 六盘水市 钟山区');
INSERT INTO dv_area_base VALUES ('520203', '六枝特区', '', '', '', '3', '520200', '贵州省 六盘水市 六枝特区');
INSERT INTO dv_area_base VALUES ('520221', '水城县', '', '', '', '3', '520200', '贵州省 六盘水市 水城县');
INSERT INTO dv_area_base VALUES ('520222', '盘县', '', '', '', '3', '520200', '贵州省 六盘水市 盘县');
INSERT INTO dv_area_base VALUES ('520300', '遵义市', '遵义', '', '', '2', '520000', '贵州省 遵义市');
INSERT INTO dv_area_base VALUES ('520302', '红花岗区', '', '', '', '3', '520300', '贵州省 遵义市 红花岗区');
INSERT INTO dv_area_base VALUES ('520303', '汇川区', '', '', '', '3', '520300', '贵州省 遵义市 汇川区');
INSERT INTO dv_area_base VALUES ('520321', '遵义县', '', '', '', '3', '520300', '贵州省 遵义市 遵义县');
INSERT INTO dv_area_base VALUES ('520322', '桐梓县', '', '', '', '3', '520300', '贵州省 遵义市 桐梓县');
INSERT INTO dv_area_base VALUES ('520323', '绥阳县', '', '', '', '3', '520300', '贵州省 遵义市 绥阳县');
INSERT INTO dv_area_base VALUES ('520324', '正安县', '', '', '', '3', '520300', '贵州省 遵义市 正安县');
INSERT INTO dv_area_base VALUES ('520325', '道真仡佬族苗族自治县', '', '', '', '3', '520300', '贵州省 遵义市 道真仡佬族苗族自治县');
INSERT INTO dv_area_base VALUES ('520326', '务川仡佬族苗族自治县', '', '', '', '3', '520300', '贵州省 遵义市 务川仡佬族苗族自治县');
INSERT INTO dv_area_base VALUES ('520327', '凤冈县', '', '', '', '3', '520300', '贵州省 遵义市 凤冈县');
INSERT INTO dv_area_base VALUES ('520328', '湄潭县', '', '', '', '3', '520300', '贵州省 遵义市 湄潭县');
INSERT INTO dv_area_base VALUES ('520329', '余庆县', '', '', '', '3', '520300', '贵州省 遵义市 余庆县');
INSERT INTO dv_area_base VALUES ('520330', '习水县', '', '', '', '3', '520300', '贵州省 遵义市 习水县');
INSERT INTO dv_area_base VALUES ('520381', '赤水市', '', '', '', '3', '520300', '贵州省 遵义市 赤水市');
INSERT INTO dv_area_base VALUES ('520382', '仁怀市', '', '', '', '3', '520300', '贵州省 遵义市 仁怀市');
INSERT INTO dv_area_base VALUES ('520400', '安顺市', '安顺', '', '', '2', '520000', '贵州省 安顺市');
INSERT INTO dv_area_base VALUES ('520402', '西秀区', '', '', '', '3', '520400', '贵州省 安顺市 西秀区');
INSERT INTO dv_area_base VALUES ('520421', '平坝县', '', '', '', '3', '520400', '贵州省 安顺市 平坝县');
INSERT INTO dv_area_base VALUES ('520422', '普定县', '', '', '', '3', '520400', '贵州省 安顺市 普定县');
INSERT INTO dv_area_base VALUES ('520423', '镇宁布依族苗族自治县', '', '', '', '3', '520400', '贵州省 安顺市 镇宁布依族苗族自治县');
INSERT INTO dv_area_base VALUES ('520424', '关岭布依族苗族自治县', '', '', '', '3', '520400', '贵州省 安顺市 关岭布依族苗族自治县');
INSERT INTO dv_area_base VALUES ('520425', '紫云苗族布依族自治县', '', '', '', '3', '520400', '贵州省 安顺市 紫云苗族布依族自治县');
INSERT INTO dv_area_base VALUES ('520500', '毕节市', '毕节', '', '', '2', '520000', '贵州省 毕节市');
INSERT INTO dv_area_base VALUES ('520600', '铜仁市', '铜仁', '', '', '2', '520000', '贵州省 铜仁市');
INSERT INTO dv_area_base VALUES ('522201', '碧江区', '', '', '', '3', '522200', '贵州省 铜仁市 碧江区');
INSERT INTO dv_area_base VALUES ('522222', '江口县', '', '', '', '3', '522200', '贵州省 铜仁市 江口县');
INSERT INTO dv_area_base VALUES ('522223', '玉屏侗族自治县', '', '', '', '3', '522200', '贵州省 铜仁市 玉屏侗族自治县');
INSERT INTO dv_area_base VALUES ('522224', '石阡县', '', '', '', '3', '522200', '贵州省 铜仁市 石阡县');
INSERT INTO dv_area_base VALUES ('522225', '思南县', '', '', '', '3', '522200', '贵州省 铜仁市 思南县');
INSERT INTO dv_area_base VALUES ('522226', '印江土家族苗族自治县', '', '', '', '3', '522200', '贵州省 铜仁市 印江土家族苗族自治县');
INSERT INTO dv_area_base VALUES ('522227', '德江县', '', '', '', '3', '522200', '贵州省 铜仁市 德江县');
INSERT INTO dv_area_base VALUES ('522228', '沿河土家族自治县', '', '', '', '3', '522200', '贵州省 铜仁市 沿河土家族自治县');
INSERT INTO dv_area_base VALUES ('522229', '松桃苗族自治县', '', '', '', '3', '522200', '贵州省 铜仁市 松桃苗族自治县');
INSERT INTO dv_area_base VALUES ('522230', '万山区', '', '', '', '3', '522200', '贵州省 铜仁市 万山区');
INSERT INTO dv_area_base VALUES ('522300', '黔西南布依族苗族自治州', '黔西南', '', '', '2', '520000', '贵州省 黔西南布依族苗族自治州');
INSERT INTO dv_area_base VALUES ('522301', '兴义市', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 兴义市');
INSERT INTO dv_area_base VALUES ('522322', '兴仁县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 兴仁县');
INSERT INTO dv_area_base VALUES ('522323', '普安县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 普安县');
INSERT INTO dv_area_base VALUES ('522324', '晴隆县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 晴隆县');
INSERT INTO dv_area_base VALUES ('522325', '贞丰县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 贞丰县');
INSERT INTO dv_area_base VALUES ('522326', '望谟县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 望谟县');
INSERT INTO dv_area_base VALUES ('522327', '册亨县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 册亨县');
INSERT INTO dv_area_base VALUES ('522328', '安龙县', '', '', '', '3', '522300', '贵州省 黔西南布依族苗族自治州 安龙县');
INSERT INTO dv_area_base VALUES ('522401', '七星关区', '', '', '', '3', '522400', '贵州省 毕节市 七星关区');
INSERT INTO dv_area_base VALUES ('522422', '大方县', '', '', '', '3', '522400', '贵州省 毕节市 大方县');
INSERT INTO dv_area_base VALUES ('522423', '黔西县', '', '', '', '3', '522400', '贵州省 毕节市 黔西县');
INSERT INTO dv_area_base VALUES ('522424', '金沙县', '', '', '', '3', '522400', '贵州省 毕节市 金沙县');
INSERT INTO dv_area_base VALUES ('522425', '织金县', '', '', '', '3', '522400', '贵州省 毕节市 织金县');
INSERT INTO dv_area_base VALUES ('522426', '纳雍县', '', '', '', '3', '522400', '贵州省 毕节市 纳雍县');
INSERT INTO dv_area_base VALUES ('522427', '威宁彝族回族苗族自治县', '', '', '', '3', '522400', '贵州省 毕节市 威宁彝族回族苗族自治县');
INSERT INTO dv_area_base VALUES ('522428', '赫章县', '', '', '', '3', '522400', '贵州省 毕节市 赫章县');
INSERT INTO dv_area_base VALUES ('522600', '黔东南苗族侗族自治州', '黔东南', '', '', '2', '520000', '贵州省 黔东南苗族侗族自治州');
INSERT INTO dv_area_base VALUES ('522601', '凯里市', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 凯里市');
INSERT INTO dv_area_base VALUES ('522622', '黄平县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 黄平县');
INSERT INTO dv_area_base VALUES ('522623', '施秉县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 施秉县');
INSERT INTO dv_area_base VALUES ('522624', '三穗县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 三穗县');
INSERT INTO dv_area_base VALUES ('522625', '镇远县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 镇远县');
INSERT INTO dv_area_base VALUES ('522626', '岑巩县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 岑巩县');
INSERT INTO dv_area_base VALUES ('522627', '天柱县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 天柱县');
INSERT INTO dv_area_base VALUES ('522628', '锦屏县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 锦屏县');
INSERT INTO dv_area_base VALUES ('522629', '剑河县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 剑河县');
INSERT INTO dv_area_base VALUES ('522630', '台江县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 台江县');
INSERT INTO dv_area_base VALUES ('522631', '黎平县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 黎平县');
INSERT INTO dv_area_base VALUES ('522632', '榕江县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 榕江县');
INSERT INTO dv_area_base VALUES ('522633', '从江县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 从江县');
INSERT INTO dv_area_base VALUES ('522634', '雷山县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 雷山县');
INSERT INTO dv_area_base VALUES ('522635', '麻江县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 麻江县');
INSERT INTO dv_area_base VALUES ('522636', '丹寨县', '', '', '', '3', '522600', '贵州省 黔东南苗族侗族自治州 丹寨县');
INSERT INTO dv_area_base VALUES ('522700', '黔南布依族苗族自治州', '黔南', '', '', '2', '520000', '贵州省 黔南布依族苗族自治州');
INSERT INTO dv_area_base VALUES ('522701', '都匀市', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 都匀市');
INSERT INTO dv_area_base VALUES ('522702', '福泉市', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 福泉市');
INSERT INTO dv_area_base VALUES ('522722', '荔波县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 荔波县');
INSERT INTO dv_area_base VALUES ('522723', '贵定县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 贵定县');
INSERT INTO dv_area_base VALUES ('522725', '瓮安县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 瓮安县');
INSERT INTO dv_area_base VALUES ('522726', '独山县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 独山县');
INSERT INTO dv_area_base VALUES ('522727', '平塘县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 平塘县');
INSERT INTO dv_area_base VALUES ('522728', '罗甸县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 罗甸县');
INSERT INTO dv_area_base VALUES ('522729', '长顺县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 长顺县');
INSERT INTO dv_area_base VALUES ('522730', '龙里县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 龙里县');
INSERT INTO dv_area_base VALUES ('522731', '惠水县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 惠水县');
INSERT INTO dv_area_base VALUES ('522732', '三都水族自治县', '', '', '', '3', '522700', '贵州省 黔南布依族苗族自治州 三都水族自治县');
INSERT INTO dv_area_base VALUES ('530000', '云南省', '云南', 'yunan', '渝', '1', '0', '云南省');
INSERT INTO dv_area_base VALUES ('530100', '昆明市', '昆明', '', '', '2', '530000', '云南省 昆明市');
INSERT INTO dv_area_base VALUES ('530102', '五华区', '', '', '', '3', '530100', '云南省 昆明市 五华区');
INSERT INTO dv_area_base VALUES ('530103', '盘龙区', '', '', '', '3', '530100', '云南省 昆明市 盘龙区');
INSERT INTO dv_area_base VALUES ('530111', '官渡区', '', '', '', '3', '530100', '云南省 昆明市 官渡区');
INSERT INTO dv_area_base VALUES ('530112', '西山区', '', '', '', '3', '530100', '云南省 昆明市 西山区');
INSERT INTO dv_area_base VALUES ('530113', '东川区', '', '', '', '3', '530100', '云南省 昆明市 东川区');
INSERT INTO dv_area_base VALUES ('530121', '呈贡区', '', '', '', '3', '530100', '云南省 昆明市 呈贡区');
INSERT INTO dv_area_base VALUES ('530122', '晋宁县', '', '', '', '3', '530100', '云南省 昆明市 晋宁县');
INSERT INTO dv_area_base VALUES ('530124', '富民县', '', '', '', '3', '530100', '云南省 昆明市 富民县');
INSERT INTO dv_area_base VALUES ('530125', '宜良县', '', '', '', '3', '530100', '云南省 昆明市 宜良县');
INSERT INTO dv_area_base VALUES ('530126', '石林彝族自治县', '', '', '', '3', '530100', '云南省 昆明市 石林彝族自治县');
INSERT INTO dv_area_base VALUES ('530127', '嵩明县', '', '', '', '3', '530100', '云南省 昆明市 嵩明县');
INSERT INTO dv_area_base VALUES ('530128', '禄劝彝族苗族自治县', '', '', '', '3', '530100', '云南省 昆明市 禄劝彝族苗族自治县');
INSERT INTO dv_area_base VALUES ('530129', '寻甸回族彝族自治县', '', '', '', '3', '530100', '云南省 昆明市 寻甸回族彝族自治县');
INSERT INTO dv_area_base VALUES ('530181', '安宁市', '', '', '', '3', '530100', '云南省 昆明市 安宁市');
INSERT INTO dv_area_base VALUES ('530300', '曲靖市', '曲靖', '', '', '2', '530000', '云南省 曲靖市');
INSERT INTO dv_area_base VALUES ('530302', '麒麟区', '', '', '', '3', '530300', '云南省 曲靖市 麒麟区');
INSERT INTO dv_area_base VALUES ('530321', '马龙县', '', '', '', '3', '530300', '云南省 曲靖市 马龙县');
INSERT INTO dv_area_base VALUES ('530322', '陆良县', '', '', '', '3', '530300', '云南省 曲靖市 陆良县');
INSERT INTO dv_area_base VALUES ('530323', '师宗县', '', '', '', '3', '530300', '云南省 曲靖市 师宗县');
INSERT INTO dv_area_base VALUES ('530324', '罗平县', '', '', '', '3', '530300', '云南省 曲靖市 罗平县');
INSERT INTO dv_area_base VALUES ('530325', '富源县', '', '', '', '3', '530300', '云南省 曲靖市 富源县');
INSERT INTO dv_area_base VALUES ('530326', '会泽县', '', '', '', '3', '530300', '云南省 曲靖市 会泽县');
INSERT INTO dv_area_base VALUES ('530328', '沾益县', '', '', '', '3', '530300', '云南省 曲靖市 沾益县');
INSERT INTO dv_area_base VALUES ('530381', '宣威市', '', '', '', '3', '530300', '云南省 曲靖市 宣威市');
INSERT INTO dv_area_base VALUES ('530400', '玉溪市', '玉溪', '', '', '2', '530000', '云南省 玉溪市');
INSERT INTO dv_area_base VALUES ('530402', '红塔区', '', '', '', '3', '530400', '云南省 玉溪市 红塔区');
INSERT INTO dv_area_base VALUES ('530421', '江川县', '', '', '', '3', '530400', '云南省 玉溪市 江川县');
INSERT INTO dv_area_base VALUES ('530422', '澄江县', '', '', '', '3', '530400', '云南省 玉溪市 澄江县');
INSERT INTO dv_area_base VALUES ('530423', '通海县', '', '', '', '3', '530400', '云南省 玉溪市 通海县');
INSERT INTO dv_area_base VALUES ('530424', '华宁县', '', '', '', '3', '530400', '云南省 玉溪市 华宁县');
INSERT INTO dv_area_base VALUES ('530425', '易门县', '', '', '', '3', '530400', '云南省 玉溪市 易门县');
INSERT INTO dv_area_base VALUES ('530426', '峨山彝族自治县', '', '', '', '3', '530400', '云南省 玉溪市 峨山彝族自治县');
INSERT INTO dv_area_base VALUES ('530427', '新平彝族傣族自治县', '', '', '', '3', '530400', '云南省 玉溪市 新平彝族傣族自治县');
INSERT INTO dv_area_base VALUES ('530428', '元江哈尼族彝族傣族自治县', '', '', '', '3', '530400', '云南省 玉溪市 元江哈尼族彝族傣族自治县');
INSERT INTO dv_area_base VALUES ('530500', '保山市', '保山', '', '', '2', '530000', '云南省 保山市');
INSERT INTO dv_area_base VALUES ('530502', '隆阳区', '', '', '', '3', '530500', '云南省 保山市 隆阳区');
INSERT INTO dv_area_base VALUES ('530521', '施甸县', '', '', '', '3', '530500', '云南省 保山市 施甸县');
INSERT INTO dv_area_base VALUES ('530522', '腾冲县', '', '', '', '3', '530500', '云南省 保山市 腾冲县');
INSERT INTO dv_area_base VALUES ('530523', '龙陵县', '', '', '', '3', '530500', '云南省 保山市 龙陵县');
INSERT INTO dv_area_base VALUES ('530524', '昌宁县', '', '', '', '3', '530500', '云南省 保山市 昌宁县');
INSERT INTO dv_area_base VALUES ('530600', '昭通市', '昭通', '', '', '2', '530000', '云南省 昭通市');
INSERT INTO dv_area_base VALUES ('530602', '昭阳区', '', '', '', '3', '530600', '云南省 昭通市 昭阳区');
INSERT INTO dv_area_base VALUES ('530621', '鲁甸县', '', '', '', '3', '530600', '云南省 昭通市 鲁甸县');
INSERT INTO dv_area_base VALUES ('530622', '巧家县', '', '', '', '3', '530600', '云南省 昭通市 巧家县');
INSERT INTO dv_area_base VALUES ('530623', '盐津县', '', '', '', '3', '530600', '云南省 昭通市 盐津县');
INSERT INTO dv_area_base VALUES ('530624', '大关县', '', '', '', '3', '530600', '云南省 昭通市 大关县');
INSERT INTO dv_area_base VALUES ('530625', '永善县', '', '', '', '3', '530600', '云南省 昭通市 永善县');
INSERT INTO dv_area_base VALUES ('530626', '绥江县', '', '', '', '3', '530600', '云南省 昭通市 绥江县');
INSERT INTO dv_area_base VALUES ('530627', '镇雄县', '', '', '', '3', '530600', '云南省 昭通市 镇雄县');
INSERT INTO dv_area_base VALUES ('530628', '彝良县', '', '', '', '3', '530600', '云南省 昭通市 彝良县');
INSERT INTO dv_area_base VALUES ('530629', '威信县', '', '', '', '3', '530600', '云南省 昭通市 威信县');
INSERT INTO dv_area_base VALUES ('530630', '水富县', '', '', '', '3', '530600', '云南省 昭通市 水富县');
INSERT INTO dv_area_base VALUES ('530700', '丽江市', '丽江', '', '', '2', '530000', '云南省 丽江市');
INSERT INTO dv_area_base VALUES ('530702', '古城区', '', '', '', '3', '530700', '云南省 丽江市 古城区');
INSERT INTO dv_area_base VALUES ('530721', '玉龙纳西族自治县', '', '', '', '3', '530700', '云南省 丽江市 玉龙纳西族自治县');
INSERT INTO dv_area_base VALUES ('530722', '永胜县', '', '', '', '3', '530700', '云南省 丽江市 永胜县');
INSERT INTO dv_area_base VALUES ('530723', '华坪县', '', '', '', '3', '530700', '云南省 丽江市 华坪县');
INSERT INTO dv_area_base VALUES ('530724', '宁蒗彝族自治县', '', '', '', '3', '530700', '云南省 丽江市 宁蒗彝族自治县');
INSERT INTO dv_area_base VALUES ('530800', '普洱市', '普洱', '', '', '2', '530000', '云南省 普洱市');
INSERT INTO dv_area_base VALUES ('530802', '思茅区', '', '', '', '3', '530800', '云南省 普洱市 思茅区');
INSERT INTO dv_area_base VALUES ('530821', '宁洱哈尼族彝族自治县', '', '', '', '3', '530800', '云南省 普洱市 宁洱哈尼族彝族自治县');
INSERT INTO dv_area_base VALUES ('530822', '墨江哈尼族自治县', '', '', '', '3', '530800', '云南省 普洱市 墨江哈尼族自治县');
INSERT INTO dv_area_base VALUES ('530823', '景东彝族自治县', '', '', '', '3', '530800', '云南省 普洱市 景东彝族自治县');
INSERT INTO dv_area_base VALUES ('530824', '景谷傣族彝族自治县', '', '', '', '3', '530800', '云南省 普洱市 景谷傣族彝族自治县');
INSERT INTO dv_area_base VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '', '', '', '3', '530800', '云南省 普洱市 镇沅彝族哈尼族拉祜族自治县');
INSERT INTO dv_area_base VALUES ('530826', '江城哈尼族彝族自治县', '', '', '', '3', '530800', '云南省 普洱市 江城哈尼族彝族自治县');
INSERT INTO dv_area_base VALUES ('530827', '孟连傣族拉祜族佤族自治县', '', '', '', '3', '530800', '云南省 普洱市 孟连傣族拉祜族佤族自治县');
INSERT INTO dv_area_base VALUES ('530828', '澜沧拉祜族自治县', '', '', '', '3', '530800', '云南省 普洱市 澜沧拉祜族自治县');
INSERT INTO dv_area_base VALUES ('530829', '西盟佤族自治县', '', '', '', '3', '530800', '云南省 普洱市 西盟佤族自治县');
INSERT INTO dv_area_base VALUES ('530900', '临沧市', '临沧', '', '', '2', '530000', '云南省 临沧市');
INSERT INTO dv_area_base VALUES ('530902', '临翔区', '', '', '', '3', '530900', '云南省 临沧市 临翔区');
INSERT INTO dv_area_base VALUES ('530921', '凤庆县', '', '', '', '3', '530900', '云南省 临沧市 凤庆县');
INSERT INTO dv_area_base VALUES ('530922', '云县', '', '', '', '3', '530900', '云南省 临沧市 云县');
INSERT INTO dv_area_base VALUES ('530923', '永德县', '', '', '', '3', '530900', '云南省 临沧市 永德县');
INSERT INTO dv_area_base VALUES ('530924', '镇康县', '', '', '', '3', '530900', '云南省 临沧市 镇康县');
INSERT INTO dv_area_base VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '', '', '', '3', '530900', '云南省 临沧市 双江拉祜族佤族布朗族傣族自治县');
INSERT INTO dv_area_base VALUES ('530926', '耿马傣族佤族自治县', '', '', '', '3', '530900', '云南省 临沧市 耿马傣族佤族自治县');
INSERT INTO dv_area_base VALUES ('530927', '沧源佤族自治县', '', '', '', '3', '530900', '云南省 临沧市 沧源佤族自治县');
INSERT INTO dv_area_base VALUES ('532300', '楚雄彝族自治州', '楚雄', '', '', '2', '530000', '云南省 楚雄彝族自治州');
INSERT INTO dv_area_base VALUES ('532301', '楚雄市', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 楚雄市');
INSERT INTO dv_area_base VALUES ('532322', '双柏县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 双柏县');
INSERT INTO dv_area_base VALUES ('532323', '牟定县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 牟定县');
INSERT INTO dv_area_base VALUES ('532324', '南华县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 南华县');
INSERT INTO dv_area_base VALUES ('532325', '姚安县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 姚安县');
INSERT INTO dv_area_base VALUES ('532326', '大姚县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 大姚县');
INSERT INTO dv_area_base VALUES ('532327', '永仁县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 永仁县');
INSERT INTO dv_area_base VALUES ('532328', '元谋县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 元谋县');
INSERT INTO dv_area_base VALUES ('532329', '武定县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 武定县');
INSERT INTO dv_area_base VALUES ('532331', '禄丰县', '', '', '', '3', '532300', '云南省 楚雄彝族自治州 禄丰县');
INSERT INTO dv_area_base VALUES ('532500', '红河哈尼族彝族自治州', '红河', '', '', '2', '530000', '云南省 红河哈尼族彝族自治州');
INSERT INTO dv_area_base VALUES ('532501', '个旧市', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 个旧市');
INSERT INTO dv_area_base VALUES ('532502', '开远市', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 开远市');
INSERT INTO dv_area_base VALUES ('532522', '蒙自市', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 蒙自市');
INSERT INTO dv_area_base VALUES ('532523', '屏边苗族自治县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 屏边苗族自治县');
INSERT INTO dv_area_base VALUES ('532524', '建水县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 建水县');
INSERT INTO dv_area_base VALUES ('532525', '石屏县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 石屏县');
INSERT INTO dv_area_base VALUES ('532526', '弥勒县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 弥勒县');
INSERT INTO dv_area_base VALUES ('532527', '泸西县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 泸西县');
INSERT INTO dv_area_base VALUES ('532528', '元阳县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 元阳县');
INSERT INTO dv_area_base VALUES ('532529', '红河县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 红河县');
INSERT INTO dv_area_base VALUES ('532530', '金平苗族瑶族傣族自治县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 金平苗族瑶族傣族自治县');
INSERT INTO dv_area_base VALUES ('532531', '绿春县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 绿春县');
INSERT INTO dv_area_base VALUES ('532532', '河口瑶族自治县', '', '', '', '3', '532500', '云南省 红河哈尼族彝族自治州 河口瑶族自治县');
INSERT INTO dv_area_base VALUES ('532600', '文山壮族苗族自治州', '文山', '', '', '2', '530000', '云南省 文山壮族苗族自治州');
INSERT INTO dv_area_base VALUES ('532621', '文山市', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 文山市');
INSERT INTO dv_area_base VALUES ('532622', '砚山县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 砚山县');
INSERT INTO dv_area_base VALUES ('532623', '西畴县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 西畴县');
INSERT INTO dv_area_base VALUES ('532624', '麻栗坡县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 麻栗坡县');
INSERT INTO dv_area_base VALUES ('532625', '马关县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 马关县');
INSERT INTO dv_area_base VALUES ('532626', '丘北县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 丘北县');
INSERT INTO dv_area_base VALUES ('532627', '广南县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 广南县');
INSERT INTO dv_area_base VALUES ('532628', '富宁县', '', '', '', '3', '532600', '云南省 文山壮族苗族自治州 富宁县');
INSERT INTO dv_area_base VALUES ('532800', '西双版纳傣族自治州', '西双版纳', '', '', '2', '530000', '云南省 西双版纳傣族自治州');
INSERT INTO dv_area_base VALUES ('532801', '景洪市', '', '', '', '3', '532800', '云南省 西双版纳傣族自治州 景洪市');
INSERT INTO dv_area_base VALUES ('532822', '勐海县', '', '', '', '3', '532800', '云南省 西双版纳傣族自治州 勐海县');
INSERT INTO dv_area_base VALUES ('532823', '勐腊县', '', '', '', '3', '532800', '云南省 西双版纳傣族自治州 勐腊县');
INSERT INTO dv_area_base VALUES ('532900', '大理白族自治州', '大理', '', '', '2', '530000', '云南省 大理白族自治州');
INSERT INTO dv_area_base VALUES ('532901', '大理市', '', '', '', '3', '532900', '云南省 大理白族自治州 大理市');
INSERT INTO dv_area_base VALUES ('532922', '漾濞彝族自治县', '', '', '', '3', '532900', '云南省 大理白族自治州 漾濞彝族自治县');
INSERT INTO dv_area_base VALUES ('532923', '祥云县', '', '', '', '3', '532900', '云南省 大理白族自治州 祥云县');
INSERT INTO dv_area_base VALUES ('532924', '宾川县', '', '', '', '3', '532900', '云南省 大理白族自治州 宾川县');
INSERT INTO dv_area_base VALUES ('532925', '弥渡县', '', '', '', '3', '532900', '云南省 大理白族自治州 弥渡县');
INSERT INTO dv_area_base VALUES ('532926', '南涧彝族自治县', '', '', '', '3', '532900', '云南省 大理白族自治州 南涧彝族自治县');
INSERT INTO dv_area_base VALUES ('532927', '巍山彝族回族自治县', '', '', '', '3', '532900', '云南省 大理白族自治州 巍山彝族回族自治县');
INSERT INTO dv_area_base VALUES ('532928', '永平县', '', '', '', '3', '532900', '云南省 大理白族自治州 永平县');
INSERT INTO dv_area_base VALUES ('532929', '云龙县', '', '', '', '3', '532900', '云南省 大理白族自治州 云龙县');
INSERT INTO dv_area_base VALUES ('532930', '洱源县', '', '', '', '3', '532900', '云南省 大理白族自治州 洱源县');
INSERT INTO dv_area_base VALUES ('532931', '剑川县', '', '', '', '3', '532900', '云南省 大理白族自治州 剑川县');
INSERT INTO dv_area_base VALUES ('532932', '鹤庆县', '', '', '', '3', '532900', '云南省 大理白族自治州 鹤庆县');
INSERT INTO dv_area_base VALUES ('533100', '德宏傣族景颇族自治州', '德宏', '', '', '2', '530000', '云南省 德宏傣族景颇族自治州');
INSERT INTO dv_area_base VALUES ('533102', '瑞丽市', '', '', '', '3', '533100', '云南省 德宏傣族景颇族自治州 瑞丽市');
INSERT INTO dv_area_base VALUES ('533103', '芒市', '', '', '', '3', '533100', '云南省 德宏傣族景颇族自治州 芒市');
INSERT INTO dv_area_base VALUES ('533122', '梁河县', '', '', '', '3', '533100', '云南省 德宏傣族景颇族自治州 梁河县');
INSERT INTO dv_area_base VALUES ('533123', '盈江县', '', '', '', '3', '533100', '云南省 德宏傣族景颇族自治州 盈江县');
INSERT INTO dv_area_base VALUES ('533124', '陇川县', '', '', '', '3', '533100', '云南省 德宏傣族景颇族自治州 陇川县');
INSERT INTO dv_area_base VALUES ('533300', '怒江傈僳族自治州', '怒江', '', '', '2', '530000', '云南省 怒江傈僳族自治州');
INSERT INTO dv_area_base VALUES ('533321', '泸水县', '', '', '', '3', '533300', '云南省 怒江傈僳族自治州 泸水县');
INSERT INTO dv_area_base VALUES ('533323', '福贡县', '', '', '', '3', '533300', '云南省 怒江傈僳族自治州 福贡县');
INSERT INTO dv_area_base VALUES ('533324', '贡山独龙族怒族自治县', '', '', '', '3', '533300', '云南省 怒江傈僳族自治州 贡山独龙族怒族自治县');
INSERT INTO dv_area_base VALUES ('533325', '兰坪白族普米族自治县', '', '', '', '3', '533300', '云南省 怒江傈僳族自治州 兰坪白族普米族自治县');
INSERT INTO dv_area_base VALUES ('533400', '迪庆藏族自治州', '迪庆', '', '', '2', '530000', '云南省 迪庆藏族自治州');
INSERT INTO dv_area_base VALUES ('533421', '香格里拉县', '', '', '', '3', '533400', '云南省 迪庆藏族自治州 香格里拉县');
INSERT INTO dv_area_base VALUES ('533422', '德钦县', '', '', '', '3', '533400', '云南省 迪庆藏族自治州 德钦县');
INSERT INTO dv_area_base VALUES ('533423', '维西傈僳族自治县', '', '', '', '3', '533400', '云南省 迪庆藏族自治州 维西傈僳族自治县');
INSERT INTO dv_area_base VALUES ('540000', '西藏自治区', '西藏', 'xizang', '藏', '1', '0', '西藏自治区');
INSERT INTO dv_area_base VALUES ('540100', '拉萨市', '拉萨', '', '', '2', '540000', '西藏自治区 拉萨市');
INSERT INTO dv_area_base VALUES ('540102', '城关区', '', '', '', '3', '540100', '西藏自治区 拉萨市 城关区');
INSERT INTO dv_area_base VALUES ('540121', '林周县', '', '', '', '3', '540100', '西藏自治区 拉萨市 林周县');
INSERT INTO dv_area_base VALUES ('540122', '当雄县', '', '', '', '3', '540100', '西藏自治区 拉萨市 当雄县');
INSERT INTO dv_area_base VALUES ('540123', '尼木县', '', '', '', '3', '540100', '西藏自治区 拉萨市 尼木县');
INSERT INTO dv_area_base VALUES ('540124', '曲水县', '', '', '', '3', '540100', '西藏自治区 拉萨市 曲水县');
INSERT INTO dv_area_base VALUES ('540125', '堆龙德庆县', '', '', '', '3', '540100', '西藏自治区 拉萨市 堆龙德庆县');
INSERT INTO dv_area_base VALUES ('540126', '达孜县', '', '', '', '3', '540100', '西藏自治区 拉萨市 达孜县');
INSERT INTO dv_area_base VALUES ('540127', '墨竹工卡县', '', '', '', '3', '540100', '西藏自治区 拉萨市 墨竹工卡县');
INSERT INTO dv_area_base VALUES ('540200', '日喀则市', '日喀则', '', '', '2', '540000', '西藏自治区 日喀则市');
INSERT INTO dv_area_base VALUES ('540300', '昌都市', '昌都', '', '', '2', '540000', '西藏自治区 昌都市');
INSERT INTO dv_area_base VALUES ('540400', '林芝市', '林芝', '', '', '2', '540000', '西藏自治区 林芝市');
INSERT INTO dv_area_base VALUES ('542121', '昌都县', '', '', '', '3', '542100', '西藏自治区 昌都地区 昌都县');
INSERT INTO dv_area_base VALUES ('542122', '江达县', '', '', '', '3', '542100', '西藏自治区 昌都地区 江达县');
INSERT INTO dv_area_base VALUES ('542123', '贡觉县', '', '', '', '3', '542100', '西藏自治区 昌都地区 贡觉县');
INSERT INTO dv_area_base VALUES ('542124', '类乌齐县', '', '', '', '3', '542100', '西藏自治区 昌都地区 类乌齐县');
INSERT INTO dv_area_base VALUES ('542125', '丁青县', '', '', '', '3', '542100', '西藏自治区 昌都地区 丁青县');
INSERT INTO dv_area_base VALUES ('542126', '察雅县', '', '', '', '3', '542100', '西藏自治区 昌都地区 察雅县');
INSERT INTO dv_area_base VALUES ('542127', '八宿县', '', '', '', '3', '542100', '西藏自治区 昌都地区 八宿县');
INSERT INTO dv_area_base VALUES ('542128', '左贡县', '', '', '', '3', '542100', '西藏自治区 昌都地区 左贡县');
INSERT INTO dv_area_base VALUES ('542129', '芒康县', '', '', '', '3', '542100', '西藏自治区 昌都地区 芒康县');
INSERT INTO dv_area_base VALUES ('542132', '洛隆县', '', '', '', '3', '542100', '西藏自治区 昌都地区 洛隆县');
INSERT INTO dv_area_base VALUES ('542133', '边坝县', '', '', '', '3', '542100', '西藏自治区 昌都地区 边坝县');
INSERT INTO dv_area_base VALUES ('542200', '山南地区', '山南', '', '', '2', '540000', '西藏自治区 山南地区');
INSERT INTO dv_area_base VALUES ('542221', '乃东县', '', '', '', '3', '542200', '西藏自治区 山南地区 乃东县');
INSERT INTO dv_area_base VALUES ('542222', '扎囊县', '', '', '', '3', '542200', '西藏自治区 山南地区 扎囊县');
INSERT INTO dv_area_base VALUES ('542223', '贡嘎县', '', '', '', '3', '542200', '西藏自治区 山南地区 贡嘎县');
INSERT INTO dv_area_base VALUES ('542224', '桑日县', '', '', '', '3', '542200', '西藏自治区 山南地区 桑日县');
INSERT INTO dv_area_base VALUES ('542225', '琼结县', '', '', '', '3', '542200', '西藏自治区 山南地区 琼结县');
INSERT INTO dv_area_base VALUES ('542226', '曲松县', '', '', '', '3', '542200', '西藏自治区 山南地区 曲松县');
INSERT INTO dv_area_base VALUES ('542227', '措美县', '', '', '', '3', '542200', '西藏自治区 山南地区 措美县');
INSERT INTO dv_area_base VALUES ('542228', '洛扎县', '', '', '', '3', '542200', '西藏自治区 山南地区 洛扎县');
INSERT INTO dv_area_base VALUES ('542229', '加查县', '', '', '', '3', '542200', '西藏自治区 山南地区 加查县');
INSERT INTO dv_area_base VALUES ('542231', '隆子县', '', '', '', '3', '542200', '西藏自治区 山南地区 隆子县');
INSERT INTO dv_area_base VALUES ('542232', '错那县', '', '', '', '3', '542200', '西藏自治区 山南地区 错那县');
INSERT INTO dv_area_base VALUES ('542233', '浪卡子县', '', '', '', '3', '542200', '西藏自治区 山南地区 浪卡子县');
INSERT INTO dv_area_base VALUES ('542301', '日喀则市', '', '', '', '3', '542300', '西藏自治区 日喀则地区 日喀则市');
INSERT INTO dv_area_base VALUES ('542322', '南木林县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 南木林县');
INSERT INTO dv_area_base VALUES ('542323', '江孜县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 江孜县');
INSERT INTO dv_area_base VALUES ('542324', '定日县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 定日县');
INSERT INTO dv_area_base VALUES ('542325', '萨迦县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 萨迦县');
INSERT INTO dv_area_base VALUES ('542326', '拉孜县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 拉孜县');
INSERT INTO dv_area_base VALUES ('542327', '昂仁县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 昂仁县');
INSERT INTO dv_area_base VALUES ('542328', '谢通门县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 谢通门县');
INSERT INTO dv_area_base VALUES ('542329', '白朗县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 白朗县');
INSERT INTO dv_area_base VALUES ('542330', '仁布县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 仁布县');
INSERT INTO dv_area_base VALUES ('542331', '康马县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 康马县');
INSERT INTO dv_area_base VALUES ('542332', '定结县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 定结县');
INSERT INTO dv_area_base VALUES ('542333', '仲巴县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 仲巴县');
INSERT INTO dv_area_base VALUES ('542334', '亚东县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 亚东县');
INSERT INTO dv_area_base VALUES ('542335', '吉隆县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 吉隆县');
INSERT INTO dv_area_base VALUES ('542336', '聂拉木县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 聂拉木县');
INSERT INTO dv_area_base VALUES ('542337', '萨嘎县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 萨嘎县');
INSERT INTO dv_area_base VALUES ('542338', '岗巴县', '', '', '', '3', '542300', '西藏自治区 日喀则地区 岗巴县');
INSERT INTO dv_area_base VALUES ('542400', '那曲地区', '那曲', '', '', '2', '540000', '西藏自治区 那曲地区');
INSERT INTO dv_area_base VALUES ('542421', '那曲县', '', '', '', '3', '542400', '西藏自治区 那曲地区 那曲县');
INSERT INTO dv_area_base VALUES ('542422', '嘉黎县', '', '', '', '3', '542400', '西藏自治区 那曲地区 嘉黎县');
INSERT INTO dv_area_base VALUES ('542423', '比如县', '', '', '', '3', '542400', '西藏自治区 那曲地区 比如县');
INSERT INTO dv_area_base VALUES ('542424', '聂荣县', '', '', '', '3', '542400', '西藏自治区 那曲地区 聂荣县');
INSERT INTO dv_area_base VALUES ('542425', '安多县', '', '', '', '3', '542400', '西藏自治区 那曲地区 安多县');
INSERT INTO dv_area_base VALUES ('542426', '申扎县', '', '', '', '3', '542400', '西藏自治区 那曲地区 申扎县');
INSERT INTO dv_area_base VALUES ('542427', '索县', '', '', '', '3', '542400', '西藏自治区 那曲地区 索县');
INSERT INTO dv_area_base VALUES ('542428', '班戈县', '', '', '', '3', '542400', '西藏自治区 那曲地区 班戈县');
INSERT INTO dv_area_base VALUES ('542429', '巴青县', '', '', '', '3', '542400', '西藏自治区 那曲地区 巴青县');
INSERT INTO dv_area_base VALUES ('542430', '尼玛县', '', '', '', '3', '542400', '西藏自治区 那曲地区 尼玛县');
INSERT INTO dv_area_base VALUES ('542500', '阿里地区', '阿里', '', '', '2', '540000', '西藏自治区 阿里地区');
INSERT INTO dv_area_base VALUES ('542521', '普兰县', '', '', '', '3', '542500', '西藏自治区 阿里地区 普兰县');
INSERT INTO dv_area_base VALUES ('542522', '札达县', '', '', '', '3', '542500', '西藏自治区 阿里地区 札达县');
INSERT INTO dv_area_base VALUES ('542523', '噶尔县', '', '', '', '3', '542500', '西藏自治区 阿里地区 噶尔县');
INSERT INTO dv_area_base VALUES ('542524', '日土县', '', '', '', '3', '542500', '西藏自治区 阿里地区 日土县');
INSERT INTO dv_area_base VALUES ('542525', '革吉县', '', '', '', '3', '542500', '西藏自治区 阿里地区 革吉县');
INSERT INTO dv_area_base VALUES ('542526', '改则县', '', '', '', '3', '542500', '西藏自治区 阿里地区 改则县');
INSERT INTO dv_area_base VALUES ('542527', '措勤县', '', '', '', '3', '542500', '西藏自治区 阿里地区 措勤县');
INSERT INTO dv_area_base VALUES ('542621', '林芝县', '', '', '', '3', '542600', '西藏自治区 林芝地区 林芝县');
INSERT INTO dv_area_base VALUES ('542622', '工布江达县', '', '', '', '3', '542600', '西藏自治区 林芝地区 工布江达县');
INSERT INTO dv_area_base VALUES ('542623', '米林县', '', '', '', '3', '542600', '西藏自治区 林芝地区 米林县');
INSERT INTO dv_area_base VALUES ('542624', '墨脱县', '', '', '', '3', '542600', '西藏自治区 林芝地区 墨脱县');
INSERT INTO dv_area_base VALUES ('542625', '波密县', '', '', '', '3', '542600', '西藏自治区 林芝地区 波密县');
INSERT INTO dv_area_base VALUES ('542626', '察隅县', '', '', '', '3', '542600', '西藏自治区 林芝地区 察隅县');
INSERT INTO dv_area_base VALUES ('542627', '朗县', '', '', '', '3', '542600', '西藏自治区 林芝地区 朗县');
INSERT INTO dv_area_base VALUES ('610000', '陕西省', '陕西', 'shanxi', '陕', '1', '0', '陕西省');
INSERT INTO dv_area_base VALUES ('610100', '西安市', '西安', '', '', '2', '610000', '陕西省 西安市');
INSERT INTO dv_area_base VALUES ('610102', '新城区', '', '', '', '3', '610100', '陕西省 西安市 新城区');
INSERT INTO dv_area_base VALUES ('610103', '碑林区', '', '', '', '3', '610100', '陕西省 西安市 碑林区');
INSERT INTO dv_area_base VALUES ('610104', '莲湖区', '', '', '', '3', '610100', '陕西省 西安市 莲湖区');
INSERT INTO dv_area_base VALUES ('610111', '灞桥区', '', '', '', '3', '610100', '陕西省 西安市 灞桥区');
INSERT INTO dv_area_base VALUES ('610112', '未央区', '', '', '', '3', '610100', '陕西省 西安市 未央区');
INSERT INTO dv_area_base VALUES ('610113', '雁塔区', '', '', '', '3', '610100', '陕西省 西安市 雁塔区');
INSERT INTO dv_area_base VALUES ('610114', '阎良区', '', '', '', '3', '610100', '陕西省 西安市 阎良区');
INSERT INTO dv_area_base VALUES ('610115', '临潼区', '', '', '', '3', '610100', '陕西省 西安市 临潼区');
INSERT INTO dv_area_base VALUES ('610116', '长安区', '', '', '', '3', '610100', '陕西省 西安市 长安区');
INSERT INTO dv_area_base VALUES ('610122', '蓝田县', '', '', '', '3', '610100', '陕西省 西安市 蓝田县');
INSERT INTO dv_area_base VALUES ('610124', '周至县', '', '', '', '3', '610100', '陕西省 西安市 周至县');
INSERT INTO dv_area_base VALUES ('610125', '户县', '', '', '', '3', '610100', '陕西省 西安市 户县');
INSERT INTO dv_area_base VALUES ('610126', '高陵县', '', '', '', '3', '610100', '陕西省 西安市 高陵县');
INSERT INTO dv_area_base VALUES ('610200', '铜川市', '铜川', '', '', '2', '610000', '陕西省 铜川市');
INSERT INTO dv_area_base VALUES ('610202', '王益区', '', '', '', '3', '610200', '陕西省 铜川市 王益区');
INSERT INTO dv_area_base VALUES ('610203', '印台区', '', '', '', '3', '610200', '陕西省 铜川市 印台区');
INSERT INTO dv_area_base VALUES ('610204', '耀州区', '', '', '', '3', '610200', '陕西省 铜川市 耀州区');
INSERT INTO dv_area_base VALUES ('610222', '宜君县', '', '', '', '3', '610200', '陕西省 铜川市 宜君县');
INSERT INTO dv_area_base VALUES ('610300', '宝鸡市', '宝鸡', '', '', '2', '610000', '陕西省 宝鸡市');
INSERT INTO dv_area_base VALUES ('610302', '渭滨区', '', '', '', '3', '610300', '陕西省 宝鸡市 渭滨区');
INSERT INTO dv_area_base VALUES ('610303', '金台区', '', '', '', '3', '610300', '陕西省 宝鸡市 金台区');
INSERT INTO dv_area_base VALUES ('610304', '陈仓区', '', '', '', '3', '610300', '陕西省 宝鸡市 陈仓区');
INSERT INTO dv_area_base VALUES ('610322', '凤翔县', '', '', '', '3', '610300', '陕西省 宝鸡市 凤翔县');
INSERT INTO dv_area_base VALUES ('610323', '岐山县', '', '', '', '3', '610300', '陕西省 宝鸡市 岐山县');
INSERT INTO dv_area_base VALUES ('610324', '扶风县', '', '', '', '3', '610300', '陕西省 宝鸡市 扶风县');
INSERT INTO dv_area_base VALUES ('610326', '眉县', '', '', '', '3', '610300', '陕西省 宝鸡市 眉县');
INSERT INTO dv_area_base VALUES ('610327', '陇县', '', '', '', '3', '610300', '陕西省 宝鸡市 陇县');
INSERT INTO dv_area_base VALUES ('610328', '千阳县', '', '', '', '3', '610300', '陕西省 宝鸡市 千阳县');
INSERT INTO dv_area_base VALUES ('610329', '麟游县', '', '', '', '3', '610300', '陕西省 宝鸡市 麟游县');
INSERT INTO dv_area_base VALUES ('610330', '凤县', '', '', '', '3', '610300', '陕西省 宝鸡市 凤县');
INSERT INTO dv_area_base VALUES ('610331', '太白县', '', '', '', '3', '610300', '陕西省 宝鸡市 太白县');
INSERT INTO dv_area_base VALUES ('610400', '咸阳市', '咸阳', '', '', '2', '610000', '陕西省 咸阳市');
INSERT INTO dv_area_base VALUES ('610402', '秦都区', '', '', '', '3', '610400', '陕西省 咸阳市 秦都区');
INSERT INTO dv_area_base VALUES ('610403', '杨陵区', '', '', '', '3', '610400', '陕西省 咸阳市 杨陵区');
INSERT INTO dv_area_base VALUES ('610404', '渭城区', '', '', '', '3', '610400', '陕西省 咸阳市 渭城区');
INSERT INTO dv_area_base VALUES ('610422', '三原县', '', '', '', '3', '610400', '陕西省 咸阳市 三原县');
INSERT INTO dv_area_base VALUES ('610423', '泾阳县', '', '', '', '3', '610400', '陕西省 咸阳市 泾阳县');
INSERT INTO dv_area_base VALUES ('610424', '乾县', '', '', '', '3', '610400', '陕西省 咸阳市 乾县');
INSERT INTO dv_area_base VALUES ('610425', '礼泉县', '', '', '', '3', '610400', '陕西省 咸阳市 礼泉县');
INSERT INTO dv_area_base VALUES ('610426', '永寿县', '', '', '', '3', '610400', '陕西省 咸阳市 永寿县');
INSERT INTO dv_area_base VALUES ('610427', '彬县', '', '', '', '3', '610400', '陕西省 咸阳市 彬县');
INSERT INTO dv_area_base VALUES ('610428', '长武县', '', '', '', '3', '610400', '陕西省 咸阳市 长武县');
INSERT INTO dv_area_base VALUES ('610429', '旬邑县', '', '', '', '3', '610400', '陕西省 咸阳市 旬邑县');
INSERT INTO dv_area_base VALUES ('610430', '淳化县', '', '', '', '3', '610400', '陕西省 咸阳市 淳化县');
INSERT INTO dv_area_base VALUES ('610431', '武功县', '', '', '', '3', '610400', '陕西省 咸阳市 武功县');
INSERT INTO dv_area_base VALUES ('610481', '兴平市', '', '', '', '3', '610400', '陕西省 咸阳市 兴平市');
INSERT INTO dv_area_base VALUES ('610500', '渭南市', '渭南', '', '', '2', '610000', '陕西省 渭南市');
INSERT INTO dv_area_base VALUES ('610502', '临渭区', '', '', '', '3', '610500', '陕西省 渭南市 临渭区');
INSERT INTO dv_area_base VALUES ('610521', '华县', '', '', '', '3', '610500', '陕西省 渭南市 华县');
INSERT INTO dv_area_base VALUES ('610522', '潼关县', '', '', '', '3', '610500', '陕西省 渭南市 潼关县');
INSERT INTO dv_area_base VALUES ('610523', '大荔县', '', '', '', '3', '610500', '陕西省 渭南市 大荔县');
INSERT INTO dv_area_base VALUES ('610524', '合阳县', '', '', '', '3', '610500', '陕西省 渭南市 合阳县');
INSERT INTO dv_area_base VALUES ('610525', '澄城县', '', '', '', '3', '610500', '陕西省 渭南市 澄城县');
INSERT INTO dv_area_base VALUES ('610526', '蒲城县', '', '', '', '3', '610500', '陕西省 渭南市 蒲城县');
INSERT INTO dv_area_base VALUES ('610527', '白水县', '', '', '', '3', '610500', '陕西省 渭南市 白水县');
INSERT INTO dv_area_base VALUES ('610528', '富平县', '', '', '', '3', '610500', '陕西省 渭南市 富平县');
INSERT INTO dv_area_base VALUES ('610581', '韩城市', '', '', '', '3', '610500', '陕西省 渭南市 韩城市');
INSERT INTO dv_area_base VALUES ('610582', '华阴市', '', '', '', '3', '610500', '陕西省 渭南市 华阴市');
INSERT INTO dv_area_base VALUES ('610600', '延安市', '延安', '', '', '2', '610000', '陕西省 延安市');
INSERT INTO dv_area_base VALUES ('610602', '宝塔区', '', '', '', '3', '610600', '陕西省 延安市 宝塔区');
INSERT INTO dv_area_base VALUES ('610621', '延长县', '', '', '', '3', '610600', '陕西省 延安市 延长县');
INSERT INTO dv_area_base VALUES ('610622', '延川县', '', '', '', '3', '610600', '陕西省 延安市 延川县');
INSERT INTO dv_area_base VALUES ('610623', '子长县', '', '', '', '3', '610600', '陕西省 延安市 子长县');
INSERT INTO dv_area_base VALUES ('610624', '安塞县', '', '', '', '3', '610600', '陕西省 延安市 安塞县');
INSERT INTO dv_area_base VALUES ('610625', '志丹县', '', '', '', '3', '610600', '陕西省 延安市 志丹县');
INSERT INTO dv_area_base VALUES ('610626', '吴起县', '', '', '', '3', '610600', '陕西省 延安市 吴起县');
INSERT INTO dv_area_base VALUES ('610627', '甘泉县', '', '', '', '3', '610600', '陕西省 延安市 甘泉县');
INSERT INTO dv_area_base VALUES ('610628', '富县', '', '', '', '3', '610600', '陕西省 延安市 富县');
INSERT INTO dv_area_base VALUES ('610629', '洛川县', '', '', '', '3', '610600', '陕西省 延安市 洛川县');
INSERT INTO dv_area_base VALUES ('610630', '宜川县', '', '', '', '3', '610600', '陕西省 延安市 宜川县');
INSERT INTO dv_area_base VALUES ('610631', '黄龙县', '', '', '', '3', '610600', '陕西省 延安市 黄龙县');
INSERT INTO dv_area_base VALUES ('610632', '黄陵县', '', '', '', '3', '610600', '陕西省 延安市 黄陵县');
INSERT INTO dv_area_base VALUES ('610700', '汉中市', '汉中', '', '', '2', '610000', '陕西省 汉中市');
INSERT INTO dv_area_base VALUES ('610702', '汉台区', '', '', '', '3', '610700', '陕西省 汉中市 汉台区');
INSERT INTO dv_area_base VALUES ('610721', '南郑县', '', '', '', '3', '610700', '陕西省 汉中市 南郑县');
INSERT INTO dv_area_base VALUES ('610722', '城固县', '', '', '', '3', '610700', '陕西省 汉中市 城固县');
INSERT INTO dv_area_base VALUES ('610723', '洋县', '', '', '', '3', '610700', '陕西省 汉中市 洋县');
INSERT INTO dv_area_base VALUES ('610724', '西乡县', '', '', '', '3', '610700', '陕西省 汉中市 西乡县');
INSERT INTO dv_area_base VALUES ('610725', '勉县', '', '', '', '3', '610700', '陕西省 汉中市 勉县');
INSERT INTO dv_area_base VALUES ('610726', '宁强县', '', '', '', '3', '610700', '陕西省 汉中市 宁强县');
INSERT INTO dv_area_base VALUES ('610727', '略阳县', '', '', '', '3', '610700', '陕西省 汉中市 略阳县');
INSERT INTO dv_area_base VALUES ('610728', '镇巴县', '', '', '', '3', '610700', '陕西省 汉中市 镇巴县');
INSERT INTO dv_area_base VALUES ('610729', '留坝县', '', '', '', '3', '610700', '陕西省 汉中市 留坝县');
INSERT INTO dv_area_base VALUES ('610730', '佛坪县', '', '', '', '3', '610700', '陕西省 汉中市 佛坪县');
INSERT INTO dv_area_base VALUES ('610800', '榆林市', '榆林', '', '', '2', '610000', '陕西省 榆林市');
INSERT INTO dv_area_base VALUES ('610802', '榆阳区', '', '', '', '3', '610800', '陕西省 榆林市 榆阳区');
INSERT INTO dv_area_base VALUES ('610821', '神木县', '', '', '', '3', '610800', '陕西省 榆林市 神木县');
INSERT INTO dv_area_base VALUES ('610822', '府谷县', '', '', '', '3', '610800', '陕西省 榆林市 府谷县');
INSERT INTO dv_area_base VALUES ('610823', '横山县', '', '', '', '3', '610800', '陕西省 榆林市 横山县');
INSERT INTO dv_area_base VALUES ('610824', '靖边县', '', '', '', '3', '610800', '陕西省 榆林市 靖边县');
INSERT INTO dv_area_base VALUES ('610825', '定边县', '', '', '', '3', '610800', '陕西省 榆林市 定边县');
INSERT INTO dv_area_base VALUES ('610826', '绥德县', '', '', '', '3', '610800', '陕西省 榆林市 绥德县');
INSERT INTO dv_area_base VALUES ('610827', '米脂县', '', '', '', '3', '610800', '陕西省 榆林市 米脂县');
INSERT INTO dv_area_base VALUES ('610828', '佳县', '', '', '', '3', '610800', '陕西省 榆林市 佳县');
INSERT INTO dv_area_base VALUES ('610829', '吴堡县', '', '', '', '3', '610800', '陕西省 榆林市 吴堡县');
INSERT INTO dv_area_base VALUES ('610830', '清涧县', '', '', '', '3', '610800', '陕西省 榆林市 清涧县');
INSERT INTO dv_area_base VALUES ('610831', '子洲县', '', '', '', '3', '610800', '陕西省 榆林市 子洲县');
INSERT INTO dv_area_base VALUES ('610900', '安康市', '安康', '', '', '2', '610000', '陕西省 安康市');
INSERT INTO dv_area_base VALUES ('610902', '汉滨区', '', '', '', '3', '610900', '陕西省 安康市 汉滨区');
INSERT INTO dv_area_base VALUES ('610921', '汉阴县', '', '', '', '3', '610900', '陕西省 安康市 汉阴县');
INSERT INTO dv_area_base VALUES ('610922', '石泉县', '', '', '', '3', '610900', '陕西省 安康市 石泉县');
INSERT INTO dv_area_base VALUES ('610923', '宁陕县', '', '', '', '3', '610900', '陕西省 安康市 宁陕县');
INSERT INTO dv_area_base VALUES ('610924', '紫阳县', '', '', '', '3', '610900', '陕西省 安康市 紫阳县');
INSERT INTO dv_area_base VALUES ('610925', '岚皋县', '', '', '', '3', '610900', '陕西省 安康市 岚皋县');
INSERT INTO dv_area_base VALUES ('610926', '平利县', '', '', '', '3', '610900', '陕西省 安康市 平利县');
INSERT INTO dv_area_base VALUES ('610927', '镇坪县', '', '', '', '3', '610900', '陕西省 安康市 镇坪县');
INSERT INTO dv_area_base VALUES ('610928', '旬阳县', '', '', '', '3', '610900', '陕西省 安康市 旬阳县');
INSERT INTO dv_area_base VALUES ('610929', '白河县', '', '', '', '3', '610900', '陕西省 安康市 白河县');
INSERT INTO dv_area_base VALUES ('611000', '商洛市', '商洛', '', '', '2', '610000', '陕西省 商洛市');
INSERT INTO dv_area_base VALUES ('611002', '商州区', '', '', '', '3', '611000', '陕西省 商洛市 商州区');
INSERT INTO dv_area_base VALUES ('611021', '洛南县', '', '', '', '3', '611000', '陕西省 商洛市 洛南县');
INSERT INTO dv_area_base VALUES ('611022', '丹凤县', '', '', '', '3', '611000', '陕西省 商洛市 丹凤县');
INSERT INTO dv_area_base VALUES ('611023', '商南县', '', '', '', '3', '611000', '陕西省 商洛市 商南县');
INSERT INTO dv_area_base VALUES ('611024', '山阳县', '', '', '', '3', '611000', '陕西省 商洛市 山阳县');
INSERT INTO dv_area_base VALUES ('611025', '镇安县', '', '', '', '3', '611000', '陕西省 商洛市 镇安县');
INSERT INTO dv_area_base VALUES ('611026', '柞水县', '', '', '', '3', '611000', '陕西省 商洛市 柞水县');
INSERT INTO dv_area_base VALUES ('620000', '甘肃省', '甘肃', 'gansu', '甘', '1', '0', '甘肃省');
INSERT INTO dv_area_base VALUES ('620100', '兰州市', '兰州', '', '', '2', '620000', '甘肃省 兰州市');
INSERT INTO dv_area_base VALUES ('620102', '城关区', '', '', '', '3', '620100', '甘肃省 兰州市 城关区');
INSERT INTO dv_area_base VALUES ('620103', '七里河区', '', '', '', '3', '620100', '甘肃省 兰州市 七里河区');
INSERT INTO dv_area_base VALUES ('620104', '西固区', '', '', '', '3', '620100', '甘肃省 兰州市 西固区');
INSERT INTO dv_area_base VALUES ('620105', '安宁区', '', '', '', '3', '620100', '甘肃省 兰州市 安宁区');
INSERT INTO dv_area_base VALUES ('620111', '红古区', '', '', '', '3', '620100', '甘肃省 兰州市 红古区');
INSERT INTO dv_area_base VALUES ('620121', '永登县', '', '', '', '3', '620100', '甘肃省 兰州市 永登县');
INSERT INTO dv_area_base VALUES ('620122', '皋兰县', '', '', '', '3', '620100', '甘肃省 兰州市 皋兰县');
INSERT INTO dv_area_base VALUES ('620123', '榆中县', '', '', '', '3', '620100', '甘肃省 兰州市 榆中县');
INSERT INTO dv_area_base VALUES ('620200', '嘉峪关市', '嘉峪关', '', '', '2', '620000', '甘肃省 嘉峪关市');
INSERT INTO dv_area_base VALUES ('620201', '市辖区', '', '', '', '3', '620200', '甘肃省 嘉峪关市 市辖区');
INSERT INTO dv_area_base VALUES ('620300', '金昌市', '金昌', '', '', '2', '620000', '甘肃省 金昌市');
INSERT INTO dv_area_base VALUES ('620302', '金川区', '', '', '', '3', '620300', '甘肃省 金昌市 金川区');
INSERT INTO dv_area_base VALUES ('620321', '永昌县', '', '', '', '3', '620300', '甘肃省 金昌市 永昌县');
INSERT INTO dv_area_base VALUES ('620400', '白银市', '白银', '', '', '2', '620000', '甘肃省 白银市');
INSERT INTO dv_area_base VALUES ('620402', '白银区', '', '', '', '3', '620400', '甘肃省 白银市 白银区');
INSERT INTO dv_area_base VALUES ('620403', '平川区', '', '', '', '3', '620400', '甘肃省 白银市 平川区');
INSERT INTO dv_area_base VALUES ('620421', '靖远县', '', '', '', '3', '620400', '甘肃省 白银市 靖远县');
INSERT INTO dv_area_base VALUES ('620422', '会宁县', '', '', '', '3', '620400', '甘肃省 白银市 会宁县');
INSERT INTO dv_area_base VALUES ('620423', '景泰县', '', '', '', '3', '620400', '甘肃省 白银市 景泰县');
INSERT INTO dv_area_base VALUES ('620500', '天水市', '天水', '', '', '2', '620000', '甘肃省 天水市');
INSERT INTO dv_area_base VALUES ('620502', '秦州区', '', '', '', '3', '620500', '甘肃省 天水市 秦州区');
INSERT INTO dv_area_base VALUES ('620503', '麦积区', '', '', '', '3', '620500', '甘肃省 天水市 麦积区');
INSERT INTO dv_area_base VALUES ('620521', '清水县', '', '', '', '3', '620500', '甘肃省 天水市 清水县');
INSERT INTO dv_area_base VALUES ('620522', '秦安县', '', '', '', '3', '620500', '甘肃省 天水市 秦安县');
INSERT INTO dv_area_base VALUES ('620523', '甘谷县', '', '', '', '3', '620500', '甘肃省 天水市 甘谷县');
INSERT INTO dv_area_base VALUES ('620524', '武山县', '', '', '', '3', '620500', '甘肃省 天水市 武山县');
INSERT INTO dv_area_base VALUES ('620525', '张家川回族自治县', '', '', '', '3', '620500', '甘肃省 天水市 张家川回族自治县');
INSERT INTO dv_area_base VALUES ('620600', '武威市', '武威', '', '', '2', '620000', '甘肃省 武威市');
INSERT INTO dv_area_base VALUES ('620602', '凉州区', '', '', '', '3', '620600', '甘肃省 武威市 凉州区');
INSERT INTO dv_area_base VALUES ('620621', '民勤县', '', '', '', '3', '620600', '甘肃省 武威市 民勤县');
INSERT INTO dv_area_base VALUES ('620622', '古浪县', '', '', '', '3', '620600', '甘肃省 武威市 古浪县');
INSERT INTO dv_area_base VALUES ('620623', '天祝藏族自治县', '', '', '', '3', '620600', '甘肃省 武威市 天祝藏族自治县');
INSERT INTO dv_area_base VALUES ('620700', '张掖市', '张掖', '', '', '2', '620000', '甘肃省 张掖市');
INSERT INTO dv_area_base VALUES ('620702', '甘州区', '', '', '', '3', '620700', '甘肃省 张掖市 甘州区');
INSERT INTO dv_area_base VALUES ('620721', '肃南裕固族自治县', '', '', '', '3', '620700', '甘肃省 张掖市 肃南裕固族自治县');
INSERT INTO dv_area_base VALUES ('620722', '民乐县', '', '', '', '3', '620700', '甘肃省 张掖市 民乐县');
INSERT INTO dv_area_base VALUES ('620723', '临泽县', '', '', '', '3', '620700', '甘肃省 张掖市 临泽县');
INSERT INTO dv_area_base VALUES ('620724', '高台县', '', '', '', '3', '620700', '甘肃省 张掖市 高台县');
INSERT INTO dv_area_base VALUES ('620725', '山丹县', '', '', '', '3', '620700', '甘肃省 张掖市 山丹县');
INSERT INTO dv_area_base VALUES ('620800', '平凉市', '平凉', '', '', '2', '620000', '甘肃省 平凉市');
INSERT INTO dv_area_base VALUES ('620802', '崆峒区', '', '', '', '3', '620800', '甘肃省 平凉市 崆峒区');
INSERT INTO dv_area_base VALUES ('620821', '泾川县', '', '', '', '3', '620800', '甘肃省 平凉市 泾川县');
INSERT INTO dv_area_base VALUES ('620822', '灵台县', '', '', '', '3', '620800', '甘肃省 平凉市 灵台县');
INSERT INTO dv_area_base VALUES ('620823', '崇信县', '', '', '', '3', '620800', '甘肃省 平凉市 崇信县');
INSERT INTO dv_area_base VALUES ('620824', '华亭县', '', '', '', '3', '620800', '甘肃省 平凉市 华亭县');
INSERT INTO dv_area_base VALUES ('620825', '庄浪县', '', '', '', '3', '620800', '甘肃省 平凉市 庄浪县');
INSERT INTO dv_area_base VALUES ('620826', '静宁县', '', '', '', '3', '620800', '甘肃省 平凉市 静宁县');
INSERT INTO dv_area_base VALUES ('620900', '酒泉市', '酒泉', '', '', '2', '620000', '甘肃省 酒泉市');
INSERT INTO dv_area_base VALUES ('620902', '肃州区', '', '', '', '3', '620900', '甘肃省 酒泉市 肃州区');
INSERT INTO dv_area_base VALUES ('620921', '金塔县', '', '', '', '3', '620900', '甘肃省 酒泉市 金塔县');
INSERT INTO dv_area_base VALUES ('620922', '瓜州县', '', '', '', '3', '620900', '甘肃省 酒泉市 瓜州县');
INSERT INTO dv_area_base VALUES ('620923', '肃北蒙古族自治县', '', '', '', '3', '620900', '甘肃省 酒泉市 肃北蒙古族自治县');
INSERT INTO dv_area_base VALUES ('620924', '阿克塞哈萨克族自治县', '', '', '', '3', '620900', '甘肃省 酒泉市 阿克塞哈萨克族自治县');
INSERT INTO dv_area_base VALUES ('620981', '玉门市', '', '', '', '3', '620900', '甘肃省 酒泉市 玉门市');
INSERT INTO dv_area_base VALUES ('620982', '敦煌市', '', '', '', '3', '620900', '甘肃省 酒泉市 敦煌市');
INSERT INTO dv_area_base VALUES ('621000', '庆阳市', '庆阳', '', '', '2', '620000', '甘肃省 庆阳市');
INSERT INTO dv_area_base VALUES ('621002', '西峰区', '', '', '', '3', '621000', '甘肃省 庆阳市 西峰区');
INSERT INTO dv_area_base VALUES ('621021', '庆城县', '', '', '', '3', '621000', '甘肃省 庆阳市 庆城县');
INSERT INTO dv_area_base VALUES ('621022', '环县', '', '', '', '3', '621000', '甘肃省 庆阳市 环县');
INSERT INTO dv_area_base VALUES ('621023', '华池县', '', '', '', '3', '621000', '甘肃省 庆阳市 华池县');
INSERT INTO dv_area_base VALUES ('621024', '合水县', '', '', '', '3', '621000', '甘肃省 庆阳市 合水县');
INSERT INTO dv_area_base VALUES ('621025', '正宁县', '', '', '', '3', '621000', '甘肃省 庆阳市 正宁县');
INSERT INTO dv_area_base VALUES ('621026', '宁县', '', '', '', '3', '621000', '甘肃省 庆阳市 宁县');
INSERT INTO dv_area_base VALUES ('621027', '镇原县', '', '', '', '3', '621000', '甘肃省 庆阳市 镇原县');
INSERT INTO dv_area_base VALUES ('621100', '定西市', '定西', '', '', '2', '620000', '甘肃省 定西市');
INSERT INTO dv_area_base VALUES ('621102', '安定区', '', '', '', '3', '621100', '甘肃省 定西市 安定区');
INSERT INTO dv_area_base VALUES ('621121', '通渭县', '', '', '', '3', '621100', '甘肃省 定西市 通渭县');
INSERT INTO dv_area_base VALUES ('621122', '陇西县', '', '', '', '3', '621100', '甘肃省 定西市 陇西县');
INSERT INTO dv_area_base VALUES ('621123', '渭源县', '', '', '', '3', '621100', '甘肃省 定西市 渭源县');
INSERT INTO dv_area_base VALUES ('621124', '临洮县', '', '', '', '3', '621100', '甘肃省 定西市 临洮县');
INSERT INTO dv_area_base VALUES ('621125', '漳县', '', '', '', '3', '621100', '甘肃省 定西市 漳县');
INSERT INTO dv_area_base VALUES ('621126', '岷县', '', '', '', '3', '621100', '甘肃省 定西市 岷县');
INSERT INTO dv_area_base VALUES ('621200', '陇南市', '陇南', '', '', '2', '620000', '甘肃省 陇南市');
INSERT INTO dv_area_base VALUES ('621202', '武都区', '', '', '', '3', '621200', '甘肃省 陇南市 武都区');
INSERT INTO dv_area_base VALUES ('621221', '成县', '', '', '', '3', '621200', '甘肃省 陇南市 成县');
INSERT INTO dv_area_base VALUES ('621222', '文县', '', '', '', '3', '621200', '甘肃省 陇南市 文县');
INSERT INTO dv_area_base VALUES ('621223', '宕昌县', '', '', '', '3', '621200', '甘肃省 陇南市 宕昌县');
INSERT INTO dv_area_base VALUES ('621224', '康县', '', '', '', '3', '621200', '甘肃省 陇南市 康县');
INSERT INTO dv_area_base VALUES ('621225', '西和县', '', '', '', '3', '621200', '甘肃省 陇南市 西和县');
INSERT INTO dv_area_base VALUES ('621226', '礼县', '', '', '', '3', '621200', '甘肃省 陇南市 礼县');
INSERT INTO dv_area_base VALUES ('621227', '徽县', '', '', '', '3', '621200', '甘肃省 陇南市 徽县');
INSERT INTO dv_area_base VALUES ('621228', '两当县', '', '', '', '3', '621200', '甘肃省 陇南市 两当县');
INSERT INTO dv_area_base VALUES ('622900', '临夏回族自治州', '临夏', '', '', '2', '620000', '甘肃省 临夏回族自治州');
INSERT INTO dv_area_base VALUES ('622901', '临夏市', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 临夏市');
INSERT INTO dv_area_base VALUES ('622921', '临夏县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 临夏县');
INSERT INTO dv_area_base VALUES ('622922', '康乐县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 康乐县');
INSERT INTO dv_area_base VALUES ('622923', '永靖县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 永靖县');
INSERT INTO dv_area_base VALUES ('622924', '广河县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 广河县');
INSERT INTO dv_area_base VALUES ('622925', '和政县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 和政县');
INSERT INTO dv_area_base VALUES ('622926', '东乡族自治县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 东乡族自治县');
INSERT INTO dv_area_base VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '', '', '', '3', '622900', '甘肃省 临夏回族自治州 积石山保安族东乡族撒拉族自治县');
INSERT INTO dv_area_base VALUES ('623000', '甘南藏族自治州', '甘南', '', '', '2', '620000', '甘肃省 甘南藏族自治州');
INSERT INTO dv_area_base VALUES ('623001', '合作市', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 合作市');
INSERT INTO dv_area_base VALUES ('623021', '临潭县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 临潭县');
INSERT INTO dv_area_base VALUES ('623022', '卓尼县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 卓尼县');
INSERT INTO dv_area_base VALUES ('623023', '舟曲县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 舟曲县');
INSERT INTO dv_area_base VALUES ('623024', '迭部县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 迭部县');
INSERT INTO dv_area_base VALUES ('623025', '玛曲县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 玛曲县');
INSERT INTO dv_area_base VALUES ('623026', '碌曲县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 碌曲县');
INSERT INTO dv_area_base VALUES ('623027', '夏河县', '', '', '', '3', '623000', '甘肃省 甘南藏族自治州 夏河县');
INSERT INTO dv_area_base VALUES ('630000', '青海省', '青海', 'qinghai', '青', '1', '0', '青海省');
INSERT INTO dv_area_base VALUES ('630100', '西宁市', '西宁', '', '', '2', '630000', '青海省 西宁市');
INSERT INTO dv_area_base VALUES ('630102', '城东区', '', '', '', '3', '630100', '青海省 西宁市 城东区');
INSERT INTO dv_area_base VALUES ('630103', '城中区', '', '', '', '3', '630100', '青海省 西宁市 城中区');
INSERT INTO dv_area_base VALUES ('630104', '城西区', '', '', '', '3', '630100', '青海省 西宁市 城西区');
INSERT INTO dv_area_base VALUES ('630105', '城北区', '', '', '', '3', '630100', '青海省 西宁市 城北区');
INSERT INTO dv_area_base VALUES ('630121', '大通回族土族自治县', '', '', '', '3', '630100', '青海省 西宁市 大通回族土族自治县');
INSERT INTO dv_area_base VALUES ('630122', '湟中县', '', '', '', '3', '630100', '青海省 西宁市 湟中县');
INSERT INTO dv_area_base VALUES ('630123', '湟源县', '', '', '', '3', '630100', '青海省 西宁市 湟源县');
INSERT INTO dv_area_base VALUES ('630200', '海东市', '海东', '', '', '2', '630000', '青海省 海东市');
INSERT INTO dv_area_base VALUES ('632121', '平安县', '', '', '', '3', '632100', '青海省 海东地区 平安县');
INSERT INTO dv_area_base VALUES ('632122', '民和回族土族自治县', '', '', '', '3', '632100', '青海省 海东地区 民和回族土族自治县');
INSERT INTO dv_area_base VALUES ('632123', '乐都县', '', '', '', '3', '632100', '青海省 海东地区 乐都县');
INSERT INTO dv_area_base VALUES ('632126', '互助土族自治县', '', '', '', '3', '632100', '青海省 海东地区 互助土族自治县');
INSERT INTO dv_area_base VALUES ('632127', '化隆回族自治县', '', '', '', '3', '632100', '青海省 海东地区 化隆回族自治县');
INSERT INTO dv_area_base VALUES ('632128', '循化撒拉族自治县', '', '', '', '3', '632100', '青海省 海东地区 循化撒拉族自治县');
INSERT INTO dv_area_base VALUES ('632200', '海北藏族自治州', '海北', '', '', '2', '630000', '青海省 海北藏族自治州');
INSERT INTO dv_area_base VALUES ('632221', '门源回族自治县', '', '', '', '3', '632200', '青海省 海北藏族自治州 门源回族自治县');
INSERT INTO dv_area_base VALUES ('632222', '祁连县', '', '', '', '3', '632200', '青海省 海北藏族自治州 祁连县');
INSERT INTO dv_area_base VALUES ('632223', '海晏县', '', '', '', '3', '632200', '青海省 海北藏族自治州 海晏县');
INSERT INTO dv_area_base VALUES ('632224', '刚察县', '', '', '', '3', '632200', '青海省 海北藏族自治州 刚察县');
INSERT INTO dv_area_base VALUES ('632300', '黄南藏族自治州', '黄南', '', '', '2', '630000', '青海省 黄南藏族自治州');
INSERT INTO dv_area_base VALUES ('632321', '同仁县', '', '', '', '3', '632300', '青海省 黄南藏族自治州 同仁县');
INSERT INTO dv_area_base VALUES ('632322', '尖扎县', '', '', '', '3', '632300', '青海省 黄南藏族自治州 尖扎县');
INSERT INTO dv_area_base VALUES ('632323', '泽库县', '', '', '', '3', '632300', '青海省 黄南藏族自治州 泽库县');
INSERT INTO dv_area_base VALUES ('632324', '河南蒙古族自治县', '', '', '', '3', '632300', '青海省 黄南藏族自治州 河南蒙古族自治县');
INSERT INTO dv_area_base VALUES ('632500', '海南藏族自治州', '海南', '', '', '2', '630000', '青海省 海南藏族自治州');
INSERT INTO dv_area_base VALUES ('632521', '共和县', '', '', '', '3', '632500', '青海省 海南藏族自治州 共和县');
INSERT INTO dv_area_base VALUES ('632522', '同德县', '', '', '', '3', '632500', '青海省 海南藏族自治州 同德县');
INSERT INTO dv_area_base VALUES ('632523', '贵德县', '', '', '', '3', '632500', '青海省 海南藏族自治州 贵德县');
INSERT INTO dv_area_base VALUES ('632524', '兴海县', '', '', '', '3', '632500', '青海省 海南藏族自治州 兴海县');
INSERT INTO dv_area_base VALUES ('632525', '贵南县', '', '', '', '3', '632500', '青海省 海南藏族自治州 贵南县');
INSERT INTO dv_area_base VALUES ('632600', '果洛藏族自治州', '果洛', '', '', '2', '630000', '青海省 果洛藏族自治州');
INSERT INTO dv_area_base VALUES ('632621', '玛沁县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 玛沁县');
INSERT INTO dv_area_base VALUES ('632622', '班玛县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 班玛县');
INSERT INTO dv_area_base VALUES ('632623', '甘德县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 甘德县');
INSERT INTO dv_area_base VALUES ('632624', '达日县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 达日县');
INSERT INTO dv_area_base VALUES ('632625', '久治县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 久治县');
INSERT INTO dv_area_base VALUES ('632626', '玛多县', '', '', '', '3', '632600', '青海省 果洛藏族自治州 玛多县');
INSERT INTO dv_area_base VALUES ('632700', '玉树藏族自治州', '玉树', '', '', '2', '630000', '青海省 玉树藏族自治州');
INSERT INTO dv_area_base VALUES ('632721', '玉树县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 玉树县');
INSERT INTO dv_area_base VALUES ('632722', '杂多县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 杂多县');
INSERT INTO dv_area_base VALUES ('632723', '称多县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 称多县');
INSERT INTO dv_area_base VALUES ('632724', '治多县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 治多县');
INSERT INTO dv_area_base VALUES ('632725', '囊谦县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 囊谦县');
INSERT INTO dv_area_base VALUES ('632726', '曲麻莱县', '', '', '', '3', '632700', '青海省 玉树藏族自治州 曲麻莱县');
INSERT INTO dv_area_base VALUES ('632800', '海西蒙古族藏族自治州', '海西', '', '', '2', '630000', '青海省 海西蒙古族藏族自治州');
INSERT INTO dv_area_base VALUES ('632801', '格尔木市', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 格尔木市');
INSERT INTO dv_area_base VALUES ('632802', '德令哈市', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 德令哈市');
INSERT INTO dv_area_base VALUES ('632821', '乌兰县', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 乌兰县');
INSERT INTO dv_area_base VALUES ('632822', '都兰县', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 都兰县');
INSERT INTO dv_area_base VALUES ('632823', '天峻县', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 天峻县');
INSERT INTO dv_area_base VALUES ('632824', '海西蒙古族藏族自治州直辖', '', '', '', '3', '632800', '青海省 海西蒙古族藏族自治州 海西蒙古族藏族自治州直辖');
INSERT INTO dv_area_base VALUES ('640000', '宁夏回族自治区', '宁夏', 'ningxia', '宁', '1', '0', '宁夏回族自治区');
INSERT INTO dv_area_base VALUES ('640100', '银川市', '银川', '', '', '2', '640000', '宁夏回族自治区 银川市');
INSERT INTO dv_area_base VALUES ('640104', '兴庆区', '', '', '', '3', '640100', '宁夏回族自治区 银川市 兴庆区');
INSERT INTO dv_area_base VALUES ('640105', '西夏区', '', '', '', '3', '640100', '宁夏回族自治区 银川市 西夏区');
INSERT INTO dv_area_base VALUES ('640106', '金凤区', '', '', '', '3', '640100', '宁夏回族自治区 银川市 金凤区');
INSERT INTO dv_area_base VALUES ('640121', '永宁县', '', '', '', '3', '640100', '宁夏回族自治区 银川市 永宁县');
INSERT INTO dv_area_base VALUES ('640122', '贺兰县', '', '', '', '3', '640100', '宁夏回族自治区 银川市 贺兰县');
INSERT INTO dv_area_base VALUES ('640181', '灵武市', '', '', '', '3', '640100', '宁夏回族自治区 银川市 灵武市');
INSERT INTO dv_area_base VALUES ('640200', '石嘴山市', '石嘴山', '', '', '2', '640000', '宁夏回族自治区 石嘴山市');
INSERT INTO dv_area_base VALUES ('640202', '大武口区', '', '', '', '3', '640200', '宁夏回族自治区 石嘴山市 大武口区');
INSERT INTO dv_area_base VALUES ('640205', '惠农区', '', '', '', '3', '640200', '宁夏回族自治区 石嘴山市 惠农区');
INSERT INTO dv_area_base VALUES ('640221', '平罗县', '', '', '', '3', '640200', '宁夏回族自治区 石嘴山市 平罗县');
INSERT INTO dv_area_base VALUES ('640300', '吴忠市', '吴忠', '', '', '2', '640000', '宁夏回族自治区 吴忠市');
INSERT INTO dv_area_base VALUES ('640302', '利通区', '', '', '', '3', '640300', '宁夏回族自治区 吴忠市 利通区');
INSERT INTO dv_area_base VALUES ('640303', '红寺堡区', '', '', '', '3', '640300', '宁夏回族自治区 吴忠市 红寺堡区');
INSERT INTO dv_area_base VALUES ('640323', '盐池县', '', '', '', '3', '640300', '宁夏回族自治区 吴忠市 盐池县');
INSERT INTO dv_area_base VALUES ('640324', '同心县', '', '', '', '3', '640300', '宁夏回族自治区 吴忠市 同心县');
INSERT INTO dv_area_base VALUES ('640381', '青铜峡市', '', '', '', '3', '640300', '宁夏回族自治区 吴忠市 青铜峡市');
INSERT INTO dv_area_base VALUES ('640400', '固原市', '固原', '', '', '2', '640000', '宁夏回族自治区 固原市');
INSERT INTO dv_area_base VALUES ('640402', '原州区', '', '', '', '3', '640400', '宁夏回族自治区 固原市 原州区');
INSERT INTO dv_area_base VALUES ('640422', '西吉县', '', '', '', '3', '640400', '宁夏回族自治区 固原市 西吉县');
INSERT INTO dv_area_base VALUES ('640423', '隆德县', '', '', '', '3', '640400', '宁夏回族自治区 固原市 隆德县');
INSERT INTO dv_area_base VALUES ('640424', '泾源县', '', '', '', '3', '640400', '宁夏回族自治区 固原市 泾源县');
INSERT INTO dv_area_base VALUES ('640425', '彭阳县', '', '', '', '3', '640400', '宁夏回族自治区 固原市 彭阳县');
INSERT INTO dv_area_base VALUES ('640500', '中卫市', '中卫', '', '', '2', '640000', '宁夏回族自治区 中卫市');
INSERT INTO dv_area_base VALUES ('640502', '沙坡头区', '', '', '', '3', '640500', '宁夏回族自治区 中卫市 沙坡头区');
INSERT INTO dv_area_base VALUES ('640521', '中宁县', '', '', '', '3', '640500', '宁夏回族自治区 中卫市 中宁县');
INSERT INTO dv_area_base VALUES ('640522', '海原县', '', '', '', '3', '640500', '宁夏回族自治区 中卫市 海原县');
INSERT INTO dv_area_base VALUES ('650000', '新疆维吾尔自治区', '新疆', 'xinjiang', '新', '1', '0', '新疆维吾尔自治区');
INSERT INTO dv_area_base VALUES ('650100', '乌鲁木齐市', '乌鲁木齐', '', '', '2', '650000', '新疆维吾尔自治区 乌鲁木齐市');
INSERT INTO dv_area_base VALUES ('650102', '天山区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 天山区');
INSERT INTO dv_area_base VALUES ('650103', '沙依巴克区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 沙依巴克区');
INSERT INTO dv_area_base VALUES ('650104', '新市区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 新市区');
INSERT INTO dv_area_base VALUES ('650105', '水磨沟区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 水磨沟区');
INSERT INTO dv_area_base VALUES ('650106', '头屯河区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 头屯河区');
INSERT INTO dv_area_base VALUES ('650107', '达坂城区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 达坂城区');
INSERT INTO dv_area_base VALUES ('650109', '米东区', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 米东区');
INSERT INTO dv_area_base VALUES ('650121', '乌鲁木齐县', '', '', '', '3', '650100', '新疆维吾尔族自治区 乌鲁木齐市 乌鲁木齐县');
INSERT INTO dv_area_base VALUES ('650200', '克拉玛依市', '克拉玛依', '', '', '2', '650000', '新疆维吾尔自治区 克拉玛依市');
INSERT INTO dv_area_base VALUES ('650202', '独山子区', '', '', '', '3', '650200', '新疆维吾尔族自治区 克拉玛依市 独山子区');
INSERT INTO dv_area_base VALUES ('650203', '克拉玛依区', '', '', '', '3', '650200', '新疆维吾尔族自治区 克拉玛依市 克拉玛依区');
INSERT INTO dv_area_base VALUES ('650204', '白碱滩区', '', '', '', '3', '650200', '新疆维吾尔族自治区 克拉玛依市 白碱滩区');
INSERT INTO dv_area_base VALUES ('650205', '乌尔禾区', '', '', '', '3', '650200', '新疆维吾尔族自治区 克拉玛依市 乌尔禾区');
INSERT INTO dv_area_base VALUES ('650400', '吐鲁番市', '吐鲁番', '', '', '2', '650000', '新疆维吾尔自治区 吐鲁番市');
INSERT INTO dv_area_base VALUES ('652101', '吐鲁番市', '', '', '', '3', '652100', '新疆维吾尔族自治区 吐鲁番地区 吐鲁番市');
INSERT INTO dv_area_base VALUES ('652122', '鄯善县', '', '', '', '3', '652100', '新疆维吾尔族自治区 吐鲁番地区 鄯善县');
INSERT INTO dv_area_base VALUES ('652123', '托克逊县', '', '', '', '3', '652100', '新疆维吾尔族自治区 吐鲁番地区 托克逊县');
INSERT INTO dv_area_base VALUES ('652200', '哈密地区', '哈密', '', '', '2', '650000', '新疆维吾尔自治区 哈密地区');
INSERT INTO dv_area_base VALUES ('652201', '哈密市', '', '', '', '3', '652200', '新疆维吾尔族自治区 哈密地区 哈密市');
INSERT INTO dv_area_base VALUES ('652222', '巴里坤哈萨克自治县', '', '', '', '3', '652200', '新疆维吾尔族自治区 哈密地区 巴里坤哈萨克自治县');
INSERT INTO dv_area_base VALUES ('652223', '伊吾县', '', '', '', '3', '652200', '新疆维吾尔族自治区 哈密地区 伊吾县');
INSERT INTO dv_area_base VALUES ('652300', '昌吉回族自治州', '昌吉', '', '', '2', '650000', '新疆维吾尔自治区 昌吉回族自治州');
INSERT INTO dv_area_base VALUES ('652301', '昌吉市', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 昌吉市');
INSERT INTO dv_area_base VALUES ('652302', '阜康市', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 阜康市');
INSERT INTO dv_area_base VALUES ('652323', '呼图壁县', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 呼图壁县');
INSERT INTO dv_area_base VALUES ('652324', '玛纳斯县', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 玛纳斯县');
INSERT INTO dv_area_base VALUES ('652325', '奇台县', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 奇台县');
INSERT INTO dv_area_base VALUES ('652327', '吉木萨尔县', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 吉木萨尔县');
INSERT INTO dv_area_base VALUES ('652328', '木垒哈萨克自治县', '', '', '', '3', '652300', '新疆维吾尔族自治区 昌吉回族自治州 木垒哈萨克自治县');
INSERT INTO dv_area_base VALUES ('652700', '博尔塔拉蒙古自治州', '博尔塔拉', '', '', '2', '650000', '新疆维吾尔自治区 博尔塔拉蒙古自治州');
INSERT INTO dv_area_base VALUES ('652701', '博乐市', '', '', '', '3', '652700', '新疆维吾尔族自治区 博尔塔拉蒙古自治州 博乐市');
INSERT INTO dv_area_base VALUES ('652722', '精河县', '', '', '', '3', '652700', '新疆维吾尔族自治区 博尔塔拉蒙古自治州 精河县');
INSERT INTO dv_area_base VALUES ('652723', '温泉县', '', '', '', '3', '652700', '新疆维吾尔族自治区 博尔塔拉蒙古自治州 温泉县');
INSERT INTO dv_area_base VALUES ('652800', '巴音郭楞蒙古自治州', '巴音郭楞', '', '', '2', '650000', '新疆维吾尔自治区 巴音郭楞蒙古自治州');
INSERT INTO dv_area_base VALUES ('652801', '库尔勒市', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 库尔勒市');
INSERT INTO dv_area_base VALUES ('652822', '轮台县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 轮台县');
INSERT INTO dv_area_base VALUES ('652823', '尉犁县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 尉犁县');
INSERT INTO dv_area_base VALUES ('652824', '若羌县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 若羌县');
INSERT INTO dv_area_base VALUES ('652825', '且末县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 且末县');
INSERT INTO dv_area_base VALUES ('652826', '焉耆回族自治县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 焉耆回族自治县');
INSERT INTO dv_area_base VALUES ('652827', '和静县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 和静县');
INSERT INTO dv_area_base VALUES ('652828', '和硕县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 和硕县');
INSERT INTO dv_area_base VALUES ('652829', '博湖县', '', '', '', '3', '652800', '新疆维吾尔族自治区 巴音郭楞蒙古自治州 博湖县');
INSERT INTO dv_area_base VALUES ('652900', '阿克苏地区', '阿克苏', '', '', '2', '650000', '新疆维吾尔自治区 阿克苏地区');
INSERT INTO dv_area_base VALUES ('652901', '阿克苏市', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 阿克苏市');
INSERT INTO dv_area_base VALUES ('652922', '温宿县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 温宿县');
INSERT INTO dv_area_base VALUES ('652923', '库车县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 库车县');
INSERT INTO dv_area_base VALUES ('652924', '沙雅县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 沙雅县');
INSERT INTO dv_area_base VALUES ('652925', '新和县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 新和县');
INSERT INTO dv_area_base VALUES ('652926', '拜城县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 拜城县');
INSERT INTO dv_area_base VALUES ('652927', '乌什县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 乌什县');
INSERT INTO dv_area_base VALUES ('652928', '阿瓦提县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 阿瓦提县');
INSERT INTO dv_area_base VALUES ('652929', '柯坪县', '', '', '', '3', '652900', '新疆维吾尔族自治区 阿克苏地区 柯坪县');
INSERT INTO dv_area_base VALUES ('653000', '克孜勒苏柯尔克孜自治州', '克孜勒苏柯尔克孜', '', '', '2', '650000', '新疆维吾尔自治区 克孜勒苏柯尔克孜自治州');
INSERT INTO dv_area_base VALUES ('653001', '阿图什市', '', '', '', '3', '653000', '新疆维吾尔族自治区 克孜勒苏柯尔克孜自治州 阿图什市');
INSERT INTO dv_area_base VALUES ('653022', '阿克陶县', '', '', '', '3', '653000', '新疆维吾尔族自治区 克孜勒苏柯尔克孜自治州 阿克陶县');
INSERT INTO dv_area_base VALUES ('653023', '阿合奇县', '', '', '', '3', '653000', '新疆维吾尔族自治区 克孜勒苏柯尔克孜自治州 阿合奇县');
INSERT INTO dv_area_base VALUES ('653024', '乌恰县', '', '', '', '3', '653000', '新疆维吾尔族自治区 克孜勒苏柯尔克孜自治州 乌恰县');
INSERT INTO dv_area_base VALUES ('653100', '喀什地区', '喀什', '', '', '2', '650000', '新疆维吾尔自治区 喀什地区');
INSERT INTO dv_area_base VALUES ('653101', '喀什市', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 喀什市');
INSERT INTO dv_area_base VALUES ('653121', '疏附县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 疏附县');
INSERT INTO dv_area_base VALUES ('653122', '疏勒县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 疏勒县');
INSERT INTO dv_area_base VALUES ('653123', '英吉沙县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 英吉沙县');
INSERT INTO dv_area_base VALUES ('653124', '泽普县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 泽普县');
INSERT INTO dv_area_base VALUES ('653125', '莎车县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 莎车县');
INSERT INTO dv_area_base VALUES ('653126', '叶城县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 叶城县');
INSERT INTO dv_area_base VALUES ('653127', '麦盖提县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 麦盖提县');
INSERT INTO dv_area_base VALUES ('653128', '岳普湖县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 岳普湖县');
INSERT INTO dv_area_base VALUES ('653129', '伽师县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 伽师县');
INSERT INTO dv_area_base VALUES ('653130', '巴楚县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 巴楚县');
INSERT INTO dv_area_base VALUES ('653131', '塔什库尔干塔吉克自治县', '', '', '', '3', '653100', '新疆维吾尔族自治区 喀什地区 塔什库尔干塔吉克自治县');
INSERT INTO dv_area_base VALUES ('653200', '和田地区', '和田', '', '', '2', '650000', '新疆维吾尔自治区 和田地区');
INSERT INTO dv_area_base VALUES ('653201', '和田市', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 和田市');
INSERT INTO dv_area_base VALUES ('653221', '和田县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 和田县');
INSERT INTO dv_area_base VALUES ('653222', '墨玉县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 墨玉县');
INSERT INTO dv_area_base VALUES ('653223', '皮山县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 皮山县');
INSERT INTO dv_area_base VALUES ('653224', '洛浦县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 洛浦县');
INSERT INTO dv_area_base VALUES ('653225', '策勒县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 策勒县');
INSERT INTO dv_area_base VALUES ('653226', '于田县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 于田县');
INSERT INTO dv_area_base VALUES ('653227', '民丰县', '', '', '', '3', '653200', '新疆维吾尔族自治区 和田地区 民丰县');
INSERT INTO dv_area_base VALUES ('654000', '伊犁哈萨克自治州', '伊犁哈萨克', '', '', '2', '650000', '新疆维吾尔自治区 伊犁哈萨克自治州');
INSERT INTO dv_area_base VALUES ('654002', '伊宁市', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 伊宁市');
INSERT INTO dv_area_base VALUES ('654003', '奎屯市', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 奎屯市');
INSERT INTO dv_area_base VALUES ('654021', '伊宁县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 伊宁县');
INSERT INTO dv_area_base VALUES ('654022', '察布查尔锡伯自治县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 察布查尔锡伯自治县');
INSERT INTO dv_area_base VALUES ('654023', '霍城县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 霍城县');
INSERT INTO dv_area_base VALUES ('654024', '巩留县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 巩留县');
INSERT INTO dv_area_base VALUES ('654025', '新源县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 新源县');
INSERT INTO dv_area_base VALUES ('654026', '昭苏县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 昭苏县');
INSERT INTO dv_area_base VALUES ('654027', '特克斯县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 特克斯县');
INSERT INTO dv_area_base VALUES ('654028', '尼勒克县', '', '', '', '3', '654000', '新疆维吾尔族自治区 伊犁哈萨克自治州 尼勒克县');
INSERT INTO dv_area_base VALUES ('654200', '塔城地区', '塔城', '', '', '2', '650000', '新疆维吾尔自治区 塔城地区');
INSERT INTO dv_area_base VALUES ('654201', '塔城市', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 塔城市');
INSERT INTO dv_area_base VALUES ('654202', '乌苏市', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 乌苏市');
INSERT INTO dv_area_base VALUES ('654221', '额敏县', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 额敏县');
INSERT INTO dv_area_base VALUES ('654223', '沙湾县', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 沙湾县');
INSERT INTO dv_area_base VALUES ('654224', '托里县', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 托里县');
INSERT INTO dv_area_base VALUES ('654225', '裕民县', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 裕民县');
INSERT INTO dv_area_base VALUES ('654226', '和布克赛尔蒙古自治县', '', '', '', '3', '654200', '新疆维吾尔族自治区 塔城地区 和布克赛尔蒙古自治县');
INSERT INTO dv_area_base VALUES ('654300', '阿勒泰地区', '阿勒泰', '', '', '2', '650000', '新疆维吾尔自治区 阿勒泰地区');
INSERT INTO dv_area_base VALUES ('654301', '阿勒泰市', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 阿勒泰市');
INSERT INTO dv_area_base VALUES ('654321', '布尔津县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 布尔津县');
INSERT INTO dv_area_base VALUES ('654322', '富蕴县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 富蕴县');
INSERT INTO dv_area_base VALUES ('654323', '福海县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 福海县');
INSERT INTO dv_area_base VALUES ('654324', '哈巴河县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 哈巴河县');
INSERT INTO dv_area_base VALUES ('654325', '青河县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 青河县');
INSERT INTO dv_area_base VALUES ('654326', '吉木乃县', '', '', '', '3', '654300', '新疆维吾尔族自治区 阿勒泰地区 吉木乃县');
INSERT INTO dv_area_base VALUES ('659001', '石河子市', '石河子', '', '', '2', '650000', '新疆维吾尔自治区 石河子市');
INSERT INTO dv_area_base VALUES ('659002', '阿拉尔市', '阿拉尔', '', '', '2', '650000', '新疆维吾尔自治区 阿拉尔市');
INSERT INTO dv_area_base VALUES ('659003', '图木舒克市', '图木舒', '', '', '2', '650000', '新疆维吾尔自治区 图木舒克市');
INSERT INTO dv_area_base VALUES ('659004', '五家渠市', '五家渠', '', '', '2', '650000', '新疆维吾尔自治区 五家渠市');
INSERT INTO dv_area_base VALUES ('659005', '北屯市', '北屯', '', '', '2', '650000', '新疆维吾尔自治区 北屯市');
INSERT INTO dv_area_base VALUES ('659006', '铁门关市', '铁门关', '', '', '2', '650000', '新疆维吾尔自治区 铁门关市');
INSERT INTO dv_area_base VALUES ('659007', '双河市', '双河', '', '', '2', '650000', '新疆维吾尔自治区 双河市');
INSERT INTO dv_area_base VALUES ('710000', '台湾省', '台湾', 'taiwan', '台', '1', '0', '台湾省');
INSERT INTO dv_area_base VALUES ('710100', '高雄市', '', '', '', '2', '710000', '台湾省 高雄市');
INSERT INTO dv_area_base VALUES ('710200', '屏东县', '', '', '', '2', '710000', '台湾省 屏东县');
INSERT INTO dv_area_base VALUES ('710300', '台南市', '', '', '', '2', '710000', '台湾省 台南市');
INSERT INTO dv_area_base VALUES ('710400', '新竹市', '', '', '', '2', '710000', '台湾省 新竹市');
INSERT INTO dv_area_base VALUES ('710500', '新竹县', '', '', '', '2', '710000', '台湾省 新竹县');
INSERT INTO dv_area_base VALUES ('710600', '宜兰县', '', '', '', '2', '710000', '台湾省 宜兰县');
INSERT INTO dv_area_base VALUES ('710700', '基隆市', '', '', '', '2', '710000', '台湾省 基隆市');
INSERT INTO dv_area_base VALUES ('710800', '苗栗县', '', '', '', '2', '710000', '台湾省 苗栗县');
INSERT INTO dv_area_base VALUES ('710900', '台北市', '', '', '', '2', '710000', '台湾省 台北市');
INSERT INTO dv_area_base VALUES ('711000', '新北市', '', '', '', '2', '710000', '台湾省 新北市');
INSERT INTO dv_area_base VALUES ('711100', '桃园县', '', '', '', '2', '710000', '台湾省 桃园县');
INSERT INTO dv_area_base VALUES ('711200', '彰化县', '', '', '', '2', '710000', '台湾省 彰化县');
INSERT INTO dv_area_base VALUES ('711300', '嘉义县', '', '', '', '2', '710000', '台湾省 嘉义县');
INSERT INTO dv_area_base VALUES ('711400', '嘉义市', '', '', '', '2', '710000', '台湾省 嘉义市');
INSERT INTO dv_area_base VALUES ('711500', '花莲县', '', '', '', '2', '710000', '台湾省 花莲县');
INSERT INTO dv_area_base VALUES ('711600', '南投县', '', '', '', '2', '710000', '台湾省 南投县');
INSERT INTO dv_area_base VALUES ('711700', '台中市', '', '', '', '2', '710000', '台湾省 台中市');
INSERT INTO dv_area_base VALUES ('711800', '云林县', '', '', '', '2', '710000', '台湾省 云林县');
INSERT INTO dv_area_base VALUES ('711900', '台东县', '', '', '', '2', '710000', '台湾省 台东县');
INSERT INTO dv_area_base VALUES ('712000', '澎湖县', '', '', '', '2', '710000', '台湾省 澎湖县');
INSERT INTO dv_area_base VALUES ('712100', '金门', '', '', '', '2', '710000', '台湾省 金门');
INSERT INTO dv_area_base VALUES ('712200', '马祖', '', '', '', '2', '710000', '台湾省 马祖');
INSERT INTO dv_area_base VALUES ('810000', '香港特别行政区', '香港', 'xianggang', '港', '1', '0', '香港特别行政区');
INSERT INTO dv_area_base VALUES ('810100', '中西区', '', '', '', '2', '810000', '香港特别行政区 中西区');
INSERT INTO dv_area_base VALUES ('810200', '东区', '', '', '', '2', '810000', '香港特别行政区 东区');
INSERT INTO dv_area_base VALUES ('810300', '南区', '', '', '', '2', '810000', '香港特别行政区 南区');
INSERT INTO dv_area_base VALUES ('810400', '湾仔区', '', '', '', '2', '810000', '香港特别行政区 湾仔区');
INSERT INTO dv_area_base VALUES ('810500', '九龙区', '', '', '', '2', '810000', '香港特别行政区 九龙区');
INSERT INTO dv_area_base VALUES ('810600', '观塘区', '', '', '', '2', '810000', '香港特别行政区 观塘区');
INSERT INTO dv_area_base VALUES ('810700', '深水埗区', '', '', '', '2', '810000', '香港特别行政区 深水埗区');
INSERT INTO dv_area_base VALUES ('810800', '黄大仙区', '', '', '', '2', '810000', '香港特别行政区 黄大仙区');
INSERT INTO dv_area_base VALUES ('810900', '油尖旺区', '', '', '', '2', '810000', '香港特别行政区 油尖旺区');
INSERT INTO dv_area_base VALUES ('811000', '离岛区', '', '', '', '2', '810000', '香港特别行政区 离岛区');
INSERT INTO dv_area_base VALUES ('811100', '葵青区', '', '', '', '2', '810000', '香港特别行政区 葵青区');
INSERT INTO dv_area_base VALUES ('811200', '北区', '', '', '', '2', '810000', '香港特别行政区 北区');
INSERT INTO dv_area_base VALUES ('811300', '西贡区', '', '', '', '2', '810000', '香港特别行政区 西贡区');
INSERT INTO dv_area_base VALUES ('811400', '沙田区', '', '', '', '2', '810000', '香港特别行政区 沙田区');
INSERT INTO dv_area_base VALUES ('811500', '大埔区', '', '', '', '2', '810000', '香港特别行政区 大埔区');
INSERT INTO dv_area_base VALUES ('811600', '荃湾区', '', '', '', '2', '810000', '香港特别行政区 荃湾区');
INSERT INTO dv_area_base VALUES ('811700', '屯门区', '', '', '', '2', '810000', '香港特别行政区 屯门区 ');
INSERT INTO dv_area_base VALUES ('811800', '元朗区', '', '', '', '2', '810000', '香港特别行政区 元朗区');
INSERT INTO dv_area_base VALUES ('820000', '澳门特别行政区', '澳门', 'aomen', '澳', '1', '0', '澳门特别行政区');
INSERT INTO dv_area_base VALUES ('820100', '花地玛堂区', '', '', '', '2', '820000', '澳门特别行政区 花地玛堂区');
INSERT INTO dv_area_base VALUES ('820200', '花王堂区', '', '', '', '2', '820000', '澳门特别行政区 花王堂区');
INSERT INTO dv_area_base VALUES ('820300', '望德堂区', '', '', '', '2', '820000', '澳门特别行政区 望德堂区');
INSERT INTO dv_area_base VALUES ('820400', '风顺堂区', '', '', '', '2', '820000', '澳门特别行政区 风顺堂区');
INSERT INTO dv_area_base VALUES ('820500', '大堂区', '', '', '', '2', '820000', '澳门特别行政区 大堂区');
INSERT INTO dv_area_base VALUES ('820600', '嘉模堂区', '', '', '', '2', '820000', '澳门特别行政区 嘉模堂区');
INSERT INTO dv_area_base VALUES ('820700', '圣方济各堂区', '', '', '', '2', '820000', '澳门特别行政区 圣方济各堂区');
INSERT INTO dv_area_base VALUES ('820800', '路氹填海区', '', '', '', '2', '820000', '澳门特别行政区 路氹填海区');

INSERT INTO dv_graphiccategory VALUES ('0', '表格');
INSERT INTO dv_graphiccategory VALUES ('1', '条形图');
INSERT INTO dv_graphiccategory VALUES ('10', '仪表盘');
INSERT INTO dv_graphiccategory VALUES ('11', '地理图');
INSERT INTO dv_graphiccategory VALUES ('12', '流图');
INSERT INTO dv_graphiccategory VALUES ('13', '百分比');
INSERT INTO dv_graphiccategory VALUES ('14', '3D图');
INSERT INTO dv_graphiccategory VALUES ('15', '其他图');
INSERT INTO dv_graphiccategory VALUES ('2', '折线图');
INSERT INTO dv_graphiccategory VALUES ('3', '饼图');
INSERT INTO dv_graphiccategory VALUES ('4', '散点图');
INSERT INTO dv_graphiccategory VALUES ('5', '地图');
INSERT INTO dv_graphiccategory VALUES ('6', '热力图');
INSERT INTO dv_graphiccategory VALUES ('7', '日历图');
INSERT INTO dv_graphiccategory VALUES ('8', '关系图');
INSERT INTO dv_graphiccategory VALUES ('9', '标签云');

INSERT INTO dv_graphiccomponent VALUES ('1', '1.0', '双向柱图', '1', '0', '1.0', '0', null, null, '/comp/json/1/1_0/meta.json', '/comp/json/1/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/1/1_0/bar1.png', null, '1', '0', '1', '0', '108');
INSERT INTO dv_graphiccomponent VALUES ('2', '1_0', '多系列条形图', '1', '0', '1.0', '0', null, null, '/comp/json/2/1_0/meta.json', '/comp/json/2/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/2/1_0/bar.png', null, '2', '0', '2', '0', '105');
INSERT INTO dv_graphiccomponent VALUES ('3', '1.0', '多系列柱图', '1', '0', '1.0', '0', null, null, '/comp/json/3/1_0/meta.json', '/comp/json/3/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/3/1_0/bar.png', null, '3', '0', '3', '0', '103');
INSERT INTO dv_graphiccomponent VALUES ('4', '1.0', '滚动多系列柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/4/1_0/meta.json', '/comp/json/4/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/4/1_0/bar.png', null, '4', '0', '4', '0', '104');
INSERT INTO dv_graphiccomponent VALUES ('5', '1.0', '柱线图', '1', '0', '1.0', '0', null, null, '/comp/json/5/1_0/meta.json', '/comp/json/5/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/0/1_0/bar1.png', null, '5', '1', '5', '1', '112');
INSERT INTO dv_graphiccomponent VALUES ('6', '1.0', '弦图', '8', '0', '1.0', '0', null, null, '/comp/json/6/1_0/meta.json', '/comp/json/6/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/6/1_0/chord.png', null, '6', '0', '6', '0', '1304');
INSERT INTO dv_graphiccomponent VALUES ('7', '1.0', '南丁格尔图', '3', '0', '1.0', '0', null, null, '/comp/json/7/1_0/meta.json', '/comp/json/7/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/7/1_0/pie.png', null, '7', '0', '7', '0', '301');
INSERT INTO dv_graphiccomponent VALUES ('8', '1.0', '表格', '0', '1', '1.0', '0', null, null, '/comp/json/8/1_0/meta.json', '/comp/json/8/1_0/simple.json', '{"string":"n", "number": "n","map": "0","time":"0"}', '/comp/icon/8/1_0/grid.png', null, '8', '0', '8', '0', '1');
INSERT INTO dv_graphiccomponent VALUES ('9', '1.0', '极坐标系图', '2', '0', '1.0', '0', null, null, '/comp/json/9/1_0/meta.json', '/comp/json/9/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/9/1_0/line.png', null, '9', '1', '9', '1', '208');
INSERT INTO dv_graphiccomponent VALUES ('10', '1.0', '堆积面积图', '2', '0', '1.0', '0', null, null, '/comp/json/10/1_0/meta.json', '/comp/json/10/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/10/1_0/line.png', null, '10', '0', '10', '0', '203');
INSERT INTO dv_graphiccomponent VALUES ('11', '1.0', '堆积折线图', '2', '0', '1.0', '0', null, null, '/comp/json/11/1_0/meta.json', '/comp/json/11/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/11/1_0/line.png', null, '11', '0', '11', '0', '200');
INSERT INTO dv_graphiccomponent VALUES ('12', '1.0', '纵向折线图', '2', '0', '1.0', '0', null, null, '/comp/json/12/1_0/meta.json', '/comp/json/12/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/12/1_0/line.png', null, '12', '0', '12', '0', '206');
INSERT INTO dv_graphiccomponent VALUES ('13', '1.0', '滚动折线图', '2', '0', '1.0', '0', null, null, '/comp/json/13/1_0/meta.json', '/comp/json/13/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/13/1_0/line.png', null, '13', '0', '13', '0', '202');
INSERT INTO dv_graphiccomponent VALUES ('14', '1.0', '传统折线图', '2', '0', '1.0', '0', null, null, '/comp/json/14/1_0/meta.json', '/comp/json/14/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/14/1_0/line.png', null, '14', '0', '14', '0', '201');
INSERT INTO dv_graphiccomponent VALUES ('15', '1.0', '传统面积图', '2', '0', '1.0', '0', null, null, '/comp/json/15/1_0/meta.json', '/comp/json/15/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/15/1_0/line.png', null, '15', '0', '15', '0', '204');
INSERT INTO dv_graphiccomponent VALUES ('16', '1.0', '滚动面积图', '2', '0', '1.0', '0', null, null, '/comp/json/16/1_0/meta.json', '/comp/json/16/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/16/1_0/line.png', null, '16', '0', '16', '0', '205');
INSERT INTO dv_graphiccomponent VALUES ('17', '1.0', '阶梯折线图', '2', '0', '1.0', '0', null, null, '/comp/json/17/1_0/meta.json', '/comp/json/17/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/17/1_0/line.png', null, '17', '0', '17', '0', '207');
INSERT INTO dv_graphiccomponent VALUES ('18', '1.0', '数据点折线图', '2', '0', '1.0', '0', null, null, '/comp/json/18/1_0/meta.json', '/comp/json/18/1_0/simple.json', '{"string":"0", "number": "m","map": "0","time":"0"}', '/comp/icon/18/1_0/line.png', null, '18', '1', '18', '1', '209');
INSERT INTO dv_graphiccomponent VALUES ('19', '1.0', '数据点阶梯面积图', '2', '0', '1.0', '0', null, null, '/comp/json/19/1_0/meta.json', '/comp/json/19/1_0/simple.json', '{"string":"0", "number": "m","map": "0","time":"0"}', '/comp/icon/19/1_0/line.png', null, '19', '1', '19', '1', '210');
INSERT INTO dv_graphiccomponent VALUES ('20', '1.0', '数据点面积图', '2', '0', '1.0', '0', null, null, '/comp/json/20/1_0/meta.json', '/comp/json/20/1_0/simple.json', '{"string":"0", "number": "m","map": "0","time":"0"}', '/comp/icon/20/1_0/line.png', null, '20', '1', '20', '1', '211');
INSERT INTO dv_graphiccomponent VALUES ('21', '1.0', '时序图', '2', '0', '1.0', '0', null, null, '/comp/json/21/1_0/meta.json', '/comp/json/21/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"1"}', '/comp/icon/21/1_0/line.png', null, '21', '1', '21', '1', '212');
INSERT INTO dv_graphiccomponent VALUES ('22', '1.0', '堆积柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/22/1_0/meta.json', '/comp/json/22/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/22/1_0/bar.png', null, '22', '0', '22', '0', '100');
INSERT INTO dv_graphiccomponent VALUES ('23', '1.0', '滚动堆积柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/23/1_0/meta.json', '/comp/json/23/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/23/1_0/bar.png', null, '23', '0', '23', '0', '101');
INSERT INTO dv_graphiccomponent VALUES ('24', '1.0', '堆积柱线图', '1', '0', '1.0', '0', null, null, '/comp/json/24/1_0/meta.json', '/comp/json/24/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/24/1_0/bar.png', null, '24', '1', '24', '1', '113');
INSERT INTO dv_graphiccomponent VALUES ('25', '1.0', '系列柱线图', '1', '0', '1.0', '0', null, null, '/comp/json/25/1_0/meta.json', '/comp/json/25/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/25/1_0/bar.png', null, '25', '0', '25', '0', '107');
INSERT INTO dv_graphiccomponent VALUES ('26', '1.0', '数据点柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/26/1_0/meta.json', '/comp/json/26/1_0/simple.json', '{"string":"0", "number": "n","map": "0","time":"0"}', '/comp/icon/26/1_0/bar.png', null, '26', '1', '26', '1', '114');
INSERT INTO dv_graphiccomponent VALUES ('27', '1.0', '横向箱线图', '15', '0', '1.0', '0', null, null, '/comp/json/27/1_0/meta.json', '/comp/json/27/1_0/simple.json', '{"string":"2", "number": "n","map": "0","time":"0"}', '/comp/icon/27/1_0/boxplot.png', null, '27', '0', '27', '0', '1504');
INSERT INTO dv_graphiccomponent VALUES ('28', '1.0', '纵向箱线图', '15', '0', '1.0', '0', null, null, '/comp/json/28/1_0/meta.json', '/comp/json/28/1_0/simple.json', '{"string":"2", "number": "n","map": "0","time":"0"}', '/comp/icon/28/1_0/boxplot.png', null, '28', '0', '28', '1', '1507');
INSERT INTO dv_graphiccomponent VALUES ('29', '1.0', '日历图', '7', '1', '1.0', '0', null, null, '/comp/json/29/1_0/meta.json', '/comp/json/29/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/29/1_0/calendar.png', null, '29', '0', '29', '1', '1300');
INSERT INTO dv_graphiccomponent VALUES ('30', '1.0', 'K线图', '15', '0', '1.0', '0', null, null, '/comp/json/30/1_0/meta.json', '/comp/json/30/1_0/simple.json', '{"string":"1", "number": "4","map": "0","time":"0"}', '/comp/icon/30/1_0/candlestick.png', null, '30', '0', '30', '0', '1505');
INSERT INTO dv_graphiccomponent VALUES ('31', '1.0', '圆点图', '4', '0', '1.0', '0', null, null, '/comp/json/31/1_0/meta.json', '/comp/json/31/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/31/1_0/dot.png', null, '31', '0', '31', '0', '400');
INSERT INTO dv_graphiccomponent VALUES ('32', '1.0', '双线图', '15', '0', '1.0', '0', null, null, '/comp/json/32/1_0/meta.json', '/comp/json/32/1_0/simple.json', '{"string":"2", "number": "2","map": "0","time":"0"}', '/comp/icon/32/1_0/doubleline.png', null, '32', '0', '32', '0', '1506');
INSERT INTO dv_graphiccomponent VALUES ('33', '1.0', '漏斗图', '15', '0', '1.0', '0', null, null, '/comp/json/33/1_0/meta.json', '/comp/json/33/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/33/1_0/funnel.png', null, '33', '0', '33', '0', '1502');
INSERT INTO dv_graphiccomponent VALUES ('34', '1.0', '关系图', '8', '0', '1.0', '0', null, null, '/comp/json/34/1_0/meta.json', '/comp/json/34/1_0/simple.json', '{"string":"4", "number": "1","map": "0","time":"0"}', '/comp/icon/34/1_0/graph.png', null, '34', '0', '34', '0', '1300');
INSERT INTO dv_graphiccomponent VALUES ('35', '1.0', '仪表盘', '10', '0', '1.0', '0', null, null, '/comp/json/35/1_0/meta.json', '/comp/json/35/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/35/1_0/gauge.png', null, '35', '0', '35', '0', '901');
INSERT INTO dv_graphiccomponent VALUES ('36', '1.0', '热力图', '6', '1', '1.0', '0', null, null, '/comp/json/36/1_0/meta.json', '/comp/json/36/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/36/1_0/heatmap.png', null, '36', '0', '36', '0', '1100');
INSERT INTO dv_graphiccomponent VALUES ('37', '1.0', '热力地图', '5', '1', '1.0', '0', null, null, '/comp/json/37/1_0/meta.json', '/comp/json/37/1_0/simple.json', '{"string":"0", "number": "1","map": "1","time":"0"}', '/comp/icon/37/1_0/heatmap.png', null, '37', '0', '37', '0', '504');
INSERT INTO dv_graphiccomponent VALUES ('38', '1.0', '航线图', '5', '0', '1.0', '0', null, null, '/comp/json/38/1_0/meta.json', '/comp/json/38/1_0/simple.json', '{"string":"0", "number": "1","map": "2","time":"0"}', '/comp/icon/38/1_0/lines.png', null, '38', '0', '38', '0', '503');
INSERT INTO dv_graphiccomponent VALUES ('39', '1.0', '地图', '5', '1', '1.0', '0', null, null, '/comp/json/39/1_0/meta.json', '/comp/json/39/1_0/simple.json', '{"string":"0", "number": "1","map": "1","time":"0"}', '/comp/icon/39/1_0/map.png', null, '39', '0', '39', '0', '500');
INSERT INTO dv_graphiccomponent VALUES ('40', '1.0', '平行线图', '15', '1', '1.0', '0', null, null, '/comp/json/40/1_0/meta.json', '/comp/json/40/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/40/1_0/parallel.png', null, '40', '0', '40', '0', '1503');
INSERT INTO dv_graphiccomponent VALUES ('41', '1.0', '环形图', '3', '0', '1.0', '0', null, null, '/comp/json/41/1_0/meta.json', '/comp/json/41/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/41/1_0/pie.png', null, '41', '0', '41', '0', '302');
INSERT INTO dv_graphiccomponent VALUES ('42', '1.0', '饼图', '3', '0', '1.0', '0', null, null, '/comp/json/42/1_0/meta.json', '/comp/json/42/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/42/1_0/pie.png', null, '42', '0', '42', '0', '300');
INSERT INTO dv_graphiccomponent VALUES ('43', '1.0', '复合饼图', '3', '0', '1.0', '0', null, null, '/comp/json/43/1_0/meta.json', '/comp/json/43/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/43/1_0/pie.png', null, '43', '1', '43', '1', '305');
INSERT INTO dv_graphiccomponent VALUES ('44', '1.0', '兴趣图', '15', '0', '1.0', '0', null, null, '/comp/json/44/1_0/meta.json', '/comp/json/44/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/44/1_0/pinterest.png', null, '44', '1', '44', '0', '1501');
INSERT INTO dv_graphiccomponent VALUES ('45', '1.0', '雷达图', '15', '0', '1.0', '0', null, null, '/comp/json/45/1_0/meta.json', '/comp/json/45/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/45/1_0/radar.png', null, '45', '0', '45', '0', '1500');
INSERT INTO dv_graphiccomponent VALUES ('46', '1.0', '玫瑰花图', '3', '0', '1.0', '0', null, null, '/comp/json/46/1_0/meta.json', '/comp/json/46/1_0/simple.json', '{"string":"1", "number": "2","map": "0","time":"0"}', '/comp/icon/46/1_0/rose.png', null, '46', '0', '46', '0', '303');
INSERT INTO dv_graphiccomponent VALUES ('47', '1.0', '桑基图', '8', '0', '1.0', '0', null, null, '/comp/json/47/1_0/meta.json', '/comp/json/47/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/47/1_0/sankey.png', null, '47', '0', '47', '0', '1305');
INSERT INTO dv_graphiccomponent VALUES ('48', '1.0', '流图', '12', '0', '1.0', '0', null, null, '/comp/json/48/1_0/meta.json', '/comp/json/48/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/48/1_0/stream.png', null, '48', '0', '48', '0', '1200');
INSERT INTO dv_graphiccomponent VALUES ('49', '1.0', '太阳辐射图', '8', '0', '1.0', '0', null, null, '/comp/json/49/1_0/meta.json', '/comp/json/49/1_0/simple.json', '{"string":"n", "number": "1","map": "0","time":"0"}', '/comp/icon/49/1_0/sunburst.png', null, '49', '0', '49', '0', '1303');
INSERT INTO dv_graphiccomponent VALUES ('50', '1.0', '标签云图', '9', '0', '1.0', '0', null, null, '/comp/json/50/1_0/meta.json', '/comp/json/50/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/50/1_0/tagcloud.png', null, '50', '0', '50', '0', '800');
INSERT INTO dv_graphiccomponent VALUES ('51', '1.0', '矩形树图', '8', '0', '1.0', '0', null, null, '/comp/json/51/1_0/meta.json', '/comp/json/51/1_0/simple.json', '{"string":"n", "number": "1","map": "0","time":"0"}', '/comp/icon/51/1_0/treemap.png', null, '51', '0', '51', '0', '1306');
INSERT INTO dv_graphiccomponent VALUES ('52', '1.0', '多系列散点图', '4', '0', '1.0', '0', null, null, '/comp/json/52/1_0/meta.json', '/comp/json/52/1_0/simple.json', '{"string":"0", "number": "2","map": "0","time":"0"}', '/comp/icon/52/1_0/scatter.png', null, '52', '0', '52', '0', '401');
INSERT INTO dv_graphiccomponent VALUES ('53', '1.0', '散点图', '4', '0', '1.0', '0', null, null, '/comp/json/53/1_0/meta.json', '/comp/json/53/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/53/1_0/scatter.png', null, '53', '0', '53', '0', '402');
INSERT INTO dv_graphiccomponent VALUES ('54', '1.0', '散点地图(大小)', '5', '0', '1.0', '0', null, null, '/comp/json/54/1_0/meta.json', '/comp/json/54/1_0/simple.json', '{"string":"0", "number": "1","map": "1","time":"0"}', '/comp/icon/54/1_0/scatter.png', null, '54', '0', '54', '0', '501');
INSERT INTO dv_graphiccomponent VALUES ('55', '1.0', '散点地图(明暗)', '5', '0', '1.0', '0', null, null, '/comp/json/55/1_0/meta.json', '/comp/json/55/1_0/simple.json', '{"string":"0", "number": "1","map": "1","time":"0"}', '/comp/icon/55/1_0/scatter.png', null, '55', '0', '55', '0', '502');
INSERT INTO dv_graphiccomponent VALUES ('56', '1.0', '散点地图(多系列)', '5', '0', '1.0', '0', null, null, '/comp/json/56/1_0/meta.json', '/comp/json/56/1_0/simple.json', '{"string":"1", "number": "1","map": "1","time":"0"}', '/comp/icon/56/1_0/scatter.png', null, '56', '0', '56', '0', '506');
INSERT INTO dv_graphiccomponent VALUES ('57', '1.0', '极坐标散点图', '4', '0', '1.0', '0', null, null, '/comp/json/57/1_0/meta.json', '/comp/json/57/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/57/1_0/scatter.png', null, '57', '0', '57', '0', '403');
INSERT INTO dv_graphiccomponent VALUES ('58', '1.0', '列表', '0', '0', '1.0', '0', null, null, '/comp/json/58/1_0/meta.json', '/comp/json/58/1_0/simple.json', '{"string":"n", "number": "0","map": "0","time":"0"}', '/comp/icon/58/1_0/table.png', null, '58', '0', '58', '0', '0');
INSERT INTO dv_graphiccomponent VALUES ('59', '1.0', '日历热图', '6', '1', '1.0', '0', null, null, '/comp/json/59/1_0/meta.json', '/comp/json/59/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/59/1_0/hotmap.png', null, '59', '0', '59', '0', '1101');
INSERT INTO dv_graphiccomponent VALUES ('60', '1.0', '新标签云图', '9', '0', '1.0', '0', null, null, '/comp/json/60/1_0/meta.json', '/comp/json/60/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/60/1_0/tagcloud.png', null, '60', '0', '60', '0', '801');
INSERT INTO dv_graphiccomponent VALUES ('61', '1.0', '地理轨迹图', '11', '0', '1.0', '0', null, null, '/comp/json/61/1_0/meta.json', '/comp/json/61/1_0/simple.json', '{"string":"2", "number": "0","map": "0","time":"1"}', '/comp/icon/61/1_0/path.png', null, '61', '0', '61', '0', '602');
INSERT INTO dv_graphiccomponent VALUES ('62', '1.0', '地理热力图', '11', '0', '1.0', '0', null, null, '/comp/json/62/1_0/meta.json', '/comp/json/62/1_0/simple.json', '{"string":"2", "number": "0","map": "0","time":"0"}', '/comp/icon/62/1_0/gis.png', null, '62', '0', '62', '0', '601');
INSERT INTO dv_graphiccomponent VALUES ('63', '1.0', '水球图', '13', '0', '1.0', '0', null, null, '/comp/json/63/1_0/meta.json', '/comp/json/63/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/63/1_0/logo.png', null, '63', '0', '63', '0', '1001');
INSERT INTO dv_graphiccomponent VALUES ('64', '1.0', '树图', '8', '0', '1.0', '0', null, null, '/comp/json/64/1_0/meta.json', '/comp/json/64/1_0/simple.json', '{"string":"3", "number": "1","map": "0","time":"0"}', '/comp/icon/64/1_0/treechart.png', null, '64', '0', '64', '0', '1302');
INSERT INTO dv_graphiccomponent VALUES ('65', '1.0', '主题河流图', '12', '0', '1.0', '0', null, null, '/comp/json/65/1_0/meta.json', '/comp/json/65/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/65/1_0/themeRiver.png', null, '65', '0', '65', '0', '1201');
INSERT INTO dv_graphiccomponent VALUES ('66', '1.0', '力引导图', '8', '0', '1.0', '0', null, null, '/comp/json/66/1_0/meta.json', '/comp/json/66/1_0/simple.json', '{"string":"4", "number": "1","map": "0","time":"0"}', '/comp/icon/66/1_0/graph.png', null, '66', '0', '66', '0', '1301');
INSERT INTO dv_graphiccomponent VALUES ('67', '1.0', '地理标记图', '11', '0', '1.0', '0', null, null, '/comp/json/67/1_0/meta.json', '/comp/json/67/1_0/simple.json', '{"string":"2", "number": "0","map": "0","time":"0"}', '/comp/icon/62/1_0/gis.png', null, '67', '0', '67', '0', '600');
INSERT INTO dv_graphiccomponent VALUES ('68', '1.0', 'KPI', '10', '0', '1.0', '0', null, null, '/comp/json/68/1_0/meta.json', '/comp/json/68/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/68/1_0/kpi.png', null, '68', '0', '68', '0', '900');
INSERT INTO dv_graphiccomponent VALUES ('69', '1.0', '坐标系散点图', '4', '0', '1.0', '0', null, null, '/comp/json/69/1_0/meta.json', '/comp/json/69/1_0/simple.json', '{"string":"1", "number": "2","map": "0","time":"0"}', '/comp/icon/69/1_0/coordinate_scatter.png', null, '69', '1', '69', '1', '405');
INSERT INTO dv_graphiccomponent VALUES ('70', '1.0', '百分比进度条图', '13', '0', '1.0', '0', null, null, '/comp/json/70/1_0/meta.json', '/comp/json/70/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/69/1_0/percentage.png', null, '70', '0', '70', '0', '1000');
INSERT INTO dv_graphiccomponent VALUES ('71', '1.0', '3D_Globe_BAR', '14', '0', '1.0', '0', null, null, '/comp/json/71/1_0/meta.json', '/comp/json/71/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/71/1_0/bar3D_globe.png', null, '71', '0', '71', '0', '702');
INSERT INTO dv_graphiccomponent VALUES ('72', '1.0', '3D_MapBox_BAR', '14', '0', '1.0', '0', null, null, '/comp/json/72/1_0/meta.json', '/comp/json/72/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/72/1_0/bar3D_mapbox.png', null, '72', '0', '72', '0', '701');
INSERT INTO dv_graphiccomponent VALUES ('73', '1.0', '3D_Grid_BAR', '14', '0', '1.0', '0', null, null, '/comp/json/73/1_0/meta.json', '/comp/json/73/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/73/1_0/Grid3D_Bar.png', null, '73', '0', '73', '0', '700');
INSERT INTO dv_graphiccomponent VALUES ('74', '1.0', '多维柱图', '1', '0', '1.0', '0', null, null, '/comp/json/74/1_0/meta.json', '/comp/json/74/1_0/simple.json', '{"string":"2", "number": "1","map": "0","time":"0"}', '/comp/icon/74/1_0/Multi_Bar.png', null, '74', '0', '74', '0', '106');
INSERT INTO dv_graphiccomponent VALUES ('75', '1.0', '地理地图', '5', '1', '1.0', '0', null, null, '/comp/json/75/1_0/meta.json', '/comp/json/75/1_0/simple.json', '{"string":"0", "number": "1","map": "1","time":"0"}', '/comp/icon/39/1_0/map.png', null, '75', '0', '75', '1', '505');
INSERT INTO dv_graphiccomponent VALUES ('76', '1.0', '3D堆积柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/76/1_0/meta.json', '/comp/json/76/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/76/1_0/bar.png', null, '76', '0', '76', '0', '109');
INSERT INTO dv_graphiccomponent VALUES ('77', '1.0', '3D多系列柱状图', '1', '0', '1.0', '0', null, null, '/comp/json/77/1_0/meta.json', '/comp/json/77/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/77/1_0/bar.png', null, '77', '0', '77', '0', '110');
INSERT INTO dv_graphiccomponent VALUES ('78', '1.0', '3D饼图', '3', '0', '1.0', '0', null, null, '/comp/json/78/1_0/meta.json', '/comp/json/78/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/78/1_0/bar.png', null, '78', '0', '78', '0', '304');
INSERT INTO dv_graphiccomponent VALUES ('0', '1.0', '堆积条形图', '1', '0', '1.0', '0', null, null, '/comp/sample/0/1_0/sampleData.json', '/comp/json/0/1_0/simple.json', '{"string":"1", "number": "n","map": "0","time":"0"}', '/comp/icon/0/1_0/bar1.png', null, '0', '0', '79', '0', '102');
INSERT INTO dv_graphiccomponent VALUES ('1075', '1.0', '多散点地图(大小)', '4', '0', '1.0', '0', null, null, '/comp/json/1075/1_0/meta.json', '/comp/json/1075/1_0/simple.json', '{"string":"0", "number": "3","map": "1","time":"0"}', '/comp/icon/75/1_0/funnel.png', null, '1075', '0', '1075', '1', '404');
INSERT INTO dv_graphiccomponent VALUES ('1077', '1.0', '仪表盘(多系列)', '10', '0', '1.0', '0', null, null, '/comp/json/35/1_0/meta.json', '/comp/json/35/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/35/1_0/gauge.png', null, '1077', '0', '1077', '0', '901');
INSERT INTO dv_graphiccomponent VALUES ('1078', '1.0', '树图（风格2）', '8', '0', '1.0', '0', NULL, NULL, '/comp/json/1078/1_0/meta.json', '/comp/json/1078/1_0/simple.json', '{"string":"n", "number": "0","map": "0","time":"0"}', '/comp/icon/1078/1_0/treechart.png', NULL, '1078', '0', '1078', '0', '1303');
INSERT INTO dv_graphiccomponent VALUES ('1079', '1.0', '环形柱状图', '1', '0', '1.0', '0', NULL, NULL, '/comp/json/1079/1_0/meta.json', '/comp/json/1079/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/1079/1_0/bar.png', NULL, '1079', '0', '1079', '0', '115');
INSERT INTO dv_graphiccomponent VALUES ('1080', '1.0', '嵌套环形图', '8', '0', '1.0', '0', NULL, NULL, '/comp/json/1080/1_0/meta.json', '/comp/json/1080/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/1080/1_0/pie.png', NULL, '1080', '0', '1080', '0', '1305');
INSERT INTO dv_graphiccomponent VALUES ('81', '1.0', '极坐标柱状图', '1', '0', '1.0', '0', NULL, NULL, '/comp/json/81/1_0/meta.json', '/comp/json/81/1_0/simple.json', '{"string":"1", "number": "1","map": "0","time":"0"}', '/comp/icon/81/1_0/bar.png', NULL, '81', '1', '81', '1', '116');
INSERT INTO dv_graphiccomponent VALUES ('82', '1.0', '点亮图', '11', 0, '1.0', '0', NULL, NULL, '/comp/json/82/1_0/meta.json', '/comp/json/82/1_0/simple.json', '{"string":"2", "number": "0","map": "0","time":"0"}', '/comp/icon/82/1_0/lightup.png', NULL, '82', '0', '82', '0', '603');
INSERT INTO dv_graphiccomponent VALUES ('83', '1.0', '百分比条形图', '13', 0, '1.0', '0', NULL, NULL, '/comp/json/83/1_0/meta.json', '/comp/json/83/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/83/1_0/percent.png', NULL, '83', '0', '83', '0', '1002');
INSERT INTO dv_graphiccomponent VALUES ('84', '1.0', '树图（风格3）', '8', 0, '1.0', '0', NULL, NULL, '/comp/json/84/1_0/meta.json', '/comp/json/84/1_0/simple.json', '{"string":"n", "number": "0","map": "0","time":"0"}', '/comp/icon/84/1_0/treechart.png', NULL, '84', '0', '84', '0', '1309');

INSERT INTO dv_theme VALUES ('0', '明经典蓝红', '{"qualitative":["#1776B6","#D7241F","#FF7F00","#24A322","#9564BF","#8b3660"],"sequential":["#cc0000","#bd1105","#cc3e30","#db8063","#eac298","#f9cb9c"],"diverging":["#00471D","#228D47","#3AB692","#A3E2D7","#CAF7F2","#FFFFFD"],"background":"rgb(255, 255, 255)","backgroundImage":"","text":"#000","title":"#000","subtitle":"#333","legendText":"#222","tooltipBackground":"rgba(10,10,10,0.7)","tooltipBorder":"#111","tooltipText":"#fff","axisLine":"#333","axisText":"#111","axisGrid":"#ccc","axisTick":"#222","visualMap":["#f9cb9c","#db8063","#cc0000"],"widgetBorder":"#ddd","widgetBackground":"rgba(238,238,238,.7)","tableHeaderBG":"rgba(0,0,0,.1)","tableDataBG":"rgba(51,51,51,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('2', '明现代战场', '{"qualitative":["#56D3F2","#CBB0E3","#68B2F1","#F6A65F","#23C2AB","#839BBF","#2ADAD8","#ADAFE0","#FA8283","#4EA8A6","#DA8BCC","#56D3F2","#23C2AB","#839BBF"],"sequential":["#f98706","#ea8012","#d7a052","#c3c1a0","#b9e1e5","#b9edf2"],"diverging":["#328a7e","#47a491","#61c39d","#90e2b4","#adefcb","#bdf6d6","#adefcb","#90e2b4","#61c39d","#47a491","#328a7e"],"background":"#FFF","backgroundImage":"","text":"#333333","title":"#008acd","subtitle":"#AAAAAA","legendText":"#333333","tooltipBackground":"#FFF2C0","tooltipBorder":"#FFF2C0","tooltipText":"#333","axisLine":"#27727B","axisText":"#333333","axisGrid":"#27727B","axisTick":"#27727B","visualMap":["#b9edf2","#c3c1a0","#f98706"],"widgetBorder":"#FFF","widgetBackground":"#FFF","widgetTitleBackground":"#FFF"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('3', '暗纯蓝深', '{"qualitative":["#4a72c9","#6071c6","#78a9f2","#a3ccf8","#47b8e2","#6087bf"],"sequential":["#204494","#1b3c85","#4769a6","#7297c7","#9dc3e8","#a0cef9"],"diverging":["#b899dd","#a6a1e1","#8dace7","#74b6ec","#5dc0f2","#4ac8f6","#5dc0f2","#74b6ec","#8dace7","#a6a1e1","#b899dd"],"background":"rgb(1, 7, 12)","backgroundImage":"","text":"rgb(221, 230, 236)","title":"rgb(221, 230, 236)","subtitle":"rgb(221, 230, 236)","legendText":"rgb(221, 230, 236)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(221, 230, 236)","axisLine":"rgb(58, 65, 71)","axisText":"rgb(221, 230, 236)","axisGrid":"rgb(58, 65, 71)","axisTick":"rgb(58, 65, 71)","originBgColor":"#3A3E41","visualMap":["#a0cef9","#7297c7","#204494"],"widgetTitleBorder":"#77A4D6","widgetTitleBackground":"#11253D","widgetCorner":"#77A4D6","widgetBorder":"rgba(36,60,88,1)","widgetBackground":"rgba(68,68,68,0)","tableHeaderBG":"rgba(204,204,204,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('4', '暗橙黄', '{"qualitative":["#fd6941","#96ce52","#ffd966","#42bce1","#0084C6","#db2f28"],"sequential":["#f04212","#e13b0e","#e56f32","#e9a356","#efd679","#fbe07c"],"diverging":["#6959a2","#6e59a4","#8a5aa4","#a159a3","#b769b5","#cf87c7"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"rgb(102, 118, 133)","title":"rgb(102, 118, 133)","subtitle":"rgb(102, 118, 133)","legendText":"rgb(102, 118, 133)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(102, 118, 133)","axisLine":"#ccc","axisText":"rgb(102, 118, 133)","axisGrid":"#999","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#fbe07c","#e9a356","#f04212"],"widgetBorder":"#ddd","widgetBackground":"rgba(68,68,68,.7)","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('5', '暗亮蓝绿', '{"qualitative":["#2ef6a0","#27b9de","#c5317d","#f6e519","#7852e8","#70f670"],"sequential":["#2ef3a3","#33e79c","#63dba9","#96cdb6","#c2c2c2","#cccccc"],"diverging":["#a25ba1","#bd658b","#de7571","#ee8b54","#f3ac30","#f8cd0d"],"background":"rgb(32, 42, 51)","backgroundImage":"","text":"rgb(255, 255, 255)","title":"rgb(255, 255, 255)","subtitle":"rgb(255, 255, 255)","legendText":"rgb(255, 255, 255)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(255, 255, 255)","axisLine":"#CCCCCC","axisText":"rgb(255, 255, 255)","axisGrid":"#999","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#cccccc","#96cdb6","#2ef3a3"],"widgetBorder":"#999","widgetBackground":"#0D161F","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('6', '暗红橙黄', '{"qualitative":["#d02222","#e89b4a","#ffd966","#b6d7a8","#6fa8dc","#317381"],"sequential":["#cc0000","#bd1105","#cc3e30","#db8063","#eac298","#f9cb9c"],"diverging":["#a25ba1","#bd658b","#de7571","#ee8b54","#f3ac30","#f8cd0d"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"rgb(79, 117, 151)","title":"rgb(79, 117, 151)","subtitle":"rgb(79, 117, 151)","legendText":"rgb(79, 117, 151)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(79, 117, 151)","axisLine":"#CCCCCC","axisText":"rgb(79, 117, 151)","axisGrid":"#999","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#f9cb9c","#db8063","#cc0000"],"widgetBorder":"#999","widgetBackground":"#0D161F","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('7', '暗安监', '{"qualitative":["#1878b8","#d92722","#ff8104","#25a523","#9b6ac5","#838383"],"sequential":["#cc0000","#bd1105","#cd473f","#dc9183","#eedcca","#fce5cd"],"diverging":["#6959a2","#6e59a4","#8a5aa4","#a159a3","#b769b5","#cf87c7"],"background":"#333","backgroundImage":"","text":"#92cae3","title":"#92cae3","subtitle":"#92cae3","legendText":"#92cae3","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"#92cae3","axisLine":"#0a5670","axisText":"#92cae3","axisGrid":"#0a5670","axisTick":"#0a5670","originBgColor":"#3A3E41","visualMap":["#fce5cd","#dc9183","#cc0000"],"widgetTitleBorder":"rgb(31,104,148)","widgetTitleBackground":"#11253D","widgetCorner":"rgba(31,95,136,1)","widgetBorder":"rgba(36,60,88,1)","widgetBackground":"rgba(68,68,68,0)","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('70', '暗现代战场', '{"qualitative":["#19d7dd","#816bc2","#ff8104","#25a523","#9b6ac5","#838383"],"sequential":["#0f5697","#184e87","#477daa","#7dadcc","#b5def0","#b6e9ff"],"diverging":["#6959a2","#6e59a4","#8a5aa4","#a159a3","#b769b5","#cf87c7"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"#92cae3","title":"#92cae3","subtitle":"#92cae3","legendText":"#92cae3","tooltipBackground":"#666666","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"#92cae3","axisLine":"#0a5670","axisText":"#92cae3","axisGrid":"#0a5670","axisTick":"#0a5670","originBgColor":"#3A3E41","visualMap":["#b6e9ff","#7dadcc","#0f5697"],"widgetTitleBorder":"rgb(31,104,148)","widgetTitleBackground":"rgba(17, 37, 61, 0.4)","widgetCorner":"rgba(31,95,136,1)","widgetBorder":"rgba(36,60,88,1)","widgetBackground":"rgba(68,68,68,0)","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('71', '明黑黄红', '{"qualitative":["#43546C","#FFBF53","#EE7675","#04B2C5","#6B3C7E","#C45984"],"sequential":["#1776B6","#226ca6","#568fb7","#93b3c7","#d1d6d8","#d4e1e8"],"diverging":["#00471D","#228D47","#3AB692","#A3E2D7","#CAF7F2","#FFFFFD"],"background":"rgb(244, 244, 244)","backgroundImage":"","text":"#838383","title":"#838383","subtitle":"#333","legendText":"#222","tooltipBackground":"rgba(10,10,10,0.7)","tooltipBorder":"#111","tooltipText":"#fff","axisLine":"rgb(232, 232, 232)","axisText":"#959595","axisGrid":"rgb(232, 232, 232)","axisTick":"rgb(232, 232, 232)","visualMap":["#d4e1e8","#93b3c7","#1776B6"],"widgetBorder":"#ddd","widgetBackground":"rgba(238,238,238,.7)","tableHeaderBG":"rgba(102,102,102,.1)","tableDataBG":"rgba(102,102,102,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('72', '明灰红橙', '{"qualitative":["#94A4BB","#F2897A","#AAD5D6","#91C1E7","#FFD46C","#B9D174"],"sequential":["#f68a7b","#e68375","#d79890","#c9aeaa","#c2c2c2","#cccccc"],"diverging":["#cf471f","#f67c29","#ffa944","#fec456","#fdd854","#fceb55"],"background":"rgb(255, 255, 255)","backgroundImage":"","text":"rgb(73, 68, 68)","title":"rgb(73, 68, 68)","subtitle":"rgb(73, 68, 68)","legendText":"rgb(73, 68, 68)","tooltipBackground":"#FFF2C0","tooltipBorder":"#FFF2C0","tooltipText":"rgb(73, 68, 68)","axisLine":"#D7D7D7","axisText":"rgb(73, 68, 68)","axisGrid":"#D1D1D1","axisTick":"#27727B","visualMap":["#cccccc","#c9aeaa","#f68a7b"],"widgetBorder":"#ddd","widgetBackground":"rgba(238,238,238,.7)","tableHeaderBG":"rgba(102,102,102,.1)","tableDataBG":"rgba(102,102,102,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('73', '明清新蓝绿', '{"qualitative":["#52A9ED","#85D3A1","#97C8E8","#b6d7a8","#7bd2e0","#e0e577"],"sequential":["#2796ee","#2b8dde","#559edb","#81b0d8","#acc2d5","#afcde5"],"diverging":["#328a7e","#47a491","#61c39d","#90e2b4","#adefcb","#bdf6d6"],"background":"rgb(255, 255, 255)","backgroundImage":"","text":"rgb(48, 47, 47)","title":"rgb(48, 47, 47)","subtitle":"rgb(48, 47, 47)","legendText":"rgb(48, 47, 47)","tooltipBackground":"#FFF2C0","tooltipBorder":"#FFF2C0","tooltipText":"rgb(48, 47, 47)","axisLine":"#D1D1D1","axisText":"rgb(48, 47, 47)","axisGrid":"#D1D1D1","axisTick":"#27727B","visualMap":["#afcde5","#81b0d8","#2796ee"],"widgetBorder":"#ddd","widgetBackground":"rgba(238,238,238,.7)","tableHeaderBG":"rgba(102,102,102,.1)","tableDataBG":"rgba(102,102,102,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('74', '暗黑黄红', '{"qualitative":["#43546C","#FFBF53","#EE7675","#04B2C5","#6B3C7E","#C45984"],"sequential":["#f76a43","#e7633e","#e6855f","#e4a780","#e3c9a1","#f0d3a4"],"diverging":["#6959a2","#6e59a4","#8a5aa4","#a159a3","#b769b5","#cf87c7"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"rgb(67, 124, 177)","title":"rgb(67, 124, 177)","subtitle":"rgb(67, 124, 177)","legendText":"rgb(67, 124, 177)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(67, 124, 177)","axisLine":"#454648","axisText":"rgb(67, 124, 177)","axisGrid":"#48474B","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#f0d3a4","#e4a780","#f76a43"],"widgetBorder":"#ddd","widgetBackground":"rgba(68,68,68,.7)","tableHeaderBG":"rgba(204,204,204,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('75', '暗灰红橙', '{"qualitative":["#94A4BB","#F2897A","#AAD5D6","#91C1E7","#FFD46C","#B9D174"],"sequential":["#ce262d","#bf2028","#cd605d","#dc9e92","#eedcca","#fce5cd"],"diverging":["#a25ba1","#bd658b","#de7571","#ee8b54","#f3ac30","#f8cd0d"],"background":"rgb(51, 51, 51)","backgroundImage":"","text":"#FEFEFE","title":"#FFFFFF","subtitle":"#9A9A9A","legendText":"#FFFFFF","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"#FFF","axisLine":"#47484B","axisText":"#868587","axisGrid":"#4C4D4F","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#fce5cd","#dc9e92","#ce262d"],"widgetBorder":"#ddd","widgetBackground":"rgba(68,68,68,.7)","tableHeaderBG":"rgba(204,204,204,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('76', '暗清新蓝绿', '{"qualitative":["#52A9ED","#85D3A1","#97C8E8","#8BDAB5","#E0E577","#7BD2E0"],"sequential":["#093e6f","#113661","#3d668d","#7095ba","#a3c5e6","#a6d0f7"],"diverging":["#b899dd","#a6a1e1","#8dace7","#74b6ec","#5dc0f2","#4ac8f6","#5dc0f2","#74b6ec","#8dace7","#a6a1e1","#b899dd"],"background":"rgb(1, 8, 14)","backgroundImage":"","text":"rgb(222, 239, 255)","title":"rgb(222, 239, 255)","subtitle":"rgb(222, 239, 255)","legendText":"rgb(222, 239, 255)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(222, 239, 255)","axisLine":"rgb(120, 124, 128)","axisText":"rgb(222, 239, 255)","axisGrid":"rgb(120, 124, 128)","axisTick":"rgb(120, 124, 128)","originBgColor":"#3A3E41","visualMap":["#a6d0f7","#7095ba","#093e6f"],"widgetTitleBorder":"#759ABA","widgetTitleBackground":"#2F3E4C","widgetCorner":"#759ABA","widgetBorder":"#3D4D5B","widgetBackground":"#0F151B","tableHeaderBG":"rgba(204,204,204,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('77', '暗柠檬', '{"qualitative":["#027a4c","#42bc02","#ffd100","#d9b307","#ff8f00","#994100"],"sequential":["#0e7d58","#1a7351","#549156","#9bb05b","#e3cf5e","#eed960"],"diverging":["#a25ba1","#bd658b","#de7571","#ee8b54","#f3ac30","#f8cd0d"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"rgb(243, 243, 243)","title":"rgb(243, 243, 243)","subtitle":"rgb(243, 243, 243)","legendText":"rgb(243, 243, 243)","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"rgb(243, 243, 243)","axisLine":"#CCCCCC","axisText":"rgb(243, 243, 243)","axisGrid":"#999","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#eed960","#9bb05b","#0e7d58"],"widgetBorder":"#999","widgetBackground":"#0D161F","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('79', '暗兼容主题', '{"qualitative":["#C12E34","#E6B600","#0098D9","#2B821D","#005EAA","#339CA8"],"sequential":["#C12E34","#b2292f","#c55c51","#d78f75","#eac298","#f9cb9c"],"diverging":["#a25ba1","#bd658b","#de7571","#ee8b54","#f3ac30","#f8cd0d"],"background":"rgb(51, 51, 51)","backgroundImage":"","text":"#FEFEFE","title":"#FFFFFF","subtitle":"#9A9A9A","legendText":"#FFFFFF","tooltipBackground":"rgba(200,200,200,0.5)","tooltipBorder":"rgba(200,200,200,0.5)","tooltipText":"#FFF","axisLine":"#CCCCCC","axisText":"#FFFFFF","axisGrid":"#999","axisTick":"#CCCCCC","originBgColor":"#3A3E41","visualMap":["#f9cb9c","#d78f75","#C12E34"],"widgetBorder":"#ddd","widgetBackground":"rgba(68,68,68,.7)","tableHeaderBG":"rgba(153,153,153,.1)","tableDataBG":"rgba(204,204,204,.1)"}', null, null, null, null);
INSERT INTO dv_theme VALUES ('8', '暗绿植', '{"qualitative":["#0e9448","#01613e","#c7ec59","#87cd43","#80ae9c","#aecdc3"],"sequential":["#135738","#144d32","#41735b","#6e9785","#9cbdae","#9fc9b6"],"diverging":["#328a7e","#47a491","#61c39d","#90e2b4","#adefcb","#bdf6d6"],"background":"rgb(0, 0, 0)","backgroundImage":"","text":"rgb(241, 243, 240)","title":"rgb(241, 243, 240)","subtitle":"rgb(241, 243, 240)","legendText":"rgb(241, 243, 240)","tooltipBackground":"rgb(153, 163, 148)","tooltipBorder":"#FFF2C0","tooltipText":"rgb(241, 243, 240)","axisLine":"rgb(42, 46, 40)","axisText":"rgb(241, 243, 240)","axisGrid":"rgb(42, 46, 40)","axisTick":"rgb(42, 46, 40)","visualMap":["#9fc9b6","#6e9785","#135738"],"widgetTitleBorder":"#517032","widgetTitleBackground":"#20241F","widgetCorner":"#517032","widgetBorder":"#2C3927","widgetBackground":"rgba(68,68,68,0)","tableHeaderBG":"rgba(0,0,0,.1)","tableDataBG":"rgba(51,51,51,.1)"}', null, null, null, null);

commit;