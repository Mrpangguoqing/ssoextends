﻿
/*==============================================================*/
/* 2018-08-02变更如下                             */
/*==============================================================*/
ALTER TABLE dv_datasource_info
ADD  system_shared  SMALLINT  DEFAULT 0;



CREATE TABLE qrtz_job_details
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL,
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtz_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(20) NULL,
    PREV_FIRE_TIME NUMBER(20) NULL,
    PRIORITY INTEGER NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(20) NOT NULL,
    END_TIME NUMBER(20) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR SMALLINT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
        REFERENCES qrtz_job_details(SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(20) NOT NULL,
    REPEAT_INTERVAL NUMBER(20) NOT NULL,
    TIMES_TRIGGERED NUMBER(20) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(200) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_simprop_triggers
  (          
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    STR_PROP_1 VARCHAR2(512) NULL,
    STR_PROP_2 VARCHAR2(512) NULL,
    STR_PROP_3 VARCHAR2(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 NUMBER(20) NULL,
    LONG_PROP_2 NUMBER(20) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR2(1) NULL,
    BOOL_PROP_2 VARCHAR2(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_calendars
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL,
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);

CREATE TABLE qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtz_fired_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(20) NOT NULL,
    SCHED_TIME NUMBER(20) NOT NULL,
    PRIORITY INTEGER NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);

CREATE TABLE qrtz_scheduler_state
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(20) NOT NULL,
    CHECKIN_INTERVAL NUMBER(20) NOT NULL,
    PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);

CREATE TABLE qrtz_locks
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

CREATE TABLE qrtztask_job_details (
  SCHED_NAME varchar2(120) NOT NULL,
  JOB_NAME varchar2(200) NOT NULL,
  JOB_GROUP varchar2(200) NOT NULL,
  DESCRIPTION varchar2(250) DEFAULT NULL,
  JOB_CLASS_NAME varchar2(250) NOT NULL,
  IS_DURABLE varchar2(1) NOT NULL,
  IS_NONCONCURRENT varchar2(1) NOT NULL,
  IS_UPDATE_DATA varchar2(1) NOT NULL,
  REQUESTS_RECOVERY varchar2(1) NOT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE qrtztask_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  JOB_NAME varchar2(200) NOT NULL,
  JOB_GROUP varchar2(200) NOT NULL,
  DESCRIPTION varchar2(250) DEFAULT NULL,
  NEXT_FIRE_TIME NUMBER(13) DEFAULT NULL,
  PREV_FIRE_TIME NUMBER(13) DEFAULT NULL,
  PRIORITY INTEGER DEFAULT NULL,
  TRIGGER_STATE varchar2(16) NOT NULL,
  TRIGGER_TYPE varchar2(8) NOT NULL,
  START_TIME NUMBER(13) NOT NULL,
  END_TIME NUMBER(13) DEFAULT NULL,
  CALENDAR_NAME varchar2(200) DEFAULT NULL,
  MISFIRE_INSTR SMALLINT DEFAULT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
  REFERENCES qrtztask_job_details (SCHED_NAME, JOB_NAME, JOB_GROUP)
);

CREATE TABLE qrtztask_blob_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  BLOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_calendars (
  SCHED_NAME varchar2(120) NOT NULL,
  CALENDAR_NAME varchar2(200) NOT NULL,
  CALENDAR blob NOT NULL,
  PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);

CREATE TABLE qrtztask_cron_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  CRON_EXPRESSION varchar2(200) NOT NULL,
  TIME_ZONE_ID varchar2(80) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_fired_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  ENTRY_ID varchar2(95) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  INSTANCE_NAME varchar2(200) NOT NULL,
  FIRED_TIME NUMBER(13) NOT NULL,
  SCHED_TIME NUMBER(13) NOT NULL,
  PRIORITY INTEGER NOT NULL,
  STATE varchar2(16) NOT NULL,
  JOB_NAME varchar2(200) DEFAULT NULL,
  JOB_GROUP varchar2(200) DEFAULT NULL,
  IS_NONCONCURRENT varchar2(1) DEFAULT NULL,
  REQUESTS_RECOVERY varchar2(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);



CREATE TABLE qrtztask_locks (
  SCHED_NAME varchar2(120) NOT NULL,
  LOCK_NAME varchar2(40) NOT NULL,
  PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

CREATE TABLE qrtztask_paused_trigger_grps (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);

CREATE TABLE qrtztask_scheduler_state (
  SCHED_NAME varchar2(120) NOT NULL,
  INSTANCE_NAME varchar2(200) NOT NULL,
  LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
  CHECKIN_INTERVAL NUMBER(13) NOT NULL,
  PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);

CREATE TABLE qrtztask_simple_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  REPEAT_COUNT NUMBER(7) NOT NULL,
  REPEAT_INTERVAL NUMBER(12) NOT NULL,
  TIMES_TRIGGERED NUMBER(10) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
  FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtztask_simprop_triggers (
  SCHED_NAME varchar2(120) NOT NULL,
  TRIGGER_NAME varchar2(200) NOT NULL,
  TRIGGER_GROUP varchar2(200) NOT NULL,
  STR_PROP_1 varchar2(512) DEFAULT NULL,
  STR_PROP_2 varchar2(512) DEFAULT NULL,
  STR_PROP_3 varchar2(512) DEFAULT NULL,
  INT_PROP_1 int DEFAULT NULL,
  INT_PROP_2 int DEFAULT NULL,
  LONG_PROP_1 NUMBER(20) DEFAULT NULL,
  LONG_PROP_2 NUMBER(20) DEFAULT NULL,
  DEC_PROP_1 NUMERIC(13,4) DEFAULT NULL,
  DEC_PROP_2 NUMERIC(13,4) DEFAULT NULL,
  BOOL_PROP_1 varchar2(1) DEFAULT NULL,
  BOOL_PROP_2 varchar2(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
 FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtztask_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE dv_datasync_log (
  id varchar2(64) NOT NULL,
  source_name varchar2(64) DEFAULT NULL,
  source_owner varchar2(64) DEFAULT NULL,
  source_id varchar2(64) DEFAULT NULL,
  source_type smallint DEFAULT NULL,
  store_type smallint DEFAULT NULL,
  local_data_id varchar2(64) NOT NULL,
  creator varchar2(64) DEFAULT NULL,
  start_time date DEFAULT NULL,
  end_time date DEFAULT NULL,
  sync_method smallint DEFAULT NULL,
  sync_type smallint DEFAULT NULL,
  rows_inserted int DEFAULT NULL,
  rows_deleted int DEFAULT NULL,
  rows_updated int DEFAULT NULL,
  store_server varchar2(64) DEFAULT NULL,
  PRIMARY KEY (id)
);
create index source_data_log_idx on dv_datasync_log (source_type,source_id)
  compress;

CREATE TABLE dv_cktable_info (
  id varchar2(64) NOT NULL,
  server_id varchar2(64) DEFAULT NULL,
  db_name varchar2(64) DEFAULT NULL,
  table_name varchar2(64) DEFAULT NULL,
  table_engine varchar2(64) DEFAULT NULL,
  cluster_name varchar2(64) DEFAULT NULL,
  shard_table_id varchar2(64) DEFAULT NULL,
  created_by varchar2(64) DEFAULT NULL,
  created_on date DEFAULT NULL,
  write_lock varchar2(64) DEFAULT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE dv_local_entity_store (
  localdata_id varchar2(64) NOT NULL,
  entity_name varchar2(64) NOT NULL,
  table_id varchar2(64) DEFAULT NULL,
  fields blob,
  PRIMARY KEY (localdata_id,entity_name)
);

CREATE TABLE dv_localdata (
  id varchar2(64) NOT NULL,
  store_type smallint NOT NULL,
  source_id varchar2(64) NOT NULL,
  source_type smallint NOT NULL,
  created_by varchar2(64) DEFAULT NULL,
  created_on date DEFAULT NULL,
  sync_settings blob,
  other_settings blob,
  PRIMARY KEY (id)
);

create index source_data_idx on dv_localdata (source_id,source_type)
  compress;

/*==============================================================*/
/* 2018-08-27变更如下                             */
/*==============================================================*/
ALTER TABLE dv_datasource_info
ADD  code_table  BLOB  DEFAULT NULL;

ALTER TABLE dv_dataset_info
ADD  bind_code_table  BLOB DEFAULT NULL;

/*==============================================================*/
/* 2018-09-20变更如下                               */
/*==============================================================*/
INSERT INTO dv_graphiccomponent VALUES ('82', '1.0', '点亮图', '11', 0, '1.0', '0', NULL, NULL, '/comp/json/82/1_0/meta.json', '/comp/json/82/1_0/simple.json', '{"string":"2", "number": "0","map": "0","time":"0"}', '/comp/icon/82/1_0/lightup.png', NULL, '82', '0', '82', '0', '603');
INSERT INTO dv_graphiccomponent VALUES ('83', '1.0', '百分比条形图', '13', 0, '1.0', '0', NULL, NULL, '/comp/json/83/1_0/meta.json', '/comp/json/83/1_0/simple.json', '{"string":"0", "number": "1","map": "0","time":"0"}', '/comp/icon/83/1_0/percent.png', NULL, '83', '0', '83', '0', '1002');
INSERT INTO dv_graphiccomponent VALUES ('84', '1.0', '树图（风格3）', '8', 0, '1.0', '0', NULL, NULL, '/comp/json/84/1_0/meta.json', '/comp/json/84/1_0/simple.json', '{"string":"n", "number": "0","map": "0","time":"0"}', '/comp/icon/84/1_0/treechart.png', NULL, '84', '0', '84', '0', '1309');
update dv_graphiccomponent set access_type = '1' where id = '75';
commit;
/*==============================================================*/
/* 2018-09-30变更如下                               */
/*==============================================================*/
UPDATE dv_graphiccomponent set data_require = '{"string":"0", "number": "2","map": "0","time":"0"}' where id = '52';
UPDATE dv_graphiccomponent set data_require = '{"string":"3", "number": "1","map": "0","time":"0"}' where id = '64';
UPDATE dv_graphiccomponent set name = '树图（风格2）' where id = '1078';
commit;